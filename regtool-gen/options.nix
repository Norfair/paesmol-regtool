{ lib }:
{
  database-file = lib.mkOption {
    default = null;
    description = "database-file";
    type = lib.types.nullOr lib.types.str;
  };
  db-file = lib.mkOption {
    default = null;
    description = "database-file";
    type = lib.types.nullOr lib.types.str;
  };
  email-address = lib.mkOption {
    default = null;
    description = "The email address to send emails from";
    type = lib.types.nullOr lib.types.str;
  };
  environment = lib.mkOption {
    default = null;
    description = "Environment";
    type = lib.types.nullOr (lib.types.enum [
      "Production"
      "Staging"
      "ManualTesting"
      "AutomatedTesting"
    ]);
  };
  host = lib.mkOption {
    default = null;
    description = "Host";
    type = lib.types.nullOr lib.types.str;
  };
  log-level = lib.mkOption {
    default = null;
    description = "Minimal severity of log messages";
    type = lib.types.nullOr (lib.types.enum [
      "Debug"
      "Info"
      "Warn"
      "Error"
    ]);
  };
  looper = lib.mkOption {
    default = { };
    type = lib.types.submodule {
      options = {
        emailer = lib.mkOption {
          default = { };
          type = lib.types.submodule {
            options = {
              enable = lib.mkOption {
                default = null;
                description = "enable the emailer looper";
                type = lib.types.nullOr lib.types.bool;
              };
              period = lib.mkOption {
                default = null;
                description = "period of the emailer looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
              phase = lib.mkOption {
                default = null;
                description = "phase of the emailer looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
            };
          };
        };
        garbage-collector = lib.mkOption {
          default = { };
          type = lib.types.submodule {
            options = {
              enable = lib.mkOption {
                default = null;
                description = "enable the garbage-collector looper";
                type = lib.types.nullOr lib.types.bool;
              };
              period = lib.mkOption {
                default = null;
                description = "period of the garbage-collector looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
              phase = lib.mkOption {
                default = null;
                description = "phase of the garbage-collector looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
            };
          };
        };
        verifier = lib.mkOption {
          default = { };
          type = lib.types.submodule {
            options = {
              enable = lib.mkOption {
                default = null;
                description = "enable the verifier looper";
                type = lib.types.nullOr lib.types.bool;
              };
              period = lib.mkOption {
                default = null;
                description = "period of the verifier looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
              phase = lib.mkOption {
                default = null;
                description = "phase of the verifier looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
            };
          };
        };
      };
    };
  };
  maintenance = lib.mkOption {
    default = null;
    description = "maintenance mode";
    type = lib.types.nullOr lib.types.bool;
  };
  necrork = lib.mkOption {
    default = { };
    type = lib.types.submodule {
      options = {
        intray = lib.mkOption {
          default = { };
          type = lib.types.submodule {
            options = {
              key = lib.mkOption {
                default = null;
                description = "Access key";
                type = lib.types.nullOr lib.types.str;
              };
              username = lib.mkOption {
                default = null;
                description = "Username";
                type = lib.types.nullOr lib.types.str;
              };
            };
          };
        };
        notifier = lib.mkOption {
          default = { };
          type = lib.types.submodule {
            options = {
              enable = lib.mkOption {
                default = null;
                description = "enable the notifier looper";
                type = lib.types.nullOr lib.types.bool;
              };
              period = lib.mkOption {
                default = null;
                description = "period of the notifier looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
              phase = lib.mkOption {
                default = null;
                description = "phase of the notifier looper in seconds";
                type = lib.types.nullOr lib.types.number;
              };
            };
          };
        };
        switch = lib.mkOption {
          default = null;
          description = "Name of the necrork switch";
          type = lib.types.nullOr lib.types.str;
        };
        timeout = lib.mkOption {
          default = null;
          description = "How long after last hearing from this switch, nodes should consider it dead";
          type = lib.types.nullOr lib.types.ints.u32;
        };
        url = lib.mkOption {
          default = null;
          description = "Base url of the necrork server";
          type = lib.types.nullOr lib.types.str;
        };
      };
    };
  };
  not-paid = lib.mkOption {
    default = null;
    description = "The due date for an unpaid payment";
    type = lib.types.nullOr lib.types.str;
  };
  port = lib.mkOption {
    default = null;
    description = "Port";
    type = lib.types.nullOr lib.types.int;
  };
  stripe = lib.mkOption {
    default = { };
    type = lib.types.submodule {
      options = {
        publishable-key = lib.mkOption {
          default = null;
          description = "Publishable key";
          type = lib.types.nullOr lib.types.str;
        };
        secret-key = lib.mkOption {
          default = null;
          description = "Secret key";
          type = lib.types.nullOr lib.types.str;
        };
      };
    };
  };
}
