{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Gen
  ( module Regtool.Data,
    module Regtool.Data.Gen,
    module Regtool.Calculation.Gen,
    module Regtool.Gen,
  )
where

import GenImport
import Regtool.Calculation.Gen
import Regtool.Core.AdminConfiguration as Admin
import Regtool.Data hiding (get)
import Regtool.Data.Gen
import Regtool.Handler.Children
import Regtool.Handler.Doctors
import Regtool.Handler.Parents

genSimpleRegisterParent :: Gen RegisterParent
genSimpleRegisterParent =
  RegisterParent
    <$> genSimpleFirstName
    <*> genSimpleLastName
    <*> genSimpleAddressLine1
    <*> genSimpleAddressLine2
    <*> genSimplePostalCode
    <*> genSimpleCity
    <*> genSimpleCountry
    <*> genSimplePhoneNumber1
    <*> genSimplePhoneNumber2
    <*> genSimpleEmail1
    <*> genSimpleEmail2
    <*> genSimpleCompany
    <*> genSimpleRelationShip
    <*> genSimpleComments
    <*> pure Nothing

genSimpleRegisterDoctor :: Gen RegisterDoctor
genSimpleRegisterDoctor =
  RegisterDoctor
    <$> genSimpleFirstName
    <*> genSimpleLastName
    <*> genSimpleAddressLine1
    <*> genSimpleAddressLine2
    <*> genSimplePostalCode
    <*> genSimpleCity
    <*> genSimpleCountry
    <*> genSimplePhoneNumber1
    <*> genSimplePhoneNumber2
    <*> genSimpleEmail1
    <*> genSimpleEmail2
    <*> genSimpleComments
    <*> pure Nothing

genSimpleRegisterChild :: Gen RegisterChild
genSimpleRegisterChild =
  RegisterChild
    <$> genSimpleFirstName
    <*> genSimpleLastName
    <*> genSimpleBirthDate
    <*> genSimpleSchoolLevel
    <*> genValid
    <*> genSimpleNationality
    <*> genSimpleAllergies
    <*> genSimpleHealthInfo
    <*> genSimpleMedication
    <*> genSimpleComments
    <*> genSimplePickupAllowed
    <*> genSimplePickupDisallowed
    <*> genSimplePicturePermission
    <*> genSimpleDelijnBus
    <*> genValid
    <*> pure Nothing

instance GenValid Admin.Configuration where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
