{ mkDerivation, base, bytestring, containers, genvalidity
, genvalidity-bytestring, genvalidity-containers, genvalidity-path
, genvalidity-sydtest, genvalidity-sydtest-aeson, genvalidity-text
, genvalidity-time, http-client, http-types, lib, mtl
, opt-env-conf-test, path, path-io, persistent, persistent-sqlite
, pretty-show, QuickCheck, regtool, regtool-calculate
, regtool-calculate-gen, regtool-data, regtool-data-gen, safe
, sydtest, sydtest-discover, sydtest-persistent-sqlite, sydtest-wai
, sydtest-yesod, text, time, validity-path, validity-text
, yesod-auth, yesod-core, yesod-form
}:
mkDerivation {
  pname = "regtool-gen";
  version = "0.0.0";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-sydtest
    genvalidity-sydtest-aeson genvalidity-text genvalidity-time path
    path-io pretty-show QuickCheck regtool regtool-calculate-gen
    regtool-data regtool-data-gen safe sydtest text validity-path
    validity-text
  ];
  testHaskellDepends = [
    base bytestring containers genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-sydtest
    genvalidity-sydtest-aeson genvalidity-text genvalidity-time
    http-client http-types mtl opt-env-conf-test path path-io
    persistent persistent-sqlite pretty-show QuickCheck regtool
    regtool-calculate regtool-data safe sydtest
    sydtest-persistent-sqlite sydtest-wai sydtest-yesod text time
    validity-path validity-text yesod-auth yesod-core yesod-form
  ];
  testToolDepends = [ sydtest-discover ];
  license = "unknown";
}
