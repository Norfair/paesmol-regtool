{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.VerificationEmailSpec
  ( spec,
  )
where

import Data.Time
import Regtool.Core.Foundation.Regtool
import Regtool.Data
import Regtool.Looper.Emailer.Verification
import Regtool.TestUtils
import Regtool.TestUtils.Looper.Emailer
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "verification email"
    $ verificationGoldenTest
      "test_resources/email/verification/1"
      VerificationEmail
        { verificationEmailTo = unsafeEmailAddress "example" "domain.com",
          verificationEmailKey = "key",
          verificationEmailLink = "link",
          verificationEmailTimestamp = UTCTime (fromGregorian 1970 1 1) 0,
          verificationEmailEmail = Nothing
        }

verificationGoldenTest :: String -> VerificationEmail -> YesodSpec Regtool
verificationGoldenTest path_ =
  emailGoldenTests path_ verificationEmailTextContent verificationEmailHtmlContent
