{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PasswordResetSpec
  ( spec,
  )
where

import Data.Time
import Regtool.Core.Foundation.Regtool
import Regtool.Data
import Regtool.Looper.Emailer.PasswordReset
import Regtool.TestUtils
import Regtool.TestUtils.Looper.Emailer
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "password reset email"
    $ passwordResetGoldenTest
      "test_resources/email/password-reset/1"
      PasswordResetEmail
        { passwordResetEmailTo = unsafeEmailAddress "example" "domain.com",
          passwordResetEmailKey = "key",
          passwordResetEmailLink = "link",
          passwordResetEmailTimestamp = UTCTime (fromGregorian 1970 1 1) 0,
          passwordResetEmailEmail = Nothing
        }

passwordResetGoldenTest :: String -> PasswordResetEmail -> YesodSpec Regtool
passwordResetGoldenTest path_ =
  emailGoldenTests path_ passwordResetEmailTextContent passwordResetEmailHtmlContent
