{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.DaycareSpec
  ( spec,
  )
where

import Data.Set (empty)
import Data.Time (TimeOfDay (..), UTCTime (..), fromGregorian)
import Database.Persist.Sqlite (toSqlKey)
import Regtool.Core.Foundation.Regtool (Regtool)
import Regtool.Data
import Regtool.Looper.Emailer.Daycare (daycareEmailHtmlContent, daycareEmailTextContent)
import Regtool.TestUtils (regtoolSpec)
import Regtool.TestUtils.Looper.Emailer (emailGoldenTests)
import Test.Syd.Yesod (YesodSpec, ydescribe)
import TestImport hiding (empty)

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe " daycare email"
    $ daycareGoldenTest
      "test_resources/email/daycare/1"
      [ Account
          { accountUsername = Nothing,
            accountEmailAddress = unsafeEmailAddress "test" "example.com",
            accountVerified = True,
            accountVerificationKey = Nothing,
            accountSaltedPassphraseHash = "test",
            accountResetPassphraseKey = Nothing,
            accountCreationTime = UTCTime (fromGregorian 1970 1 1) 12,
            accountAdmin = True
          }
      ]
      Child
        { childFirstName = "Daycare",
          childLastName = "Child",
          childBirthdate = fromGregorian 1970 1 1,
          childLevel = Primary2,
          childLanguageSection = toSqlKey 1,
          childNationality = Standard NatBelgian,
          childDoctor = toSqlKey 1,
          childAllergies = Nothing,
          childHealthInfo = Nothing,
          childMedication = Nothing,
          childComments = Nothing,
          childPickupAllowed = Nothing,
          childPickupDisallowed = Nothing,
          childPicturePermission = True,
          childDelijnBus = True,
          childRegistrationTime = UTCTime (fromGregorian 1970 1 1) 12,
          childLatestConfirmationTime = Just $ UTCTime (fromGregorian 1970 1 1) 12
        }
      DaycareSemestre
        { daycareSemestreYear = SchoolYear 2019,
          daycareSemestreSemestre = Fall,
          daycareSemestreCreationTime = UTCTime (fromGregorian 1970 1 1) 19,
          daycareSemestreOpen = True
        }
      DaycareTimeslot
        { daycareTimeslotStart = TimeOfDay 16 15 0,
          daycareTimeslotEnd = TimeOfDay 18 0 0,
          daycareTimeslotSemestre = toSqlKey 1,
          daycareTimeslotSchoolDay = SchoolThursday,
          daycareTimeslotFee = amountInCents 500,
          daycareTimeslotOccasionalFee = amountInCents 50,
          daycareTimeslotAccessible = empty,
          daycareTimeslotCreationTime = UTCTime (fromGregorian 1970 1 1) 18
        }

daycareGoldenTest ::
  String ->
  [Account] ->
  Child ->
  DaycareSemestre ->
  DaycareTimeslot ->
  YesodSpec Regtool
daycareGoldenTest path as child semestre =
  emailGoldenTests
    path
    (daycareEmailTextContent child semestre)
    (daycareEmailHtmlContent as child semestre)
