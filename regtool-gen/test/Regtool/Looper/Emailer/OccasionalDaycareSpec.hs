{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.OccasionalDaycareSpec
  ( spec,
  )
where

import Data.Set (empty)
import Data.Time (TimeOfDay (..), UTCTime (..), fromGregorian)
import Database.Persist.Sqlite (toSqlKey)
import Regtool.Core.Foundation.Regtool (Regtool)
import Regtool.Data
  ( Account (..),
    Child (..),
    DaycareTimeslot (..),
    OccasionalDaycareSignup (..),
    SchoolDay (..),
    SchoolLevel (Primary2),
    StandardNationality (NatBelgian),
    StandardOrRaw (Standard),
    amountInCents,
    unsafeEmailAddress,
  )
import Regtool.Looper.Emailer.OccasionalDaycare
  ( occasionalDaycareEmailHtmlContent,
    occasionalDaycareEmailTextContent,
  )
import Regtool.TestUtils (regtoolSpec)
import Regtool.TestUtils.Looper.Emailer (emailGoldenTests)
import Test.Syd.Yesod (YesodSpec, ydescribe)
import TestImport hiding (empty)

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "occasional daycare email"
    $ occasionalDaycareGoldenTest
      "test_resources/email/occasional-daycare/1"
      [ Account
          { accountUsername = Nothing,
            accountEmailAddress = unsafeEmailAddress "test" "example.com",
            accountVerified = True,
            accountVerificationKey = Nothing,
            accountSaltedPassphraseHash = "test",
            accountResetPassphraseKey = Nothing,
            accountCreationTime = UTCTime (fromGregorian 1970 1 1) 12,
            accountAdmin = False
          }
      ]
      Child
        { childFirstName = "Daycare",
          childLastName = "Child",
          childBirthdate = fromGregorian 1970 1 1,
          childLevel = Primary2,
          childLanguageSection = toSqlKey 1,
          childNationality = Standard NatBelgian,
          childDoctor = toSqlKey 1,
          childAllergies = Nothing,
          childHealthInfo = Nothing,
          childMedication = Nothing,
          childComments = Nothing,
          childPickupAllowed = Nothing,
          childPickupDisallowed = Nothing,
          childPicturePermission = True,
          childDelijnBus = True,
          childRegistrationTime = UTCTime (fromGregorian 1970 1 1) 12,
          childLatestConfirmationTime = Just $ UTCTime (fromGregorian 1970 1 1) 12
        }
      DaycareTimeslot
        { daycareTimeslotStart = TimeOfDay 16 15 0,
          daycareTimeslotEnd = TimeOfDay 18 0 0,
          daycareTimeslotSemestre = toSqlKey 1,
          daycareTimeslotSchoolDay = SchoolThursday,
          daycareTimeslotFee = amountInCents 500,
          daycareTimeslotOccasionalFee = amountInCents 500,
          daycareTimeslotAccessible = empty,
          daycareTimeslotCreationTime = UTCTime (fromGregorian 1970 1 1) 18
        }
      OccasionalDaycareSignup
        { occasionalDaycareSignupDay = fromGregorian 1970 1 1,
          occasionalDaycareSignupChild = toSqlKey 1,
          occasionalDaycareSignupTimeslot = toSqlKey 1,
          occasionalDaycareSignupCreationTime = UTCTime (fromGregorian 1970 1 1) 18,
          occasionalDaycareSignupDeletedByAdmin = False
        }

occasionalDaycareGoldenTest ::
  String -> [Account] -> Child -> DaycareTimeslot -> OccasionalDaycareSignup -> YesodSpec Regtool
occasionalDaycareGoldenTest path as child timeslot =
  emailGoldenTests
    path
    (occasionalDaycareEmailTextContent child timeslot)
    (occasionalDaycareEmailHtmlContent as child timeslot)
