{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.OccasionalTransportSpec
  ( spec,
  )
where

import Data.Time (UTCTime (..), fromGregorian)
import Database.Persist.Sqlite (toSqlKey)
import Regtool.Core.Foundation.Regtool (Regtool)
import Regtool.Data
  ( Account (..),
    BusDirection (FromSchool),
    BusStop (..),
    Child (..),
    OccasionalTransportSignup (..),
    SchoolLevel (Primary2),
    StandardNationality (NatBelgian),
    StandardOrRaw (Standard),
    unsafeEmailAddress,
  )
import Regtool.Looper.Emailer.OccasionalTransport
  ( occasionalTransportEmailHtmlContent,
    occasionalTransportEmailTextContent,
  )
import Regtool.TestUtils (regtoolSpec)
import Regtool.TestUtils.Looper.Emailer (emailGoldenTests)
import Test.Syd.Yesod (YesodSpec, ydescribe)
import TestImport
import Yesod.Form.Fields (Textarea (..))

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "occasional transport email"
    $ occasionalTransportGoldenTest
      "test_resources/email/occasional-transport/1"
      [ Account
          { accountUsername = Nothing,
            accountEmailAddress = unsafeEmailAddress "test" "example.com",
            accountVerified = True,
            accountVerificationKey = Nothing,
            accountSaltedPassphraseHash = "test",
            accountResetPassphraseKey = Nothing,
            accountCreationTime = UTCTime (fromGregorian 1970 1 1) 12,
            accountAdmin = False
          }
      ]
      Child
        { childFirstName = "Bus",
          childLastName = "Child",
          childBirthdate = fromGregorian 1970 1 1,
          childLevel = Primary2,
          childLanguageSection = toSqlKey 1,
          childNationality = Standard NatBelgian,
          childDoctor = toSqlKey 1,
          childAllergies = Nothing,
          childHealthInfo = Nothing,
          childMedication = Nothing,
          childComments = Nothing,
          childPickupAllowed = Nothing,
          childPickupDisallowed = Nothing,
          childPicturePermission = True,
          childDelijnBus = True,
          childRegistrationTime = UTCTime (fromGregorian 1970 1 1) 12,
          childLatestConfirmationTime = Just $ UTCTime (fromGregorian 1970 1 1) 12
        }
      BusStop
        { busStopCity = Textarea "Veldhoven",
          busStopLocation = Textarea "Parking Hospital Maxima Medisch Centrum",
          busStopArchived = False,
          busStopCreationTime = UTCTime (fromGregorian 1970 1 1) 13
        }
      OccasionalTransportSignup
        { occasionalTransportSignupDay = fromGregorian 1970 1 1,
          occasionalTransportSignupDirection = FromSchool,
          occasionalTransportSignupChild = toSqlKey 1,
          occasionalTransportSignupBusStop = toSqlKey 1,
          occasionalTransportSignupDeletedByAdmin = False,
          occasionalTransportSignupTime = UTCTime (fromGregorian 1970 1 1) 14
        }

occasionalTransportGoldenTest ::
  String -> [Account] -> Child -> BusStop -> OccasionalTransportSignup -> YesodSpec Regtool
occasionalTransportGoldenTest path as child busStop =
  emailGoldenTests
    path
    (occasionalTransportEmailTextContent child busStop Nothing)
    (occasionalTransportEmailHtmlContent as child busStop Nothing)
