{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PaymentReceivedSpec
  ( spec,
  )
where

import Data.Time
import Database.Persist.Sqlite (toSqlKey)
import Regtool.Calculation.Costs.Checkout
import Regtool.Core.Foundation.Regtool
import Regtool.Data
import Regtool.Looper.Emailer.PaymentReceived
import Regtool.TestUtils
import Regtool.TestUtils.Looper.Emailer
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "payment received"
    $ do
      let cid = toSqlKey 0
          aid = toSqlKey 1
          yfpa = amountInCents 20
          tpa1 = amountInCents 60
          tpa2 = amountInCents 60
          otpa1 = amountInCents 11
          otpa2 = amountInCents 11
          dca1 = amountInCents 19
          dca2 = amountInCents 17
          capa1 = amountInCents 25
          capa2 = amountInCents 25
          eca = amountInCents 66
          da = amountInCents 40
          totalAmount = amountInCents 188
          spid = toSqlKey 2
          scid = toSqlKey 3
          yfpid = toSqlKey 4
          tppid1 = toSqlKey 5
          tppid2 = toSqlKey 6
          tpid1 = toSqlKey 7
          tpid2 = toSqlKey 8
          otsid1 = toSqlKey 9
          otsid2 = toSqlKey 10
          otpid1 = toSqlKey 11
          otpid2 = toSqlKey 12
          dpid1 = toSqlKey 13
          dpid2 = toSqlKey 14
          dpid3 = toSqlKey 15
          dapid1 = toSqlKey 15
          dapid2 = toSqlKey 16
          dapid3 = toSqlKey 17
          dapid4 = toSqlKey 18
          odcsid1 = toSqlKey 19
          odcsid2 = toSqlKey 20
          odcpid1 = toSqlKey 21
          odcpid2 = toSqlKey 22
          casid1 = toSqlKey 23
          casid2 = toSqlKey 24
          capid1 = toSqlKey 25
          capid2 = toSqlKey 26
          ecid = toSqlKey 27
          did = toSqlKey 28
          coo =
            CheckoutOverview
              { checkoutOverviewCheckout =
                  Checkout
                    { checkoutAccount = aid,
                      checkoutAmount = totalAmount,
                      checkoutPayment = Just spid,
                      checkoutTimestamp = UTCTime (fromGregorian 1970 1 1) 9
                    },
                checkoutOverviewPayment =
                  Just
                    ( Entity
                        spid
                        StripePayment
                          { stripePaymentCustomer = scid,
                            stripePaymentAmount = totalAmount,
                            stripePaymentCharge = "charge",
                            stripePaymentTimestamp = UTCTime (fromGregorian 1970 1 1) 8,
                            stripePaymentEvent = Nothing,
                            stripePaymentSession = Nothing,
                            stripePaymentPaymentIntent = Nothing
                          }
                    ),
                checkoutOverviewYearlyFeePayments =
                  [ Entity
                      yfpid
                      YearlyFeePayment
                        { yearlyFeePaymentAccount = aid,
                          yearlyFeePaymentSchoolyear = SchoolYear 2017,
                          yearlyFeePaymentCheckout = cid,
                          yearlyFeePaymentAmount = yfpa
                        }
                  ],
                checkoutOverviewTransportPayments =
                  [ Entity
                      tpid1
                      TransportPayment
                        { transportPaymentPaymentPlan = tppid1,
                          transportPaymentCheckout = cid,
                          transportPaymentAmount = tpa1
                        },
                    Entity
                      tpid2
                      TransportPayment
                        { transportPaymentPaymentPlan = tppid2,
                          transportPaymentCheckout = cid,
                          transportPaymentAmount = tpa2
                        }
                  ],
                checkoutOverviewOccasionalTransportPayments =
                  [ Entity
                      otpid1
                      OccasionalTransportPayment
                        { occasionalTransportPaymentSignup = otsid1,
                          occasionalTransportPaymentCheckout = cid,
                          occasionalTransportPaymentAmount = otpa1
                        },
                    Entity
                      otpid2
                      OccasionalTransportPayment
                        { occasionalTransportPaymentSignup = otsid2,
                          occasionalTransportPaymentCheckout = cid,
                          occasionalTransportPaymentAmount = otpa2
                        }
                  ],
                checkoutOverviewDaycarePayments =
                  [ Entity
                      dpid1
                      DaycarePayment
                        { daycarePaymentSignup = toSqlKey 1,
                          daycarePaymentCheckout = cid,
                          daycarePaymentAmount = dca1,
                          daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 7
                        },
                    Entity
                      dpid2
                      DaycarePayment
                        { daycarePaymentSignup = toSqlKey 2,
                          daycarePaymentCheckout = cid,
                          daycarePaymentAmount = dca2,
                          daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 6
                        },
                    Entity
                      dpid3
                      DaycarePayment
                        { daycarePaymentSignup = toSqlKey 3,
                          daycarePaymentCheckout = cid,
                          daycarePaymentAmount = dca2,
                          daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 5
                        }
                  ],
                checkoutOverviewDaycareActivityPayments =
                  [ Entity
                      dapid1
                      DaycareActivityPayment
                        { daycareActivityPaymentSignup = toSqlKey 1,
                          daycareActivityPaymentCheckout = cid,
                          daycareActivityPaymentAmount = dca1,
                          daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 10
                        },
                    Entity
                      dapid2
                      DaycareActivityPayment
                        { daycareActivityPaymentSignup = toSqlKey 2,
                          daycareActivityPaymentCheckout = cid,
                          daycareActivityPaymentAmount = dca2,
                          daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 9
                        },
                    Entity
                      dapid3
                      DaycareActivityPayment
                        { daycareActivityPaymentSignup = toSqlKey 3,
                          daycareActivityPaymentCheckout = cid,
                          daycareActivityPaymentAmount = dca2,
                          daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 8
                        },
                    Entity
                      dapid4
                      DaycareActivityPayment
                        { daycareActivityPaymentSignup = toSqlKey 3,
                          daycareActivityPaymentCheckout = cid,
                          daycareActivityPaymentAmount = dca2,
                          daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 7
                        }
                  ],
                checkoutOverviewOccasionalDaycarePayments =
                  [ Entity
                      odcpid1
                      OccasionalDaycarePayment
                        { occasionalDaycarePaymentSignup = odcsid1,
                          occasionalDaycarePaymentCheckout = cid,
                          occasionalDaycarePaymentAmount = otpa1,
                          occasionalDaycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 22
                        },
                    Entity
                      odcpid2
                      OccasionalDaycarePayment
                        { occasionalDaycarePaymentSignup = odcsid2,
                          occasionalDaycarePaymentCheckout = cid,
                          occasionalDaycarePaymentAmount = otpa2,
                          occasionalDaycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 21
                        }
                  ],
                checkoutOverviewCustomActivityPayments =
                  [ Entity
                      capid1
                      CustomActivityPayment
                        { customActivityPaymentSignup = casid1,
                          customActivityPaymentCheckout = cid,
                          customActivityPaymentAmount = capa1,
                          customActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 24
                        },
                    Entity
                      capid2
                      CustomActivityPayment
                        { customActivityPaymentSignup = casid2,
                          customActivityPaymentCheckout = cid,
                          customActivityPaymentAmount = capa2,
                          customActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 23
                        }
                  ],
                checkoutOverviewExtraCharges =
                  [ Entity
                      ecid
                      ExtraCharge
                        { extraChargeAccount = aid,
                          extraChargeAmount = eca,
                          extraChargeReason = Just "Some reason",
                          extraChargeCheckout = Just cid,
                          extraChargeTimestamp = UTCTime (fromGregorian 1970 1 1) 4
                        }
                  ],
                checkoutOverviewDiscounts =
                  [ Entity
                      did
                      Discount
                        { discountAccount = aid,
                          discountAmount = da,
                          discountReason = Just "Some other reason",
                          discountCheckout = Just cid,
                          discountTimestamp = UTCTime (fromGregorian 1970 1 1) 5
                        }
                  ]
              }
      -- shouldBeValid coo TODO This doesn't seem to work?
      paymentReceivedGoldenTest "test_resources/email/payment-received/1" coo

paymentReceivedGoldenTest ::
  String ->
  CheckoutOverview ->
  YesodSpec Regtool
paymentReceivedGoldenTest path_ =
  emailGoldenTests
    path_
    paymentReceivedEmailTextContent
    paymentReceivedEmailHtmlContent
