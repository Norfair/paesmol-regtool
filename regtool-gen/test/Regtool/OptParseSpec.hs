{-# LANGUAGE TypeApplications #-}

module Regtool.OptParseSpec (spec) where

import OptEnvConf.Test
import Regtool.OptParse
import Test.Syd
import TestImport

spec :: Spec
spec = do
  describe "Intructions" $ do
    settingsLintSpec @Settings
    goldenSettingsReferenceDocumentationSpec @Settings "test_resources/documentation.txt" "regtool"
    goldenSettingsNixOptionsSpec @Settings "options.nix"
