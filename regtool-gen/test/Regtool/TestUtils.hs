{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.TestUtils
  ( RegtoolSpec,
    RegtoolExample,
    regtoolSpec,
    runDB,
    registerTestAccount,
    registerAndVerifyTestAccount,
    registerVerifyAndLoginTestAccount,
    withAnyVerifiedAccount_,
    withAnyVerifiedAccountLogin,
    withAnyVerifiedAccountLogin_,
    withAdminAccountLogin,
    withAdminAccountLogin_,
    lookupExistingByEmail,
    loginTo,
    needsLogin,
    needsLoginSpec,
    addPostParamMaybe,
    addPostParamStandardOrOther,
    StdMethod (..),
    boolText,
    sqlKeyText,
    dayText,
    registerBusSchoolYear,
  )
where

import Control.Monad.Reader
import qualified Data.Text as T
import Data.Time
import qualified Database.Persist as DB
import Database.Persist.Sqlite hiding (get)
import qualified Network.HTTP.Client as Http
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation hiding (get, runDB)
import Regtool.Gen
import Test.Syd
import Test.Syd.Path
import Test.Syd.Persistent.Sqlite
import Test.Syd.Wai (managerSpec)
import Test.Syd.Yesod
import TestImport

type RegtoolSpec = YesodSpec Regtool

type RegtoolExample a = YesodExample Regtool a

regtoolSpec :: RegtoolSpec -> Spec
regtoolSpec = managerSpec . yesodSpecWithSiteSetupFunc siteSetupFunc . modifyMaxSuccess (`div` 20)

runDB :: SqlPersistM a -> YesodExample Regtool a
runDB func = do
  pool <- asks (regtoolConnectionPool . yesodClientSite)
  liftIO $ runSqlPersistMPool func pool

siteSetupFunc :: Http.Manager -> SetupFunc Regtool
siteSetupFunc man = do
  pool <- connectionPoolSetupFunc migrateAll
  tempdir <- tempDirSetupFunc "regtool-test"
  clientSessionKeyFile <- resolveFile tempdir "client-session-key.aes"
  adminConfigFile <- resolveFile tempdir "config.yaml"
  pure
    $ Regtool
      { regtoolEnvironment = EnvAutomatedTesting,
        regtoolApproot = Nothing,
        regtoolConnectionPool = pool,
        regtoolStatic = regtoolEmbeddedStatic,
        regtoolHttpManager = man,
        regtoolStripeSettings = Nothing,
        regtoolClientSessionKeyFile = clientSessionKeyFile,
        regtoolAdminConfigFile = adminConfigFile,
        regtoolNotPaid = Nothing
      }

withAnyEmailAndPass ::
  (Testable test) =>
  (EmailAddress -> Text -> RegtoolExample () -> test) ->
  (RegtoolExample () -> Property)
withAnyEmailAndPass func1 func2 = forAll genSimpleEmailAddress $ \e -> forAll genSimplePassword $ \p ->
  func1 e p func2

withAnyEmailAndPass1 ::
  (Testable test) =>
  (EmailAddress -> Text -> (arg -> RegtoolExample ()) -> test) ->
  ((arg -> RegtoolExample ()) -> Property)
withAnyEmailAndPass1 func1 func2 = forAll genSimpleEmailAddress $ \e -> forAll genSimplePassword $ \p -> func1 e p func2

registerTestAccount :: EmailAddress -> Text -> RegtoolExample (Entity Account)
registerTestAccount e p = do
  get $ AuthR registerR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl $ AuthR registerR
    addTokenFromCookie
    addPostParam "email" $ emailAddressText e
    addPostParam "passphrase" p
    addPostParam "passphrase-confirm" p
  statusIs 303
  locationShouldBe $ AuthR LoginR
  lookupExistingByEmail e

verifyTestAccount :: Entity Account -> RegtoolExample ()
verifyTestAccount (Entity _ acc) = do
  verkey <-
    case accountVerificationKey acc of
      Just key -> pure key
      _ -> liftIO $ expectationFailure "Account should have been unverified and have a verkey"
  request $ do
    setMethod methodGet
    setUrl $ AuthR $ verifyR (accountEmailAddress acc) verkey
    addTokenFromCookie
  statusIs 303

registerAndVerifyTestAccount :: EmailAddress -> Text -> RegtoolExample (Entity Account)
registerAndVerifyTestAccount e p = do
  ent <- registerTestAccount e p
  verifyTestAccount ent
  pure ent

loginTo :: Userkey -> Text -> RegtoolExample ()
loginTo ukey passphrase = do
  get $ AuthR LoginR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl $ AuthR loginFormPostTargetR
    addTokenFromCookie
    addPostParam "userkey" ukey
    addPostParam "passphrase" passphrase
  statusIs 303
  locationShouldBe HomeR

registerVerifyAndLoginTestAccount :: EmailAddress -> Text -> RegtoolExample (Entity Account)
registerVerifyAndLoginTestAccount e p = do
  ent <- registerTestAccount e p
  verifyTestAccount ent
  loginTo (emailAddressText e) p
  pure ent

withAnyVerifiedAccount_ :: YesodClient Regtool -> RegtoolExample () -> Property
withAnyVerifiedAccount_ yc = withAnyEmailAndPass $ \ea pw func -> runYesodClientM yc $ do
  _ <- registerAndVerifyTestAccount ea pw
  func

withAnyVerifiedAccountLogin :: YesodClient Regtool -> (Entity Account -> RegtoolExample ()) -> Property
withAnyVerifiedAccountLogin yc = withAnyEmailAndPass1 $ \ea pw func -> runYesodClientM yc $ do
  ent <- registerVerifyAndLoginTestAccount ea pw
  func ent

withAnyVerifiedAccountLogin_ :: YesodClient Regtool -> RegtoolExample () -> Property
withAnyVerifiedAccountLogin_ yc = withAnyEmailAndPass $ \ea pw func -> runYesodClientM yc $ do
  _ <- registerVerifyAndLoginTestAccount ea pw
  func

withAdminAccountLogin :: YesodClient Regtool -> (Entity Account -> RegtoolExample ()) -> Property
withAdminAccountLogin yc func =
  withAnyVerifiedAccountLogin yc $ \e@(Entity aid _) -> do
    runDB $ update aid [AccountAdmin =. True]
    func e

withAdminAccountLogin_ :: YesodClient Regtool -> RegtoolExample () -> Property
withAdminAccountLogin_ yc = withAdminAccountLogin yc . const

lookupExistingByEmail :: EmailAddress -> RegtoolExample (Entity Account)
lookupExistingByEmail email = do
  ment <- runDB $ getBy $ UniqueAccountEmail email
  case ment of
    Nothing -> liftIO $ expectationFailure $ unwords ["Should have existed:", show email]
    Just ent -> pure ent

needsLogin :: (RedirectUrl Regtool url) => StdMethod -> url -> RegtoolExample ()
needsLogin method_ url = do
  errOrLoc <- firstRedirect method_ url
  case errOrLoc of
    Left err -> liftIO $ expectationFailure $ show err
    Right loc -> do
      liftIO $ loc `shouldBe` AuthR LoginR
      get loc
      statusIs 200
      bodyContains "Login"

needsLoginSpec :: (RedirectUrl Regtool url) => StdMethod -> url -> RegtoolSpec
needsLoginSpec method_ route =
  yit "requires the user to be logged in by redirecting to the login page" $ do
    let isPost = renderStdMethod method_ == methodPost
    when isPost $ get $ AuthR LoginR -- To make sure there's a CSRF cookie available
    needsLogin method_ route

addPostParamMaybe :: Text -> Maybe Text -> RequestBuilder site ()
addPostParamMaybe f mt =
  forM_ mt $ \t ->
    addPostParam f t

addPostParamStandardOrOther ::
  forall a site.
  (Bounded a, Enum a) =>
  Text ->
  StandardOrRaw a ->
  RequestBuilder site ()
addPostParamStandardOrOther f sor = do
  addPostParam (f <> "-a") $ T.pack $ show $ standardOrOtherIndex sor
  case sor of
    Standard _ -> pure ()
    Raw t -> addPostParam (f <> "-b") t

-- Stages in login process, used below
firstRedirect ::
  (RedirectUrl Regtool url) => StdMethod -> url -> RegtoolExample (Either Text (Route Regtool))
firstRedirect method_ url = do
  request $ do
    let isPost = renderStdMethod method_ == methodPost
    setMethod $ renderStdMethod method_
    when isPost addTokenFromCookie
    setUrl url
  getLocation -- We should get redirected to the login page

boolText :: Bool -> Text
boolText True = "yes"
boolText False = "no"

sqlKeyText :: (ToBackendKey SqlBackend record) => Key record -> Text
sqlKeyText = T.pack . show . fromSqlKey

dayText :: Day -> Text
dayText = T.pack . show

registerBusSchoolYear :: SchoolYear -> SqlPersistM BusSchoolYearId
registerBusSchoolYear sy =
  DB.insert $ BusSchoolYear {busSchoolYearYear = sy, busSchoolYearOpen = True}
