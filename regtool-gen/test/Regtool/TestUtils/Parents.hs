{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Parents
  ( RegisterParent (..),
    registerParent,
    genSimpleRegisterParent,
    verifyParentRegistered,
  )
where

import qualified Data.Text as T
import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Parents
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport
import Yesod.Form

registerParent :: RegisterParent -> RegtoolExample ()
registerParent RegisterParent {..} = do
  get RegisterParentR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterParentR
    addToken
    addPostParam "first-name" registerParentFirstName
    addPostParam "last-name" registerParentLastName
    addPostParam "address-1" registerParentAddressLine1
    addPostParamMaybe "address-2" registerParentAddressLine2
    addPostParam "post-code" registerParentPostalCode
    addPostParam "city" registerParentCity
    addPostParamStandardOrOther "country" registerParentCountry
    addPostParam "phone-number-1" registerParentPhoneNumber1
    addPostParamMaybe "phone-number-2" registerParentPhoneNumber2
    addPostParam "email-1" $ emailAddressText registerParentEmail1
    addPostParamMaybe "email-2" $ emailAddressText <$> registerParentEmail2
    addPostParam "company"
      $ T.pack
      $ show
      $ fromJust
      $ lookup registerParentCompany (zip [minBound .. maxBound] [(1 :: Int) ..])
    addPostParamStandardOrOther "relationship" registerParentRelationShip
    addPostParamMaybe "comments" $ unTextarea <$> registerParentComments
  locationShouldBe ParentsR

verifyParentRegistered :: AccountId -> RegisterParent -> RegtoolExample ()
verifyParentRegistered aid RegisterParent {..} = do
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  (Entity _ p) <-
    case parents of
      [] -> liftIO $ expectationFailure "Should have found one parent, got empty list instead."
      [p] -> pure p
      ps ->
        liftIO
          $ expectationFailure
          $ unlines
          $ "Should have found one parent, got these instead: "
          : map show ps
  liftIO $ do
    parentAccount p `shouldBe` aid
    parentFirstName p `shouldBe` registerParentFirstName
    parentLastName p `shouldBe` registerParentLastName
    parentAddressLine1 p `shouldBe` registerParentAddressLine1
    parentAddressLine2 p `shouldBe` registerParentAddressLine2
    parentPostalCode p `shouldBe` registerParentPostalCode
    parentCity p `shouldBe` registerParentCity
    parentCountry p `shouldBe` registerParentCountry
    parentPhoneNumber1 p `shouldBe` registerParentPhoneNumber1
    parentPhoneNumber2 p `shouldBe` registerParentPhoneNumber2
    parentEmail1 p `shouldBe` registerParentEmail1
    parentEmail2 p `shouldBe` registerParentEmail2
    parentCompany p `shouldBe` registerParentCompany
    parentComments p `shouldBe` registerParentComments
