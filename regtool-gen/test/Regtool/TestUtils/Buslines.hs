module Regtool.TestUtils.Buslines
  ( getBusStop,
  )
where

import Database.Persist
import Regtool.Application ()
import Regtool.Data
import Regtool.Gen ()
import Regtool.TestUtils
import TestImport

getBusStop :: RegtoolExample (Entity BusStop)
getBusStop = do
  mbs <- runDB $ selectFirst ([] :: [Filter BusStop]) []
  case mbs of
    Nothing -> liftIO $ expectationFailure "Should have had a bus stop"
    Just bs -> pure bs
