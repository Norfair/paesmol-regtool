{-# LANGUAGE OverloadedStrings #-}

module Regtool.TestUtils.LanguageSection
  ( getLanguageSection,
  )
where

import Database.Persist
import Regtool.Application ()
import Regtool.Data
import Regtool.Gen ()
import Regtool.TestUtils
import TestImport

getLanguageSection :: RegtoolExample (Entity LanguageSection)
getLanguageSection = do
  ml <- runDB $ selectFirst ([] :: [Filter LanguageSection]) []
  case ml of
    Nothing -> liftIO $ expectationFailure "Should have had a language section"
    Just els -> pure els
