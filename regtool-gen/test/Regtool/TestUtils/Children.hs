{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Children
  ( RegisterChild (..),
    registerChild,
    genSimpleRegisterChild,
    verifyChildRegistered,
    getChildByRegister,
  )
where

import qualified Data.Text as T
import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Calculation.Children
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Children
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport
import Yesod.Form

{-# ANN module ("HLint: ignore Use &&" :: String) #-}

registerChild :: RegisterChild -> RegtoolExample ()
registerChild RegisterChild {..} = do
  get RegisterChildR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterChildR
    addToken
    addPostParam "first-name" registerChildFirstName
    addPostParam "last-name" registerChildLastName
    addPostParam "birth-date" $ dayText registerChildBirthDate
    addPostParam "school-level" $ T.pack $ show $ schoolLevelInt registerChildSchoolLevel
    addPostParam "language-section" $ sqlKeyText registerChildLanguageSection
    addPostParamStandardOrOther "nationality" registerChildNationality
    addPostParamMaybe "allergy-info" $ unTextarea <$> registerChildAllergies
    addPostParamMaybe "other-health-info" $ unTextarea <$> registerChildHealthInfo
    addPostParamMaybe "medication" $ unTextarea <$> registerChildMedication
    addPostParamMaybe "extra-comments" $ unTextarea <$> registerChildComments
    addPostParamMaybe "pickup-allowed" $ unTextarea <$> registerChildPickupAllowed
    addPostParamMaybe "pickup-disallowed" $ unTextarea <$> registerChildPickupDisallowed
    addPostParam "picture-permission" $ boolText registerChildPicturePermission
    addPostParam "brought-to-bus" $ boolText registerChildDelijnBus
    addPostParam "doctor" $ sqlKeyText registerChildDoctor
  statusIs 303
  locationShouldBe ChildrenR

verifyChildRegistered :: AccountId -> RegisterChild -> RegtoolExample ()
verifyChildRegistered aid RegisterChild {..} = do
  children <- runDB $ getChildrenOf aid
  (Entity _ Child {..}) <-
    case children of
      [] -> liftIO $ expectationFailure "Should have found one child, got empty list instead."
      [c] -> pure c
      cs ->
        liftIO
          $ expectationFailure
          $ unlines
          $ "Should have found one child, got these instead: "
          : map show cs
  liftIO $ do
    context "first name" $ childFirstName `shouldBe` registerChildFirstName
    context "last name" $ childLastName `shouldBe` registerChildLastName
    context "birth date" $ childBirthdate `shouldBe` registerChildBirthDate
    context "school level" $ childLevel `shouldBe` registerChildSchoolLevel
    context "nationality" $ childNationality `shouldBe` registerChildNationality
    context "allergies" $ childAllergies `shouldBe` registerChildAllergies
    context "health-info" $ childHealthInfo `shouldBe` registerChildHealthInfo
    context "medication" $ childMedication `shouldBe` registerChildMedication
    context "comments" $ childComments `shouldBe` registerChildComments
    context "pickup allowed" $ childPickupAllowed `shouldBe` registerChildPickupAllowed
    context "pickup disallowed" $ childPickupDisallowed `shouldBe` registerChildPickupDisallowed
    context "picture permission" $ childPicturePermission `shouldBe` registerChildPicturePermission
    context "bus pickup" $ childDelijnBus `shouldBe` registerChildDelijnBus
    context "doctor" $ childDoctor `shouldBe` registerChildDoctor

getChildByRegister :: AccountId -> RegisterChild -> RegtoolExample (Entity Child)
getChildByRegister aid RegisterChild {..} = do
  children <- runDB $ getChildrenOf aid
  let mc =
        find
          ( \(Entity _ Child {..}) ->
              and [childFirstName == registerChildFirstName, childLastName == registerChildLastName]
          )
          children
  case mc of
    Nothing -> liftIO $ expectationFailure "Should have had a child"
    Just ch -> pure ch
