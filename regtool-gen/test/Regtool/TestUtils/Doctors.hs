{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Doctors
  ( RegisterDoctor (..),
    registerDoctor,
    genSimpleRegisterDoctor,
    verifyDoctorRegistered,
    getDoctorByRegister,
  )
where

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Doctors
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport
import Yesod.Form

registerDoctor :: RegisterDoctor -> RegtoolExample ()
registerDoctor RegisterDoctor {..} = do
  get RegisterDoctorR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterDoctorR
    addToken
    addPostParam "first-name" registerDoctorFirstName
    addPostParam "last-name" registerDoctorLastName
    addPostParam "address-1" registerDoctorAddressLine1
    addPostParamMaybe "address-2" registerDoctorAddressLine2
    addPostParam "post-code" registerDoctorPostalCode
    addPostParam "city" registerDoctorCity
    addPostParamStandardOrOther "country" registerDoctorCountry
    addPostParam "phone-number-1" registerDoctorPhoneNumber1
    addPostParamMaybe "phone-number-2" registerDoctorPhoneNumber2
    addPostParam "email-1" $ emailAddressText registerDoctorEmail1
    addPostParamMaybe "email-2" $ emailAddressText <$> registerDoctorEmail2
    addPostParamMaybe "comments" $ unTextarea <$> registerDoctorComments
  locationShouldBe DoctorsR

verifyDoctorRegistered :: AccountId -> RegisterDoctor -> RegtoolExample ()
verifyDoctorRegistered aid RegisterDoctor {..} = do
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  (Entity _ p) <-
    case doctors of
      [] -> liftIO $ expectationFailure "Should have found one doctor, got empty list instead."
      [p] -> pure p
      ps ->
        liftIO
          $ expectationFailure
          $ unlines
          $ "Should have found one doctor, got these instead: "
          : map show ps
  liftIO $ do
    doctorAccount p `shouldBe` aid
    doctorFirstName p `shouldBe` registerDoctorFirstName
    doctorLastName p `shouldBe` registerDoctorLastName
    doctorAddressLine1 p `shouldBe` registerDoctorAddressLine1
    doctorAddressLine2 p `shouldBe` registerDoctorAddressLine2
    doctorPostalCode p `shouldBe` registerDoctorPostalCode
    doctorCity p `shouldBe` registerDoctorCity
    doctorCountry p `shouldBe` registerDoctorCountry
    doctorPhoneNumber1 p `shouldBe` registerDoctorPhoneNumber1
    doctorPhoneNumber2 p `shouldBe` registerDoctorPhoneNumber2
    doctorEmail1 p `shouldBe` registerDoctorEmail1
    doctorEmail2 p `shouldBe` registerDoctorEmail2
    doctorComments p `shouldBe` registerDoctorComments

getDoctorByRegister :: AccountId -> RegisterDoctor -> RegtoolExample (Entity Doctor)
getDoctorByRegister aid RegisterDoctor {..} = do
  md <-
    runDB
      $ selectFirst
        [ DoctorAccount ==. aid,
          DoctorFirstName ==. registerDoctorFirstName,
          DoctorLastName ==. registerDoctorLastName
        ]
        []
  case md of
    Nothing -> liftIO $ expectationFailure "Should have had a doctor"
    Just ed -> pure ed
