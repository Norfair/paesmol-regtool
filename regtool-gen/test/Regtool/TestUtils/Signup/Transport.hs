{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Signup.Transport
  ( SignupChildTransport (..),
    signupChildTransport,
    verifyChildSignedUpForTransport,
  )
where

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation hiding (get, runDB)
import Regtool.Handler.Signup.Transport
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

signupChildTransport :: SignupChildTransport -> RegtoolExample ()
signupChildTransport SignupChildTransport {..} = do
  get SignupTransportR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl SignupTransportR
    addToken
    addPostParam "f1" $ sqlKeyText signupChildTransportChild
    addPostParam "f2" $ sqlKeyText signupChildTransportBusStop
    addPostParam "f3" "1" -- The first option in the drop-down
    addPostParam "f4" "2" -- The second option in the drop-down
  locationShouldBe SignupTransportR

verifyChildSignedUpForTransport :: SignupChildTransport -> RegtoolExample ()
verifyChildSignedUpForTransport SignupChildTransport {..} = do
  schoolYears <- runDB $ selectList [BusSchoolYearId ==. signupChildTransportSchoolYear] []
  (Entity _ bsy) <-
    case schoolYears of
      [] -> liftIO $ expectationFailure "Should have had the bus school year"
      [e] -> pure e
      _ -> liftIO $ expectationFailure "Should have found one schoolyear"
  signups <-
    runDB
      $ selectList
        [ TransportSignupChild ==. signupChildTransportChild,
          TransportSignupBusStop ==. signupChildTransportBusStop,
          TransportSignupSchoolYear ==. busSchoolYearYear bsy
        ]
        []
  (Entity _ TransportSignup {..}) <-
    case signups of
      [] -> liftIO $ expectationFailure "Should have found one signup, got an empty list instead."
      [tsu] -> pure tsu
      tsus ->
        liftIO
          $ expectationFailure
          $ unlines
          $ "Should have found one transport signup, got these instead: "
          : map show tsus
  liftIO $ do
    transportSignupChild `shouldBe` signupChildTransportChild
    transportSignupBusStop `shouldBe` signupChildTransportBusStop
    transportSignupSchoolYear `shouldBe` busSchoolYearYear bsy
