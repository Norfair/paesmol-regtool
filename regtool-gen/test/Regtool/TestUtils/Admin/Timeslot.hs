{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Admin.Timeslot
  ( registerDaycareTimeslot,
    verifyDaycareTimeslotRegistered,
  )
where

import qualified Data.Set as S
import qualified Data.Text as T
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Admin.Daycare.Timeslot
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

registerDaycareTimeslot ::
  DaycareSemestreId -> RegisterDaycareTimeslot -> RegtoolExample (Entity DaycareTimeslot)
registerDaycareTimeslot sid RegisterDaycareTimeslot {..} = do
  get $ AdminR $ AdminRegisterDaycareTimeslotR sid
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl $ AdminR $ AdminRegisterDaycareTimeslotR sid
    addToken
    addPostParam "f1" $ T.pack $ show (fromEnum registerDaycareTimeslotSchoolDay + 1)
    addPostParam "f2" $ T.pack $ show registerDaycareTimeslotStart
    addPostParam "f3" $ T.pack $ show registerDaycareTimeslotEnd
    addPostParam "f4" $ T.pack $ show $ unsafeUnAmount registerDaycareTimeslotFee
    addPostParam "f5" $ T.pack $ show $ unsafeUnAmount registerDaycareTimeslotOccasionalFee
    forM_ [minBound .. maxBound] $ \a ->
      addPostParam (T.pack $ "f" ++ show (6 + fromEnum a))
        $ if S.member a registerDaycareTimeslotAccessible
          then "yes"
          else "no"
  dtss <- runDB $ selectList [DaycareTimeslotSemestre ==. sid] []
  case dtss of
    [] ->
      liftIO $ expectationFailure "Should have found one daycare timeslot, got empty list instead."
    [e@(Entity tsid _)] -> do
      locationShouldBe $ AdminR (AdminDaycareTimeslotR sid tsid)
      pure e
    _ ->
      liftIO
        $ expectationFailure
        $ unlines
        $ "Should have found one daycare timeslots, got these instead: "
        : map show dtss

verifyDaycareTimeslotRegistered ::
  Entity DaycareTimeslot -> DaycareSemestreId -> RegisterDaycareTimeslot -> RegtoolExample ()
verifyDaycareTimeslotRegistered (Entity _ DaycareTimeslot {..}) sid RegisterDaycareTimeslot {..} = do
  liftIO $ do
    daycareTimeslotSemestre `shouldBe` sid
    daycareTimeslotSchoolDay `shouldBe` registerDaycareTimeslotSchoolDay
    daycareTimeslotStart `shouldBe` registerDaycareTimeslotStart
    daycareTimeslotEnd `shouldBe` registerDaycareTimeslotEnd
    daycareTimeslotFee `shouldBe` registerDaycareTimeslotFee
    daycareTimeslotOccasionalFee `shouldBe` registerDaycareTimeslotOccasionalFee
    daycareTimeslotAccessible `shouldBe` registerDaycareTimeslotAccessible
