{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.TestUtils.Admin.Semestre
  ( registerDaycareSemestre,
    verifyDaycareSemestreRegistered,
  )
where

import qualified Data.Text as T
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Admin.Daycare.Semestre
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

registerDaycareSemestre :: RegisterDaycareSemestre -> RegtoolExample (Entity DaycareSemestre)
registerDaycareSemestre RegisterDaycareSemestre {..} = do
  get $ AdminR AdminRegisterDaycareSemestreR
  statusIs 200
  sys <- liftIO getCurrentAndNextSchoolYears
  request $ do
    setMethod methodPost
    setUrl $ AdminR AdminRegisterDaycareSemestreR
    addToken
    let (registerDaycareSemestreSchoolYear, registerDaycareSemestreSemestre) =
          registerDaycareSemestreSchoolYearAndSemestre
        semestreOptions = length [minBound .. maxBound :: Semestre]
        yearIndex =
          fromMaybe
            (error $ "Cannot use this schoolyear for this test, pick one in this list: " <> show sys)
            $ elemIndex registerDaycareSemestreSchoolYear sys
        index = yearIndex * semestreOptions + fromEnum registerDaycareSemestreSemestre + 1
    addPostParam "f1" $ T.pack $ show index
  dcss <- runDB $ selectList [] []
  case dcss of
    [] ->
      liftIO $ expectationFailure "Should have found one daycare semestre, got empty list instead."
    [e@(Entity sid _)] -> do
      locationShouldBe $ AdminR $ AdminDaycareSemestreR sid
      pure e
    _ ->
      liftIO
        $ expectationFailure
        $ unlines
        $ "Should have found one daycare semestres, got these instead: "
        : map show dcss

verifyDaycareSemestreRegistered ::
  Entity DaycareSemestre -> RegisterDaycareSemestre -> RegtoolExample ()
verifyDaycareSemestreRegistered (Entity _ DaycareSemestre {..}) RegisterDaycareSemestre {..} = do
  let (registerDaycareSemestreSchoolYear, registerDaycareSemestreSemestre) =
        registerDaycareSemestreSchoolYearAndSemestre
  liftIO $ do
    daycareSemestreYear `shouldBe` registerDaycareSemestreSchoolYear
    daycareSemestreSemestre `shouldBe` registerDaycareSemestreSemestre
