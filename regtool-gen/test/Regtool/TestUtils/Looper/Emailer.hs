{-# LANGUAGE OverloadedStrings #-}

module Regtool.TestUtils.Looper.Emailer
  ( emailGoldenTests,
  )
where

import Regtool.Core.Foundation.Regtool
import Regtool.Looper.Emailer.Types
import Test.Syd.Yesod
import TestImport
import Yesod.Core

emailGoldenTests ::
  String ->
  (value -> Renderer -> Text) ->
  (value -> Renderer -> Text) ->
  value ->
  YesodSpec Regtool
emailGoldenTests path_ textMaker htmlMaker value = do
  let resolvedRoot = "https://registration.paesmol.eu"
  it "is compliant with the golden test for text" $ \yc ->
    let y = yesodClientSite yc
        render = yesodRender y resolvedRoot
        actual = textMaker value render
        ePath = path_ ++ ".txt"
     in pureGoldenTextFile ePath actual
  it "is compliant with the golden test for html" $ \yc ->
    let y = yesodClientSite yc
        render = yesodRender y resolvedRoot
        actual = htmlMaker value render
        ePath = path_ ++ ".html"
     in pureGoldenTextFile ePath actual
