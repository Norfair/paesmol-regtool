{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.OverviewSpec
  ( spec,
  )
where

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "OverviewR"
    $ do
      needsLoginSpec GET OverviewR
      it "returns a 200 for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get OverviewR
          statusIs 200
      it "returns a 200 for a verified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get OverviewR
          statusIs 200
