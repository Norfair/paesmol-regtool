module Regtool.Handler.ProfileSpec
  ( spec,
  )
where

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "ProfileR" $ do
      needsLoginSpec GET ProfileR
      it "returns a 200 for a verified account" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ChildrenR
          statusIs 200
    ydescribe "ProfileSetUsernameR" $ needsLoginSpec POST ProfileSetUserNameR
