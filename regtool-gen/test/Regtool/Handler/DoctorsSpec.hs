{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.DoctorsSpec
  ( spec,
  )
where

import Database.Persist.Sqlite hiding (get)
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils
import Regtool.TestUtils.Doctors
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "RegisterDoctorR" $ do
      needsLoginSpec GET RegisterDoctorR
      needsLoginSpec POST RegisterDoctorR
      it "Registers a doctor succesfully" $ \yc ->
        forAll genSimpleRegisterDoctor $ \rd ->
          withAnyVerifiedAccountLogin yc $ \(Entity aid _) -> do
            registerDoctor rd
            verifyDoctorRegistered aid rd
    ydescribe "DoctorsR" $ do
      needsLoginSpec GET RegisterDoctorR
      it "returns a 200 for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get DoctorsR
          statusIs 200
      it "returns a 200 for a verified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get DoctorsR
          statusIs 200
    ydescribe "DoctorR" $ needsLoginSpec GET $ DoctorR $ toSqlKey 0
