{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.ChildrenSpec
  ( spec,
  )
where

import Database.Persist hiding (get)
import Database.Persist.Sqlite (toSqlKey)
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Core.Migrations
import Regtool.Gen
import Regtool.TestUtils
import Regtool.TestUtils.Children
import Regtool.TestUtils.Doctors
import Regtool.TestUtils.LanguageSection
import Regtool.TestUtils.Parents
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "RegisterChildR" $ do
      needsLoginSpec GET RegisterChildR
      needsLoginSpec POST RegisterChildR
      it "Registers a child succesfully" $ \yc ->
        forAll genSimpleRegisterParent $ \rp ->
          forAll genSimpleRegisterDoctor $ \rd ->
            forAll genSimpleRegisterChild $ \rc ->
              withAnyVerifiedAccountLogin yc $ \(Entity aid _) -> do
                registerParent rp
                registerDoctor rd
                runDB setupLanguageSections
                (Entity did _) <- getDoctorByRegister aid rd
                (Entity lsid _) <- getLanguageSection
                let rc' = rc {registerChildDoctor = did, registerChildLanguageSection = lsid}
                registerChild rc'
                verifyChildRegistered aid rc'
    ydescribe "ChildrenR" $ do
      needsLoginSpec GET ChildrenR
      it "returns a 200 for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ChildrenR
          statusIs 200
      it "returns a 200 for a verified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ChildrenR
          statusIs 200
    ydescribe "ChildR" $ needsLoginSpec GET $ ChildR $ toSqlKey 0
    ydescribe "ChildrenConfirmLevelR" $ do
      needsLoginSpec GET ChildrenConfirmLevelR
      it "returns a 200 for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ChildrenR
          statusIs 200
