{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.HomeSpec
  ( spec,
  )
where

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "Home"
    $ do
      yit "returns a 200 if no user is logged in" $ do
        get HomeR
        statusIs 200
      it "redirects to the overview for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get HomeR
          locationShouldBe OverviewR
      it "redirects to the overview for a verified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get HomeR
          locationShouldBe OverviewR
