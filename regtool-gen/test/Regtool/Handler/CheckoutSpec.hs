{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Handler.CheckoutSpec
  ( spec,
  )
where

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "CheckoutR"
    $ do
      ydescribe "GET" $ do
        needsLoginSpec GET CheckoutChooseR
        it "returns a 200 for a verified user" $ \yc ->
          withAnyVerifiedAccountLogin_ yc $ do
            get CheckoutChooseR
            statusIs 200
      ydescribe "POST" $ needsLoginSpec POST CheckoutChooseR
