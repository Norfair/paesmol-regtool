{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.ParentsSpec (spec) where

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.TestUtils
import Regtool.TestUtils.Parents
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "RegisterParentR" $ do
      needsLoginSpec GET RegisterParentR
      needsLoginSpec POST RegisterParentR
      it "Registers a parent succesfully" $ \yc ->
        forAll genSimpleRegisterParent $ \rp ->
          withAnyVerifiedAccountLogin yc $ \(Entity aid _) -> do
            registerParent rp
            verifyParentRegistered aid rp
    ydescribe "ParentsR" $ do
      needsLoginSpec GET RegisterParentR
      it "returns a 200 for an unverified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ParentsR
          statusIs 200
      it "returns a 200 for a verified user" $ \yc ->
        withAnyVerifiedAccountLogin_ yc $ do
          get ParentsR
          statusIs 200
    ydescribe "ParentR" $ needsLoginSpec GET $ ParentR $ toSqlKey 0
