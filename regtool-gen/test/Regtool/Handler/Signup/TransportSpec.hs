{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.Signup.TransportSpec
  ( spec,
  )
where

import Database.Persist
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Core.Migrations
import Regtool.Data
import Regtool.Gen ()
import Regtool.TestUtils
import Regtool.TestUtils.Buslines
import Regtool.TestUtils.Children
import Regtool.TestUtils.Doctors
import Regtool.TestUtils.LanguageSection
import Regtool.TestUtils.Parents
import Regtool.TestUtils.Signup.Transport
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec
    $ ydescribe "SignupTransport"
    $ do
      needsLoginSpec GET SignupTransportR
      needsLoginSpec POST SignupTransportR
      it "Signs up a child succesfully" $ \yc ->
        forAll genSimpleRegisterParent $ \rp ->
          forAll genSimpleRegisterDoctor $ \rd ->
            forAll genSimpleRegisterChild $ \rc ->
              withAnyVerifiedAccountLogin yc $ \(Entity aid _) -> do
                csy <- liftIO getCurrentSchoolyear
                bsyid <- runDB $ registerBusSchoolYear csy
                registerParent rp
                registerDoctor rd
                runDB setupLanguageSections
                (Entity did _) <- getDoctorByRegister aid rd
                (Entity lsid _) <- getLanguageSection
                let rc' = rc {registerChildDoctor = did, registerChildLanguageSection = lsid}
                registerChild rc'
                (Entity cid _) <- getChildByRegister aid rc'
                runDB setupBusLines
                (Entity bsid _) <- getBusStop
                let sct =
                      SignupChildTransport
                        { signupChildTransportChild = cid,
                          signupChildTransportBusStop = bsid,
                          signupChildTransportSchoolYear = bsyid,
                          signupChildTransportInstalments = 5
                        }
                signupChildTransport sct
                verifyChildSignedUpForTransport sct
