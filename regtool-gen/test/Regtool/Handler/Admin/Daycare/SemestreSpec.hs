{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.Admin.Daycare.SemestreSpec
  ( spec,
  )
where

import Regtool.Application ()
import Regtool.Data
import Regtool.Gen ()
import Regtool.Handler.Admin.Daycare.Semestre
import Regtool.TestUtils
import Regtool.TestUtils.Admin.Semestre
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "AdminRegisterDaycareSemestreR"
      $ it "can register this example daycare semestre"
      $ \yc -> do
        withAdminAccountLogin_ yc $ do
          sy <- liftIO getCurrentSchoolyear
          let ds =
                RegisterDaycareSemestre
                  { registerDaycareSemestreSchoolYearAndSemestre = (sy, Spring),
                    registerDaycareSemestreId = Nothing,
                    registerDaycareSemestreOpen = True
                  }
          e <- registerDaycareSemestre ds
          verifyDaycareSemestreRegistered e ds
