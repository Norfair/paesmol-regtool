{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.Admin.Daycare.TimeslotSpec
  ( spec,
  )
where

import qualified Data.Set as S
import Data.Time
import Regtool.Application ()
import Regtool.Data
import Regtool.Gen ()
import Regtool.Handler.Admin.Daycare.Semestre
import Regtool.Handler.Admin.Daycare.Timeslot
import Regtool.TestUtils
import Regtool.TestUtils.Admin.Semestre
import Regtool.TestUtils.Admin.Timeslot
import Test.Syd.Yesod
import TestImport

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "AdminRegisterDaycareTimeslotR"
      $ it "can register this example daycare timeslot"
      $ \yc -> do
        withAdminAccountLogin_ yc $ do
          sy <- liftIO getCurrentSchoolyear
          let ds =
                RegisterDaycareSemestre
                  { registerDaycareSemestreSchoolYearAndSemestre = (sy, Spring),
                    registerDaycareSemestreId = Nothing,
                    registerDaycareSemestreOpen = True
                  }
          (Entity dsid _) <- registerDaycareSemestre ds
          let dts =
                RegisterDaycareTimeslot
                  { registerDaycareTimeslotSchoolDay = SchoolMonday,
                    registerDaycareTimeslotStart = TimeOfDay 13 30 0,
                    registerDaycareTimeslotEnd = TimeOfDay 16 15 0,
                    registerDaycareTimeslotFee = unsafeAmount 120,
                    registerDaycareTimeslotOccasionalFee = unsafeAmount 15,
                    registerDaycareTimeslotAccessible = S.fromList [Nursery1 .. Primary2],
                    registerDaycareTimeslotId = Nothing
                  }
          ets <- registerDaycareTimeslot dsid dts
          verifyDaycareTimeslotRegistered ets dsid dts
