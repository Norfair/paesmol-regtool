{-# LANGUAGE FlexibleContexts #-}

module Regtool.Handler.AdminSpec
  ( spec,
  )
where

import Network.HTTP.Types
import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils
import Test.Syd.Yesod
import TestImport
import Yesod.Auth
import Yesod.Core (RedirectUrl)

spec :: Spec
spec =
  regtoolSpec $ do
    ydescribe "AdminR" $ adminSpec GET $ AdminR PanelR
    ydescribe "AdminRawDataR" $ adminSpec GET $ AdminR $ AdminRawDataR []

adminSpec :: (RedirectUrl Regtool url) => StdMethod -> url -> RegtoolSpec
adminSpec method_ route = do
  yit "returns a 303 if a user is not logged-in" $ t 303
  it "returns a 403 for a non-admin" $ \yc -> withAnyVerifiedAccount_ yc $ t 403
  where
    t s = do
      let isPost = renderStdMethod method_ == methodPost
      when isPost $ get $ AuthR LoginR -- To make sure there's a CSRF cookie available
      request $ do
        setMethod $ renderStdMethod method_
        when isPost addTokenFromCookie
        setUrl route
      statusIs s
