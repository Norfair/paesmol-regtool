{-# LANGUAGE TypeApplications #-}

module Regtool.Core.AdminConfigurationSpec
  ( spec,
  )
where

import Regtool.Core.AdminConfiguration as Admin
import Regtool.Gen ()
import Test.Syd
import Test.Syd.Validity.Aeson
import TestImport

spec :: Spec
spec = describe "Configuration" $ jsonSpec @Admin.Configuration
