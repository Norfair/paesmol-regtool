{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.ExtraCharge where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.Entity ()

instance GenValid ExtraCharge where
  genValid = do
    extraChargeAccount <- genValid
    extraChargeAmount <- strictlyPositiveValidAmount
    extraChargeReason <- genValid
    extraChargeCheckout <- genValid
    extraChargeTimestamp <- genValid
    pure ExtraCharge {..}
  shrinkValid = shrinkValidStructurally
