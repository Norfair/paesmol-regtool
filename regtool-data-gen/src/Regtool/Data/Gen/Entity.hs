{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -O0 #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Entity where

import Database.Persist.Sql
import Import

modEnt :: (a -> a) -> Entity a -> Entity a
modEnt f (Entity i e) = Entity i (f e)

modEntL :: (a -> a) -> [Entity a] -> [Entity a]
modEntL f = map (modEnt f)
