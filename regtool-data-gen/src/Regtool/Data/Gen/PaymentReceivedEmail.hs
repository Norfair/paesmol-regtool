{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.PaymentReceivedEmail where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid PaymentReceivedEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
