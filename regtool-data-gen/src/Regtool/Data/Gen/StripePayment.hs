{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.StripePayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenValid StripePayment where
  genValid = do
    stripePaymentCustomer <- genValid
    stripePaymentAmount <- strictlyPositiveValidAmount
    stripePaymentCharge <- genValid
    stripePaymentEvent <- genValid
    stripePaymentSession <- genValid
    stripePaymentPaymentIntent <- genValid
    stripePaymentTimestamp <- genValid
    pure StripePayment {..}
  shrinkValid = shrinkValidStructurally
