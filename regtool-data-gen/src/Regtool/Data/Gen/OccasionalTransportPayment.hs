{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalTransportPayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()

instance GenValid OccasionalTransportPayment where
  genValid = genValidStructurally
