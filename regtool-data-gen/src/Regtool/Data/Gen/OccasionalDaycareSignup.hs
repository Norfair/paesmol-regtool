{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalDaycareSignup where

import Import
import Regtool.Data
import Regtool.Data.Gen.BusDirection ()
import Regtool.Data.Gen.Entity
import Regtool.Data.Gen.Utils

instance GenValid OccasionalDaycareSignup where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

occasionalDaycareSignupsForChildren :: [ChildId] -> Gen [Entity OccasionalDaycareSignup]
occasionalDaycareSignupsForChildren cids =
  fmap
    catMaybes
    (forM cids $ \cid -> mgen $ modEnt (\su -> su {occasionalDaycareSignupChild = cid}) <$> genValid)
    `suchThat` seperateIds
