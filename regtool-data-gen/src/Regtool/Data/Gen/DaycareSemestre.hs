{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareSemestre where

import Import
import Regtool.Data
import Regtool.Data.Gen.SchoolYear ()
import Regtool.Data.Gen.Semestre ()
import Regtool.Data.Gen.Utils ()

instance GenValid DaycareSemestre where
  genValid = genValidStructurally
