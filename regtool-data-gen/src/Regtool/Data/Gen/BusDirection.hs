{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusDirection where

import Import
import Regtool.Data
import Regtool.Data.Gen.Utils ()

instance GenValid BusDirection where
  genValid = genValidStructurally
