{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Company where

import Import
import Regtool.Data

instance GenValid Company

genSimpleCompany :: Gen Company
genSimpleCompany = genValid
