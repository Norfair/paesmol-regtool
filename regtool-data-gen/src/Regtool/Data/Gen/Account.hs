{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Account where

import Import
import Regtool.Data
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Utils ()

instance GenValid Account where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
