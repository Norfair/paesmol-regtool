{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.CustomActivityPayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()

instance GenValid CustomActivityPayment where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
