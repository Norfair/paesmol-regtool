{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportEnrollment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenValid TransportEnrollment where
  genValid = genValidStructurally
