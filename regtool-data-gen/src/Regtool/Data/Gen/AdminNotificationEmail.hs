{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.AdminNotificationEmail where

import Import
import Regtool.Data
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid AdminNotificationEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
