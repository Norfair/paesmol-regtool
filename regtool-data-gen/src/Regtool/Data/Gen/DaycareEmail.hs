{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareEmail where

import Import
import Regtool.Data (DaycareEmail (..))
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid DaycareEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
