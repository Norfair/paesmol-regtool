{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.StandardOrRaw where

import Import
import Regtool.Data
import Regtool.Data.Gen.Utils

instance (GenValid a) => GenValid (StandardOrRaw a) where
  genValid = frequency [(4, Standard <$> genValid), (1, Raw <$> genValid)]
  shrinkValid (Standard a) = Standard <$> shrinkValid a
  shrinkValid (Raw t) = Raw <$> shrinkValid t

genSimpleStandardOrRaw :: Gen a -> Gen (StandardOrRaw a)
genSimpleStandardOrRaw gen = frequency [(4, Standard <$> gen), (1, Raw <$> genSimpleText)]
