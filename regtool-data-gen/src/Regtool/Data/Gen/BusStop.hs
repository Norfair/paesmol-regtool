{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusStop where

import Import
import Regtool.Data
import Regtool.Data.Gen.Utils ()

instance GenValid BusStop where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
