{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalDaycareEmail where

import Import
import Regtool.Data (OccasionalDaycareEmail (..))
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid OccasionalDaycareEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
