{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalDaycarePayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()

instance GenValid OccasionalDaycarePayment where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
