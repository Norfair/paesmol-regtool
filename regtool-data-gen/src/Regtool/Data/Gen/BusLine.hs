{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusLine where

import Import
import Regtool.Data
import Regtool.Data.Gen.Utils ()

instance GenValid BusLine where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
