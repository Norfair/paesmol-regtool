{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.SchoolDay where

import Import
import Regtool.Data

instance GenValid SchoolDay where
  genValid = genValidStructurallyWithoutExtraChecking
