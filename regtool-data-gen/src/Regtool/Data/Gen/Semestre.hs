{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Semestre where

import Import
import Regtool.Data

instance GenValid Semestre where
  genValid = genValidStructurallyWithoutExtraChecking
