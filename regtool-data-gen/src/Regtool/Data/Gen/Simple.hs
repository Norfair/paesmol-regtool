{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Data.Gen.Simple where

import Data.Time
import Import
import Regtool.Data
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.EmailStatus ()
import Regtool.Data.Gen.Nationality ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.StandardOrRaw
import Regtool.Data.Gen.Utils
import Yesod.Form.Fields

instance GenValid LanguageSection where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid ChildOf where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid Email where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid PasswordResetEmail where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid VerificationEmail where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

genSimpleBirthDate :: Gen Day
genSimpleBirthDate = fromGregorian <$> choose (1990, 2020) <*> choose (1, 12) <*> choose (1, 31)

genSimpleSchoolLevel :: Gen SchoolLevel
genSimpleSchoolLevel = genValid

genSimpleNationality :: Gen Nationality
genSimpleNationality = genSimpleStandardOrRaw genValid

genSimpleAllergies :: Gen (Maybe Textarea)
genSimpleAllergies = mgen genSimpleTextarea

genSimpleHealthInfo :: Gen (Maybe Textarea)
genSimpleHealthInfo = mgen genSimpleTextarea

genSimpleMedication :: Gen (Maybe Textarea)
genSimpleMedication = mgen genSimpleTextarea

genSimplePickupAllowed :: Gen (Maybe Textarea)
genSimplePickupAllowed = mgen genSimpleTextarea

genSimplePickupDisallowed :: Gen (Maybe Textarea)
genSimplePickupDisallowed = mgen genSimpleTextarea

genSimplePicturePermission :: Gen Bool
genSimplePicturePermission = genValid

genSimpleDelijnBus :: Gen Bool
genSimpleDelijnBus = genValid
