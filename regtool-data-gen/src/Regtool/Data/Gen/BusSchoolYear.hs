{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusSchoolYear where

import Import
import Regtool.Data
import Regtool.Data.Gen.SchoolYear ()
import Regtool.Data.Gen.Utils ()

instance GenValid BusSchoolYear where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
