{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.YearlyFeePayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.SchoolYear ()

instance GenValid YearlyFeePayment where
  genValid = genValidStructurally
