{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Country where

import Import
import Regtool.Data
import Regtool.Data.Gen.StandardOrRaw

instance GenValid StandardCountry

genSimpleCountry :: Gen Country
genSimpleCountry = genSimpleStandardOrRaw genValid
