{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalTransportEmail where

import Import
import Regtool.Data (OccasionalTransportEmail (..))
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid OccasionalTransportEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
