{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportPayment where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenValid TransportPayment where
  genValid = do
    transportPaymentPaymentPlan <- genValid
    transportPaymentCheckout <- genValid
    transportPaymentAmount <- strictlyPositiveValidAmount
    pure TransportPayment {..}
