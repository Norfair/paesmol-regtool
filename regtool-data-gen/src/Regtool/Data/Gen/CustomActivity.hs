{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.CustomActivity where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.SchoolYear ()

instance GenValid CustomActivity where
  genValid = do
    customActivitySchoolYear <- genValid
    customActivityDescription <- genValid
    customActivityDate <- genValid
    customActivityStart <- genValid
    customActivityEnd <- genValid `suchThat` (>= customActivityStart)
    customActivityFee <- positiveValidAmount
    customActivitySpaces <- genValid
    customActivityCreationTime <- genValid
    customActivityOpen <- genValid
    pure CustomActivity {..}
  shrinkValid = shrinkValidStructurally
