{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.CustomActivitySignup where

import Import
import Regtool.Data
import Regtool.Data.Gen.Entity ()

instance GenValid CustomActivitySignup where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
