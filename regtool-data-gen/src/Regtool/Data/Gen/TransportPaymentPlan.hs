{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportPaymentPlan where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()

instance GenValid TransportPaymentPlan where
  genValid = genValidStructurally
