{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Permission where

import Import
import Regtool.Data

instance GenValid Permission where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
