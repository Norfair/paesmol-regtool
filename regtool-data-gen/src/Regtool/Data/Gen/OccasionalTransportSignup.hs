{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.OccasionalTransportSignup where

import Import
import Regtool.Data
import Regtool.Data.Gen.BusDirection ()
import Regtool.Data.Gen.Entity
import Regtool.Data.Gen.Utils

instance GenValid OccasionalTransportSignup where
  genValid = genValidStructurally

occasionalTransportSignupsForChildren :: [ChildId] -> Gen [Entity OccasionalTransportSignup]
occasionalTransportSignupsForChildren cids =
  fmap
    catMaybes
    ( forM cids $ \cid ->
        mgen $ modEnt (\su -> su {occasionalTransportSignupChild = cid}) <$> genValid
    )
    `suchThat` seperateIds
