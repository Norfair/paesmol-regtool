{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Amount where

import Import
import Regtool.Data

{-# ANN module "HLint: ignore Use ***" #-}

instance GenValid Amount where
  genValid = do
    ma <- safeAmount <$> arbitrary -- Arbitrary is fine, because we only deal with small amounts.
    case ma of
      Nothing -> genValid
      Just a -> pure a

positiveAmountUpTo :: Amount -> Gen Amount
positiveAmountUpTo a
  | a < zeroAmount = pure zeroAmount -- cannot generate a positive amount up to a negative amount
  | otherwise = do
      let cents = amountCents a
      cents' <- choose (0, cents)
      pure $ amountInCents cents'

strictlyPositiveAmountUpTo :: Amount -> Gen Amount
strictlyPositiveAmountUpTo a
  | a <= zeroAmount = pure zeroAmount -- cannot generate a strictly positive amount up to a negative amount
  | otherwise = do
      let cents = amountCents a
      cents' <- choose (1, cents)
      pure $ amountInCents cents'

splitAmount :: Amount -> Gen (Amount, Amount)
splitAmount a =
  case compare a zeroAmount of
    EQ -> pure (a, zeroAmount)
    GT -> go a
    LT -> (\(a1, a2) -> (mulAmount (-1) a1, mulAmount (-1) a2)) <$> go (absAmount a)
  where
    go a_ = do
      a' <- strictlyPositiveAmountUpTo a_
      pure (subAmount a_ a', a')

splitAmounts :: Amount -> Gen [Amount]
splitAmounts = go >=> shuffle
  where
    go :: Amount -> Gen [Amount]
    go a =
      if a == zeroAmount
        then pure []
        else do
          (a1, a2) <- splitAmount a
          case (a1 == zeroAmount, a2 == zeroAmount) of
            (True, True) -> pure []
            (True, False) -> pure [a2]
            (False, True) -> pure [a1]
            (False, False) -> do
              rest <- go a2
              pure (a1 : rest)

positiveValidAmount :: Gen Amount
positiveValidAmount = (absAmount <$> genValid) `suchThat` (>= zeroAmount)

strictlyPositiveValidAmount :: Gen Amount
strictlyPositiveValidAmount = positiveValidAmount `suchThat` (> zeroAmount)
