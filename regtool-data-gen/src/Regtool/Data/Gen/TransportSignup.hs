{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportSignup where

import Import
import Regtool.Data
import Regtool.Data.Gen.Entity
import Regtool.Data.Gen.SchoolYear ()
import Regtool.Data.Gen.Utils

instance GenValid TransportSignup where
  genValid = do
    transportSignupChild <- genValid
    transportSignupBusStop <- genValid
    transportSignupSchoolYear <- genValid
    transportSignupTime <- genValid
    transportSignupInstalments <- (+ 1) . abs <$> arbitrary -- We use arbitrary (which only scales with size) because the number of installments will be small anyway
    transportSignupDeletedByAdmin <- genValid
    pure TransportSignup {..}

transportSignupsForChildren :: [ChildId] -> Gen [Entity TransportSignup]
transportSignupsForChildren cids =
  fmap
    catMaybes
    (forM cids $ \cid -> mgen $ modEnt (\su -> su {transportSignupChild = cid}) <$> genValid)
    `suchThat` seperateIds
