{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Checkout where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.StripePayment ()

instance GenValid Checkout where
  genValid = genValidStructurally
