{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.RelationShip where

import Import
import Regtool.Data
import Regtool.Data.Gen.StandardOrRaw

instance GenValid StandardRelationShip

genSimpleRelationShip :: Gen RelationShip
genSimpleRelationShip = genSimpleStandardOrRaw genValid
