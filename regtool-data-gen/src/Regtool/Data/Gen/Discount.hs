{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Discount where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.Entity ()

instance GenValid Discount where
  genValid = do
    discountAccount <- genValid
    discountAmount <- strictlyPositiveValidAmount
    discountReason <- genValid
    discountCheckout <- genValid
    discountTimestamp <- genValid
    pure Discount {..}
  shrinkValid = shrinkValidStructurally
