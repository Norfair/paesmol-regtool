{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusLineStop where

import Import
import Regtool.Data
import Regtool.Data.Gen.Entity ()

instance GenValid BusLineStop where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
