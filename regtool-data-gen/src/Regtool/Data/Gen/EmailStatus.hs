{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.EmailStatus where

import Import
import Regtool.Data

instance GenValid EmailStatus where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
