{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Nationality where

import Import
import Regtool.Data

instance GenValid StandardNationality
