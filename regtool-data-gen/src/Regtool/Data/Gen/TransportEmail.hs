{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportEmail where

import Import
import Regtool.Data (TransportEmail (..))
import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()

instance GenValid TransportEmail where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
