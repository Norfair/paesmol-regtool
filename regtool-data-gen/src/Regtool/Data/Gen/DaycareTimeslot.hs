{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareTimeslot where

import Import
import Regtool.Data
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.SchoolDay ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.Utils ()

instance GenValid DaycareTimeslot where
  genValid = do
    daycareTimeslotSemestre <- genValid
    daycareTimeslotSchoolDay <- genValid
    daycareTimeslotStart <- genValid
    daycareTimeslotEnd <- genValid `suchThat` (>= daycareTimeslotStart)
    daycareTimeslotFee <- positiveValidAmount
    daycareTimeslotOccasionalFee <- positiveValidAmount
    daycareTimeslotAccessible <- genValid
    daycareTimeslotCreationTime <- genValid
    pure DaycareTimeslot {..}
