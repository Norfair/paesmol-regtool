{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.CartStatus where

import Import
import Regtool.Data

instance GenValid CartStatus where
  genValid = genValidStructurally
