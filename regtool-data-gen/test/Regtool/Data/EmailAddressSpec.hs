{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.EmailAddressSpec
  ( spec,
  )
where

import Regtool.Data
import Regtool.Data.Gen ()
import TestImport

spec :: Spec
spec = do
  genValidSpec @EmailAddress
  describe "normalizeEmail" $ it "produces valids" $ producesValid normalizeEmail
  describe "emailValidateFromText"
    $ it "produces valids"
    $ producesValid emailValidateFromText
  describe "emailValidateFromString"
    $ it "produces valids"
    $ producesValid emailValidateFromString
  describe "emailAddressFromText"
    $ it "produces valids"
    $ producesValid emailAddressFromText
  describe "emailAddressFromString"
    $ it "produces valids"
    $ producesValid emailAddressFromString
  describe "emailAddressText" $ it "produces valids" $ producesValid emailAddressText
  describe "emailAddressByteString"
    $ it "produces valids"
    $ producesValid emailAddressByteString
  describe "domainPart" $ it "produces valids" $ producesValid domainPart
  describe "localPart" $ it "produces valids" $ producesValid localPart
