module Regtool.Data.Gen.TransportSignupSpec where

import Regtool.Data.Gen.TransportSignup
import TestImport

spec :: Spec
spec =
  describe "transportSignupsForChildren"
    $ it "generates valids on valids"
    $ forAllValid
    $ \cids -> forAll (transportSignupsForChildren cids) shouldBeValid
