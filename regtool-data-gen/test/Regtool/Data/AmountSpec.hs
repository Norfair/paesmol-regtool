{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.AmountSpec
  ( spec,
  )
where

import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NE
import Regtool.Data
import Regtool.Data.Gen
import TestImport

spec :: Spec
spec = do
  genValidSpec @Amount
  describe "positiveValidAmount"
    $ it "produces valid abouts"
    $ genGeneratesValid positiveValidAmount
  describe "positiveAmountUpTo"
    $ it "produces valid abouts"
    $ forAll positiveValidAmount
    $ genGeneratesValid
    . positiveAmountUpTo
  describe "strictlyPositiveValidAmount"
    $ it "produces valid abouts"
    $ genGeneratesValid strictlyPositiveValidAmount
  describe "strictlyPositiveAmountUpTo"
    $ it "produces valid abouts"
    $ forAll strictlyPositiveValidAmount
    $ genGeneratesValid
    . strictlyPositiveAmountUpTo
  describe "splitAmount" $ do
    it "splits an amount into amounts that sum to the original amount"
      $ forAll positiveValidAmount
      $ \a ->
        forAll (splitAmount a) $ \(a1, a2) -> addAmount a1 a2 `shouldBe` a
    it "generates values with the same sign"
      $ forAll positiveValidAmount
      $ \a ->
        forAll (splitAmount a) $ \(a1, a2) ->
          let as = [a1, a2]
           in all (>= zeroAmount) as || all (<= zeroAmount) as
  describe "splitAmounts" $ do
    it "splits an amount into amounts that sum to the original amount"
      $ forAll positiveValidAmount
      $ \a -> forAll (splitAmounts a) $ \as -> sumAmount as `shouldBe` a
    it "generates nonzero amount"
      $ forAll positiveValidAmount
      $ \a -> forAll (splitAmounts a) $ \as -> zeroAmount `notElem` as
    it "generates values with the same sign"
      $ forAll positiveValidAmount
      $ \a ->
        forAll (splitAmounts a) $ \as -> all (>= zeroAmount) as || all (<= zeroAmount) as
  describe "safeAmount" $ it "produces valid amounts" $ validIfSucceeds safeAmount
  describe "zeroAmount" $ it "is valid" $ shouldBeValid zeroAmount
  describe "absAmount" $ it "produces valids on valids" $ producesValid absAmount
  describe "negateAmount" $ it "produces valids on valids" $ producesValid negateAmount
  describe "addAmount" $ it "produces valids on valids" $ producesValid2 addAmount
  describe "subAmount" $ it "produces valids on valids" $ producesValid2 subAmount
  describe "sumAmount" $ it "produces valids on valids" $ producesValid sumAmount
  describe "mulAmount" $ it "produces valids on valids" $ producesValid2 mulAmount
  describe "divAmount" $ do
    let genSmallWord = scale (`div` 5) $ sized $ \n -> fromIntegral <$> choose (1, n)
    it "produces valids on valids"
      $
      -- Small word generator for convenience
      forAll genSmallWord
      $ \w -> producesValid (`divAmount` w)
    it "succeeds if neither argument is zero"
      $
      -- Small word generator for convenience
      forAll (genSmallWord `suchThat` (/= 0))
      $ \w ->
        forAll (genValid `suchThat` ((/= 0) . amountCents)) $ \a ->
          case a `divAmount` w of
            DivSuccess _ -> pure ()
            r -> expectationFailure $ show r
    it "fails if the first argument is zero"
      $ forAll genSmallWord
      $ \w -> divAmount (amountInCents 0) w `shouldBe` DivOfZero
    it "fails if the second argument is zero and the first isn't"
      $ forAll (genValid `suchThat` ((/= 0) . amountCents))
      $ \a -> divAmount a 0 `shouldBe` DivByZero
    it "produces a list of amounts that all have the same sign"
      $ forAllValid
      $ \a ->
        forAll genSmallWord $ \w ->
          case divAmount a w of
            DivOfZero -> pure () -- Nothing to check
            DivByZero -> pure () -- Nothing to check
            DivSuccess ne ->
              ne `shouldSatisfy` (\ne_ -> all (>= zeroAmount) ne_ || all (<= zeroAmount) ne_)
    it "produces a list without zero amounts"
      $ forAllValid
      $ \a ->
        forAll genSmallWord $ \w ->
          case divAmount a w of
            DivOfZero -> pure () -- Nothing to check
            DivByZero -> pure () -- Nothing to check
            DivSuccess ne -> ne `shouldSatisfy` notElem zeroAmount
    it "produces a list that sums to the amount"
      $ forAllValid
      $ \a ->
        forAll genSmallWord $ \w ->
          case divAmount a w of
            DivOfZero -> pure () -- Nothing to check
            DivByZero -> pure () -- Nothing to check
            DivSuccess ne -> sumAmount (NE.toList ne) `shouldBe` a
    it "works on this example of 5/12"
      $ divAmount (amountInCents 5) 12
      `shouldBe` DivSuccess (amountInCents 1 :| replicate 4 (amountInCents 1))
    it "works on this example of 54/12"
      $ divAmount (amountInCents 54) 12
      `shouldBe` DivSuccess (amountInCents 5 :| map amountInCents (replicate 5 5 ++ replicate 6 4))
    it "works on this example of 540/13"
      $ divAmount (amountInCents 540) 13
      `shouldBe` DivSuccess (amountInCents 42 :| map amountInCents (replicate 6 42 ++ replicate 6 41))
  describe "fmtAmount" $ it "produces valids on valids" $ producesValid fmtAmount
