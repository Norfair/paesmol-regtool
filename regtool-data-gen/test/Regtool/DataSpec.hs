{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.DataSpec
  ( spec,
  )
where

import Data.Aeson
import Database.Persist
import Regtool.Data
import Regtool.Data.Gen ()
import Test.Syd.Persistent.Sqlite
import TestImport

spec :: Spec
spec = do
  describe "Helper types" $ do
    helperTypeSpec @CartStatus
    helperTypeSpec @Company
    helperTypeSpec @Nationality
    helperTypeSpec @RelationShip
    helperTypeSpec @SchoolLevel
    helperTypeSpec @SchoolYear
    helperTypeSpec @EmailStatus

  describe "Database types" $ do
    dbTypeSpec @Account
    dbTypeSpec @AdminNotificationEmail
    dbTypeSpec @BusLine
    dbTypeSpec @BusLineStop
    dbTypeSpec @BusSchoolYear
    dbTypeSpec @BusStop
    dbTypeSpec @Checkout
    dbTypeSpec @Child
    dbTypeSpec @ChildOf
    dbTypeSpec @CustomActivity
    dbTypeSpec @CustomActivityPayment
    dbTypeSpec @CustomActivitySignup
    dbTypeSpec @DaycareActivity
    dbTypeSpec @DaycareActivityPayment
    dbTypeSpec @DaycareActivitySignup
    dbTypeSpec @DaycareEmail
    dbTypeSpec @DaycarePayment
    dbTypeSpec @DaycareSemestre
    dbTypeSpec @DaycareSignup
    dbTypeSpec @DaycareTimeslot
    dbTypeSpec @Discount
    dbTypeSpec @Doctor
    dbTypeSpec @Email
    dbTypeSpec @ExtraCharge
    dbTypeSpec @LanguageSection
    dbTypeSpec @OccasionalDaycareEmail
    dbTypeSpec @OccasionalDaycarePayment
    dbTypeSpec @OccasionalDaycareSignup
    dbTypeSpec @OccasionalTransportEmail
    dbTypeSpec @OccasionalTransportPayment
    dbTypeSpec @OccasionalTransportSignup
    dbTypeSpec @Parent
    dbTypeSpec @PasswordResetEmail
    dbTypeSpec @PaymentReceivedEmail
    dbTypeSpec @Permission
    dbTypeSpec @Semestre
    dbTypeSpec @StripeCustomer
    dbTypeSpec @StripePayment
    dbTypeSpec @TransportEmail
    dbTypeSpec @TransportEnrollment
    dbTypeSpec @TransportPayment
    dbTypeSpec @TransportPaymentPlan
    dbTypeSpec @TransportSignup
    dbTypeSpec @VerificationEmail
    dbTypeSpec @YearlyFeePayment

  sqliteMigrationSucceedsSpec "test_resources/migration.sql" migrateAll

helperTypeSpec ::
  forall a.
  (Show a, Typeable a, GenValid a) =>
  Spec
helperTypeSpec = do
  genValidSpec @a

dbTypeSpec ::
  forall a.
  (Show a, Eq a, GenValid a, Typeable a, FromJSON a, ToJSON a, PersistField a) =>
  Spec
dbTypeSpec = do
  genValidSpec @a
  jsonSpec @a
  persistSpec @a
