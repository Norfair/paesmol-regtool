{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Criterion.Main as Criterion
import Data.GenValidity.Criterion
import Regtool.Data
import Regtool.Data.Gen

main :: IO ()
main =
  Criterion.defaultMain
    [ genValidBench @Amount,
      genBench "positiveValidAmount" positiveValidAmount,
      genBench "strictlyPositiveValidAmount" strictlyPositiveValidAmount,
      genValidBench @Account,
      genValidBench @Permission,
      genValidBench @BusSchoolYear,
      genValidBench @BusLine,
      genValidBench @BusLineStop,
      genValidBench @BusStop,
      genValidBench @CartStatus,
      genValidBench @Checkout,
      genValidBench @Child,
      genValidBench @ChildOf,
      genValidBench @DaycareActivity,
      genValidBench @DaycareActivityPayment,
      genValidBench @DaycareActivitySignup,
      genValidBench @DaycarePayment,
      genValidBench @DaycareSemestre,
      genValidBench @DaycareSignup,
      genValidBench @DaycareTimeslot,
      genValidBench @ExtraCharge,
      genValidBench @Discount,
      genValidBench @Doctor,
      genValidBench @Email,
      genValidBench @LanguageSection,
      genValidBench @OccasionalDaycarePayment,
      genValidBench @OccasionalDaycareSignup,
      genValidBench @OccasionalTransportPayment,
      genValidBench @OccasionalTransportSignup,
      genValidBench @Parent,
      genValidBench @PasswordResetEmail,
      genValidBench @PaymentReceivedEmail,
      genValidBench @AdminNotificationEmail,
      genValidBench @Semestre,
      genValidBench @StripeCustomer,
      genValidBench @StripePayment,
      genValidBench @TransportEnrollment,
      genValidBench @TransportPayment,
      genValidBench @TransportPaymentPlan,
      genValidBench @TransportSignup,
      genValidBench @VerificationEmail,
      genValidBench @DaycareEmail,
      genValidBench @TransportEmail,
      genValidBench @OccasionalDaycareEmail,
      genValidBench @OccasionalTransportEmail,
      genValidBench @YearlyFeePayment,
      genValidBench @CustomActivity,
      genValidBench @CustomActivitySignup,
      genValidBench @CustomActivityPayment
    ]
