{ mkDerivation, aeson, base, bytestring, criterion, genvalidity
, genvalidity-bytestring, genvalidity-containers
, genvalidity-criterion, genvalidity-path, genvalidity-persistent
, genvalidity-sydtest, genvalidity-sydtest-aeson
, genvalidity-sydtest-persistent, genvalidity-text
, genvalidity-time, lib, persistent, pretty-show, QuickCheck
, regtool-data, safe, sydtest, sydtest-discover
, sydtest-persistent-sqlite, text, time, yesod-form
}:
mkDerivation {
  pname = "regtool-data-gen";
  version = "0.0.0";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-persistent
    genvalidity-text genvalidity-time persistent QuickCheck
    regtool-data safe text time yesod-form
  ];
  testHaskellDepends = [
    aeson base bytestring genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-sydtest
    genvalidity-sydtest-aeson genvalidity-sydtest-persistent
    genvalidity-text genvalidity-time persistent pretty-show QuickCheck
    regtool-data safe sydtest sydtest-persistent-sqlite text
  ];
  testToolDepends = [ sydtest-discover ];
  benchmarkHaskellDepends = [
    base criterion genvalidity-criterion regtool-data
  ];
  license = "unknown";
}
