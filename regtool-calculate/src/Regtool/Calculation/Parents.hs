module Regtool.Calculation.Parents
  ( getParentsOf,
    getParentsOfChild,
  )
where

import Database.Persist.Sqlite
import Import
import Regtool.Data

getParentsOf :: (MonadIO m) => AccountId -> ReaderT SqlBackend m [Entity Parent]
getParentsOf aid = selectList [ParentAccount ==. aid] []

getParentsOfChild :: (MonadIO m) => ChildId -> ReaderT SqlBackend m [Entity Parent]
getParentsOfChild cid = do
  as <- map (childOfParent . entityVal) <$> selectList [ChildOfChild ==. cid] []
  fmap concat $ forM as $ \aid -> selectList [ParentAccount ==. aid] []
