{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Calculation.Children
  ( getAccountsOf,
    getChildrenOf,
    childName,
  )
where

import qualified Data.Text as T
import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Import
import Regtool.Data

getAccountsOf :: (MonadIO m) => ChildId -> ReaderT SqlBackend m [Entity Account]
getAccountsOf cid =
  E.select
    $ E.from
    $ \(account `E.InnerJoin` childOf) -> do
      E.on $ childOf ^. ChildOfParent E.==. account ^. AccountId
      E.where_ $ childOf ^. ChildOfChild E.==. E.val cid
      pure account

getChildrenOf :: (MonadIO m) => AccountId -> ReaderT SqlBackend m [Entity Child]
getChildrenOf accid =
  E.select
    $ E.from
    $ \(child `E.InnerJoin` childOf) -> do
      E.on $ childOf ^. ChildOfChild E.==. child ^. ChildId
      E.where_ $ childOf ^. ChildOfParent E.==. E.val accid
      pure child

childName :: Child -> Text
childName Child {..} = T.unwords [childFirstName, childLastName]
