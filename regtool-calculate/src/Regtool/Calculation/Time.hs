module Regtool.Calculation.Time where

import Import

adminTime :: UTCTime -> String
adminTime = formatTime defaultTimeLocale "%F %R"

formatTimestamp :: TimeOfDay -> String
formatTimestamp = formatTime defaultTimeLocale "%R"

formatDay :: Day -> String
formatDay = formatTime defaultTimeLocale "%F"
