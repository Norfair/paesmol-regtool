{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Calculation.Costs.Invoice
  ( Invoice (..),
    deriveInvoice,
    invoiceTotal,
    CostLine (..),
    DiscountLine (..),
  )
where

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import qualified Data.Text as T
import Database.Persist
import Import
import Regtool.Calculation.Children
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Time
import Regtool.Data

{-# ANN module ("HLint: ignore Use ++" :: String) #-}

data Invoice = Invoice
  { invoiceCostLines :: [CostLine],
    invoiceDiscountLines :: [DiscountLine]
  }
  deriving (Show, Generic)

instance Validity Invoice

instance NFData Invoice

deriveInvoice :: Choices -> Invoice
deriveInvoice cs =
  Invoice {invoiceCostLines = deriveCostLines cs, invoiceDiscountLines = deriveDiscountLines cs}

invoiceTotal :: Invoice -> Amount
invoiceTotal Invoice {..} =
  costLinesTotal invoiceCostLines `subAmount` discountLinesTotal invoiceDiscountLines

data CostLine = CostLine
  { costLineDescription :: Text,
    costLineAmount :: Amount
  }
  deriving (Show, Generic)

instance Validity CostLine where
  validate cl@CostLine {..} = mconcat [genericValidate cl, validatePositiveAmount costLineAmount]

instance NFData CostLine

deriveCostLines :: Choices -> [CostLine]
deriveCostLines Choices {..} =
  concat
    [ concatMap (uncurry deriveYearlyCostLines) (M.toAscList choicesYearlyMap),
      map deriveOccasionalTransportCostLine choicesOccasionalTransport,
      map deriveOccasionalDaycareCostLine choicesOccasionalDaycare,
      map deriveCustomActivityCostLine choicesCustomActivity,
      map deriveExtraChargeCostLine choicesExtraCharges
    ]

deriveYearlyCostLines :: BusSchoolYear -> YearlyChoices -> [CostLine]
deriveYearlyCostLines sy YearlyChoices {..} =
  concat
    [ yearlyCostLine (busSchoolYearYear sy) yearlyChoicesYearlyFee,
      concatMap (NE.toList . uncurry busCostLines) $ M.toAscList yearlyChoicesTransportInstalments,
      concatMap (uncurry deriveSemestrelyCostLines) $ M.toAscList yearlyChoicesSemestrelyMap
    ]

deriveSemestrelyCostLines :: Entity DaycareSemestre -> SemestrelyChoices -> [CostLine]
deriveSemestrelyCostLines sem SemestrelyChoices {..} =
  concat
    [ map (daycareChoiceLine sem) semestrelyChoicesDaycareChoices,
      map (daycareActivityChoiceLine sem) semestrelyChoicesDaycareActivityChoices
    ]

daycareChoiceLine :: Entity DaycareSemestre -> DaycareChoice -> CostLine
daycareChoiceLine (Entity _ DaycareSemestre {..}) DaycareChoice {..} =
  CostLine
    { costLineDescription =
        T.unwords
          [ "Daycare services for",
            childName (entityVal daycareChoiceChild),
            "on",
            schoolDayText $ daycareTimeslotSchoolDay $ entityVal daycareChoiceTimeslot,
            "at",
            T.pack $ formatTimestamp $ daycareTimeslotStart $ entityVal daycareChoiceTimeslot,
            "-",
            T.pack $ formatTimestamp $ daycareTimeslotEnd $ entityVal daycareChoiceTimeslot,
            "of semestre",
            semestreText daycareSemestreSemestre,
            schoolYearText daycareSemestreYear
          ],
      costLineAmount = daycareChoiceAmount
    }

daycareActivityChoiceLine :: Entity DaycareSemestre -> DaycareActivityChoice -> CostLine
daycareActivityChoiceLine (Entity _ DaycareSemestre {..}) DaycareActivityChoice {..} =
  CostLine
    { costLineDescription =
        T.unwords
          [ "Daycare activity",
            daycareActivityName $ entityVal daycareActivityChoiceActivity,
            "for",
            childName (entityVal daycareActivityChoiceChild),
            "on",
            schoolDayText $ daycareActivitySchoolDay $ entityVal daycareActivityChoiceActivity,
            "at",
            T.pack
              $ formatTimestamp
              $ daycareActivityStart
              $ entityVal daycareActivityChoiceActivity,
            "-",
            T.pack $ formatTimestamp $ daycareActivityEnd $ entityVal daycareActivityChoiceActivity,
            "of semestre",
            semestreText daycareSemestreSemestre,
            schoolYearText daycareSemestreYear
          ],
      costLineAmount = daycareActivityChoiceAmount
    }

yearlyCostLine :: SchoolYear -> Maybe Amount -> [CostLine]
yearlyCostLine _ Nothing = []
yearlyCostLine sy (Just a) =
  [ CostLine
      { costLineDescription = T.concat ["Yearly membership fee (", schoolYearText sy, ")"],
        costLineAmount = a
      }
  ]

busCostLines :: Entity Child -> TransportInstallmentChoices -> NonEmpty CostLine
busCostLines (Entity _ c) TransportInstallmentChoices {..} =
  flip NE.map transportInstallmentChoicesInstalments $ \a ->
    CostLine
      { costLineDescription =
          case transportSignupInstalments (entityVal transportInstallmentChoicesTransportSignup) of
            1 -> T.unwords ["Yearly bus fee for", childName c]
            _ -> T.unwords ["Installment of the yearly bus fee for", childName c],
        costLineAmount = a
      }

costLinesTotal :: [CostLine] -> Amount
costLinesTotal = sumAmount . map costLineAmount

data DiscountLine = DiscountLine
  { discountLineDescription :: Maybe Text,
    discountLineAmount :: Amount
  }
  deriving (Show, Generic)

instance Validity DiscountLine where
  validate dl@DiscountLine {..} =
    mconcat [genericValidate dl, validateStrictlyPositiveAmount discountLineAmount]

instance NFData DiscountLine

deriveDiscountLines :: Choices -> [DiscountLine]
deriveDiscountLines = map deriveDiscountLine . choicesDiscounts

deriveDiscountLine :: Entity Discount -> DiscountLine
deriveDiscountLine (Entity _ Discount {..}) =
  DiscountLine {discountLineDescription = discountReason, discountLineAmount = discountAmount}

discountLinesTotal :: [DiscountLine] -> Amount
discountLinesTotal = sumAmount . map discountLineAmount

deriveOccasionalTransportCostLine :: OccasionalTransportChoice -> CostLine
deriveOccasionalTransportCostLine OccasionalTransportChoice {..} =
  CostLine
    { costLineDescription =
        let Entity _ child = occasionalTransportChoiceChild
            Entity _ ots = occasionalTransportChoiceSignup
         in T.unwords
              [ "Occasional Transport for",
                childName child,
                "on",
                T.pack $ invoiceFormatDay $ occasionalTransportSignupDay ots,
                prettyBusDirection $ occasionalTransportSignupDirection ots
              ],
      costLineAmount = occasionalTransportChoiceAmount
    }

deriveOccasionalDaycareCostLine :: OccasionalDaycareChoice -> CostLine
deriveOccasionalDaycareCostLine OccasionalDaycareChoice {..} =
  CostLine
    { costLineDescription =
        let Entity _ child = occasionalDaycareChoiceChild
            Entity _ ots = occasionalDaycareChoiceSignup
         in T.unwords
              [ "Occasional Daycare for",
                childName child,
                "on",
                T.pack $ invoiceFormatDay $ occasionalDaycareSignupDay ots
              ],
      costLineAmount = occasionalDaycareChoiceAmount
    }

deriveCustomActivityCostLine :: CustomActivityChoice -> CostLine
deriveCustomActivityCostLine CustomActivityChoice {..} =
  CostLine
    { costLineDescription =
        T.unwords
          [ customActivityDescription ca,
            "for",
            childName child,
            "on",
            T.pack $ invoiceFormatDay $ customActivityDate ca,
            "at",
            T.pack $ formatTimestamp $ customActivityStart ca
          ],
      costLineAmount = customActivityFee ca
    }
  where
    Entity _ child = customActivityChoiceChild
    Entity _ ca = customActivityChoiceActivity

deriveExtraChargeCostLine :: Entity ExtraCharge -> CostLine
deriveExtraChargeCostLine (Entity _ ExtraCharge {..}) =
  CostLine
    { costLineDescription = fromMaybe "Extra Charge" extraChargeReason,
      costLineAmount = extraChargeAmount
    }

invoiceDayFormat :: String
invoiceDayFormat = "%A %d %B %Y"

invoiceFormatDay :: Day -> String
invoiceFormatDay = formatTime defaultTimeLocale invoiceDayFormat
