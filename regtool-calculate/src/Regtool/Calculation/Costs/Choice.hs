{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Calculation.Costs.Choice
  ( Choices (..),
    YearlyChoices (..),
    SemestrelyChoices (..),
    TransportInstallmentChoices (..),
    OccasionalTransportChoice (..),
    DaycareChoice (..),
    DaycareActivityChoice (..),
    OccasionalDaycareChoice (..),
    CustomActivityChoice (..),
    deriveChoices,
    trimChoices,
    deriveYearlyChoices,
    deriveSemestrelyChoices,
    deriveOccasionalTransportChoices,
    deriveOccasionalDaycareChoices,
    deriveDaycareChoice,
  )
where

import Data.Aeson
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Import
import Regtool.Calculation.Costs.Calculate
import Regtool.Data

data Choices = Choices
  { choicesYearlyMap :: Map BusSchoolYear YearlyChoices,
    choicesOccasionalTransport :: [OccasionalTransportChoice],
    choicesOccasionalDaycare :: [OccasionalDaycareChoice],
    choicesCustomActivity :: [CustomActivityChoice],
    choicesExtraCharges :: [Entity ExtraCharge],
    choicesDiscounts :: [Entity Discount]
  }
  deriving (Show, Eq, Generic)

instance Validity Choices where
  validate cs@Choices {..} =
    mconcat
      [ genericValidate cs,
        decorate "None of the yearly choices are empty"
          $ decorateList (M.toList choicesYearlyMap)
          $ \(_, v) ->
            declare "The yearly choices are not empty" $ v /= mempty,
        decorate "None of the extraCharges have a checkout"
          $ decorateList choicesExtraCharges
          $ \(Entity _ ExtraCharge {..}) ->
            declare "the extraCharge has no checkout" $ isNothing extraChargeCheckout,
        decorate "None of the discounts have a checkout"
          $ decorateList choicesDiscounts
          $ \(Entity _ Discount {..}) ->
            declare "the discount has no checkout" $ isNothing discountCheckout
      ]

instance NFData Choices

instance FromJSON Choices

instance ToJSON Choices

instance Semigroup Choices where
  (<>) c1 c2 =
    Choices
      { choicesYearlyMap = M.union (choicesYearlyMap c1) (choicesYearlyMap c2),
        choicesOccasionalTransport = choicesOccasionalTransport c1 <> choicesOccasionalTransport c2,
        choicesOccasionalDaycare = choicesOccasionalDaycare c1 <> choicesOccasionalDaycare c2,
        choicesCustomActivity = choicesCustomActivity c1 <> choicesCustomActivity c2,
        choicesExtraCharges = choicesExtraCharges c1 <> choicesExtraCharges c2,
        choicesDiscounts = choicesDiscounts c1 <> choicesDiscounts c2
      }

instance Monoid Choices where
  mempty = Choices M.empty [] [] [] [] []
  mappend = (<>)

data YearlyChoices = YearlyChoices
  { yearlyChoicesYearlyFee :: Maybe Amount,
    yearlyChoicesTransportInstalments :: Map (Entity Child) TransportInstallmentChoices,
    yearlyChoicesSemestrelyMap :: Map (Entity DaycareSemestre) SemestrelyChoices
  }
  deriving (Show, Eq, Generic)

instance Validity YearlyChoices where
  validate yc@YearlyChoices {..} =
    mconcat
      [ genericValidate yc,
        declare "The amount is not zero" $ maybe True (/= zeroAmount) yearlyChoicesYearlyFee,
        declare "The amount is positive" $ maybe True (>= zeroAmount) yearlyChoicesYearlyFee,
        declare "None of the semestrely choices are empty"
          $ notElem mempty yearlyChoicesSemestrelyMap
      ]

instance NFData YearlyChoices

instance FromJSON YearlyChoices

instance ToJSON YearlyChoices

instance Semigroup YearlyChoices where
  (<>) yc1 yc2 =
    YearlyChoices
      { yearlyChoicesYearlyFee = yearlyChoicesYearlyFee yc1 <|> yearlyChoicesYearlyFee yc2,
        yearlyChoicesTransportInstalments =
          M.union (yearlyChoicesTransportInstalments yc1) (yearlyChoicesTransportInstalments yc2),
        yearlyChoicesSemestrelyMap =
          M.union (yearlyChoicesSemestrelyMap yc1) (yearlyChoicesSemestrelyMap yc2)
      }

instance Monoid YearlyChoices where
  mempty = YearlyChoices Nothing M.empty M.empty
  mappend = (<>)

data SemestrelyChoices = SemestrelyChoices
  { semestrelyChoicesDaycareChoices :: [DaycareChoice],
    semestrelyChoicesDaycareActivityChoices :: [DaycareActivityChoice]
  }
  deriving (Show, Eq, Generic)

instance Validity SemestrelyChoices

instance FromJSON SemestrelyChoices

instance ToJSON SemestrelyChoices

instance Semigroup SemestrelyChoices where
  (<>) sc1 sc2 =
    SemestrelyChoices
      { semestrelyChoicesDaycareChoices =
          semestrelyChoicesDaycareChoices sc1 <> semestrelyChoicesDaycareChoices sc2,
        semestrelyChoicesDaycareActivityChoices =
          semestrelyChoicesDaycareActivityChoices sc1 <> semestrelyChoicesDaycareActivityChoices sc2
      }

instance Monoid SemestrelyChoices where
  mempty = SemestrelyChoices [] []
  mappend = (<>)

instance NFData SemestrelyChoices

data TransportInstallmentChoices = TransportInstallmentChoices
  { transportInstallmentChoicesInstalments :: NonEmpty Amount,
    transportInstallmentChoicesTransportSignup :: Entity TransportSignup,
    transportInstallmentChoicesTransportPaymentPlan :: Either (Entity TransportPaymentPlan) TransportPaymentPlan
  }
  deriving (Show, Eq, Generic)

instance Validity TransportInstallmentChoices where
  validate tic@TransportInstallmentChoices {..} =
    mconcat
      [ genericValidate tic,
        decorate "NonEmpty Amount"
          $ decorateList (NE.toList transportInstallmentChoicesInstalments)
          $ \a ->
            mconcat
              [ declare "The amount is not zero" $ a /= zeroAmount,
                declare "The amount is positive" $ a >= zeroAmount
              ]
      ]

instance NFData TransportInstallmentChoices

instance FromJSON TransportInstallmentChoices

instance ToJSON TransportInstallmentChoices

data OccasionalTransportChoice = OccasionalTransportChoice
  { occasionalTransportChoiceChild :: Entity Child,
    occasionalTransportChoiceSignup :: Entity OccasionalTransportSignup,
    occasionalTransportChoiceAmount :: Amount
  }
  deriving (Show, Eq, Generic)

instance Validity OccasionalTransportChoice where
  validate otc@(OccasionalTransportChoice ec eots a) =
    mconcat
      [ genericValidate otc,
        declare "The transport signup refers to the child"
          $ occasionalTransportSignupChild (entityVal eots)
          == entityKey ec,
        validatePositiveAmount a
      ]

instance NFData OccasionalTransportChoice

instance ToJSON OccasionalTransportChoice

instance FromJSON OccasionalTransportChoice

data DaycareChoice = DaycareChoice
  { daycareChoiceChild :: Entity Child,
    daycareChoiceTimeslot :: Entity DaycareTimeslot,
    daycareChoiceSignup :: Entity DaycareSignup,
    daycareChoiceAmount :: Amount
  }
  deriving (Show, Eq, Generic)

instance Validity DaycareChoice where
  validate dc@DaycareChoice {..} =
    mconcat [genericValidate dc, validatePositiveAmount daycareChoiceAmount]

-- TODO timeslot and signup are valid together, and the amount is valid
instance NFData DaycareChoice

instance FromJSON DaycareChoice

instance ToJSON DaycareChoice

data DaycareActivityChoice = DaycareActivityChoice
  { daycareActivityChoiceChild :: Entity Child,
    daycareActivityChoiceActivity :: Entity DaycareActivity,
    daycareActivityChoiceSignup :: Entity DaycareActivitySignup,
    daycareActivityChoiceAmount :: Amount
  }
  deriving (Show, Eq, Generic)

instance Validity DaycareActivityChoice where
  validate dc@DaycareActivityChoice {..} =
    mconcat [genericValidate dc, validatePositiveAmount daycareActivityChoiceAmount]

-- TODO activity and signup are valid together, and the amount is valid
instance NFData DaycareActivityChoice

instance FromJSON DaycareActivityChoice

instance ToJSON DaycareActivityChoice

data OccasionalDaycareChoice = OccasionalDaycareChoice
  { occasionalDaycareChoiceChild :: Entity Child,
    occasionalDaycareChoiceSignup :: Entity OccasionalDaycareSignup,
    occasionalDaycareChoiceAmount :: Amount
  }
  deriving (Show, Eq, Generic)

instance Validity OccasionalDaycareChoice where
  validate otc@(OccasionalDaycareChoice ec eots a) =
    mconcat
      [ genericValidate otc,
        declare "The daycare signup refers to the child"
          $ occasionalDaycareSignupChild (entityVal eots)
          == entityKey ec,
        validatePositiveAmount a
      ]

instance NFData OccasionalDaycareChoice

instance ToJSON OccasionalDaycareChoice

instance FromJSON OccasionalDaycareChoice

data CustomActivityChoice = CustomActivityChoice
  { customActivityChoiceChild :: Entity Child,
    customActivityChoiceSignup :: Entity CustomActivitySignup,
    customActivityChoiceActivity :: Entity CustomActivity
  }
  deriving (Show, Eq, Generic)

instance Validity CustomActivityChoice where
  validate cac@(CustomActivityChoice ec ecas eca) =
    mconcat
      [ genericValidate cac,
        declare "The custom activity signup refers to the child"
          $ customActivitySignupChild (entityVal ecas)
          == entityKey ec,
        declare "The custom activity signup refers to the custom activity"
          $ customActivitySignupActivity (entityVal ecas)
          == entityKey eca,
        validatePositiveAmount $ customActivityFee $ entityVal eca
      ]

instance NFData CustomActivityChoice

instance ToJSON CustomActivityChoice

instance FromJSON CustomActivityChoice

deriveChoices :: CalculatedCosts -> Choices
deriveChoices CalculatedCosts {..} =
  Choices
    { choicesYearlyMap = M.filter (/= mempty) $ M.map deriveYearlyChoices calculatedCostsMap,
      choicesOccasionalTransport =
        deriveOccasionalTransportChoices calculatedCostsOccasionalTransport,
      choicesOccasionalDaycare = deriveOccasionalDaycareChoices calculatedCostsOccasionalDaycare,
      choicesCustomActivity = deriveCustomActivityChoices calculatedCostsCustomActivity,
      choicesExtraCharges = calculatedCostsExtraCharges,
      choicesDiscounts = calculatedCostsDiscounts
    }

deriveYearlyChoices :: CalculatedYearlyCosts -> YearlyChoices
deriveYearlyChoices CalculatedYearlyCosts {..} =
  YearlyChoices
    { yearlyChoicesYearlyFee =
        case cycYearly of
          YearlyFeeNotPaid a -> Just a
          _ -> Nothing,
      yearlyChoicesTransportInstalments =
        flip M.mapMaybe cycBus $ \case
          TransportNotSignedUp -> Nothing
          TransportPaymentNotStartedButNecessary ets ttp as ->
            Just
              TransportInstallmentChoices
                { transportInstallmentChoicesInstalments = as,
                  transportInstallmentChoicesTransportSignup = ets,
                  transportInstallmentChoicesTransportPaymentPlan = Right ttp
                }
          TransportPaymentStartedButNotDone ets _ ettp _ _ as ->
            Just
              TransportInstallmentChoices
                { transportInstallmentChoicesInstalments = as,
                  transportInstallmentChoicesTransportSignup = ets,
                  transportInstallmentChoicesTransportPaymentPlan = Left ettp
                }
          TransportPaymentEntirelyDone {} -> Nothing
          TransportPaymentNotRequiredBecauseOfAmount _ -> Nothing
          TransportPaymentNotRequiredBecauseOfParents _ -> Nothing,
      yearlyChoicesSemestrelyMap =
        M.filter (/= mempty) $ M.map deriveSemestrelyChoices cycSemestrely
    }

deriveSemestrelyChoices :: CalculatedSemestrelyCosts -> SemestrelyChoices
deriveSemestrelyChoices CalculatedSemestrelyCosts {..} =
  SemestrelyChoices
    { semestrelyChoicesDaycareChoices = mapMaybe deriveDaycareChoice cscDaycarePayments,
      semestrelyChoicesDaycareActivityChoices =
        mapMaybe deriveDaycareActivityChoice cscDaycareActivityPayments
    }

deriveDaycareChoice :: DaycarePaymentStatus -> Maybe DaycareChoice
deriveDaycareChoice (DaycarePaymentUnpaid ec edcts eds a) =
  Just
    DaycareChoice
      { daycareChoiceChild = ec,
        daycareChoiceTimeslot = edcts,
        daycareChoiceSignup = eds,
        daycareChoiceAmount = a
      }
deriveDaycareChoice DaycarePaymentPaid {} = Nothing

deriveDaycareActivityChoice :: DaycareActivityPaymentStatus -> Maybe DaycareActivityChoice
deriveDaycareActivityChoice (DaycareActivityPaymentUnpaid ec edcts eds a) =
  Just
    DaycareActivityChoice
      { daycareActivityChoiceChild = ec,
        daycareActivityChoiceActivity = edcts,
        daycareActivityChoiceSignup = eds,
        daycareActivityChoiceAmount = a
      }
deriveDaycareActivityChoice DaycareActivityPaymentPaid {} = Nothing

deriveOccasionalTransportChoices ::
  [OccasionalTransportPaymentStatus] -> [OccasionalTransportChoice]
deriveOccasionalTransportChoices =
  mapMaybe $ \case
    OccasionalTransportPaymentUnpaid ec e a -> Just $ OccasionalTransportChoice ec e a
    _ -> Nothing

deriveOccasionalDaycareChoices :: [OccasionalDaycarePaymentStatus] -> [OccasionalDaycareChoice]
deriveOccasionalDaycareChoices =
  mapMaybe $ \case
    OccasionalDaycarePaymentUnpaid ec e a -> Just $ OccasionalDaycareChoice ec e a
    _ -> Nothing

deriveCustomActivityChoices :: [CustomActivityPaymentStatus] -> [CustomActivityChoice]
deriveCustomActivityChoices =
  mapMaybe $ \case
    CustomActivityPaymentUnpaid ec ecas eca -> Just $ CustomActivityChoice ec ecas eca
    _ -> Nothing

trimChoices :: Choices -> Choices
trimChoices cs = cs {choicesYearlyMap = M.map trimYearlyChoices $ choicesYearlyMap cs}

trimYearlyChoices :: YearlyChoices -> YearlyChoices
trimYearlyChoices YearlyChoices {..} =
  YearlyChoices
    { yearlyChoicesYearlyFee = yearlyChoicesYearlyFee,
      yearlyChoicesTransportInstalments =
        M.map trimTransportInstallmentChoices yearlyChoicesTransportInstalments,
      yearlyChoicesSemestrelyMap = M.map trimSemestrelyChoices yearlyChoicesSemestrelyMap
    }

trimTransportInstallmentChoices :: TransportInstallmentChoices -> TransportInstallmentChoices
trimTransportInstallmentChoices tis =
  tis
    { transportInstallmentChoicesInstalments =
        case transportInstallmentChoicesInstalments tis of
          (a :| _) -> a :| []
    }

trimSemestrelyChoices :: SemestrelyChoices -> SemestrelyChoices
trimSemestrelyChoices = id
