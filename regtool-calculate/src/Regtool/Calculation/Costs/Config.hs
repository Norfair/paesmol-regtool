{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Calculation.Costs.Config where

import Data.Aeson
import Import
import Regtool.Data

data CostsConfig = CostsConfig
  { costsConfigAnnualFee :: Amount,
    costsConfigBusYearly :: [Amount],
    costsConfigBusYearlyDefault :: Amount,
    costsConfigOccasionalBus :: Amount
  }
  deriving (Show, Eq, Generic)

instance Validity CostsConfig where
  validate CostsConfig {..} =
    mconcat
      [ delve "costsConfigAnnualFee" costsConfigAnnualFee,
        delve "costsConfigBusYearly" costsConfigBusYearly,
        delve "costsConfigBusYearlyDefault" costsConfigBusYearlyDefault,
        decorate "costsConfigAnnualFee" $ validatePositiveAmount costsConfigAnnualFee,
        decorate "costsConfigBusYearly" $ decorateList costsConfigBusYearly validatePositiveAmount,
        decorate "costsConfigBusYearlyDefault" $ validatePositiveAmount costsConfigBusYearlyDefault,
        decorate "costsConfigOccasionalBus" $ validatePositiveAmount costsConfigOccasionalBus,
        declare "The amounts are decreasing"
          $ case costsConfigBusYearly of
            [] -> costsConfigBusYearlyDefault == zeroAmount
            _ -> decreasing $ costsConfigBusYearly ++ [costsConfigBusYearlyDefault]
      ]

instance NFData CostsConfig

decreasing :: (Ord a) => [a] -> Bool
decreasing [] = True
decreasing [_] = True
decreasing (a1 : a2 : rest) = a1 >= a2 && decreasing (a2 : rest)

instance FromJSON CostsConfig where
  parseJSON =
    withObject "CostsConfig" $ \o ->
      CostsConfig
        <$> o
        .: "annual membership fee"
        <*> o
        .: "bus yearly"
        <*> o
        .: "bus yearly default"
        <*> o
        .: "occasional bus fee"

instance ToJSON CostsConfig where
  toJSON CostsConfig {..} =
    object
      [ "annual membership fee" .= costsConfigAnnualFee,
        "bus yearly" .= costsConfigBusYearly,
        "bus yearly default" .= costsConfigBusYearlyDefault,
        "occasional bus fee" .= costsConfigOccasionalBus
      ]

defaultCostsConfig :: CostsConfig
defaultCostsConfig =
  CostsConfig
    { costsConfigAnnualFee = unsafeAmount 30,
      costsConfigBusYearly = map unsafeAmount [1600, 1200, 800],
      costsConfigBusYearlyDefault = zeroAmount,
      costsConfigOccasionalBus = unsafeAmount 15
    }
