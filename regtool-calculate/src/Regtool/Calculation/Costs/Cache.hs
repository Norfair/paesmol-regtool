{-# LANGUAGE RecordWildCards #-}

module Regtool.Calculation.Costs.Cache
  ( startShoppingCart,
    getShoppingCartOfAccount,
    decodeShoppingCart,
  )
where

import Data.Aeson as JSON
import qualified Data.ByteString.Lazy as LB
import Database.Persist
import Database.Persist.Sql
import Import
import Regtool.Calculation.Costs.Choice
import Regtool.Data

startShoppingCart :: (MonadIO m) => AccountId -> Choices -> SqlPersistT m ShoppingCartId
startShoppingCart aid choices = do
  now <- liftIO getCurrentTime
  let bs = LB.toStrict $ JSON.encode choices
  insertValid
    $ ShoppingCart
      { shoppingCartAccount = aid,
        shoppingCartJson = bs,
        shoppingCartSession = Nothing,
        shoppingCartCreated = Just now,
        shoppingCartStatus = Just CartChosen
      }

getShoppingCartOfAccount ::
  (MonadIO m) => AccountId -> ShoppingCartId -> SqlPersistT m (Maybe (ShoppingCart, Choices))
getShoppingCartOfAccount aid cid = do
  mc <- selectFirst [ShoppingCartAccount ==. aid, ShoppingCartId ==. cid] []
  pure $ mc >>= (\c -> (,) (entityVal c) <$> decodeShoppingCart (entityVal c))

decodeShoppingCart :: ShoppingCart -> Maybe Choices
decodeShoppingCart ShoppingCart {..} = JSON.decode $ LB.fromStrict shoppingCartJson
