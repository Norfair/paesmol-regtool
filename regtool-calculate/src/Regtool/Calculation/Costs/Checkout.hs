{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Calculation.Costs.Checkout where

import qualified Data.Map as M
import Import
import Regtool.Calculation.Costs.Input
import Regtool.Data

newtype CheckoutsOverview = CheckoutsOverview
  { checkoutsOverviewCheckouts :: Map CheckoutId CheckoutOverview
  }
  deriving (Show, Generic)

instance Validity CheckoutsOverview

instance NFData CheckoutsOverview

data CheckoutOverview = CheckoutOverview
  { checkoutOverviewCheckout :: !Checkout,
    checkoutOverviewPayment :: !(Maybe (Entity StripePayment)),
    checkoutOverviewYearlyFeePayments :: ![Entity YearlyFeePayment],
    checkoutOverviewTransportPayments :: ![Entity TransportPayment],
    checkoutOverviewOccasionalTransportPayments :: ![Entity OccasionalTransportPayment],
    checkoutOverviewDaycarePayments :: ![Entity DaycarePayment],
    checkoutOverviewDaycareActivityPayments :: ![Entity DaycareActivityPayment],
    checkoutOverviewOccasionalDaycarePayments :: ![Entity OccasionalDaycarePayment],
    checkoutOverviewCustomActivityPayments :: ![Entity CustomActivityPayment],
    checkoutOverviewExtraCharges :: ![Entity ExtraCharge],
    checkoutOverviewDiscounts :: ![Entity Discount]
  }
  deriving (Show, Generic)

instance Validity CheckoutOverview

instance NFData CheckoutOverview

deriveCheckoutsOverview :: CostsInput -> CheckoutsOverview
deriveCheckoutsOverview ci@CostsInput {..} =
  CheckoutsOverview
    { checkoutsOverviewCheckouts =
        M.fromList
          $ flip map costsInputCheckouts
          $ \(Entity cid co) -> (cid, deriveCheckoutOverview ci cid co)
    }

deriveCheckoutOverview :: CostsInput -> CheckoutId -> Checkout -> CheckoutOverview
deriveCheckoutOverview CostsInput {..} cid checkoutOverviewCheckout@Checkout {..} =
  let checkoutOverviewPayment = do
        pid <- checkoutPayment
        find ((== pid) . entityKey) costsInputStripePayments
      checkoutOverviewYearlyFeePayments =
        filter ((== cid) . yearlyFeePaymentCheckout . entityVal) costsInputYearlyFeePayments
      checkoutOverviewTransportPayments =
        filter ((== cid) . transportPaymentCheckout . entityVal) costsInputTransportPayments
      checkoutOverviewOccasionalTransportPayments =
        filter
          ((== cid) . occasionalTransportPaymentCheckout . entityVal)
          costsInputOccasionalTransportPayments
      checkoutOverviewDaycarePayments =
        filter ((== cid) . daycarePaymentCheckout . entityVal) costsInputDaycarePayments
      checkoutOverviewDaycareActivityPayments =
        filter
          ((== cid) . daycareActivityPaymentCheckout . entityVal)
          costsInputDaycareActivityPayments
      checkoutOverviewOccasionalDaycarePayments =
        filter
          ((== cid) . occasionalDaycarePaymentCheckout . entityVal)
          costsInputOccasionalDaycarePayments
      checkoutOverviewCustomActivityPayments =
        filter
          ((== cid) . customActivityPaymentCheckout . entityVal)
          costsInputCustomActivityPayments
      checkoutOverviewExtraCharges =
        filter ((== Just cid) . extraChargeCheckout . entityVal) costsInputExtraCharges
      checkoutOverviewDiscounts =
        filter ((== Just cid) . discountCheckout . entityVal) costsInputDiscounts
      coo = CheckoutOverview {..}
   in coo
