{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Calculation.Costs.Input where

import qualified Database.Esqueleto.Legacy as E
import Database.Persist.Sqlite
import Import
import Regtool.Calculation.Children
import Regtool.Calculation.Parents
import Regtool.Data

{-# ANN module ("HLint: ignore Avoid lambda" :: String) #-}

{-# ANN module ("HLint: ignore Redundant if" :: String) #-}

getCostsInputForAccount :: (MonadIO m) => AccountId -> ReaderT SqlBackend m CostsInput
getCostsInputForAccount costsInputAccount = do
  costsInputParents <- getParentsOf costsInputAccount
  costsInputChildren <- getChildrenOf costsInputAccount
  costsInputBusSchoolYears <- selectList [] [Asc BusSchoolYearYear]
  costsInputTransportSignups <-
    selectList [TransportSignupChild <-. map entityKey costsInputChildren] []
  costsInputStripeCustomers <-
    selectList [StripeCustomerAccount ==. costsInputAccount] [Desc StripeCustomerAccount]
  costsInputDiscounts <- selectList [DiscountAccount ==. costsInputAccount] []
  costsInputExtraCharges <- selectList [ExtraChargeAccount ==. costsInputAccount] []
  costsInputCheckouts <- selectList [CheckoutAccount ==. costsInputAccount] []
  costsInputStripePayments <-
    selectList
      [StripePaymentCustomer <-. map entityKey costsInputStripeCustomers]
      [Asc StripePaymentTimestamp]
  costsInputYearlyFeePayments <-
    selectList
      ( [YearlyFeePaymentAccount ==. costsInputAccount]
          ||. [YearlyFeePaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputTransportEnrollments <-
    selectList
      [TransportEnrollmentSignup <-. map entityKey costsInputTransportSignups]
      [Asc TransportEnrollmentId]
  costsInputTransportPaymentPlans <-
    selectList
      [ TransportPaymentPlanId
          <-. map (transportEnrollmentPaymentPlan . entityVal) costsInputTransportEnrollments
      ]
      [Asc TransportPaymentPlanId]
  costsInputTransportPayments <-
    selectList
      ( [TransportPaymentPaymentPlan <-. map entityKey costsInputTransportPaymentPlans]
          ||. [TransportPaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputOccasionalTransportSignups <-
    selectList [OccasionalTransportSignupChild <-. map entityKey costsInputChildren] []
  costsInputOccasionalTransportPayments <-
    selectList
      ( [OccasionalTransportPaymentSignup <-. map entityKey costsInputOccasionalTransportSignups]
          ||. [OccasionalTransportPaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputDaycareSemestres <- selectList [] []
  costsInputDaycareTimeslots <- selectList [] []
  costsInputDaycareSignups <-
    selectList [DaycareSignupChild <-. map entityKey costsInputChildren] []
  costsInputDaycarePayments <-
    selectList
      ( [DaycarePaymentSignup <-. map entityKey costsInputDaycareSignups]
          ||. [DaycarePaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputOccasionalDaycareSignups <-
    selectList [OccasionalDaycareSignupChild <-. map entityKey costsInputChildren] []
  costsInputOccasionalDaycarePayments <-
    selectList
      ( [OccasionalDaycarePaymentSignup <-. map entityKey costsInputOccasionalDaycareSignups]
          ||. [OccasionalDaycarePaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputDaycareActivities <- selectList [] []
  costsInputDaycareActivitySignups <-
    selectList [DaycareActivitySignupChild <-. map entityKey costsInputChildren] []
  costsInputDaycareActivityPayments <-
    selectList
      ( [DaycareActivityPaymentSignup <-. map entityKey costsInputDaycareActivitySignups]
          ||. [DaycareActivityPaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  costsInputCustomActivities <- selectList [] []
  costsInputCustomActivitySignups <-
    selectList [CustomActivitySignupChild <-. map entityKey costsInputChildren] []
  costsInputCustomActivityPayments <-
    selectList
      ( [CustomActivityPaymentSignup <-. map entityKey costsInputCustomActivitySignups]
          ||. [CustomActivityPaymentCheckout <-. map entityKey costsInputCheckouts]
      )
      []
  pure CostsInput {..}

data CostsInput = CostsInput
  { costsInputAccount :: AccountId,
    costsInputParents :: [Entity Parent],
    costsInputChildren :: [Entity Child],
    costsInputBusSchoolYears :: [Entity BusSchoolYear],
    costsInputYearlyFeePayments :: [Entity YearlyFeePayment],
    costsInputTransportSignups :: [Entity TransportSignup],
    costsInputTransportPaymentPlans :: [Entity TransportPaymentPlan],
    costsInputTransportPayments :: [Entity TransportPayment],
    costsInputTransportEnrollments :: [Entity TransportEnrollment],
    costsInputOccasionalTransportSignups :: [Entity OccasionalTransportSignup],
    costsInputOccasionalTransportPayments :: [Entity OccasionalTransportPayment],
    costsInputDaycareSemestres :: [Entity DaycareSemestre],
    costsInputDaycareTimeslots :: [Entity DaycareTimeslot],
    costsInputDaycareSignups :: [Entity DaycareSignup],
    costsInputDaycarePayments :: [Entity DaycarePayment],
    costsInputOccasionalDaycareSignups :: [Entity OccasionalDaycareSignup],
    costsInputOccasionalDaycarePayments :: [Entity OccasionalDaycarePayment],
    costsInputDaycareActivities :: [Entity DaycareActivity],
    costsInputDaycareActivitySignups :: [Entity DaycareActivitySignup],
    costsInputDaycareActivityPayments :: [Entity DaycareActivityPayment],
    costsInputCustomActivities :: [Entity CustomActivity],
    costsInputCustomActivitySignups :: [Entity CustomActivitySignup],
    costsInputCustomActivityPayments :: [Entity CustomActivityPayment],
    costsInputExtraCharges :: [Entity ExtraCharge],
    costsInputDiscounts :: [Entity Discount],
    costsInputCheckouts :: [Entity Checkout],
    costsInputStripePayments :: [Entity StripePayment]
  }
  deriving (Show, Generic)

instance Validity CostsInput where
  validate ci@CostsInput {..} =
    mconcat
      -- Each sub-part is valid.
      [ genericValidate ci,
        -- Each list of entities has entities with seperate id's.
        check (seperateIds costsInputParents) "Parents have seperate IDs",
        check (seperateIds costsInputChildren) "Children have seperate IDs",
        check (seperateIds costsInputBusSchoolYears) "Bus schoolyears have seperate IDs",
        check (seperateIds costsInputTransportSignups) "Transport Signups have seperate IDs",
        check (seperateIds costsInputCheckouts) "Checkouts have seperate IDs",
        check (seperateIds costsInputStripePayments) "StripePayments Signups have seperate IDs",
        check (seperateIds costsInputYearlyFeePayments) "Yearly Fee Payments have seperate IDs",
        check
          (seperateIds costsInputTransportPaymentPlans)
          "Transport Payment Plans have seperate IDs",
        check (seperateIds costsInputTransportPayments) "Transport Payments have seperate IDs",
        check (seperateIds costsInputTransportEnrollments) "Transport Enrollments have seperate IDs",
        check
          (seperateIds costsInputOccasionalTransportSignups)
          "Occasional Transport Signups have seperate IDs",
        check
          (seperateIds costsInputOccasionalTransportPayments)
          "Occasional Transport Payments have seperate IDs",
        check (seperateIds costsInputDaycareSemestres) "DaycareSemestres have seperate IDs",
        check (seperateIds costsInputDaycareTimeslots) "Daycare Timeslots have seperate IDs",
        check (seperateIds costsInputDaycareSignups) "Daycare Signups have seperate IDs",
        check (seperateIds costsInputDaycarePayments) "Daycare Payments have seperate IDs",
        check
          (seperateIds costsInputOccasionalDaycareSignups)
          "Occasional Daycare Signups have seperate IDs",
        check
          (seperateIds costsInputOccasionalDaycarePayments)
          "Occasional Daycare Payments have seperate IDs",
        check (seperateIds costsInputDaycareActivities) "Daycare Activities have seperate IDs",
        check
          (seperateIds costsInputDaycareActivitySignups)
          "Daycare activity Signups have seperate IDs",
        check
          (seperateIds costsInputDaycareActivityPayments)
          "Daycare Activity Payments have seperate IDs",
        check (seperateIds costsInputCustomActivities) "Custom Activities have seperate IDs",
        check
          (seperateIds costsInputCustomActivitySignups)
          "Custom activity Signups have seperate IDs",
        check
          (seperateIds costsInputCustomActivityPayments)
          "Custom Activity Payments have seperate IDs",
        check (seperateIds costsInputExtraCharges) "ExtraCharges have seperate IDs",
        check (seperateIds costsInputDiscounts) "Discounts have seperate IDs",
        declareUniquenessConstraint
          "There is a maximum of one bus school year per year"
          costsInputBusSchoolYears
          (\(Entity _ BusSchoolYear {..}) -> busSchoolYearYear),
        -- All of the foreign keys are internal.
        declareForeignKeyConstraint
          "The parents belong to the account"
          costsInputParents
          parentAccount
          costsInputAccount,
        declareForeignKeysConstraint
          "The signups belong to the children"
          costsInputTransportSignups
          transportSignupChild
          costsInputChildren,
        declareUniquenessConstraint
          "There is a maximum of one signup per child, per year, per admin deleted status"
          costsInputTransportSignups
          ( \(Entity _ TransportSignup {..}) ->
              (transportSignupChild, transportSignupSchoolYear, transportSignupDeletedByAdmin)
          ),
        declareUniquenessConstraint
          "There is a maximum of one stripe payment per charge"
          costsInputStripePayments
          (\(Entity _ StripePayment {..}) -> stripePaymentCharge),
        declareUniquenessConstraint
          "There is a maximum of one yearly fee payment per account per schoolyear"
          costsInputYearlyFeePayments
          (\(Entity _ YearlyFeePayment {..}) -> (costsInputAccount, yearlyFeePaymentSchoolyear)),
        declareForeignKeyConstraint
          "The yearly fee payments belong to the account"
          costsInputYearlyFeePayments
          yearlyFeePaymentAccount
          costsInputAccount,
        declareForeignKeysConstraint
          "The yearly fee payments use the checkouts"
          costsInputYearlyFeePayments
          yearlyFeePaymentCheckout
          costsInputCheckouts,
        declareForeignKeysConstraint
          "The transport payments belong to the payment plans"
          costsInputTransportPayments
          transportPaymentPaymentPlan
          costsInputTransportPaymentPlans,
        declareForeignKeysConstraint
          "The transport payments belong to checkouts"
          costsInputTransportPayments
          transportPaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one enrollment per signup"
          costsInputTransportEnrollments
          (\(Entity _ TransportEnrollment {..}) -> transportEnrollmentSignup),
        declareForeignKeysConstraint
          "The transport enrollments belong to the signups"
          costsInputTransportEnrollments
          transportEnrollmentSignup
          costsInputTransportSignups,
        declareForeignKeysConstraint
          "The transport enrollments belong to the payment plans"
          costsInputTransportEnrollments
          transportEnrollmentPaymentPlan
          costsInputTransportPaymentPlans,
        declareForeignKeysConstraint
          "The occasional transport signups belong to the children"
          costsInputOccasionalTransportSignups
          occasionalTransportSignupChild
          costsInputChildren,
        declareUniquenessConstraint
          "There is a maximum of one occasional transport signup per child per day"
          costsInputOccasionalTransportSignups
          ( \(Entity _ OccasionalTransportSignup {..}) ->
              ( occasionalTransportSignupChild,
                occasionalTransportSignupDay,
                occasionalTransportSignupDirection
              )
          ),
        declareForeignKeysConstraint
          "The occasional transport payments belong to occasional transport signups"
          costsInputOccasionalTransportPayments
          occasionalTransportPaymentSignup
          costsInputOccasionalTransportSignups,
        declareForeignKeysConstraint
          "The occasional transport payments belong to the checkouts"
          costsInputOccasionalTransportPayments
          occasionalTransportPaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one occasional transport payment per occasional transport signup"
          costsInputOccasionalTransportPayments
          (\(Entity _ OccasionalTransportPayment {..}) -> occasionalTransportPaymentSignup),
        declareUniquenessConstraint
          "There is a maximum of one daycare semestre per year per semestre"
          costsInputDaycareSemestres
          (\(Entity _ DaycareSemestre {..}) -> (daycareSemestreYear, daycareSemestreSemestre)),
        declareForeignKeysConstraint
          "The daycare timeslots belong to the semestres"
          costsInputDaycareTimeslots
          daycareTimeslotSemestre
          costsInputDaycareSemestres,
        declareForeignKeysConstraint
          "The daycare signups belong to the children"
          costsInputDaycareSignups
          daycareSignupChild
          costsInputChildren,
        declareForeignKeysConstraint
          "The daycare signups belong to the timeslots"
          costsInputDaycareSignups
          daycareSignupTimeslot
          costsInputDaycareTimeslots,
        declareUniquenessConstraint
          "There is a maximum of one daycare signup per child per time slot"
          costsInputDaycareSignups
          ( \(Entity _ DaycareSignup {..}) ->
              ( daycareSignupChild,
                daycareSignupTimeslot,
                daycareSignupDeletedByAdmin
              )
          ),
        declareForeignKeysConstraint
          "The daycare payments belong to the signups"
          costsInputDaycarePayments
          daycarePaymentSignup
          costsInputDaycareSignups,
        declareForeignKeysConstraint
          "The daycare payments belong to the checkouts"
          costsInputDaycarePayments
          daycarePaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one daycare payment per checkout per daycare signup"
          costsInputDaycarePayments
          (\(Entity _ DaycarePayment {..}) -> (daycarePaymentSignup, daycarePaymentCheckout)),
        declareForeignKeysConstraint
          "The occasional daycare signups belong to the children"
          costsInputOccasionalDaycareSignups
          occasionalDaycareSignupChild
          costsInputChildren,
        declareUniquenessConstraint
          "There is a maximum of one occasional daycare signup per child per day"
          costsInputOccasionalDaycareSignups
          ( \(Entity _ OccasionalDaycareSignup {..}) ->
              ( occasionalDaycareSignupChild,
                occasionalDaycareSignupTimeslot,
                occasionalDaycareSignupDay,
                occasionalDaycareSignupDeletedByAdmin
              )
          ),
        declareForeignKeysConstraint
          "The occasional daycare payments belong to occasional daycare signups"
          costsInputOccasionalDaycarePayments
          occasionalDaycarePaymentSignup
          costsInputOccasionalDaycareSignups,
        declareForeignKeysConstraint
          "The occasional daycare payments belong to the checkouts"
          costsInputOccasionalDaycarePayments
          occasionalDaycarePaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one occasional daycare payment per occasional daycare signup"
          costsInputOccasionalDaycarePayments
          (\(Entity _ OccasionalDaycarePayment {..}) -> occasionalDaycarePaymentSignup),
        declareForeignKeysConstraint
          "The daycareActivities belong to the semestres"
          costsInputDaycareActivities
          daycareActivitySemestre
          costsInputDaycareSemestres,
        declareForeignKeysConstraint
          "The daycareActivities belong to the timeslots"
          costsInputDaycareActivities
          daycareActivityTimeslot
          costsInputDaycareTimeslots,
        declareForeignKeysConstraint
          "The daycareActivity signups belong to the children"
          costsInputDaycareActivitySignups
          daycareActivitySignupChild
          costsInputChildren,
        declareForeignKeysConstraint
          "The daycareActivity signups belong to the timeslots"
          costsInputDaycareActivitySignups
          daycareActivitySignupActivity
          costsInputDaycareActivities,
        declareUniquenessConstraint
          "There is a maximum of one daycareActivity signup per child per activity"
          costsInputDaycareActivitySignups
          ( \(Entity _ DaycareActivitySignup {..}) ->
              ( daycareActivitySignupChild,
                daycareActivitySignupActivity,
                daycareActivitySignupDeletedByAdmin
              )
          ),
        declareForeignKeysConstraint
          "The daycareActivity payments belong to the signups"
          costsInputDaycareActivityPayments
          daycareActivityPaymentSignup
          costsInputDaycareActivitySignups,
        declareForeignKeysConstraint
          "The daycareActivity payments belong to the checkouts"
          costsInputDaycareActivityPayments
          daycareActivityPaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one daycareActivity payment per checkout per daycareActivity signup"
          costsInputDaycareActivityPayments
          ( \(Entity _ DaycareActivityPayment {..}) ->
              (daycareActivityPaymentSignup, daycareActivityPaymentCheckout)
          ),
        declareForeignKeysConstraint
          "The customActivity signups belong to the children"
          costsInputCustomActivitySignups
          customActivitySignupChild
          costsInputChildren,
        declareUniquenessConstraint
          "There is a maximum of one customActivity signup per child per custom activity"
          costsInputCustomActivitySignups
          ( \(Entity _ CustomActivitySignup {..}) ->
              (customActivitySignupChild, customActivitySignupActivity)
          ),
        declareForeignKeysConstraint
          "The customActivity payments belong to the signups"
          costsInputCustomActivityPayments
          customActivityPaymentSignup
          costsInputCustomActivitySignups,
        declareForeignKeysConstraint
          "The customActivity payments belong to the checkouts"
          costsInputCustomActivityPayments
          customActivityPaymentCheckout
          costsInputCheckouts,
        declareUniquenessConstraint
          "There is a maximum of one customActivity payment per checkout per customActivity signup"
          costsInputCustomActivityPayments
          ( \(Entity _ CustomActivityPayment {..}) ->
              (customActivityPaymentSignup, customActivityPaymentCheckout)
          ),
        declareForeignKeyConstraint
          "The extra charges belong to the account"
          costsInputExtraCharges
          extraChargeAccount
          costsInputAccount,
        declareForeignKeyConstraint
          "The discounts belong to the account"
          costsInputDiscounts
          discountAccount
          costsInputAccount,
        declare "There is a maximum of one stripe payment per checkout"
          $ distinct
          $ mapMaybe (checkoutPayment . entityVal) costsInputCheckouts,
        declareForeignKeyConstraint
          "The checkouts belong to the account"
          costsInputCheckouts
          checkoutAccount
          costsInputAccount,
        declareForeignKeysConstraintM
          "The checkouts belong to the stripe payments"
          costsInputCheckouts
          checkoutPayment
          costsInputStripePayments,
        decorate "The checkouts make sense"
          $ validateCheckouts
            costsInputCheckouts
            costsInputStripePayments
            costsInputYearlyFeePayments
            costsInputTransportPayments
            costsInputOccasionalTransportPayments
            costsInputDaycarePayments
            costsInputOccasionalDaycarePayments
            costsInputDaycareActivityPayments
            costsInputCustomActivityPayments
            costsInputExtraCharges
            costsInputDiscounts
      ]

instance NFData CostsInput

validateCheckouts ::
  [Entity Checkout] ->
  [Entity StripePayment] ->
  [Entity YearlyFeePayment] ->
  [Entity TransportPayment] ->
  [Entity OccasionalTransportPayment] ->
  [Entity DaycarePayment] ->
  [Entity OccasionalDaycarePayment] ->
  [Entity DaycareActivityPayment] ->
  [Entity CustomActivityPayment] ->
  [Entity ExtraCharge] ->
  [Entity Discount] ->
  Validation
validateCheckouts checkouts stripePayments yearlyFeePayments transportPayments occasionalTransportPayments daycarePayments occasionalDaycarePayments daycareActivityPayments customActivityPayments extraCharges discounts =
  decorateList checkouts $ \(Entity cid Checkout {..}) ->
    mconcat
      [ declare
          "If the checkout amount is strictly positive, then the checkout has as stripe payment"
          $ if checkoutAmount > zeroAmount
            then isJust checkoutPayment
            else True,
        declare
          ( unwords
              [ "If the checkout amount is strictly negative, (it is",
                show checkoutAmount,
                ",) then the checkout has a discount"
              ]
          )
          $ if checkoutAmount < zeroAmount
            then any ((== Just cid) . discountCheckout . entityVal) discounts
            else True,
        declare "The stripe payment is in the list of stripe payments"
          $ maybe True (`elem` map entityKey stripePayments) checkoutPayment,
        declare "The stripe payment has the same amount as the checkout"
          $ case checkoutPayment of
            Nothing -> True
            Just pid ->
              case find ((== pid) . entityKey) stripePayments of
                Nothing -> False
                Just (Entity _ StripePayment {..}) -> stripePaymentAmount == checkoutAmount,
        let yfas =
              map yearlyFeePaymentAmount
                $ filter ((== cid) . yearlyFeePaymentCheckout)
                $ map entityVal yearlyFeePayments
            tpas =
              map transportPaymentAmount
                $ filter ((== cid) . transportPaymentCheckout)
                $ map entityVal transportPayments
            otpas =
              map occasionalTransportPaymentAmount
                $ filter ((== cid) . occasionalTransportPaymentCheckout)
                $ map entityVal occasionalTransportPayments
            dpas =
              map daycarePaymentAmount
                $ filter ((== cid) . daycarePaymentCheckout)
                $ map entityVal daycarePayments
            dapas =
              map daycareActivityPaymentAmount
                $ filter ((== cid) . daycareActivityPaymentCheckout)
                $ map entityVal daycareActivityPayments
            capas =
              map customActivityPaymentAmount
                $ filter ((== cid) . customActivityPaymentCheckout)
                $ map entityVal customActivityPayments
            odpas =
              map occasionalDaycarePaymentAmount
                $ filter ((== cid) . occasionalDaycarePaymentCheckout)
                $ map entityVal occasionalDaycarePayments
            ecs =
              map extraChargeAmount
                $ filter ((== Just cid) . extraChargeCheckout)
                $ map entityVal extraCharges
            das =
              map discountAmount
                $ filter ((== Just cid) . discountCheckout)
                $ map entityVal discounts
            gross = sumAmount (concat [yfas, tpas, otpas, dpas, odpas, dapas, capas, ecs])
            net = gross `subAmount` sumAmount das
            b = net == checkoutAmount
            showAmount = show . amountCents
            showAmounts = show . map amountCents
            explanation =
              unlines
                [ "The checkout id is",
                  show $ E.fromSqlKey cid,
                  "The yearly fee payments were",
                  showAmounts yfas,
                  "The transport payments were",
                  showAmounts tpas,
                  "The occasional transport payments were",
                  showAmounts otpas,
                  "The daycare payments were",
                  showAmounts dpas,
                  "The occasional daycare payments were",
                  showAmounts odpas,
                  "The daycare activity payments were",
                  showAmounts dapas,
                  "The extra charges were",
                  showAmounts ecs,
                  "The sum of those is",
                  showAmount gross,
                  "The discounts were",
                  showAmounts das,
                  "The sum minus the discounts",
                  showAmount net,
                  "The total amount listed in the checkout was",
                  showAmount checkoutAmount,
                  "The difference between the actual and the expected is",
                  showAmount $ checkoutAmount `subAmount` net,
                  if checkoutAmount > net
                    then "It looks like a payment has been deleted."
                    else "It looks like a signup has been deleted"
                ]
         in declare
              ( unlines
                  [ "The amounts of everything that went into the checkout sum to the checkout amount",
                    explanation
                  ]
              )
              b
      ]
