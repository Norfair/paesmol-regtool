module Regtool.Calculation.Checkout
  ( getStripeCustomersOf,
    getPaymentsOf,
  )
where

import Database.Persist.Sqlite
import Import
import Regtool.Data

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getStripeCustomersOf :: (MonadIO m) => AccountId -> ReaderT SqlBackend m [Entity StripeCustomer]
getStripeCustomersOf aid = selectList [StripeCustomerAccount ==. aid] [Desc StripeCustomerAccount]

getPaymentsOf :: (MonadIO m) => [StripeCustomerId] -> ReaderT SqlBackend m [Entity StripePayment]
getPaymentsOf scids = selectList [StripePaymentCustomer <-. scids] [Asc StripePaymentTimestamp]
