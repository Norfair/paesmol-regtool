{ mkDerivation, aeson, base, bytestring, containers, deepseq
, esqueleto, lib, mtl, path, path-io, persistent, persistent-sqlite
, pretty-show, regtool-data, safe, text, time, validity
, validity-bytestring, validity-containers, validity-path
, validity-text, validity-time
}:
mkDerivation {
  pname = "regtool-calculate";
  version = "0.0.0";
  src = ./.;
  libraryHaskellDepends = [
    aeson base bytestring containers deepseq esqueleto mtl path path-io
    persistent persistent-sqlite pretty-show regtool-data safe text
    time validity validity-bytestring validity-containers validity-path
    validity-text validity-time
  ];
  license = "unknown";
}
