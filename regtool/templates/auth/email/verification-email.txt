To verify your email address, please visit the following link:
@{AuthR $ verifyR verificationEmailTo verificationEmailKey}
We hope to see you soon.

PAESMOL

You got this email because you tried to register at @{HomeR}.
