{ mkDerivation, aeson, amazonka, amazonka-ses, autodocodec, base
, blaze-html, bytestring, cassava, containers, data-default
, deepseq, esqueleto, hjsmin, http-client, http-client-tls
, http-types, lens, lib, looper, monad-logger, mtl, necrork, nonce
, opt-env-conf, path, path-io, persistent, persistent-sqlite
, pretty-show, regtool-calculate, regtool-data
, regtool-stripe-client, resource-pool, resourcet, safe
, safe-coloured-text, shakespeare, template-haskell, text, time
, unliftio, validity, validity-bytestring, validity-containers
, validity-path, validity-text, validity-time, wai, wai-extra, warp
, yaml, yesod, yesod-auth, yesod-autoreload, yesod-core, yesod-form
, yesod-persistent, yesod-static, yesod-static-remote, zip-archive
}:
mkDerivation {
  pname = "regtool";
  version = "0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson amazonka amazonka-ses autodocodec base blaze-html bytestring
    cassava containers data-default deepseq esqueleto hjsmin
    http-client http-client-tls http-types lens looper monad-logger mtl
    necrork nonce opt-env-conf path path-io persistent
    persistent-sqlite pretty-show regtool-calculate regtool-data
    regtool-stripe-client resource-pool resourcet safe
    safe-coloured-text shakespeare template-haskell text time unliftio
    validity validity-bytestring validity-containers validity-path
    validity-text validity-time wai wai-extra warp yaml yesod
    yesod-auth yesod-autoreload yesod-core yesod-form yesod-persistent
    yesod-static yesod-static-remote zip-archive
  ];
  executableHaskellDepends = [ base ];
  license = "unknown";
  mainProgram = "regtool";
}
