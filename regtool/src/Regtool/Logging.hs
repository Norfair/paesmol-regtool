{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Logging
  ( runMyLoggingT,
    makeLogStr,
  )
where

import Autodocodec
import Control.Monad.Logger
import qualified Data.ByteString.Char8 as SB8
import qualified Data.Text as T
import OptEnvConf
import Regtool.Core.Constants
import System.IO (stderr)
import Text.Colour
import Prelude

runMyLoggingT :: LoggingT IO a -> IO a
runMyLoggingT func =
  if development
    then runLoggingT func developmentLogFunc
    else runStderrLoggingT func
  where
    developmentLogFunc loc source level msg =
      SB8.hPutStrLn stderr $ fromLogStr $ makeLogStr loc source level msg

makeLogStr :: Loc -> LogSource -> LogLevel -> LogStr -> LogStr
makeLogStr _ source level msg =
  mconcat
    [ toLogStr $
        renderChunksUtf8BSBuilder With24BitColours $
          map
            (logLevelColour level)
            [ "[",
              logLevelChunk level,
              if T.null source
                then ""
                else chunk $ "#" `mappend` source,
              "]"
            ],
      " ",
      msg
    ]

logLevelColour :: LogLevel -> (Chunk -> Chunk)
logLevelColour = \case
  LevelDebug -> fore white
  LevelInfo -> fore yellow
  LevelWarn -> fore orange
  LevelError -> fore red
  LevelOther _ -> id
  where
    orange = color256 214

logLevelChunk :: LogLevel -> Chunk
logLevelChunk = \case
  LevelDebug -> "DEBUG"
  LevelInfo -> "INFO"
  LevelWarn -> "WARNING"
  LevelError -> "ERROR"
  LevelOther t -> chunk t

instance HasParser LogLevel where
  settingsParser =
    setting
      [ help "Minimal severity of log messages",
        reader logLevelReader,
        value LevelInfo,
        name "log-level",
        metavar "LOG_LEVEL",
        example "Info"
      ]
    where
      logLevelReader = eitherReader $ \case
        "Debug" -> Right LevelDebug
        "Info" -> Right LevelInfo
        "Warn" -> Right LevelWarn
        "Error" -> Right LevelError
        s -> Left $ "Unknown LogLevel: " <> show s

instance HasCodec LogLevel where
  codec =
    stringConstCodec
      [ (LevelDebug, "Debug"),
        (LevelInfo, "Info"),
        (LevelWarn, "Warn"),
        (LevelError, "Error")
      ]
