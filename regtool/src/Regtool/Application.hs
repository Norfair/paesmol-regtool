{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Application where

import Regtool.Core.Foundation
import Regtool.Handler

mkYesodDispatch "Regtool" resourcesRegtool
