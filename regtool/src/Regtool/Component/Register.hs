{-# LANGUAGE TemplateHaskell #-}

module Regtool.Component.Register where

import Import
import Regtool.Component.NavigationBar
import Regtool.Core.Foundation

makeRegisterForm :: Text -> Route Regtool -> AForm Handler a -> Handler Html
makeRegisterForm name postRoute form = do
  (formWidget, formEnctype) <- generateFormPost $ renderCustomForm BootstrapBasicForm form
  withNavBar $(widgetFile "register")

processRegisterForm ::
  Text -> Route Regtool -> AForm Handler a -> (a -> Handler Html) -> Handler Html
processRegisterForm name postRoute form func = do
  ((result, formWidget), formEnctype) <- runFormPost $ renderCustomForm BootstrapBasicForm form
  case result of
    FormSuccess a -> func a
    fr -> withFormResultNavBar fr $(widgetFile "register")

makeEditForm :: Text -> Route Regtool -> Route Regtool -> AForm Handler a -> Handler Html
makeEditForm name postRoute backRoute form = do
  (formWidget, formEnctype) <- generateFormPost $ renderCustomForm BootstrapBasicForm form
  withNavBar $(widgetFile "edit")

processEditForm ::
  Text ->
  Route Regtool ->
  Route Regtool ->
  AForm Handler a ->
  (a -> Handler Html) ->
  Handler Html
processEditForm name postRoute backRoute form func = do
  ((result, formWidget), formEnctype) <- runFormPost $ renderCustomForm BootstrapBasicForm form
  case result of
    FormSuccess a -> func a
    fr -> withFormResultNavBar fr $(widgetFile "edit")
