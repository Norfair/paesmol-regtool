{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Component.NavigationBar where

import Import
import Regtool.Core.Foundation
import Regtool.Utils.Readiness

makeNavigationBarWidget :: Handler RegtoolWidget
makeNavigationBarWidget = do
  mAccount <- maybeAuth
  bar <- case mAccount of
    Nothing -> pure $(widgetFile "components/navigation-bar/public")
    Just accountEntity@(Entity aid Account {..}) -> do
      -- Decide what the user is ready to see
      Readiness {..} <- getReadiness accountEntity

      -- Figure out whether the user has any special permissions
      -- for which we should show them an admin panel
      mPermission <- runDB $ getBy $ UniquePermissionAccount aid
      let userHasAnyPermission = isJust mPermission

      -- Add a message if the children's levels need to be confirmed.
      when readyChildrenNeedLevelConfirmation $ do
        urlRenderer <- getUrlRenderParams
        addMessage
          "Please confirm your children's level"
          $ [hamlet|
              <p> Please confirm your children's level before proceeding
              <a .btn .btn-danger href=@{ChildrenConfirmLevelR}>Confirm children's level
              |]
            urlRenderer

      pure $(widgetFile "components/navigation-bar/protected")
  pure $(widgetFile "components/navigation-bar")

withFormResultNavBar :: FormResult a -> Widget -> Handler Html
withFormResultNavBar fr w =
  case fr of
    FormSuccess _ -> withNavBar w
    FormFailure ts -> withFormFailureNavBar ts w
    FormMissing -> withFormFailureNavBar ["Missing data"] w

withNavBar :: Widget -> Handler Html
withNavBar = withFormFailureNavBar []

withFormFailureNavBar :: [Text] -> Widget -> Handler Html
withFormFailureNavBar errs body = do
  msgs <- getMessages
  let errMsgs =
        Just $ forM_ errs $ \msg -> [shamlet|<div class="alert alert-danger" role="alert">#{msg}|]
  navigationBar <- makeNavigationBarWidget
  (cur, bs) <- breadcrumbs
  env <- regtoolEnvironment <$> getYesod
  defaultLayout $(widgetFile "with-nav-bar")
