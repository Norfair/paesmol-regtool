{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Utils.Stripe
  ( currency,
    runStripe,
    getStripeCustomer,
    GetStripeCustomerResult (..),
    stripeCreateSession,
    makeLineItemAmount,
    LineItemsResult (..),
  )
where

import qualified Data.Text as T
import Database.Persist
import Database.Persist.Sql
import Import
import Network.HTTP.Client as HTTP
import Network.HTTP.Types as HTTP
import Regtool.Calculation.Costs.Invoice
import Regtool.Core.Foundation
import qualified StripeClient as Stripe

currency :: Text
currency = "EUR"

toStripeAmount :: Amount -> Int
toStripeAmount = amountCents

runStripe :: Stripe.ClientT IO a -> Handler a
runStripe func = do
  mStripeSets <- getsYesod regtoolStripeSettings
  case mStripeSets of
    Nothing -> sendResponseStatus HTTP.status500 ("Cannot run stripe without stripe settings." :: Html)
    Just StripeSettings {..} -> do
      let config =
            Stripe.defaultConfiguration
              { Stripe.configSecurityScheme = Stripe.bearerAuthenticationSecurityScheme stripeSettingSecretKey
              }
      liftIO $ Stripe.runWithConfiguration config func

getStripeCustomer :: EmailAddress -> Handler GetStripeCustomerResult
getStripeCustomer emailAddress = do
  getCustomersResponse <-
    runStripe
      $ Stripe.getCustomers
      $ Stripe.mkGetCustomersParameters
        { Stripe.getCustomersParametersQueryEmail = Just $ emailAddressText emailAddress
        }
  case HTTP.responseBody getCustomersResponse of
    Stripe.GetCustomersResponseError err -> sendResponseStatus HTTP.status500 $ unlines ["Could not get customers:", err]
    Stripe.GetCustomersResponseDefault err -> sendResponseStatus HTTP.status500 $ unlines ["Could not get customers:", show err]
    Stripe.GetCustomersResponse200 body ->
      pure $ case Stripe.getCustomersResponseBody200Data body of
        [customer] -> GotStripeCustomer $ Stripe.customerId customer
        [] -> NoSuchStripeCustomer
        _ -> MoreThanOneStripeCustomer

data GetStripeCustomerResult
  = GotStripeCustomer Text
  | MoreThanOneStripeCustomer
  | NoSuchStripeCustomer

stripeCreateSession ::
  Route Regtool ->
  Route Regtool ->
  Amount ->
  Handler Text -- Session ID
stripeCreateSession successUrl cancelUrl amount = do
  Entity aid Account {..} <- requireAuth
  msc <- runDB $ selectFirst [StripeCustomerAccount ==. aid] [Desc StripeCustomerId]
  render <- getUrlRender
  let mStripeCustomerId = msc >>= stripeCustomerIdentifier . entityVal
  postCheckoutSessionsResponse <-
    runStripe
      $ Stripe.postCheckoutSessions
      $ (Stripe.mkPostCheckoutSessionsRequestBody (render cancelUrl) (render successUrl))
        { Stripe.postCheckoutSessionsRequestBodyClientReferenceId = Just $ T.pack $ show $ fromSqlKey aid,
          Stripe.postCheckoutSessionsRequestBodyCustomer = mStripeCustomerId,
          Stripe.postCheckoutSessionsRequestBodyCustomerEmail =
            -- We may only supply one of the customer and the customer email
            case mStripeCustomerId of
              Just _ -> Nothing
              Nothing -> Just $ emailAddressText accountEmailAddress,
          Stripe.postCheckoutSessionsRequestBodyPaymentMethodTypes =
            Just
              [ Stripe.PostCheckoutSessionsRequestBodyPaymentMethodTypes'EnumCard,
                Stripe.PostCheckoutSessionsRequestBodyPaymentMethodTypes'EnumBancontact,
                Stripe.PostCheckoutSessionsRequestBodyPaymentMethodTypes'EnumIdeal
              ],
          Stripe.postCheckoutSessionsRequestBodyLineItems =
            Just
              [ Stripe.mkPostCheckoutSessionsRequestBodyLineItems'
                  { Stripe.postCheckoutSessionsRequestBodyLineItems'Quantity = Just 1,
                    Stripe.postCheckoutSessionsRequestBodyLineItems'PriceData =
                      Just
                        $ (Stripe.mkPostCheckoutSessionsRequestBodyLineItems'PriceData' currency)
                          { Stripe.postCheckoutSessionsRequestBodyLineItems'PriceData'UnitAmount = Just $ toStripeAmount amount,
                            Stripe.postCheckoutSessionsRequestBodyLineItems'PriceData'ProductData =
                              Just
                                $ Stripe.mkPostCheckoutSessionsRequestBodyLineItems'PriceData'ProductData' "PAESMOL invoice"
                          }
                  }
              ],
          Stripe.postCheckoutSessionsRequestBodyExpand = Just ["customer", "payment_intent"],
          Stripe.postCheckoutSessionsRequestBodyMode = Just Stripe.PostCheckoutSessionsRequestBodyMode'EnumPayment
        }
  case HTTP.responseBody postCheckoutSessionsResponse of
    Stripe.PostCheckoutSessionsResponseError err -> sendResponseStatus HTTP.status500 $ unlines ["Could not create a checkout session:", err]
    Stripe.PostCheckoutSessionsResponseDefault err -> sendResponseStatus HTTP.status500 $ unlines ["Could not create a checkout session:", show err]
    Stripe.PostCheckoutSessionsResponse200 checkoutSession -> pure $ Stripe.checkout'sessionId checkoutSession

makeLineItemAmount :: Invoice -> LineItemsResult Amount -- Line items
makeLineItemAmount Invoice {..} =
  let costs = sumAmount $ map costLineAmount invoiceCostLines
      discounts = sumAmount $ map discountLineAmount invoiceDiscountLines
      total = subAmount costs discounts
   in if total > zeroAmount
        then StrictlyPositiveAmount total
        else NegativeOrZeroAmount

data LineItemsResult a
  = NegativeOrZeroAmount
  | StrictlyPositiveAmount a
  deriving (Show, Generic)

instance (Validity a) => Validity (LineItemsResult a)
