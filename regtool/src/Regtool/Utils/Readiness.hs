{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Readiness
  ( Readiness (..),
    getReadiness,
  )
where

import Import
import Regtool.Calculation.Checkout
import Regtool.Calculation.Children
import Regtool.Core.AdminConfiguration as Admin
import Regtool.Core.Foundation
import Regtool.Utils.Children
import Regtool.Utils.Signup.CustomActivity (getOurCustomActivitySignups)
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.DaycareActivity
import Regtool.Utils.Signup.OccasionalDaycare (getOurOccasionalDaycareSignups)
import Regtool.Utils.Signup.OccasionalTransport
import Regtool.Utils.Signup.Transport

data Readiness = Readiness
  { readyToFillInDoctors :: !Bool,
    readyToFillInChildren :: !Bool,
    readyChildrenNeedLevelConfirmation :: !Bool,
    readyToSignupChildren :: !Bool,
    readyToViewChosenServices :: !Bool,
    readyToViewCosts :: !Bool,
    readyToViewPayments :: !Bool
  }

getReadiness :: Entity Account -> Handler Readiness
getReadiness (Entity aid account) = do
  let verified = accountVerified account
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  children <- runDB $ getChildrenOf aid
  mResetDay <- confChildrenLevelResetDay <$> Admin.getConfiguration
  let cids = map entityKey children
  transportSignups <- runDB $ getOurTransportSignups cids
  occasionalTransportSignups <- runDB $ getOurOccasionalTransportSignups cids
  daycareSignups <- runDB $ getOurDaycareSignups cids
  daycareActivitySignups <- runDB $ getOurDaycareActivitySignups cids
  occasionalDaycareSignups <- runDB $ getOurOccasionalDaycareSignups cids
  customActivitySignups <- runDB $ getOurCustomActivitySignups cids
  extraCharges <- runDB $ selectList [ExtraChargeAccount ==. aid] []
  scs <- runDB $ getStripeCustomersOf aid
  let readyToFillInDoctors = not (null parents)
  let readyToFillInChildren = readyToFillInDoctors && not (null doctors)
  let readyChildrenNeedLevelConfirmation = anyChildNeedsConfirmation mResetDay children
  let readyToSignupChildren = verified && readyToFillInChildren && not (null children) && not readyChildrenNeedLevelConfirmation
  let readyToViewChosenServices =
        readyToSignupChildren
          && ( not (null transportSignups)
                 || not (null occasionalTransportSignups)
                 || not (null daycareSignups)
                 || not (null daycareActivitySignups)
                 || not (null occasionalDaycareSignups)
                 || not (null customActivitySignups)
                 || not (null extraCharges)
             )
  let readyToViewCosts = readyToViewChosenServices
  let readyToViewPayments = readyToViewCosts && not (null scs)
  pure Readiness {..}
