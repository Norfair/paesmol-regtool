module Regtool.Utils.Parents
  ( canDeleteParent,
  )
where

import qualified Database.Persist as DB
import Import
import Regtool.Core.Foundation

canDeleteParent :: ParentId -> Handler (Deletable ParentId)
canDeleteParent pid = pure $ CanDelete $ DB.delete pid
