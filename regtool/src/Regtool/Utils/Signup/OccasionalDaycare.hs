{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.OccasionalDaycare
  ( getAllOurOccasionalDaycareSignups,
    getOurOccasionalDaycarePayments,
    getOurOccasionalDaycareSignups,
    occasionalSignupTrips,
  )
where

import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Import
import Regtool.Data

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getOurOccasionalDaycareSignups ::
  (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity OccasionalDaycareSignup]
getOurOccasionalDaycareSignups cids =
  selectList
    [OccasionalDaycareSignupChild <-. cids, OccasionalDaycareSignupDeletedByAdmin ==. False]
    []

getAllOurOccasionalDaycareSignups ::
  (MonadIO m) =>
  [ChildId] ->
  ReaderT SqlBackend m [(Entity OccasionalDaycareSignup, Maybe (Entity OccasionalDaycarePayment))]
getAllOurOccasionalDaycareSignups cids =
  E.select
    $ E.from
    $ \(occasionalDaycareSignup `E.LeftOuterJoin` occasionalDaycarePayment) -> do
      E.on
        ( E.just (occasionalDaycareSignup ^. OccasionalDaycareSignupId)
            E.==. occasionalDaycarePayment
              E.?. OccasionalDaycarePaymentSignup
        )
      E.where_ (occasionalDaycareSignup ^. OccasionalDaycareSignupChild `E.in_` E.valList cids)
      return (occasionalDaycareSignup, occasionalDaycarePayment)

getOurOccasionalDaycarePayments ::
  (MonadIO m) =>
  [OccasionalDaycareSignupId] ->
  ReaderT SqlBackend m [Entity OccasionalDaycarePayment]
getOurOccasionalDaycarePayments sids = selectList [OccasionalDaycarePaymentSignup <-. sids] []

occasionalSignupTrips ::
  [Entity OccasionalDaycareSignup] ->
  [Entity Child] ->
  [Entity DaycareSemestre] ->
  [Entity DaycareTimeslot] ->
  [Entity OccasionalDaycarePayment] ->
  [ ( Entity OccasionalDaycareSignup,
      Entity Child,
      Entity DaycareSemestre,
      Entity DaycareTimeslot,
      Maybe (Entity OccasionalDaycarePayment)
    )
  ]
occasionalSignupTrips signups children semestres timeslots payments =
  flip mapMaybe signups $ \odse@(Entity odsid OccasionalDaycareSignup {..}) -> do
    c <- find ((== occasionalDaycareSignupChild) . entityKey) children
    ts <- find ((== occasionalDaycareSignupTimeslot) . entityKey) timeslots
    s <- find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    let p = find ((== odsid) . occasionalDaycarePaymentSignup . entityVal) payments
    pure (odse, c, s, ts, p)
