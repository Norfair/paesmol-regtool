{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.Transport
  ( signupTrips,
    getOurTransportSignups,
    getAllOurTransportSignups,
  )
where

import Import
import Regtool.Core.Foundation

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

signupTrips ::
  [Entity TransportSignup] ->
  [Entity Child] ->
  [Entity BusStop] ->
  [(Entity TransportSignup, Entity Child, Entity BusStop)]
signupTrips signups children busStops =
  flip mapMaybe signups $ \ets@(Entity _ TransportSignup {..}) ->
    (,,) ets
      <$> find ((== transportSignupChild) . entityKey) children
      <*> find ((== transportSignupBusStop) . entityKey) busStops

getOurTransportSignups :: (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity TransportSignup]
getOurTransportSignups cids =
  selectList [TransportSignupChild <-. cids, TransportSignupDeletedByAdmin ==. False] []

getAllOurTransportSignups :: (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity TransportSignup]
getAllOurTransportSignups cids = selectList [TransportSignupChild <-. cids] []
