{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.CustomActivity where

import Data.Text (pack, unwords)
import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Database.Persist ((<-.), (==.))
import Database.Persist.Class (selectList)
import Database.Persist.Sql (SqlBackend)
import Database.Persist.Types (Entity (..))
import Import hiding (unwords)
import Regtool.Calculation.Time (formatDay, formatTimestamp)
import Regtool.Data
  ( Child,
    ChildId,
    CustomActivity (..),
    CustomActivityPayment (..),
    CustomActivitySignup (..),
    CustomActivitySignupId,
    schoolYearText,
    pattern CustomActivityPaymentSignup,
    pattern CustomActivitySignupChild,
    pattern CustomActivitySignupDeletedByAdmin,
    pattern CustomActivitySignupId,
  )

customActivityText :: CustomActivity -> Text
customActivityText CustomActivity {..} =
  unwords
    [ schoolYearText customActivitySchoolYear,
      pack $ formatDay customActivityDate,
      pack $ formatTimestamp customActivityStart,
      "-",
      pack $ formatTimestamp customActivityEnd,
      ": ",
      customActivityDescription
    ]

getAllOurCustomActivitySignups ::
  (MonadIO m) =>
  [ChildId] ->
  ReaderT SqlBackend m [(Entity CustomActivitySignup, Maybe (Entity CustomActivityPayment))]
getAllOurCustomActivitySignups cids =
  E.select
    $ E.from
    $ \(customActivitySignup `E.LeftOuterJoin` customActivityPayment) -> do
      E.on
        ( E.just (customActivitySignup ^. CustomActivitySignupId)
            E.==. customActivityPayment
              E.?. CustomActivityPaymentSignup
        )
      E.where_ (customActivitySignup ^. CustomActivitySignupChild `E.in_` E.valList cids)
      return (customActivitySignup, customActivityPayment)

signupTrips ::
  [Entity CustomActivitySignup] ->
  [Entity Child] ->
  [Entity CustomActivity] ->
  [Entity CustomActivityPayment] ->
  [(CustomActivitySignupId, Child, CustomActivity, Bool)]
signupTrips signups children activities payments =
  flip mapMaybe signups $ \(Entity casid CustomActivitySignup {..}) -> do
    (Entity _ c) <- find ((== customActivitySignupChild) . entityKey) children
    (Entity _ a) <- find ((== customActivitySignupActivity) . entityKey) activities
    let p = isJust $ find ((== casid) . customActivityPaymentSignup . entityVal) payments
    pure (casid, c, a, p)

getOurCustomActivitySignups ::
  (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity CustomActivitySignup]
getOurCustomActivitySignups cids =
  selectList [CustomActivitySignupChild <-. cids, CustomActivitySignupDeletedByAdmin ==. False] []

getOurCustomActivityPayments ::
  (MonadIO m) => [CustomActivitySignupId] -> ReaderT SqlBackend m [Entity CustomActivityPayment]
getOurCustomActivityPayments sids = selectList [CustomActivityPaymentSignup <-. sids] []
