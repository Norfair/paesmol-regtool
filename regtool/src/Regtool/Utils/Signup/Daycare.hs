{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.Daycare
  ( timeslotsTups,
    signupTrips,
    getAllOurDaycareSignups,
    getOurDaycareSignups,
    getOurDaycarePayments,
    timeslotDescription,
    occasionalTimeslotDescription,
  )
where

import qualified Data.Text as T
import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Import
import Regtool.Calculation.Time
import Regtool.Data

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

timeslotsTups ::
  [Entity DaycareSemestre] ->
  [Entity DaycareTimeslot] ->
  [(Entity DaycareSemestre, Entity DaycareTimeslot)]
timeslotsTups semestres timeslots =
  flip mapMaybe timeslots $ \ts -> do
    s <- find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    pure (s, ts)

signupTrips ::
  [Entity DaycareSignup] ->
  [Entity Child] ->
  [Entity DaycareSemestre] ->
  [Entity DaycareTimeslot] ->
  [Entity DaycarePayment] ->
  [ ( DaycareSignupId,
      Entity Child,
      Entity DaycareSemestre,
      Entity DaycareTimeslot,
      Maybe (Entity DaycarePayment)
    )
  ]
signupTrips signups children semestres timeslots payments =
  flip mapMaybe signups $ \(Entity eid DaycareSignup {..}) -> do
    c <- find ((== daycareSignupChild) . entityKey) children
    ts <- find ((== daycareSignupTimeslot) . entityKey) timeslots
    s <- find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    let p = find ((== eid) . daycarePaymentSignup . entityVal) payments
    pure (eid, c, s, ts, p)

getOurDaycareSignups :: (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity DaycareSignup]
getOurDaycareSignups cids =
  selectList [DaycareSignupChild <-. cids, DaycareSignupDeletedByAdmin ==. False] []

getAllOurDaycareSignups ::
  (MonadIO m) =>
  [ChildId] ->
  ReaderT SqlBackend m [(Entity DaycareSignup, Maybe (Entity DaycarePayment))]
getAllOurDaycareSignups cids =
  E.select
    $ E.from
    $ \(daycareSignup `E.LeftOuterJoin` daycarePayment) -> do
      E.on (E.just (daycareSignup ^. DaycareSignupId) E.==. daycarePayment E.?. DaycarePaymentSignup)
      E.where_ (daycareSignup ^. DaycareSignupChild `E.in_` E.valList cids)
      return (daycareSignup, daycarePayment)

getOurDaycarePayments ::
  (MonadIO m) => [DaycareSignupId] -> ReaderT SqlBackend m [Entity DaycarePayment]
getOurDaycarePayments sids = selectList [DaycarePaymentSignup <-. sids] []

timeslotDescription :: DaycareSemestre -> DaycareTimeslot -> Text
timeslotDescription DaycareSemestre {..} DaycareTimeslot {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear,
      semestreText daycareSemestreSemestre,
      schoolDayText daycareTimeslotSchoolDay,
      T.pack $ formatTimestamp daycareTimeslotStart,
      "-",
      T.pack $ formatTimestamp daycareTimeslotEnd,
      T.pack $ "(" <> fmtAmount daycareTimeslotFee <> " €)"
    ]

occasionalTimeslotDescription :: DaycareSemestre -> DaycareTimeslot -> Text
occasionalTimeslotDescription DaycareSemestre {..} DaycareTimeslot {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear,
      semestreText daycareSemestreSemestre,
      schoolDayText daycareTimeslotSchoolDay,
      T.pack $ formatTimestamp daycareTimeslotStart,
      "-",
      T.pack $ formatTimestamp daycareTimeslotEnd,
      T.pack $ "(" <> fmtAmount daycareTimeslotOccasionalFee <> " €)"
    ]
