{-# LANGUAGE TypeFamilies #-}

module Regtool.Utils.Signup.Delete where

import Database.Persist ((=.))
import Database.Persist.Class (EntityField, delete, update)
import Database.Persist.Sql (PersistRecordBackend, SqlBackend)
import Database.Persist.Types (Key)
import Import
import Regtool.Core.Foundation (Handler, Html, runDB)
import Regtool.Data (ChildId)
import Regtool.Utils.Signup.Redirect (redirectToAccount)
import Yesod.Core.Handler (setUltDestReferer)

-- This has a misleading name for historical reasons:
-- In the past, the admin could only delete signups that had already been paid
-- because the user could delete the unpaid ones themselves.
deleteIfPaid ::
  (PersistRecordBackend record SqlBackend) =>
  Key record -> -- signup id
  Maybe payment ->
  EntityField record Bool -> -- deletion flag to be changed
  (record -> ChildId) -> -- signup child accessor
  Handler Html
deleteIfPaid signupId mPayment deletionFlag childOf = do
  if isNothing mPayment
    then do
      runDB $ delete signupId
      setUltDestReferer
    else do
      runDB $ update signupId [deletionFlag =. True]
  redirectToAccount signupId childOf
