{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Utils.Signup.Redirect where

import Database.Persist ((==.))
import Database.Persist.Class (get, selectFirst)
import Database.Persist.Sql (PersistRecordBackend, SqlBackend)
import Database.Persist.Types (Entity (..), Key)
import Import
import Regtool.Core.Foundation (Handler, Html, runDB)
import Regtool.Core.Foundation.Regtool (AdminR (AdminAccountR, PanelR), Route (AdminR))
import Regtool.Data (ChildId, ChildOf (childOfParent), pattern ChildOfChild)
import Yesod.Core (redirectUltDest)

redirectToAccount ::
  (PersistRecordBackend record SqlBackend) => Key record -> (record -> ChildId) -> Handler Html
redirectToAccount signupId child = do
  let redirectOrContinue = flip $ maybe $ redirectUltDest $ AdminR PanelR
  msignup <- runDB $ get signupId
  redirectOrContinue msignup $ \signup -> do
    mchildOf <- runDB $ selectFirst [ChildOfChild ==. child signup] []
    redirectOrContinue mchildOf $ \(Entity _ childOf) -> do
      let accountId = childOfParent childOf
      redirectUltDest $ AdminR $ AdminAccountR accountId
