{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.DaycareActivity
  ( activitiesTups,
    signupTrips,
    getAllOurDaycareActivitySignups,
    getOurDaycareActivityPayments,
    getOurDaycareActivitySignups,
    activityDescription,
  )
where

import qualified Data.Text as T
import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Import
import Regtool.Calculation.Time
import Regtool.Data

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

activitiesTups ::
  [Entity DaycareSemestre] ->
  [(Entity DaycareActivity, String)] ->
  [(Entity DaycareSemestre, Entity DaycareActivity, String)]
activitiesTups semestres activities =
  flip mapMaybe activities $ \(ts, spaces) -> do
    s <- find ((== daycareActivitySemestre (entityVal ts)) . entityKey) semestres
    pure (s, ts, spaces)

signupTrips ::
  [Entity DaycareActivitySignup] ->
  [Entity Child] ->
  [Entity DaycareSemestre] ->
  [Entity DaycareActivity] ->
  [Entity DaycareActivityPayment] ->
  [ ( DaycareActivitySignupId,
      Entity Child,
      Entity DaycareSemestre,
      Entity DaycareActivity,
      Maybe (Entity DaycareActivityPayment)
    )
  ]
signupTrips signups children semestres activities payments =
  flip mapMaybe signups $ \(Entity eid DaycareActivitySignup {..}) -> do
    c <- find ((== daycareActivitySignupChild) . entityKey) children
    a <- find ((== daycareActivitySignupActivity) . entityKey) activities
    s <- find ((== daycareActivitySemestre (entityVal a)) . entityKey) semestres
    let p = find ((== eid) . daycareActivityPaymentSignup . entityVal) payments
    pure (eid, c, s, a, p)

getOurDaycareActivitySignups ::
  (MonadIO m) => [ChildId] -> ReaderT SqlBackend m [Entity DaycareActivitySignup]
getOurDaycareActivitySignups cids =
  selectList [DaycareActivitySignupChild <-. cids, DaycareActivitySignupDeletedByAdmin ==. False] []

getAllOurDaycareActivitySignups ::
  (MonadIO m) =>
  [ChildId] ->
  ReaderT SqlBackend m [(Entity DaycareActivitySignup, Maybe (Entity DaycareActivityPayment))]
getAllOurDaycareActivitySignups cids =
  E.select
    $ E.from
    $ \(daycareActivitySignup `E.LeftOuterJoin` daycareActivityPayment) -> do
      E.on
        ( E.just (daycareActivitySignup ^. DaycareActivitySignupId)
            E.==. daycareActivityPayment
              E.?. DaycareActivityPaymentSignup
        )
      E.where_ (daycareActivitySignup ^. DaycareActivitySignupChild `E.in_` E.valList cids)
      return (daycareActivitySignup, daycareActivityPayment)

getOurDaycareActivityPayments ::
  (MonadIO m) => [DaycareActivitySignupId] -> ReaderT SqlBackend m [Entity DaycareActivityPayment]
getOurDaycareActivityPayments sids = selectList [DaycareActivityPaymentSignup <-. sids] []

activityDescription :: DaycareSemestre -> DaycareActivity -> Text
activityDescription DaycareSemestre {..} DaycareActivity {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear,
      semestreText daycareSemestreSemestre,
      schoolDayText daycareActivitySchoolDay,
      T.pack $ formatTimestamp daycareActivityStart,
      "-",
      T.pack $ formatTimestamp daycareActivityEnd,
      ": ",
      daycareActivityName
    ]
