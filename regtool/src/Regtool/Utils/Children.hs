{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.Children
  ( withOwnChild,
    withOwnChildren,
    canDeleteChild,
    anyChildNeedsConfirmation,
  )
where

import qualified Database.Persist.Sqlite as DB
import Import
import Regtool.Core.Foundation

withOwnChild :: ChildId -> Handler Html -> Handler Html
withOwnChild cid func = do
  aid <- requireAuthId
  isOwnChild <- isChildOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnChild || isAdminAcc
    then func
    else permissionDenied "Not your child."

withOwnChildren :: [ChildId] -> Handler Html -> Handler Html
withOwnChildren cs func = go cs
  where
    go [] = func
    go (cid : rest) = withOwnChild cid $ go rest

isChildOf :: ChildId -> AccountId -> Handler Bool
isChildOf cid aid = fmap isJust $ runDB $ getBy $ UniqueChild cid aid

canDeleteChild :: ChildId -> Handler (Deletable ChildId)
canDeleteChild cid = do
  m1 <- fmap (() <$) $ runDB $ selectFirst [TransportSignupChild ==. cid] []
  m2 <- fmap (() <$) $ runDB $ selectFirst [OccasionalTransportSignupChild ==. cid] []
  m3 <- fmap (() <$) $ runDB $ selectFirst [DaycareSignupChild ==. cid] []
  m4 <- fmap (() <$) $ runDB $ selectFirst [OccasionalDaycareSignupChild ==. cid] []
  m5 <- fmap (() <$) $ runDB $ selectFirst [DaycareActivitySignupChild ==. cid] []
  m6 <- fmap (() <$) $ runDB $ selectFirst [CustomActivitySignupChild ==. cid] []
  pure
    $ case msum [m1, m2, m3, m4, m5, m6] of
      Nothing ->
        CanDelete $ do
          DB.delete cid
          DB.deleteWhere [ChildOfChild ==. cid]
      Just () -> CannotDelete ["This child is still signed up for a service."]

anyChildNeedsConfirmation :: Maybe Day -> [Entity Child] -> Bool
anyChildNeedsConfirmation mResetDay children = case mResetDay of
  Nothing -> False -- No reset day, no need to confirm
  Just resetDay ->
    any
      ( \(Entity _ c) -> case childLatestConfirmationTime c of
          Nothing -> True -- No confirmation time, definitely need to confirm
          Just ct -> utctDay ct < resetDay
      )
      children
