{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.AdminNotification
  ( sendAdminNotificationFromLooper,
  )
where

import qualified Database.Persist.Sqlite as DB
import Import
import Regtool.Data

sendAdminNotificationFromLooper :: (MonadIO m) => Text -> DB.SqlPersistT m ()
sendAdminNotificationFromLooper = sendAdminNotification Nothing

sendAdminNotification :: (MonadIO m) => Maybe AccountId -> Text -> DB.SqlPersistT m ()
sendAdminNotification maid contents = do
  now <- liftIO getCurrentTime
  insertValid_
    AdminNotificationEmail
      { adminNotificationEmailContents = contents,
        adminNotificationEmailRelevantAccount = maid,
        adminNotificationEmailTimestamp = now,
        adminNotificationEmailEmail = Nothing
      }
