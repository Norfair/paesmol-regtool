{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.Costs.Input where

import Import
import Regtool.Calculation.Costs.Input
import Regtool.Core.Foundation

{-# ANN module ("HLint: ignore Avoid lambda" :: String) #-}

{-# ANN module ("HLint: ignore Redundant if" :: String) #-}

getCostsInput :: Handler CostsInput
getCostsInput = do
  aid <- requireAuthId
  runDB $ getCostsInputForAccount aid
