{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.GarbageCollector (garbageCollectorLooper) where

import qualified Database.Persist as DB
import qualified Database.Persist.Sql as DB
import Import
import Regtool.Core.Foundation
import Regtool.Looper.Types
import Regtool.Looper.Utils

garbageCollectorLooper :: Looper ()
garbageCollectorLooper = do
  garbageCollectEmails
  garbageCollectChoices
  garbageCollectUnverifiedUsers
  vacuumDB

garbageCollectEmails :: Looper ()
garbageCollectEmails = do
  now <- liftIO getCurrentTime
  let thirtyDaysAgo = addUTCTime (-30 * nominalDay) now
  looperDB $ DB.deleteWhere [EmailAddedTimestamp <. thirtyDaysAgo]

garbageCollectChoices :: Looper ()
garbageCollectChoices = do
  now <- liftIO getCurrentTime
  let thirtyDaysAgo = addUTCTime (-30 * nominalDay) now
  looperDB $ DB.deleteWhere [ShoppingCartCreated <. Just thirtyDaysAgo]

garbageCollectUnverifiedUsers :: Looper ()
garbageCollectUnverifiedUsers = do
  now <- liftIO getCurrentTime
  let thirtyDaysAgo = addUTCTime (-30 * nominalDay) now
  looperDB $ do
    unverifiedAccounts <- DB.selectKeysList [AccountVerified ==. False, AccountCreationTime <. thirtyDaysAgo] []
    forM_ unverifiedAccounts $ \aid -> do
      accountHasParents <- DB.exists [ParentAccount ==. aid]
      when (not accountHasParents) $ DB.delete aid

vacuumDB :: Looper ()
vacuumDB = do
  pool <- asks looperConnectionPool
  liftIO $ DB.runSqlPoolNoTransaction (DB.rawExecute "VACUUM" []) pool (Just DB.Serializable)
