{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer (emailLooper) where

import qualified Amazonka as AWS
import qualified Amazonka.SES.SendEmail
import qualified Amazonka.SES.SendEmail as SES
import qualified Amazonka.SES.Types as SES
import Control.Exception
import qualified Data.Text as T
import qualified Database.Persist as DB
import Import
import Regtool.Core.Foundation
import Regtool.Looper.Emailer.AdminNotification
import Regtool.Looper.Emailer.Daycare (daycareEmailsDirective)
import Regtool.Looper.Emailer.OccasionalDaycare (occasionalDaycareEmailsDirective)
import Regtool.Looper.Emailer.OccasionalTransport (occasionalTransportEmailsDirective)
import Regtool.Looper.Emailer.PasswordReset
import Regtool.Looper.Emailer.PaymentReceived
import Regtool.Looper.Emailer.Transport (transportEmailsDirective)
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Emailer.Verification
import Regtool.Looper.Types
import Regtool.Looper.Utils
import Regtool.Utils.AdminNotification (sendAdminNotificationFromLooper)
import qualified System.IO as IO

emailLooper :: Looper ()
emailLooper = do
  makeEmailsFrom verificationEmailsDirective
  makeEmailsFrom passwordResetEmailsDirective
  makeEmailsFrom paymentReceivedEmailsDirective
  makeEmailsFrom adminNotificationEmailsDirective
  makeEmailsFrom daycareEmailsDirective
  makeEmailsFrom transportEmailsDirective
  makeEmailsFrom occasionalDaycareEmailsDirective
  makeEmailsFrom occasionalTransportEmailsDirective
  sendEmails

makeEmailsFrom :: (PersistRecordBackend a SqlBackend) => EmailCreationDirective a -> Looper ()
makeEmailsFrom dir@EmailCreationDirective {..} = do
  list <- looperDB $ selectFirst [emailIdSelector ==. Nothing] [Asc emailTimestampSelector]
  case list of
    Nothing -> pure ()
    Just (Entity seid se) -> do
      y <- asks looperRegtool
      ar <- asks looperApproot
      eEmail <- makeEmail se (yesodRender y ar)
      case eEmail of
        Left errMsg -> looperDB $ sendAdminNotificationFromLooper errMsg
        Right email ->
          looperDB $ do
            eid <- insertValid email
            DB.update seid [emailIdSelector DB.=. Just eid]
      makeEmailsFrom dir -- Go on until there are no more conversions to be made

sendEmails :: Looper ()
sendEmails = do
  env <- asks looperEnvironment
  list <-
    looperDB
      $ selectFirst
        ( (EmailStatus ==. EmailStatusUnsent)
            : [EmailSendOutsideProduction ==. True | env /= EnvProduction]
        )
        [Asc EmailAddedTimestamp]
  case list of
    Nothing -> pure ()
    Just e -> do
      handleSingleEmail e
      sendEmails -- Go on until there are no more emails to be sent.

handleSingleEmail :: Entity Email -> Looper ()
handleSingleEmail (Entity emailId email) = do
  looperDB $ do
    newStatus <- liftIO $ sendSingleEmail email
    DB.update emailId
      $ case newStatus of
        EmailSent hid -> [EmailStatus DB.=. EmailStatusSent, EmailSesId DB.=. Just hid]
        EmailErrored err -> [EmailStatus DB.=. EmailStatusError, EmailError DB.=. Just err]

sendSingleEmail :: Email -> IO EmailResult
sendSingleEmail Email {..} = do
  logger <- AWS.newLogger AWS.Debug IO.stdout
  discoveredEnv <- liftIO $ AWS.newEnv AWS.discover
  let awsEnv = discoveredEnv {AWS.logger = logger}

  AWS.runResourceT $ do
    let text = SES.newContent emailTextContent
    let html = SES.newContent emailHtmlContent
    let body = SES.newBody {SES.html = Just text, SES.text = Just html}
    let subject = SES.newContent emailSubject
    let message = SES.newMessage subject body
    let destination =
          SES.newDestination
            { SES.toAddresses = Just [emailAddressText emailTo],
              SES.ccAddresses = (\cc -> [emailAddressText cc]) <$> emailCc,
              SES.bccAddresses = (\bcc -> [emailAddressText bcc]) <$> emailBcc
            }
    let request = SES.newSendEmail (emailAddressText emailFrom) destination message
    errOrResp <- AWS.sendEither awsEnv request
    pure
      $ case errOrResp of
        Left err -> EmailErrored $ T.pack $ displayException err
        Right response ->
          case SES.httpStatus response of
            200 -> EmailSent $ Amazonka.SES.SendEmail.messageId response
            _ -> EmailErrored "Error while sending email."
