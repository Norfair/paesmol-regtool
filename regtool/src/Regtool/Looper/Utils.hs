{-# LANGUAGE FlexibleContexts #-}

module Regtool.Looper.Utils
  ( looperDB,
  )
where

import Database.Persist.Sqlite (SqlPersistT, runSqlPool)
import Import
import Regtool.Looper.Types

looperDB :: SqlPersistT IO b -> Looper b
looperDB query = do
  pool <- asks looperConnectionPool
  liftIO $ runSqlPool query pool
