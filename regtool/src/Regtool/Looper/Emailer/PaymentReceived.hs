{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.PaymentReceived
  ( paymentReceivedEmailsDirective,
    paymentReceivedEmailTextContent,
    paymentReceivedEmailHtmlContent,
  )
where

import qualified Data.Map as M
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Calculation.Costs.Checkout
import Regtool.Calculation.Costs.Input
import Regtool.Core.Foundation
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types
import Regtool.Looper.Utils
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

paymentReceivedEmailsDirective :: EmailCreationDirective PaymentReceivedEmail
paymentReceivedEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = PaymentReceivedEmailEmail,
      emailTimestampSelector = PaymentReceivedEmailTimestamp,
      makeEmail = makePaymentReceivedEmail
    }

makePaymentReceivedEmail :: PaymentReceivedEmail -> Renderer -> Looper (Either Text Email)
makePaymentReceivedEmail PaymentReceivedEmail {..} render = do
  now <- liftIO getCurrentTime
  mcoo <-
    looperDB $ do
      co <- deriveCheckoutsOverview <$> getCostsInputForAccount paymentReceivedEmailAccount
      pure $ M.lookup paymentReceivedEmailCheckout $ checkoutsOverviewCheckouts co
  ea <- asks looperEmailAddress
  case mcoo of
    Just coo ->
      pure
        $ Right
        $ Email
          { emailTo = paymentReceivedEmailTo,
            emailCc = Nothing,
            emailBcc = Nothing,
            emailFrom = ea,
            emailFromName = "Regtool Payments",
            emailSubject = "Payment Received",
            emailTextContent = paymentReceivedEmailTextContent coo render,
            emailHtmlContent = paymentReceivedEmailHtmlContent coo render,
            emailStatus = EmailStatusUnsent,
            emailError = Nothing,
            emailSesId = Nothing,
            emailAddedTimestamp = now,
            emailSendOutsideProduction = True
          }
    Nothing ->
      pure
        $ Left
          "Something went wrong while sending an email with a payment overview: the checkout was not found."

paymentReceivedEmailTextContent :: CheckoutOverview -> Renderer -> Text
paymentReceivedEmailTextContent coo render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/payment-received-email.txt") render

paymentReceivedEmailHtmlContent :: CheckoutOverview -> Renderer -> Text
paymentReceivedEmailHtmlContent coo render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/email/payment-received-email.hamlet") render
