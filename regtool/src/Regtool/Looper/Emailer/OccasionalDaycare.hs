{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.OccasionalDaycare
  ( occasionalDaycareEmailsDirective,
    occasionalDaycareEmailTextContent,
    occasionalDaycareEmailHtmlContent,
  )
where

import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Calculation.Children (getAccountsOf)
import Regtool.Calculation.Time (formatDay, formatTimestamp)
import Regtool.Data
import Regtool.Looper.Emailer.Types (EmailCreationDirective (..), Renderer)
import Regtool.Looper.Types (Looper, looperEmailAddress)
import Regtool.Looper.Utils (looperDB)
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet (hamletFile)
import Text.Shakespeare.Text (textFile)

occasionalDaycareEmailsDirective :: EmailCreationDirective OccasionalDaycareEmail
occasionalDaycareEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = OccasionalDaycareEmailEmail,
      emailTimestampSelector = OccasionalDaycareEmailTimestamp,
      makeEmail = makeOccasionalDaycareEmail
    }

makeOccasionalDaycareEmail :: OccasionalDaycareEmail -> Renderer -> Looper (Either Text Email)
makeOccasionalDaycareEmail ode@OccasionalDaycareEmail {..} render = do
  mSignup <- looperDB $ get occasionalDaycareEmailSignup
  case mSignup of
    Nothing ->
      pure
        $ Left
        $ T.unlines
          [ "Missing occasional daycare signup, cannot construct email.",
            "For this occasional daycare notification email:",
            T.pack (ppShow ode)
          ]
    Just signup -> do
      as <- map entityVal <$> looperDB (getAccountsOf $ occasionalDaycareSignupChild signup)
      mChild <- looperDB $ get $ occasionalDaycareSignupChild signup
      mTimeslot <- looperDB $ get $ occasionalDaycareSignupTimeslot signup
      case (mChild, mTimeslot) of
        (Nothing, _) ->
          pure
            $ Left
            $ T.unlines
              [ "Missing occasional daycare signup child, cannot construct email.",
                "For this occasional daycare notivication email:",
                T.pack (ppShow ode),
                "For this occasional daycare signup:",
                T.pack (ppShow signup)
              ]
        (_, Nothing) ->
          pure
            $ Left
            $ T.unlines
              [ "Missing occasional daycare signup timeslot, cannot construct email.",
                "For this occasional daycare notification email:",
                T.pack (ppShow ode),
                "For this occasional daycare signup:",
                T.pack (ppShow signup)
              ]
        (Just child, Just timeslot) -> do
          now <- liftIO getCurrentTime
          ea <- asks looperEmailAddress
          pure
            $ Right
              Email
                { emailTo = unsafeEmailAddress "daycare" "esmol.be",
                  emailCc = Just $ unsafeEmailAddress "pa" "esmol.be",
                  emailBcc = Just $ unsafeEmailAddress "registration" "paesmol.eu",
                  emailFrom = ea,
                  emailFromName = "Occasional Services Signup Email",
                  emailSubject = "Occasional Daycare Signup Information",
                  emailTextContent = occasionalDaycareEmailTextContent child timeslot signup render,
                  emailHtmlContent =
                    occasionalDaycareEmailHtmlContent as child timeslot signup render,
                  emailStatus = EmailStatusUnsent,
                  emailError = Nothing,
                  emailSesId = Nothing,
                  emailAddedTimestamp = now,
                  emailSendOutsideProduction = False
                }

occasionalDaycareEmailTextContent ::
  Child -> DaycareTimeslot -> OccasionalDaycareSignup -> Renderer -> Text
occasionalDaycareEmailTextContent Child {..} DaycareTimeslot {..} OccasionalDaycareSignup {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/occasional-daycare-email.txt") render

occasionalDaycareEmailHtmlContent ::
  [Account] -> Child -> DaycareTimeslot -> OccasionalDaycareSignup -> Renderer -> Text
occasionalDaycareEmailHtmlContent as Child {..} DaycareTimeslot {..} OccasionalDaycareSignup {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/email/occasional-daycare-email.hamlet") render
