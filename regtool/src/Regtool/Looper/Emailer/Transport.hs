{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.Transport
  ( transportEmailsDirective,
    transportEmailTextContent,
    transportEmailHtmlContent,
  )
where

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Database.Persist
import Import
import Regtool.Calculation.Children (getAccountsOf)
import Regtool.Data
import Regtool.Looper.Emailer.Types (EmailCreationDirective (..), Renderer)
import Regtool.Looper.Types (Looper, looperEmailAddress)
import Regtool.Looper.Utils (looperDB)
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet (hamletFile)
import Text.Shakespeare.Text (textFile)
import Yesod.Form.Fields (unTextarea)

transportEmailsDirective :: EmailCreationDirective TransportEmail
transportEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = TransportEmailEmail,
      emailTimestampSelector = TransportEmailTimestamp,
      makeEmail = makeTransportEmail
    }

makeTransportEmail :: TransportEmail -> Renderer -> Looper (Either Text Email)
makeTransportEmail TransportEmail {..} render = do
  mSignup <- looperDB $ get transportEmailSignup
  case mSignup of
    Nothing -> pure $ Left "Missing  transport signup, cannot construct email."
    Just signup -> do
      as <- map entityVal <$> looperDB (getAccountsOf $ transportSignupChild signup)
      mChild <- looperDB $ get $ transportSignupChild signup
      let bid = transportSignupBusStop signup
      mBusStop <- looperDB $ get bid
      case (mChild, mBusStop) of
        (Nothing, _) -> pure $ Left "Missing  transport signup child, cannot construct email."
        (_, Nothing) -> pure $ Left "Missing  transport signup bus stop, cannot construct email."
        (Just child, Just busStop) -> do
          mbls <- looperDB $ getBy $ UniqueBusLineStop bid
          mbl <-
            case mbls of
              Nothing -> pure Nothing
              Just (Entity _ bls) -> do
                mBusLine <- looperDB $ get $ busLineStopLine bls
                case mBusLine of
                  Nothing -> pure Nothing
                  Just bl -> pure $ Just bl
          now <- liftIO getCurrentTime
          ea <- asks looperEmailAddress
          pure
            $ Right
              Email
                { emailTo = unsafeEmailAddress "info" "marcelcars.be",
                  emailCc = Just $ unsafeEmailAddress "pa" "esmol.be",
                  emailBcc = Just $ unsafeEmailAddress "registration" "paesmol.eu",
                  emailFrom = ea,
                  emailFromName = " Services Signup Email",
                  emailSubject = " Transport Signup Information",
                  emailTextContent = transportEmailTextContent child busStop mbl render,
                  emailHtmlContent = transportEmailHtmlContent as child busStop mbl render,
                  emailStatus = EmailStatusUnsent,
                  emailError = Nothing,
                  emailSesId = Nothing,
                  emailAddedTimestamp = now,
                  emailSendOutsideProduction = False
                }

transportEmailTextContent ::
  Child -> BusStop -> Maybe BusLine -> Renderer -> Text
transportEmailTextContent Child {..} BusStop {..} mbl render =
  let withBusText = maybe "." (\bl -> "with bus " <> busLineName bl <> ".") mbl
   in LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/transport-email.txt") render

transportEmailHtmlContent ::
  [Account] -> Child -> BusStop -> Maybe BusLine -> Renderer -> Text
transportEmailHtmlContent as Child {..} BusStop {..} mbl render =
  let withBusText = maybe "." (\bl -> "with bus " <> busLineName bl <> ".") mbl
   in LT.toStrict $ renderHtml $ $(hamletFile "templates/email/transport-email.hamlet") render
