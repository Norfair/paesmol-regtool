{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.Verification
  ( verificationEmailsDirective,
    verificationEmailTextContent,
    verificationEmailHtmlContent,
  )
where

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Core.Foundation
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

verificationEmailsDirective :: EmailCreationDirective VerificationEmail
verificationEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = VerificationEmailEmail,
      emailTimestampSelector = VerificationEmailTimestamp,
      makeEmail = makeVerificationEmail
    }

makeVerificationEmail :: VerificationEmail -> Renderer -> Looper (Either Text Email)
makeVerificationEmail vere@VerificationEmail {..} render = do
  now <- liftIO getCurrentTime
  ea <- asks looperEmailAddress
  pure
    $ Right
      Email
        { emailTo = verificationEmailTo,
          emailCc = Nothing,
          emailBcc = Nothing,
          emailFrom = ea,
          emailFromName = "Registration Tool Verification",
          emailSubject = "Please verify your email address",
          emailTextContent = verificationEmailTextContent vere render,
          emailHtmlContent = verificationEmailHtmlContent vere render,
          emailStatus = EmailStatusUnsent,
          emailError = Nothing,
          emailSesId = Nothing,
          emailAddedTimestamp = now,
          emailSendOutsideProduction = True
        }

verificationEmailTextContent :: VerificationEmail -> Renderer -> Text
verificationEmailTextContent VerificationEmail {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/auth/email/verification-email.txt") render

verificationEmailHtmlContent :: VerificationEmail -> Renderer -> Text
verificationEmailHtmlContent VerificationEmail {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/auth/email/verification-email.hamlet") render
