{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.AdminNotification
  ( adminNotificationEmailsDirective,
    adminNotificationEmailTextContent,
    adminNotificationEmailHtmlContent,
  )
where

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Data
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

adminNotificationEmailsDirective :: EmailCreationDirective AdminNotificationEmail
adminNotificationEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = AdminNotificationEmailEmail,
      emailTimestampSelector = AdminNotificationEmailTimestamp,
      makeEmail = makeAdminNotificationEmail
    }

makeAdminNotificationEmail :: AdminNotificationEmail -> Renderer -> Looper (Either Text Email)
makeAdminNotificationEmail ane render = do
  now <- liftIO getCurrentTime
  ea <- asks looperEmailAddress
  pure
    $ Right
    $ Email
      { emailTo = unsafeEmailAddress "admin" "paesmol.eu",
        emailCc = Nothing,
        emailBcc = Nothing,
        emailFrom = ea,
        emailFromName = "Regtool Admin Notification Service",
        emailSubject = "Admin notification",
        emailTextContent = adminNotificationEmailTextContent ane render,
        emailHtmlContent = adminNotificationEmailHtmlContent ane render,
        emailStatus = EmailStatusUnsent,
        emailError = Nothing,
        emailSesId = Nothing,
        emailAddedTimestamp = now,
        emailSendOutsideProduction = False
      }

adminNotificationEmailTextContent :: AdminNotificationEmail -> Renderer -> Text
adminNotificationEmailTextContent AdminNotificationEmail {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/admin-notification-email.txt") render

adminNotificationEmailHtmlContent :: AdminNotificationEmail -> Renderer -> Text
adminNotificationEmailHtmlContent AdminNotificationEmail {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/email/admin-notification-email.hamlet") render
