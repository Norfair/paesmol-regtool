{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.OccasionalTransport
  ( occasionalTransportEmailsDirective,
    occasionalTransportEmailTextContent,
    occasionalTransportEmailHtmlContent,
  )
where

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Database.Persist
import Import
import Regtool.Calculation.Children (getAccountsOf)
import Regtool.Calculation.Time (formatDay)
import Regtool.Data
import Regtool.Looper.Emailer.Types (EmailCreationDirective (..), Renderer)
import Regtool.Looper.Types (Looper, looperEmailAddress)
import Regtool.Looper.Utils (looperDB)
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet (hamletFile)
import Text.Shakespeare.Text (textFile)
import Yesod.Form.Fields (unTextarea)

occasionalTransportEmailsDirective :: EmailCreationDirective OccasionalTransportEmail
occasionalTransportEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = OccasionalTransportEmailEmail,
      emailTimestampSelector = OccasionalTransportEmailTimestamp,
      makeEmail = makeOccasionalTransportEmail
    }

makeOccasionalTransportEmail :: OccasionalTransportEmail -> Renderer -> Looper (Either Text Email)
makeOccasionalTransportEmail OccasionalTransportEmail {..} render = do
  mSignup <- looperDB $ get occasionalTransportEmailSignup
  case mSignup of
    Nothing -> pure $ Left "Missing occasional transport signup, cannot construct email."
    Just signup -> do
      as <- map entityVal <$> looperDB (getAccountsOf $ occasionalTransportSignupChild signup)
      mChild <- looperDB $ get $ occasionalTransportSignupChild signup
      let bid = occasionalTransportSignupBusStop signup
      mBusStop <- looperDB $ get bid
      case (mChild, mBusStop) of
        (Nothing, _) ->
          pure $ Left "Missing occasional transport signup child, cannot construct email."
        (_, Nothing) ->
          pure $ Left "Missing occasional transport signup bus stop, cannot construct email."
        (Just child, Just busStop) -> do
          mbls <- looperDB $ getBy $ UniqueBusLineStop bid
          mbl <-
            case mbls of
              Nothing -> pure Nothing
              Just (Entity _ bls) -> do
                mBusLine <- looperDB $ get $ busLineStopLine bls
                case mBusLine of
                  Nothing -> pure Nothing
                  Just bl -> pure $ Just bl
          now <- liftIO getCurrentTime
          ea <- asks looperEmailAddress
          pure
            $ Right
              Email
                { emailTo = unsafeEmailAddress "info" "marcelcars.be",
                  emailCc = Just $ unsafeEmailAddress "pa" "esmol.be",
                  emailBcc = Just $ unsafeEmailAddress "registration" "paesmol.eu",
                  emailFrom = ea,
                  emailFromName = "Occasional Services Signup Email",
                  emailSubject = "Occasional Transport Signup Information",
                  emailTextContent =
                    occasionalTransportEmailTextContent child busStop mbl signup render,
                  emailHtmlContent =
                    occasionalTransportEmailHtmlContent as child busStop mbl signup render,
                  emailStatus = EmailStatusUnsent,
                  emailError = Nothing,
                  emailSesId = Nothing,
                  emailAddedTimestamp = now,
                  emailSendOutsideProduction = False
                }

occasionalTransportEmailTextContent ::
  Child -> BusStop -> Maybe BusLine -> OccasionalTransportSignup -> Renderer -> Text
occasionalTransportEmailTextContent Child {..} BusStop {..} mbl OccasionalTransportSignup {..} render =
  let withBusText = maybe "." (\bl -> "with bus " <> busLineName bl <> ".") mbl
   in case occasionalTransportSignupDirection of
        ToSchool ->
          LT.toStrict
            $ LTB.toLazyText
            $ $(textFile "templates/email/occasional-transport-to-school-email.txt") render
        FromSchool ->
          LT.toStrict
            $ LTB.toLazyText
            $ $(textFile "templates/email/occasional-transport-from-school-email.txt") render

occasionalTransportEmailHtmlContent ::
  [Account] -> Child -> BusStop -> Maybe BusLine -> OccasionalTransportSignup -> Renderer -> Text
occasionalTransportEmailHtmlContent as Child {..} BusStop {..} mbl OccasionalTransportSignup {..} render =
  LT.toStrict
    $ renderHtml
    $ $(hamletFile "templates/email/occasional-transport-email.hamlet") render
