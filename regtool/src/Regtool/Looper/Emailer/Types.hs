{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.Types where

import Import
import Regtool.Core.Foundation
import Regtool.Looper.Types
import Text.Hamlet

type Renderer = Render (Route Regtool)

data EmailCreationDirective a = EmailCreationDirective
  { emailIdSelector :: EntityField a (Maybe EmailId),
    emailTimestampSelector :: EntityField a UTCTime,
    makeEmail :: a -> Renderer -> Looper (Either Text Email)
  }

data EmailResult
  = -- | Id of the mail in the AWS API
    EmailSent Text
  | -- | Error message describing what went wrong
    EmailErrored Text
