{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.PasswordReset
  ( passwordResetEmailsDirective,
    passwordResetEmailTextContent,
    passwordResetEmailHtmlContent,
  )
where

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Core.Foundation
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

passwordResetEmailsDirective :: EmailCreationDirective PasswordResetEmail
passwordResetEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = PasswordResetEmailEmail,
      emailTimestampSelector = PasswordResetEmailTimestamp,
      makeEmail = makePasswordResetEmail
    }

makePasswordResetEmail :: PasswordResetEmail -> Renderer -> Looper (Either Text Email)
makePasswordResetEmail pre@PasswordResetEmail {..} render = do
  now <- liftIO getCurrentTime
  ea <- asks looperEmailAddress
  pure
    $ Right
      Email
        { emailTo = passwordResetEmailTo,
          emailCc = Nothing,
          emailBcc = Nothing,
          emailFrom = ea,
          emailFromName = "Registration Tool Password Reset",
          emailSubject = "Password Reset",
          emailTextContent = passwordResetEmailTextContent pre render,
          emailHtmlContent = passwordResetEmailHtmlContent pre render,
          emailStatus = EmailStatusUnsent,
          emailError = Nothing,
          emailSesId = Nothing,
          emailAddedTimestamp = now,
          emailSendOutsideProduction = True
        }

passwordResetEmailTextContent :: PasswordResetEmail -> Renderer -> Text
passwordResetEmailTextContent PasswordResetEmail {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/auth/email/password-reset-email.txt") render

passwordResetEmailHtmlContent :: PasswordResetEmail -> Renderer -> Text
passwordResetEmailHtmlContent PasswordResetEmail {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/auth/email/password-reset-email.hamlet") render
