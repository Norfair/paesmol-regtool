{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Emailer.Daycare
  ( daycareEmailsDirective,
    daycareEmailTextContent,
    daycareEmailHtmlContent,
  )
where

import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import Import
import Regtool.Calculation.Children (getAccountsOf)
import Regtool.Data
import Regtool.Looper.Emailer.Types (EmailCreationDirective (..), Renderer)
import Regtool.Looper.Types (Looper, looperEmailAddress)
import Regtool.Looper.Utils (looperDB)
import Regtool.Utils.Signup.Daycare
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet (hamletFile)
import Text.Shakespeare.Text (textFile)

daycareEmailsDirective :: EmailCreationDirective DaycareEmail
daycareEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = DaycareEmailEmail,
      emailTimestampSelector = DaycareEmailTimestamp,
      makeEmail = makeDaycareEmail
    }

makeDaycareEmail :: DaycareEmail -> Renderer -> Looper (Either Text Email)
makeDaycareEmail ode@DaycareEmail {..} render = do
  mSignup <- looperDB $ get daycareEmailSignup
  case mSignup of
    Nothing ->
      pure
        $ Left
        $ T.unlines
          [ "Missing daycare signup, cannot construct email.",
            "For this daycare notification email:",
            T.pack (ppShow ode)
          ]
    Just signup -> do
      as <- map entityVal <$> looperDB (getAccountsOf $ daycareSignupChild signup)
      mChild <- looperDB $ get $ daycareSignupChild signup
      mTimeslot <- looperDB $ get $ daycareSignupTimeslot signup
      case mChild of
        Nothing ->
          pure
            $ Left
            $ T.unlines
              [ "Missing daycare signup child, cannot construct email.",
                "For this daycare notivication email:",
                T.pack (ppShow ode),
                "For this daycare signup:",
                T.pack (ppShow signup)
              ]
        Just child ->
          case mTimeslot of
            Nothing ->
              pure
                $ Left
                $ T.unlines
                  [ "Missing daycare signup timeslot, cannot construct email.",
                    "For this daycare notification email:",
                    T.pack (ppShow ode),
                    "For this daycare signup:",
                    T.pack (ppShow signup)
                  ]
            Just timeslot -> do
              mSemestre <- looperDB $ get $ daycareTimeslotSemestre timeslot
              case mSemestre of
                Nothing ->
                  pure
                    $ Left
                    $ T.unlines
                      [ "Missing daycare signup semestre, cannot construct email.",
                        "For this daycare notification email:",
                        T.pack (ppShow ode),
                        "For this daycare signup:",
                        T.pack (ppShow signup)
                      ]
                Just semestre -> do
                  now <- liftIO getCurrentTime
                  ea <- asks looperEmailAddress
                  pure
                    $ Right
                      Email
                        { emailTo = unsafeEmailAddress "daycare" "esmol.be",
                          emailCc = Just $ unsafeEmailAddress "pa" "esmol.be",
                          emailBcc = Just $ unsafeEmailAddress "registration" "paesmol.eu",
                          emailFrom = ea,
                          emailFromName = "Services Signup Email",
                          emailSubject = "Daycare Signup Information",
                          emailTextContent = daycareEmailTextContent child semestre timeslot render,
                          emailHtmlContent = daycareEmailHtmlContent as child semestre timeslot render,
                          emailStatus = EmailStatusUnsent,
                          emailError = Nothing,
                          emailSesId = Nothing,
                          emailAddedTimestamp = now,
                          emailSendOutsideProduction = False
                        }

daycareEmailTextContent ::
  Child -> DaycareSemestre -> DaycareTimeslot -> Renderer -> Text
daycareEmailTextContent Child {..} dcs dct render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/daycare-email.txt") render

daycareEmailHtmlContent ::
  [Account] -> Child -> DaycareSemestre -> DaycareTimeslot -> Renderer -> Text
daycareEmailHtmlContent as Child {..} dcs dct render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/email/daycare-email.hamlet") render
