{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Verifier
  ( verifyLooper,
    verifyDatabaseWithResult,
    VerifyResult (..),
    failIfVerificationFailed,
    logVerificationError,
  )
where

import Control.Monad.Logger
import qualified Data.List.NonEmpty as NE
import Data.Proxy
import qualified Data.Text as T
import Database.Persist.Sqlite
import Import
import Regtool.Calculation.Costs.Input
import Regtool.Data
import Regtool.Looper.Types
import Regtool.Looper.Utils
import Regtool.Utils.AdminNotification

verifyLooper :: Looper ()
verifyLooper = do
  dbResult <- looperDB verifyDatabaseWithResult
  case dbResult of
    VerifiedFine -> pure ()
    VerificationErrors ne ->
      looperDB $ sendAdminNotificationFromLooper $ T.unlines $ map verifyErrorText $ NE.toList ne

failIfVerificationFailed :: (MonadLoggerIO m) => VerifyResult -> m ()
failIfVerificationFailed res =
  case res of
    VerifiedFine -> pure ()
    VerificationErrors ws -> do
      mapM_ logVerificationError ws
      liftIO $ die $ unwords ["Invalid database detected: ", show (length ws), "errors"]

logVerificationError :: (MonadLogger m) => VerifyError -> m ()
logVerificationError = logErrorN . verifyErrorText

verifyErrorText :: VerifyError -> Text
verifyErrorText ve =
  case ve of
    CostsInputError aid s err ->
      T.unlines
        [ "Invalid CostsInput detected:",
          T.unwords ["account:", T.pack $ show aid],
          T.unwords ["value:", s],
          T.unwords ["error:", err]
        ]
    ValidityError typ i s err ->
      T.unlines
        [ "Invalid value detected in database:",
          T.unwords ["type:", typ],
          T.unwords ["id:", T.pack $ show i],
          T.unwords ["value:", s],
          T.unwords ["error:", err]
        ]

data VerifyResult
  = VerifiedFine
  | VerificationErrors (NonEmpty VerifyError)

verifyDatabaseWithResult :: (MonadIO m) => ReaderT SqlBackend m VerifyResult
verifyDatabaseWithResult = do
  ws <- verifyDatabaseAction
  pure
    $ case NE.nonEmpty ws of
      Nothing -> VerifiedFine
      Just ne -> VerificationErrors ne

data VerifyError
  = CostsInputError AccountId Text Text
  | ValidityError Text Int64 Text Text

verifyDatabaseAction :: (MonadIO m) => ReaderT SqlBackend m [VerifyError]
verifyDatabaseAction = liftA2 (++) verifyValidityAction verifyCostsInputAction

verifyCostsInputAction :: (MonadIO m) => ReaderT SqlBackend m [VerifyError]
verifyCostsInputAction = do
  as <- map entityKey <$> selectList [] [Asc AccountId]
  fmap catMaybes
    $ forM as
    $ \aid -> do
      ci <- getCostsInputForAccount aid
      case prettyValidate ci of
        Left err -> pure $ Just $ CostsInputError aid (T.pack $ ppShow ci) (T.pack err)
        Right _ -> pure Nothing

verifyValidityAction :: (MonadIO m) => ReaderT SqlBackend m [VerifyError]
verifyValidityAction =
  concat
    <$> sequence
      [ warnInvalidForDBTable @Account,
        warnInvalidForDBTable @Permission,
        warnInvalidForDBTable @BusLine,
        warnInvalidForDBTable @BusStop,
        warnInvalidForDBTable @BusLineStop,
        warnInvalidForDBTable @Checkout,
        warnInvalidForDBTable @ExtraCharge,
        warnInvalidForDBTable @Discount,
        warnInvalidForDBTable @TransportSignup,
        warnInvalidForDBTable @TransportPayment,
        warnInvalidForDBTable @TransportPaymentPlan,
        warnInvalidForDBTable @TransportEnrollment,
        warnInvalidForDBTable @OccasionalTransportSignup,
        warnInvalidForDBTable @OccasionalTransportPayment,
        warnInvalidForDBTable @Child,
        warnInvalidForDBTable @ChildOf,
        warnInvalidForDBTable @Doctor,
        warnInvalidForDBTable @Email,
        warnInvalidForDBTable @LanguageSection,
        warnInvalidForDBTable @Parent,
        warnInvalidForDBTable @PasswordResetEmail,
        warnInvalidForDBTable @PaymentReceivedEmail,
        warnInvalidForDBTable @AdminNotificationEmail,
        warnInvalidForDBTable @StripeCustomer,
        warnInvalidForDBTable @StripePayment,
        warnInvalidForDBTable @YearlyFeePayment,
        warnInvalidForDBTable @VerificationEmail,
        warnInvalidForDBTable @DaycareEmail,
        warnInvalidForDBTable @TransportEmail,
        warnInvalidForDBTable @OccasionalDaycareEmail,
        warnInvalidForDBTable @OccasionalTransportEmail,
        warnInvalidForDBTable @DaycareSemestre,
        warnInvalidForDBTable @DaycareTimeslot,
        warnInvalidForDBTable @DaycareSignup,
        warnInvalidForDBTable @DaycarePayment,
        warnInvalidForDBTable @OccasionalDaycareSignup,
        warnInvalidForDBTable @OccasionalDaycarePayment,
        warnInvalidForDBTable @DaycareActivity,
        warnInvalidForDBTable @DaycareActivitySignup,
        warnInvalidForDBTable @DaycareActivityPayment,
        warnInvalidForDBTable @CustomActivity,
        warnInvalidForDBTable @CustomActivitySignup,
        warnInvalidForDBTable @CustomActivityPayment
      ]

warnInvalidForDBTable ::
  forall record m.
  ( ToBackendKey SqlBackend record,
    Validity record,
    Show record,
    MonadIO m
  ) =>
  ReaderT SqlBackend m [VerifyError]
warnInvalidForDBTable = do
  let def = entityDef (Proxy :: Proxy record)
  mapMaybe (warnInvalid $ unEntityNameDB $ getEntityDBName def)
    <$> selectList [] [Asc (persistIdField :: EntityField record (Key record))]

warnInvalid ::
  (Validity a, Show a, ToBackendKey SqlBackend a) => Text -> Entity a -> Maybe VerifyError
warnInvalid typ (Entity aid a) =
  case prettyValidate a of
    Left err -> Just $ ValidityError typ (fromSqlKey aid) (T.pack $ ppShow a) (T.pack err)
    Right _ -> Nothing
