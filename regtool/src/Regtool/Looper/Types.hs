{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Looper.Types where

import Control.Monad.Logger
import Database.Persist.Sqlite (ConnectionPool)
import Import
import Regtool.Core.Foundation
import Yesod.Core.Types

data LooperEnv = LooperEnv
  { looperEnvironment :: !RegtoolEnvironment,
    looperDatabaseFilename :: !(Path Abs File),
    looperConnectionPool :: !ConnectionPool,
    looperRegtool :: !Regtool,
    looperApproot :: !ResolvedApproot,
    looperEmailAddress :: !EmailAddress
  }

type Looper = ReaderT LooperEnv (LoggingT IO)
