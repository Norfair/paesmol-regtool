{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Regtool.Core.Foundation.Regtool where

import Autodocodec
import Database.Persist.Sqlite
import Import hiding (reader)
import qualified Network.HTTP.Client as Http
import OptEnvConf
import Regtool.Data
import Yesod.Auth
import Yesod.Core
import Yesod.EmbeddedStatic

type RegtoolWidget = RegtoolWidget' ()

type RegtoolWidget' = WidgetFor Regtool

type RegtoolHandler = HandlerFor Regtool

type RegtoolAuthHandler a = AuthHandler Regtool a

data RegtoolEnvironment
  = EnvProduction
  | EnvStaging
  | EnvManualTesting
  | EnvAutomatedTesting
  deriving (Show, Read, Eq, Generic)

instance Validity RegtoolEnvironment

instance HasCodec RegtoolEnvironment where
  codec =
    stringConstCodec
      [ (EnvProduction, "Production"),
        (EnvStaging, "Staging"),
        (EnvManualTesting, "ManualTesting"),
        (EnvAutomatedTesting, "AutomatedTesting")
      ]

instance HasParser RegtoolEnvironment where
  settingsParser = parseRegtoolEnvironment

{-# ANN parseRegtoolEnvironment ("NOCOVER" :: String) #-}
parseRegtoolEnvironment :: Parser RegtoolEnvironment
parseRegtoolEnvironment = do
  setting
    [ help "Environment",
      reader auto,
      name "environment",
      metavar "ENV",
      value EnvManualTesting
    ]

data Regtool = Regtool
  { -- | The environment to testing it
    regtoolEnvironment :: !RegtoolEnvironment,
    -- | The approot to use
    regtoolApproot :: !(Maybe Text),
    -- | The pool of database connections
    regtoolConnectionPool :: !ConnectionPool,
    -- | Settings concerning static files
    regtoolStatic :: !EmbeddedStatic,
    -- | The config to use for stripe requests
    regtoolHttpManager :: !Http.Manager,
    -- This must be 'Nothing' in automated tests, but never in any other environment.
    regtoolStripeSettings :: !(Maybe StripeSettings),
    -- | Where to put the client session key
    regtoolClientSessionKeyFile :: !(Path Abs File),
    -- | Where to put the admin config file
    regtoolAdminConfigFile :: !(Path Abs File),
    -- | Due date for unpaid invoice
    regtoolNotPaid :: !(Maybe Day)
  }

data StripeSettings = StripeSettings
  { stripeSettingSecretKey :: !Text,
    stripeSettingPublishableKey :: !Text
  }
  deriving (Show)

instance HasParser StripeSettings where
  settingsParser = parseStripeSettings

{-# ANN parseStripeSettings ("NOCOVER" :: String) #-}
parseStripeSettings :: Parser StripeSettings
parseStripeSettings = do
  stripeSettingPublishableKey <-
    setting
      [ help "Publishable key",
        reader str,
        name "publishable-key",
        metavar "PUBLISHABLE_KEY"
      ]
  stripeSettingSecretKey <-
    setting
      [ help "Secret key",
        reader str,
        name "secret-key",
        metavar "SECRET_KEY"
      ]
  pure StripeSettings {..}

mkYesodData "Regtool" $(parseRoutesFile "routes.txt")
