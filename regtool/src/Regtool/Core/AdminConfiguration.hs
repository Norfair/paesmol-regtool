{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Core.AdminConfiguration where

import qualified Data.ByteString as SB
import Data.Yaml as Yaml
import Import
import Regtool.Calculation.Costs.Config
import Regtool.Core.Foundation

data Configuration = Configuration
  { confCostsConfig :: !CostsConfig,
    confChildrenLevelResetDay :: !(Maybe Day)
  }
  deriving (Show, Eq, Generic)

instance Validity Configuration

instance FromJSON Configuration where
  parseJSON =
    withObject "Configuration" $ \o ->
      Configuration <$> o .: "costs" <*> o .:? "children-level-reset-day"

instance ToJSON Configuration where
  toJSON Configuration {..} =
    object ["costs" .= confCostsConfig, "children-level-reset-day" .= confChildrenLevelResetDay]

defaultConfiguration :: Configuration
defaultConfiguration =
  Configuration {confCostsConfig = defaultCostsConfig, confChildrenLevelResetDay = Nothing}

getConfiguration :: Handler Configuration
getConfiguration = do
  p <- getsYesod regtoolAdminConfigFile
  mcontents <- liftIO $ forgivingAbsence $ SB.readFile $ fromAbsFile p
  case mcontents of
    Nothing -> pure defaultConfiguration
    Just contents ->
      case Yaml.decodeEither' contents of
        Left err -> do
          setMessage
            $ fromString
            $ "Something went wrong while loading config file, using default configuration instead: "
            <> show err
          let c = defaultConfiguration
          putConfiguration c
          pure c
        Right c -> pure c

putConfiguration :: Configuration -> Handler ()
putConfiguration conf = do
  p <- getsYesod regtoolAdminConfigFile
  putConfigurationIn p conf

putConfigurationIn :: (MonadIO m) => Path Abs File -> Configuration -> m ()
putConfigurationIn p conf = liftIO $ Yaml.encodeFile (fromAbsFile p) conf
