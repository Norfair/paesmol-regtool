{-# LANGUAGE CPP #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Core.Static where

import Regtool.Core.Constants
import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Remote

mkEmbeddedStatic
  development
  "regtoolEmbeddedStatic"
  [ embedRemoteFile
      "static/bootstrap.css"
      "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
    embedRemoteFile "static/jquery.js" "https://code.jquery.com/jquery-3.2.1.min.js",
    embedRemoteFile
      "static/bootstrap.js"
      "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js",
    embedRemoteFile
      "static/font-awesome.css"
      "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
    embedRemoteFile
      "fonts/fontawesome-webfont.ttf"
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf",
    embedRemoteFile
      "fonts/fontawesome-webfont.woff"
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff",
    embedRemoteFile
      "fonts/fontawesome-webfont.woff2"
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2",
    embedFileAt "logo-H.jpg" "static/logo-H.jpg",
    embedFileAt "logo-V.jpg" "static/logo-V.jpg",
    embedFileAt "default.js" "static/default.js",
    embedFileAt "default.css" "static/default.css",
    embedFileAt "favicon.ico" "static/favicon.ico",
    embedFileAt "privacy.pdf" "static/privacy.pdf",
    embedFileAt "transport_rules.pdf" "static/transport_rules.pdf"
  ]
