module Regtool.Core.Widget where

import Data.Default
import Import
import Language.Haskell.TH.Syntax (Exp, Q)
import Yesod.Default.Util (WidgetFileSettings, widgetFileNoReload)

widgetFile :: String -> Q Exp
widgetFile = widgetFileNoReload widgetFileSettings

-- We don't use reloading widgets because it doesn't work well together with being able to run tests.
--
-- if development
--   then widgetFileReload widgetFileSettings
--   else widgetFileNoReload widgetFileSettings

widgetFileSettings :: WidgetFileSettings
widgetFileSettings = def
