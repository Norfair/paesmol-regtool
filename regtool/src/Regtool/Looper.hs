{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Looper
  ( runAllLoopers,
  )
where

import Control.Monad.Logger
import qualified Data.Text as T
import Import
import Looper
import Regtool.Looper.Emailer
import Regtool.Looper.GarbageCollector
import Regtool.Looper.Types
import Regtool.Looper.Verifier
import Regtool.OptParse

runAllLoopers :: Settings -> LooperEnv -> LoggingT IO ()
runAllLoopers sets looperEnv =
  flip runReaderT looperEnv
    $ runLoopersIgnoreOverrun customRunner
    $ regtoolLoopers
    $ settingLoopers sets
  where
    customRunner ld = do
      logDebugNS (looperDefName ld) "Starting"
      start <- liftIO getCurrentTime
      looperDefFunc ld
      end <- liftIO getCurrentTime
      logDebugNS (looperDefName ld) $ "Done, took " <> T.pack (show (diffUTCTime end start))

regtoolLoopers :: LoopersSettings -> [LooperDef Looper]
regtoolLoopers LoopersSettings {..} =
  [ mkLooperDef "emailer" loopersSetsEmailer emailLooper,
    mkLooperDef "verifier" loopersSetsVerifier verifyLooper,
    mkLooperDef "garbage-collector" loopersSetsGarbageCollector garbageCollectorLooper
  ]
