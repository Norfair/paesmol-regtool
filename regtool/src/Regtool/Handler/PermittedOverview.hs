{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.PermittedOverview
  ( getPermittedOverviewR,
  )
where

import Regtool.Handler.Import

getPermittedOverviewR :: Handler Html
getPermittedOverviewR = do
  aid <- requireAuthId
  mPerm <- runDB $ getBy $ UniquePermissionAccount aid
  let hasPermission func = (func . entityVal <$> mPerm) == Just True
  withNavBar $(widgetFile "permitted-overview")
