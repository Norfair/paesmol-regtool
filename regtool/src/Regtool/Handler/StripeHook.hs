{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.StripeHook
  ( postStripeHookR,
  )
where

import Control.Monad.Logger
import Data.Aeson as JSON
import Data.Aeson.Types as JSON
import qualified Data.Text as T
import Data.Text.Encoding as TE
import Database.Persist
import Database.Persist.Sqlite
import Import
import Network.HTTP.Client as HTTP
import Network.HTTP.Types as HTTP
import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Cache
import Regtool.Calculation.Costs.Invoice
import Regtool.Data
import Regtool.Handler.Import
import Regtool.Utils.Stripe
import StripeClient (Nullable (..))
import qualified StripeClient as Stripe

postStripeHookR :: Handler ()
postStripeHookR = requireCheckJsonBody >>= handleEvent

handleEvent :: Stripe.Event -> Handler ()
handleEvent Stripe.Event {..} =
  case eventType of
    "checkout.session.completed" ->
      case JSON.parseEither parseJSON (JSON.Object (Stripe.notificationEventDataObject eventData)) of
        Right session -> fullfillSession eventId session
        Left err -> logErrorNS "stripe-webhook" $ T.pack $ unlines ["Unable to parse event data into a session object:", err]
    _ -> logInfoNS "stripe-webhook" $ T.pack $ unwords ["Not handling event of type:", show eventType]

fullfillSession :: Text -> Stripe.Checkout'session -> Handler ()
fullfillSession eventId session =
  let sessionId = Stripe.checkout'sessionId session
   in case Stripe.checkout'sessionPaymentIntent session of
        Just (NonNull pi_) -> do
          paymentIntent <- case pi_ of
            Stripe.Checkout'sessionPaymentIntent'NonNullablePaymentIntent paymentIntent -> pure paymentIntent
            Stripe.Checkout'sessionPaymentIntent'NonNullableText paymentIntentId -> do
              getPaymentIntentsIntentResponse <- runStripe $ Stripe.getPaymentIntentsIntent $ Stripe.mkGetPaymentIntentsIntentParameters paymentIntentId
              case HTTP.responseBody getPaymentIntentsIntentResponse of
                Stripe.GetPaymentIntentsIntentResponseError err -> sendResponseStatus HTTP.status500 $ unlines ["Could not get payment intent:", show err]
                Stripe.GetPaymentIntentsIntentResponseDefault err -> sendResponseStatus HTTP.status500 $ unlines ["Could not get payment intent:", show err]
                Stripe.GetPaymentIntentsIntentResponse200 paymentIntent -> pure paymentIntent
          completePayment eventId sessionId paymentIntent
        _ -> logErrorNS "stripe-webhook" $ T.pack $ unwords ["Session did not contain a payment intent", show sessionId]

completePayment :: Text -> Text -> Stripe.PaymentIntent -> Handler ()
completePayment eventId sessionId paymentIntent = do
  mcc <- runDB $ getBy $ UniqueShoppingCartSession $ Just sessionId
  case mcc of
    Nothing -> logErrorNS "stripe-webhook" $ "Choices not found for stripe event: " <> eventId <> " and session " <> sessionId
    Just (Entity cartId sc) -> do
      let err :: Text -> Handler ()
          err str = do
            runDB $ update cartId [ShoppingCartStatus =. Just CartPaymentFailed]
            logErrorNS "stripe-webhook" str
      case decodeShoppingCart sc of
        Nothing -> err $ "Unable to decode choices: " <> TE.decodeUtf8 (shoppingCartJson sc)
        Just choices -> do
          let aid = shoppingCartAccount sc
          ma <- runDB $ get aid
          case ma of
            Nothing -> err $ "Account not found for choices cache: " <> T.pack (show (fromSqlKey aid))
            Just a -> do
              now <- liftIO getCurrentTime
              case Stripe.paymentIntentCharges paymentIntent of
                Nothing -> err "No charges field in payment intent."
                Just paymentIntentCharges' ->
                  case Stripe.paymentIntentCharges'Data paymentIntentCharges' of
                    [] -> err "No charges yet"
                    [charge] -> do
                      mscid <- runDB $ updateCustomerIfNeededBasedOnCharge aid charge
                      case mscid of
                        Nothing -> err "Neither a customer was created on their end, nor did one exist on our end."
                        Just scid ->
                          runDB $ do
                            spid <-
                              insertValid
                                StripePayment
                                  { stripePaymentAmount = invoiceTotal $ deriveInvoice choices,
                                    stripePaymentCustomer = scid,
                                    stripePaymentCharge = Stripe.chargeId charge,
                                    stripePaymentTimestamp = now,
                                    stripePaymentSession = Nothing,
                                    stripePaymentEvent = Nothing,
                                    stripePaymentPaymentIntent = Nothing
                                  }
                            Entity checkoutId _ <- makeCheckout aid choices $ Just spid
                            let pcs = paymentConsequences aid checkoutId now choices
                            realisePaymentConsequences pcs
                            sendPaymentReceivedEmail (Entity aid a) checkoutId
                            update cartId [ShoppingCartStatus =. Just CartPaymentSucceeded]
                    _ -> err "More than one charge"

updateCustomerIfNeededBasedOnCharge ::
  (MonadIO m) => AccountId -> Stripe.Charge -> SqlPersistT m (Maybe StripeCustomerId)
updateCustomerIfNeededBasedOnCharge aid charge =
  let lookupOurs = do
        msc <- selectFirst [StripeCustomerAccount ==. aid] [Desc StripeCustomerId]
        forM msc $ pure . entityKey
   in case Stripe.chargeCustomer charge of
        Just (NonNull cc) -> do
          mCustomerId <- case cc of
            Stripe.ChargeCustomer'NonNullableText customerId -> pure (Just customerId)
            Stripe.ChargeCustomer'NonNullableCustomer customer -> pure (Just (Stripe.customerId customer))
            Stripe.ChargeCustomer'NonNullableDeletedCustomer _ -> pure Nothing
          case mCustomerId of
            Nothing -> lookupOurs
            Just customerId ->
              Just
                . entityKey
                <$> upsertBy
                  (UniqueAccountCustomer aid (Just customerId))
                  (StripeCustomer {stripeCustomerAccount = aid, stripeCustomerIdentifier = Just customerId})
                  []
        _ -> lookupOurs

sendPaymentReceivedEmail :: (MonadIO m) => Entity Account -> CheckoutId -> SqlPersistT m ()
sendPaymentReceivedEmail (Entity aid Account {..}) cid = do
  now <- liftIO getCurrentTime
  insert_
    PaymentReceivedEmail
      { paymentReceivedEmailTo = accountEmailAddress,
        paymentReceivedEmailAccount = aid,
        paymentReceivedEmailCheckout = cid,
        paymentReceivedEmailTimestamp = now,
        paymentReceivedEmailEmail = Nothing
      }
