{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Overview
  ( getOverviewR,
  )
where

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Readiness

getOverviewR :: Handler Html
getOverviewR = do
  accountEntity <- requireAuth
  Readiness {..} <- getReadiness accountEntity
  withNavBar $(widgetFile "overview")
