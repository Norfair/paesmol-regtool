{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Semestre
  ( RegisterDaycareSemestre (..),
    getAdminRegisterDaycareSemestreR,
    postAdminRegisterDaycareSemestreR,
    getAdminDaycareSemestresR,
    postAdminDaycareSemestreDeleteR,
    getAdminDaycareSemestreR,
    getAdminDaycareSemestreOverviewR,
    postAdminDaycareSemestreStatusR,
  )
where

import qualified Database.Persist as DB
import Regtool.Handler.Import

getAdminDaycareSemestresR :: Handler Html
getAdminDaycareSemestresR = do
  daycareSemestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  token <- genToken
  withNavBar $(widgetFile "admin/daycare/semestres")

data RegisterDaycareSemestre = RegisterDaycareSemestre
  { registerDaycareSemestreSchoolYearAndSemestre :: (SchoolYear, Semestre),
    registerDaycareSemestreOpen :: Bool,
    registerDaycareSemestreId :: Maybe DaycareSemestreId
  }

registerDaycareSemestreForm ::
  [SchoolYear] -> Maybe (Entity DaycareSemestre) -> AForm Handler RegisterDaycareSemestre
registerDaycareSemestreForm sys mce =
  RegisterDaycareSemestre
    <$> areq
      ( selectFieldList
          $ map
            (\(sy, sem) -> (schoolYearSemestreText sy sem, (sy, sem)))
            ((,) <$> sys <*> [minBound .. maxBound])
      )
      (bfs ("Semestre" :: Text))
      ((,) <$> dv daycareSemestreYear <*> dv daycareSemestreSemestre)
    <*> areq checkBoxField (bfs ("Registrations open" :: Text)) (dv daycareSemestreOpen)
    <*> aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (DaycareSemestre -> a) -> Maybe a
    dv func = func . entityVal <$> mce

getAdminRegisterDaycareSemestreR :: Handler Html
getAdminRegisterDaycareSemestreR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  makeRegisterForm "Daycare Semestre" (AdminR AdminRegisterDaycareSemestreR)
    $ registerDaycareSemestreForm sys Nothing

postAdminRegisterDaycareSemestreR :: Handler Html
postAdminRegisterDaycareSemestreR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  processRegisterForm
    "Daycare Semestre"
    (AdminR AdminRegisterDaycareSemestreR)
    (registerDaycareSemestreForm sys Nothing)
    $ \RegisterDaycareSemestre {..} -> do
      let (registerDaycareSemestreSchoolYear, registerDaycareSemestreSemestre) =
            registerDaycareSemestreSchoolYearAndSemestre
      now <- liftIO getCurrentTime
      let daycareSemestre_ =
            DaycareSemestre
              { daycareSemestreYear = registerDaycareSemestreSchoolYear,
                daycareSemestreSemestre = registerDaycareSemestreSemestre,
                daycareSemestreOpen = registerDaycareSemestreOpen,
                daycareSemestreCreationTime = now
              }
      case registerDaycareSemestreId of
        Just i -> do
          runDB $ replaceValid i daycareSemestre_
          redirect $ AdminR AdminDaycareSemestresR
        Nothing -> do
          did <- runDB $ insertValid daycareSemestre_
          redirect $ AdminR $ AdminDaycareSemestreR did

getAdminDaycareSemestreR :: DaycareSemestreId -> Handler Html
getAdminDaycareSemestreR bid = do
  daycareSemestre <- runDB $ get404 bid
  sys <- liftIO getCurrentAndNextSchoolYears
  makeEditForm
    "Daycare Semestre"
    (AdminR AdminRegisterDaycareSemestreR)
    (AdminR AdminDaycareSemestresR)
    (registerDaycareSemestreForm sys $ Just $ Entity bid daycareSemestre)

getAdminDaycareSemestreOverviewR :: DaycareSemestreId -> Handler Html
getAdminDaycareSemestreOverviewR sid = do
  daycareSemestre <- runDB $ get404 sid
  withNavBar $(widgetFile "admin/daycare/semestre-overview")

postAdminDaycareSemestreDeleteR :: DaycareSemestreId -> Handler Html
postAdminDaycareSemestreDeleteR lsid = do
  canDeleteDaycareSemestre lsid >>= deleteOrError
  redirect $ AdminR AdminDaycareSemestresR

canDeleteDaycareSemestre :: DaycareSemestreId -> Handler (Deletable DaycareSemestreId)
canDeleteDaycareSemestre dcsid = do
  mc <- runDB $ selectFirst [DaycareTimeslotSemestre ==. dcsid] []
  ma <- runDB $ selectFirst [DaycareActivitySemestre ==. dcsid] []
  pure
    $ case (() <$ mc) <|> (() <$ ma) of
      Nothing -> CanDelete $ DB.delete dcsid
      Just () -> CannotDelete ["There is still a timeslot or activity in this daycare semestre."]

postAdminDaycareSemestreStatusR :: DaycareSemestreId -> Bool -> Handler Html
postAdminDaycareSemestreStatusR dsid open = do
  runDB $ DB.update dsid [DaycareSemestreOpen DB.=. open]
  redirect $ AdminR AdminDaycareSemestresR
