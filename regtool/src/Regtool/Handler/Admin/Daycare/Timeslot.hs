{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Timeslot
  ( RegisterDaycareTimeslot (..),
    getAdminRegisterDaycareTimeslotR,
    postAdminRegisterDaycareTimeslotR,
    getAdminDaycareTimeslotsR,
    postAdminDaycareTimeslotDeleteR,
    getAdminDaycareTimeslotR,
    getAdminDaycareTimeslotOverviewR,
  )
where

import qualified Data.Set as S
import qualified Database.Persist as DB
import Regtool.Calculation.Time
import Regtool.Handler.Import

getAdminDaycareTimeslotsR :: DaycareSemestreId -> Handler Html
getAdminDaycareTimeslotsR sid = do
  daycareTimeslots <- runDB $ selectList [DaycareTimeslotSemestre ==. sid] [Asc DaycareTimeslotId]
  token <- genToken
  withNavBar $(widgetFile "admin/daycare/timeslots")

data RegisterDaycareTimeslot = RegisterDaycareTimeslot
  { registerDaycareTimeslotSchoolDay :: SchoolDay,
    registerDaycareTimeslotStart :: TimeOfDay,
    registerDaycareTimeslotEnd :: TimeOfDay,
    registerDaycareTimeslotFee :: Amount,
    registerDaycareTimeslotOccasionalFee :: Amount,
    registerDaycareTimeslotAccessible :: Set SchoolLevel,
    registerDaycareTimeslotId :: Maybe DaycareTimeslotId
  }

registerDaycareTimeslotForm ::
  Maybe (Entity DaycareTimeslot) -> AForm Handler RegisterDaycareTimeslot
registerDaycareTimeslotForm mce =
  RegisterDaycareTimeslot
    <$> areq (selectField optionsEnum) (bfs ("School Day" :: Text)) (dv daycareTimeslotSchoolDay)
    <*> areq timeFieldTypeTime (bfs ("Start time" :: Text)) (dv daycareTimeslotStart)
    <*> areq timeFieldTypeTime (bfs ("End time" :: Text)) (dv daycareTimeslotEnd)
    <*> areq amountField (bfs ("Fee" :: Text)) (dv daycareTimeslotFee)
    <*> areq amountField (bfs ("Fee for occasional use" :: Text)) (dv daycareTimeslotOccasionalFee)
    <*> accessibilityForm mce
    <*> aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (DaycareTimeslot -> a) -> Maybe a
    dv func = func . entityVal <$> mce

accessibilityForm :: Maybe (Entity DaycareTimeslot) -> AForm Handler (Set SchoolLevel)
accessibilityForm mce =
  fmap (S.fromList . catMaybes)
    $ sequenceA
    $ flip map [minBound .. maxBound]
    $ \sl ->
      ( \b ->
          if b
            then Just sl
            else Nothing
      )
        <$> areq
          checkBoxField
          (bfs $ schoolLevelText sl)
          ((sl `elem`) . daycareTimeslotAccessible . entityVal <$> mce)

getAdminRegisterDaycareTimeslotR :: DaycareSemestreId -> Handler Html
getAdminRegisterDaycareTimeslotR sid =
  makeRegisterForm "Daycare Timeslot" (AdminR $ AdminRegisterDaycareTimeslotR sid)
    $ registerDaycareTimeslotForm Nothing

postAdminRegisterDaycareTimeslotR :: DaycareSemestreId -> Handler Html
postAdminRegisterDaycareTimeslotR sid =
  processRegisterForm
    "Daycare Timeslot"
    (AdminR $ AdminRegisterDaycareTimeslotR sid)
    (registerDaycareTimeslotForm Nothing)
    $ \RegisterDaycareTimeslot {..} -> do
      now <- liftIO getCurrentTime
      let daycareTimeslot_ =
            DaycareTimeslot
              { daycareTimeslotSemestre = sid,
                daycareTimeslotSchoolDay = registerDaycareTimeslotSchoolDay,
                daycareTimeslotStart = registerDaycareTimeslotStart,
                daycareTimeslotEnd = registerDaycareTimeslotEnd,
                daycareTimeslotFee = registerDaycareTimeslotFee,
                daycareTimeslotOccasionalFee = registerDaycareTimeslotOccasionalFee,
                daycareTimeslotAccessible = registerDaycareTimeslotAccessible,
                daycareTimeslotCreationTime = now
              }
      case registerDaycareTimeslotId of
        Just i -> do
          runDB $ replaceValid i daycareTimeslot_
          redirect $ AdminR $ AdminDaycareTimeslotsR sid
        Nothing -> do
          did <- runDB $ insertValid daycareTimeslot_
          redirect $ AdminR $ AdminDaycareTimeslotR sid did

getAdminDaycareTimeslotR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
getAdminDaycareTimeslotR sid tid = do
  daycareTimeslot <- runDB $ get404 tid
  makeEditForm
    "Daycare Timeslot"
    (AdminR $ AdminRegisterDaycareTimeslotR sid)
    (AdminR $ AdminDaycareTimeslotsR sid)
    (registerDaycareTimeslotForm $ Just $ Entity tid daycareTimeslot)

getAdminDaycareTimeslotOverviewR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
getAdminDaycareTimeslotOverviewR _ tid = do
  daycareTimeslot <- runDB $ get404 tid
  withNavBar $(widgetFile "admin/daycare/timeslot-overview")

postAdminDaycareTimeslotDeleteR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
postAdminDaycareTimeslotDeleteR sid tid = do
  canDeleteDaycareTimeslot tid >>= deleteOrError
  redirect $ AdminR $ AdminDaycareTimeslotsR sid

canDeleteDaycareTimeslot :: DaycareTimeslotId -> Handler (Deletable DaycareTimeslotId)
canDeleteDaycareTimeslot dcsid = do
  mc <- runDB $ selectFirst [DaycareSignupTimeslot ==. dcsid] []
  pure
    $ case mc of
      Nothing -> CanDelete $ DB.delete dcsid
      Just _ -> CannotDelete ["There are still registrations for this time slot."]
