{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Activity
  ( getAdminRegisterDaycareActivityR,
    postAdminRegisterDaycareActivityR,
    getAdminDaycareActivitiesR,
    postAdminDaycareActivityDeleteR,
    getAdminDaycareActivityR,
    getAdminDaycareActivityOverviewR,
  )
where

import qualified Data.Set as S
import qualified Database.Persist as DB
import Regtool.Calculation.Time
import Regtool.Handler.Import
import Regtool.Utils.Signup.Daycare

getAdminDaycareActivitiesR :: DaycareSemestreId -> Handler Html
getAdminDaycareActivitiesR sid = do
  daycareActivities <- runDB $ do
    das <- selectList [DaycareActivitySemestre ==. sid] [Asc DaycareActivityId]
    forM das $ \da@(Entity daid _) -> do
      spacesTaken <- count [DaycareActivitySignupActivity ==. daid]
      pure (da, fromIntegral spacesTaken :: Word)
  token <- genToken
  withNavBar $(widgetFile "admin/daycare/activities")

data RegisterDaycareActivity = RegisterDaycareActivity
  { registerDaycareActivityTimeslot :: DaycareTimeslotId,
    registerDaycareActivitySchoolDay :: SchoolDay,
    registerDaycareActivityStart :: TimeOfDay,
    registerDaycareActivityEnd :: TimeOfDay,
    registerDaycareActivityName :: Text,
    registerDaycareActivityFee :: Amount,
    registerDaycareActivityAccessible :: Set SchoolLevel,
    registerDaycareActivitySpaces :: Maybe Word,
    registerDaycareActivityId :: Maybe DaycareActivityId
  }

registerDaycareActivityForm ::
  Maybe (Entity DaycareActivity) ->
  [(Entity DaycareSemestre, Entity DaycareTimeslot)] ->
  AForm Handler RegisterDaycareActivity
registerDaycareActivityForm mce tups =
  RegisterDaycareActivity
    <$> areq
      ( selectFieldList
          $ map (\(Entity _ s, Entity tsid ts) -> (timeslotDescription s ts, tsid)) tups
      )
      (bfs ("Timeslot" :: Text))
      Nothing
    <*> areq (selectField optionsEnum) (bfs ("School Day" :: Text)) (dv daycareActivitySchoolDay)
    <*> areq timeFieldTypeTime (bfs ("Start time" :: Text)) (dv daycareActivityStart)
    <*> areq timeFieldTypeTime (bfs ("End time" :: Text)) (dv daycareActivityEnd)
    <*> areq textField (bfs ("Name" :: Text)) (dv daycareActivityName)
    <*> areq amountField (bfs ("Fee" :: Text)) (dv daycareActivityFee)
    <*> accessibilityForm mce
    <*> aopt intField (bfs ("Spaces" :: Text)) (dv daycareActivitySpaces)
    <*> aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (DaycareActivity -> a) -> Maybe a
    dv func = func . entityVal <$> mce

accessibilityForm :: Maybe (Entity DaycareActivity) -> AForm Handler (Set SchoolLevel)
accessibilityForm mce =
  fmap (S.fromList . catMaybes)
    $ sequenceA
    $ flip map [minBound .. maxBound]
    $ \sl ->
      ( \b ->
          if b
            then Just sl
            else Nothing
      )
        <$> areq
          checkBoxField
          (bfs $ schoolLevelText sl)
          ((sl `elem`) . daycareActivityAccessible . entityVal <$> mce)

getAdminRegisterDaycareActivityR :: DaycareSemestreId -> Handler Html
getAdminRegisterDaycareActivityR sid = do
  semestre <- runDB $ get404 sid
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let tups = timeslotsTups [Entity sid semestre] timeslots
  makeRegisterForm "Daycare Activity" (AdminR $ AdminRegisterDaycareActivityR sid)
    $ registerDaycareActivityForm Nothing tups

postAdminRegisterDaycareActivityR :: DaycareSemestreId -> Handler Html
postAdminRegisterDaycareActivityR sid = do
  semestre <- runDB $ get404 sid
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let tups = timeslotsTups [Entity sid semestre] timeslots
  processRegisterForm
    "Daycare Activity"
    (AdminR $ AdminRegisterDaycareActivityR sid)
    (registerDaycareActivityForm Nothing tups)
    $ \RegisterDaycareActivity {..} -> do
      now <- liftIO getCurrentTime
      let daycareActivity_ =
            DaycareActivity
              { daycareActivitySemestre = sid,
                daycareActivityTimeslot = registerDaycareActivityTimeslot,
                daycareActivitySchoolDay = registerDaycareActivitySchoolDay,
                daycareActivityStart = registerDaycareActivityStart,
                daycareActivityEnd = registerDaycareActivityEnd,
                daycareActivityFee = registerDaycareActivityFee,
                daycareActivityName = registerDaycareActivityName,
                daycareActivityAccessible = registerDaycareActivityAccessible,
                daycareActivitySpaces = registerDaycareActivitySpaces,
                daycareActivityCreationTime = now
              }
      case registerDaycareActivityId of
        Just i -> do
          runDB $ replaceValid i daycareActivity_
          redirect $ AdminR $ AdminDaycareActivitiesR sid
        Nothing -> do
          did <- runDB $ insertValid daycareActivity_
          redirect $ AdminR $ AdminDaycareActivityR sid did

getAdminDaycareActivityR :: DaycareSemestreId -> DaycareActivityId -> Handler Html
getAdminDaycareActivityR sid bid = do
  daycareActivity <- runDB $ get404 bid
  semestre <- runDB $ get404 sid
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let tups = timeslotsTups [Entity sid semestre] timeslots
  makeEditForm
    "Daycare Activity"
    (AdminR $ AdminRegisterDaycareActivityR sid)
    (AdminR $ AdminDaycareActivitiesR sid)
    (registerDaycareActivityForm (Just $ Entity bid daycareActivity) tups)

getAdminDaycareActivityOverviewR :: DaycareSemestreId -> DaycareActivityId -> Handler Html
getAdminDaycareActivityOverviewR _ tid = do
  daycareActivity <- runDB $ get404 tid
  withNavBar $(widgetFile "admin/daycare/activity-overview")

postAdminDaycareActivityDeleteR :: DaycareSemestreId -> DaycareActivityId -> Handler Html
postAdminDaycareActivityDeleteR sid lsid = do
  canDeleteDaycareActivity lsid >>= deleteOrError
  redirect $ AdminR $ AdminDaycareActivitiesR sid

canDeleteDaycareActivity :: DaycareActivityId -> Handler (Deletable DaycareActivityId)
canDeleteDaycareActivity dcaid = do
  mc <- runDB $ selectFirst [DaycareActivitySignupActivity ==. dcaid] []
  pure
    $ case mc of
      Nothing -> CanDelete $ DB.delete dcaid
      Just _ -> CannotDelete ["There is still a signup for this daycare activity."]
