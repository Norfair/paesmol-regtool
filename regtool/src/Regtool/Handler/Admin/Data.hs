{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Data
  ( getAdminDataR,
    postAdminExportDataR,
  )
where

import Codec.Archive.Zip
import Data.Csv
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Regtool.Handler.Import

getAdminDataR :: Handler Html
getAdminDataR = do
  token <- genToken
  rows <- runDB getPerChildData
  withNavBar $(widgetFile "admin/data")

postAdminExportDataR :: Handler TypedContent
postAdminExportDataR = do
  rows <- runDB getPerChildData
  let archive = addEntryToArchive (toEntry "data.csv" 0 (encode rows)) emptyArchive
  addHeader "Content-Disposition"
    $ T.concat ["attachment; filename=\"", "regtool-per-child-data.zip", "\""]
  sendResponse (TE.encodeUtf8 "application/zip", toContent $ fromArchive archive)

data Row = Row
  { rowFirstName :: Text,
    rowLastName :: Text,
    rowSchoolLevel :: SchoolLevel,
    rowSection :: Maybe Text,
    rowContactFirstName :: Maybe Text,
    rowContactLastName :: Maybe Text,
    rowContactAddressLine1 :: Maybe Text,
    rowContactAddressLine2 :: Maybe Text,
    rowContactPostalCode :: Maybe Text,
    rowContactCity :: Maybe Text,
    rowContactCountry :: Maybe Text,
    rowContactEmail1 :: Maybe EmailAddress,
    rowContactEmail2 :: Maybe EmailAddress,
    rowContactCompany :: Maybe Company,
    rowContactPhoneNumber1 :: Maybe Text,
    rowContactPhoneNumber2 :: Maybe Text,
    rowUsesBus :: Maybe Text,
    rowUsesDaycare :: Text
  }
  deriving (Generic)

instance ToRecord Row

getPerChildData :: (MonadIO m) => ReaderT SqlBackend m [Row]
getPerChildData = do
  children <- selectList [] [Asc ChildId]
  languageSections <- selectList [] [Asc LanguageSectionId]
  accounts <- selectList [] [Asc AccountId]
  parents <- selectList [] [Asc ParentId]
  childOfs <- selectList [] [Asc ChildOfId]
  transportSignups <- selectList [] [Asc TransportSignupId]
  busStops <- selectList [] [Asc BusStopId]
  daycareSignups <- selectList [] [Asc DaycareSignupId]
  pure
    $ sortBy (comparing rowLastName <> comparing rowFirstName)
    $ flip map children
    $ \(Entity cid Child {..}) ->
      let myChildOfs = filter ((== cid) . childOfChild . entityVal) childOfs
          myAccounts =
            filter ((`elem` map (childOfParent . entityVal) myChildOfs) . entityKey) accounts :: [Entity Account]
          myParents = filter ((`elem` map entityKey myAccounts) . parentAccount . entityVal) parents
          firstParent = entityVal <$> headMay myParents
          usesBus = do
            ts <- find ((== cid) . transportSignupChild . entityVal) transportSignups
            Entity _ bs <- find ((== transportSignupBusStop (entityVal ts)) . entityKey) busStops
            pure $ T.unwords [unTextarea $ busStopCity bs, unTextarea $ busStopLocation bs]
          usesDaycare =
            T.pack
              . show
              $ isJust
              $ find ((== cid) . daycareSignupChild . entityVal) daycareSignups
       in Row
            { rowFirstName = childFirstName,
              rowLastName = childLastName,
              rowSchoolLevel = childLevel,
              rowSection =
                languageSectionName
                  . entityVal
                  <$> find ((childLanguageSection ==) . entityKey) languageSections,
              rowContactFirstName = parentFirstName <$> firstParent,
              rowContactLastName = parentLastName <$> firstParent,
              rowContactAddressLine1 = parentAddressLine1 <$> firstParent,
              rowContactAddressLine2 = parentAddressLine2 =<< firstParent,
              rowContactPostalCode = parentPostalCode <$> firstParent,
              rowContactCity = parentCity <$> firstParent,
              rowContactCountry = countryText . parentCountry <$> firstParent,
              rowContactEmail1 = parentEmail1 <$> firstParent,
              rowContactEmail2 = parentEmail2 =<< firstParent,
              rowContactCompany = parentCompany <$> firstParent,
              rowContactPhoneNumber1 = parentPhoneNumber1 <$> firstParent,
              rowContactPhoneNumber2 = parentPhoneNumber2 =<< firstParent,
              rowUsesBus = usesBus,
              rowUsesDaycare = usesDaycare
            }
