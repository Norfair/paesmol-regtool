{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Admin.Signup.Transport where

import Database.Persist
import Regtool.Handler.Import
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupTransportDeleteR :: TransportSignupId -> Handler Html
postAdminSignupTransportDeleteR dsid = do
  mte <- runDB $ getBy $ UniqueTransportEnrollment dsid
  case mte of
    Nothing -> runDB $ delete dsid
    Just (Entity teid TransportEnrollment {..}) -> do
      payments <-
        runDB $ selectList [TransportPaymentPaymentPlan ==. transportEnrollmentPaymentPlan] []
      if null payments -- Unpaid
        then do
          runDB $ do
            delete teid
            delete transportEnrollmentPaymentPlan
            delete dsid
        else do
          runDB $ update dsid [TransportSignupDeletedByAdmin =. True]
  redirectToAccount dsid transportSignupChild

postAdminSignupTransportUnDeleteR :: TransportSignupId -> Handler Html
postAdminSignupTransportUnDeleteR dsid = do
  runDB $ update dsid [TransportSignupDeletedByAdmin =. False]
  redirectToAccount dsid transportSignupChild
