module Regtool.Handler.Admin.Signup.Daycare where

import Regtool.Handler.Import
import Regtool.Utils.Signup.Delete (deleteIfPaid)
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupDaycareDeleteR :: DaycareSignupId -> Handler Html
postAdminSignupDaycareDeleteR dsid = do
  mPayment <- runDB $ selectFirst [DaycarePaymentSignup ==. dsid] []
  deleteIfPaid dsid mPayment DaycareSignupDeletedByAdmin daycareSignupChild

postAdminSignupDaycareUnDeleteR :: DaycareSignupId -> Handler Html
postAdminSignupDaycareUnDeleteR dsid = do
  runDB $ update dsid [DaycareSignupDeletedByAdmin =. False]
  redirectToAccount dsid daycareSignupChild
