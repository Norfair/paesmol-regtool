module Regtool.Handler.Admin.Signup.OccasionalTransport where

import Regtool.Handler.Import
import Regtool.Utils.Signup.Delete (deleteIfPaid)
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupOccasionalTransportDeleteR :: OccasionalTransportSignupId -> Handler Html
postAdminSignupOccasionalTransportDeleteR odsid = do
  mPayment <- runDB $ selectFirst [OccasionalTransportPaymentSignup ==. odsid] []
  deleteIfPaid odsid mPayment OccasionalTransportSignupDeletedByAdmin occasionalTransportSignupChild

postAdminSignupOccasionalTransportUnDeleteR :: OccasionalTransportSignupId -> Handler Html
postAdminSignupOccasionalTransportUnDeleteR odsid = do
  runDB $ update odsid [OccasionalTransportSignupDeletedByAdmin =. False]
  redirectToAccount odsid occasionalTransportSignupChild
