module Regtool.Handler.Admin.Signup.CustomActivity where

import Regtool.Handler.Import
import Regtool.Utils.Signup.Delete (deleteIfPaid)
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupCustomActivityDeleteR :: CustomActivitySignupId -> Handler Html
postAdminSignupCustomActivityDeleteR casid = do
  mPayment <- runDB $ selectFirst [CustomActivityPaymentSignup ==. casid] []
  deleteIfPaid casid mPayment CustomActivitySignupDeletedByAdmin customActivitySignupChild

postAdminSignupCustomActivityUnDeleteR :: CustomActivitySignupId -> Handler Html
postAdminSignupCustomActivityUnDeleteR casid = do
  runDB $ update casid [CustomActivitySignupDeletedByAdmin =. False]
  redirectToAccount casid customActivitySignupChild
