module Regtool.Handler.Admin.Signup.OccasionalDaycare where

import Regtool.Handler.Import
import Regtool.Utils.Signup.Delete (deleteIfPaid)
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupOccasionalDaycareDeleteR :: OccasionalDaycareSignupId -> Handler Html
postAdminSignupOccasionalDaycareDeleteR odsid = do
  mPayment <- runDB $ selectFirst [OccasionalDaycarePaymentSignup ==. odsid] []
  deleteIfPaid odsid mPayment OccasionalDaycareSignupDeletedByAdmin occasionalDaycareSignupChild

postAdminSignupOccasionalDaycareUnDeleteR :: OccasionalDaycareSignupId -> Handler Html
postAdminSignupOccasionalDaycareUnDeleteR odsid = do
  runDB $ update odsid [OccasionalDaycareSignupDeletedByAdmin =. False]
  redirectToAccount odsid occasionalDaycareSignupChild
