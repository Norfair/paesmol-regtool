module Regtool.Handler.Admin.Signup.DaycareActivity where

import Regtool.Handler.Import
import Regtool.Utils.Signup.Delete (deleteIfPaid)
import Regtool.Utils.Signup.Redirect (redirectToAccount)

postAdminSignupDaycareActivityDeleteR :: DaycareActivitySignupId -> Handler Html
postAdminSignupDaycareActivityDeleteR dasid = do
  mPayment <- runDB $ selectFirst [DaycareActivityPaymentSignup ==. dasid] []
  deleteIfPaid dasid mPayment DaycareActivitySignupDeletedByAdmin daycareActivitySignupChild

postAdminSignupDaycareActivityUnDeleteR :: DaycareActivitySignupId -> Handler Html
postAdminSignupDaycareActivityUnDeleteR dasid = do
  runDB $ update dasid [DaycareActivitySignupDeletedByAdmin =. False]
  redirectToAccount dasid daycareActivitySignupChild
