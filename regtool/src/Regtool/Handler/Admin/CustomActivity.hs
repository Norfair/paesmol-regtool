{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.CustomActivity
  ( getAdminCustomActivityRegisterR,
    getAdminCustomActivityListR,
    getAdminCustomActivityEditR,
    getAdminCustomActivityOverviewR,
    postAdminCustomActivityRegisterR,
    postAdminCustomActivityDeleteR,
    postAdminCustomActivityStatusR,
  )
where

import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Regtool.Calculation.Time (formatDay, formatTimestamp)
import Regtool.Handler.Import

getAdminCustomActivityListR :: Handler Html
getAdminCustomActivityListR = do
  customActivities <- runDB $ do
    cas <- selectList [] [Asc CustomActivityDate]
    forM cas $ \ca@(Entity caid _) -> do
      spacesTaken <- count [CustomActivitySignupActivity ==. caid]
      pure (ca, fromIntegral spacesTaken :: Word)
  token <- genToken
  withNavBar $(widgetFile "admin/custom-activity/list")

getAdminCustomActivityRegisterR :: Handler Html
getAdminCustomActivityRegisterR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  makeRegisterForm "Custom Activity" (AdminR AdminCustomActivityRegisterR)
    $ registerCustomActivityForm Nothing sys

postAdminCustomActivityRegisterR :: Handler Html
postAdminCustomActivityRegisterR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  processRegisterForm
    "Custom Activity"
    (AdminR AdminCustomActivityRegisterR)
    (registerCustomActivityForm Nothing sys)
    $ \RegisterCustomActivity {..} -> do
      now <- liftIO getCurrentTime
      let customActivity_ =
            CustomActivity
              { customActivitySchoolYear = registerCustomActivitySchoolYear,
                customActivityDescription = registerCustomActivityDescription,
                customActivityDate = registerCustomActivityDate,
                customActivityStart = registerCustomActivityStart,
                customActivityEnd = registerCustomActivityEnd,
                customActivityFee = registerCustomActivityFee,
                customActivitySpaces = registerCustomActivitySpaces,
                customActivityOpen = True,
                customActivityCreationTime = now
              }
      case registerCustomActivityId of
        Just i -> do
          runDB $ replaceValid i customActivity_
          redirect $ AdminR $ AdminCustomActivityOverviewR i
        Nothing -> do
          void $ runDB $ insertValid customActivity_
          redirect $ AdminR AdminCustomActivityListR

getAdminCustomActivityEditR :: CustomActivityId -> Handler Html
getAdminCustomActivityEditR cid = do
  customActivity <- runDB $ get404 cid
  sys <- liftIO getCurrentAndNextSchoolYears
  makeEditForm
    "Custom Daycare Activity"
    (AdminR AdminCustomActivityRegisterR)
    (AdminR AdminCustomActivityListR)
    (registerCustomActivityForm (Just $ Entity cid customActivity) sys)

postAdminCustomActivityDeleteR :: CustomActivityId -> Handler Html
postAdminCustomActivityDeleteR cid = do
  canDeleteCustomActivity cid >>= deleteOrError
  redirect $ AdminR AdminCustomActivityListR

postAdminCustomActivityStatusR :: CustomActivityId -> Bool -> Handler Html
postAdminCustomActivityStatusR cid b = do
  runDB $ update cid [CustomActivityOpen =. b]
  redirect $ AdminR AdminCustomActivityListR

mClass :: Maybe a -> Text
mClass Nothing = "danger"
mClass (Just _) = "success"

getAdminCustomActivityOverviewR :: CustomActivityId -> Handler Html
getAdminCustomActivityOverviewR cid = do
  customActivity <- runDB $ get404 cid
  tups <- sortOn (Down . snd) <$> runDB (getSignupsForActivity cid)
  let paid = filter (\(_, m) -> isJust m) tups
  withNavBar $(widgetFile "admin/custom-activity/overview")

getSignupsForActivity ::
  (MonadIO m) =>
  CustomActivityId ->
  ReaderT SqlBackend m [(Entity Child, Maybe (Entity CustomActivityPayment))]
getSignupsForActivity cid =
  E.select
    $ E.from
    $ \((customActivitySignup `E.InnerJoin` child) `E.LeftOuterJoin` customActivityPayment) -> do
      E.on
        ( E.just (customActivitySignup ^. CustomActivitySignupId)
            E.==. customActivityPayment
              E.?. CustomActivityPaymentSignup
        )
      E.on (customActivitySignup ^. CustomActivitySignupChild E.==. child ^. ChildId)
      E.where_ (customActivitySignup ^. CustomActivitySignupActivity E.==. E.val cid)
      E.where_ (customActivitySignup ^. CustomActivitySignupDeletedByAdmin E.==. E.val False)
      return (child, customActivityPayment)

canDeleteCustomActivity :: CustomActivityId -> Handler (Deletable CustomActivityId)
canDeleteCustomActivity cid = do
  mc <-
    runDB $ selectFirst [CustomActivitySignupActivity ==. cid] [] :: Handler (Maybe (Entity CustomActivitySignup))
  pure
    $ case mc of
      Nothing -> CanDelete $ delete cid
      Just _ -> CannotDelete ["There is still a signup for this custom activity."]

data RegisterCustomActivity = RegisterCustomActivity
  { registerCustomActivitySchoolYear :: SchoolYear,
    registerCustomActivityDescription :: Text,
    registerCustomActivityDate :: Day,
    registerCustomActivityStart :: TimeOfDay,
    registerCustomActivityEnd :: TimeOfDay,
    registerCustomActivityFee :: Amount,
    registerCustomActivitySpaces :: Maybe Word,
    registerCustomActivityId :: Maybe CustomActivityId
  }

registerCustomActivityForm ::
  Maybe (Entity CustomActivity) -> [SchoolYear] -> AForm Handler RegisterCustomActivity
registerCustomActivityForm mce sys =
  RegisterCustomActivity
    <$> areq
      (selectFieldList $ map (\sy -> (schoolYearText sy, sy)) sys)
      (bfs ("School Year" :: Text))
      (dv customActivitySchoolYear)
    <*> areq textField (bfs ("Description" :: Text)) (dv customActivityDescription)
    <*> areq dayField (bfs ("Date" :: Text)) (dv customActivityDate)
    <*> areq timeFieldTypeTime (bfs ("Start time" :: Text)) (dv customActivityStart)
    <*> areq timeFieldTypeTime (bfs ("End time" :: Text)) (dv customActivityEnd)
    <*> areq amountField (bfs ("Fee" :: Text)) (dv customActivityFee)
    <*> aopt intField (bfs ("Spaces" :: Text)) (dv customActivitySpaces)
    <*> aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (CustomActivity -> a) -> Maybe a
    dv func = func . entityVal <$> mce
