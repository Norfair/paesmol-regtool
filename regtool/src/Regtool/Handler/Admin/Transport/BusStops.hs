{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Transport.BusStops
  ( getAdminBusStopsR,
    getAdminRegisterBusStopR,
    postAdminRegisterBusStopR,
    getAdminBusStopR,
    postAdminBusStopArchiveR,
    postAdminBusStopUnarchiveR,
    getAdminBusStopOverviewR,
  )
where

import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import qualified Database.Persist as DB
import Regtool.Component.Register
import Regtool.Handler.Import

getAdminBusStopsR :: Handler Html
getAdminBusStopsR = do
  busStops <- runDB $ selectList [] [Asc BusStopArchived, Asc BusStopId]
  token <- genToken
  withNavBar $(widgetFile "admin/transport/busstops")

data RegisterBusStop = RegisterBusStop
  { registerBusStopCity :: Textarea,
    registerBusStopLocation :: Textarea,
    registerBusStopId :: Maybe BusStopId
  }

registerBusStopForm :: Maybe (Entity BusStop) -> AForm Handler RegisterBusStop
registerBusStopForm mce =
  RegisterBusStop
    <$> areq textareaField (bfs ("City" :: Text)) (dv busStopCity)
    <*> areq textareaField (bfs ("Location" :: Text)) (dv busStopLocation)
    <*> aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (BusStop -> a) -> Maybe a
    dv func = func . entityVal <$> mce

getAdminRegisterBusStopR :: Handler Html
getAdminRegisterBusStopR =
  makeRegisterForm "Bus Stop" (AdminR AdminRegisterBusStopR) $ registerBusStopForm Nothing

postAdminRegisterBusStopR :: Handler Html
postAdminRegisterBusStopR =
  processRegisterForm "Bus Stop" (AdminR AdminRegisterBusStopR) (registerBusStopForm Nothing) $ \RegisterBusStop {..} -> do
    now <- liftIO getCurrentTime
    let busStop_ =
          BusStop
            { busStopCity = registerBusStopCity,
              busStopLocation = registerBusStopLocation,
              busStopArchived = False,
              busStopCreationTime = now
            }
    case registerBusStopId of
      Just i -> runDB $ replaceValid i busStop_
      Nothing -> runDB $ insertValid_ busStop_
    redirect $ AdminR AdminBusStopsR

getAdminBusStopR :: BusStopId -> Handler Html
getAdminBusStopR bsid = do
  busStop <- runDB $ get404 bsid
  makeEditForm
    "Bus Stop"
    (AdminR AdminRegisterBusStopR)
    (AdminR AdminBusStopsR)
    (registerBusStopForm $ Just $ Entity bsid busStop)

postAdminBusStopArchiveR :: BusStopId -> Handler Html
postAdminBusStopArchiveR bsid = do
  runDB $ DB.update bsid [BusStopArchived DB.=. True]
  redirect $ AdminR AdminBusStopsR

postAdminBusStopUnarchiveR :: BusStopId -> Handler Html
postAdminBusStopUnarchiveR bsid = do
  runDB $ DB.update bsid [BusStopArchived DB.=. False]
  redirect $ AdminR AdminBusStopsR

getAdminBusStopOverviewR :: BusStopId -> Handler Html
getAdminBusStopOverviewR bsid = do
  busStop <- runDB $ get404 bsid
  enrolledChildren <- getChildrenEnrolledAtBusStop bsid
  withNavBar $(widgetFile "admin/transport/busstop-overview")

getChildrenEnrolledAtBusStop :: BusStopId -> Handler [Entity Child]
getChildrenEnrolledAtBusStop bid =
  runDB
    $ E.select
    $ E.from
    $ \(child `E.InnerJoin` transportSignup) -> do
      E.on (child ^. ChildId E.==. transportSignup ^. TransportSignupChild)
      E.where_ $ transportSignup ^. TransportSignupDeletedByAdmin E.==. E.val False
      E.where_ $ transportSignup ^. TransportSignupBusStop E.==. E.val bid
      return child
