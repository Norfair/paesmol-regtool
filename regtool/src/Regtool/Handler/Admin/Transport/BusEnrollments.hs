{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Transport.BusEnrollments
  ( getAdminBusEnrollmentsOverviewR,
    getAdminBusEnrollmentsYearR,
    getAdminBusEnrollmentsLineR,
    getAdminBusEnrollmentsStopR,
    getAdminBusEnrollmentsDayR,
  )
where

import qualified Data.Text as T
import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Regtool.Calculation.Parents
import Regtool.Handler.Admin.Transport.BusLines.Utils
import Regtool.Handler.Import

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getAdminBusEnrollmentsOverviewR :: Handler Html
getAdminBusEnrollmentsOverviewR = do
  sys' <-
    runDB
      $ E.select
      $ E.distinct
      $ E.from
      $ \transportSignup -> do
        E.where_ $ transportSignup ^. TransportSignupDeletedByAdmin E.==. E.val False
        pure $ transportSignup ^. TransportSignupSchoolYear
  csys <- liftIO getCurrentSchoolyears
  bls <- runDB $ selectList [] []
  let sys = nub $ sys' ++ map E.Value csys
  (formWidget, formEnctype) <-
    generateFormGet' $ renderCustomForm BootstrapBasicForm $ searchBusEnrollmentsForm csys bls
  withNavBar $(widgetFile "admin/transport/bus-enrollments")

getAdminBusEnrollmentsYearR :: SchoolYear -> Handler Html
getAdminBusEnrollmentsYearR sy = do
  busLineTups <-
    runDB
      $ selectList [] [Asc BusLineId]
      >>= (\bls -> forM bls $ \bl -> (,) bl <$> getOccupancyOfLine sy (entityKey bl))
  busStopTups <-
    runDB
      $ selectList [] [Asc BusStopArchived, Asc BusStopId]
      >>= (\bss -> forM bss $ \bs -> (,) bs <$> getOccupancyOfStop sy (entityKey bs))
  withNavBar $(widgetFile "admin/transport/bus-enrollments-year")

getAdminBusEnrollmentsLineR :: SchoolYear -> BusLineId -> Handler Html
getAdminBusEnrollmentsLineR sy blid = do
  busLine <- runDB $ get404 blid
  occupancy <- runDB $ getOccupancyOfLine sy blid
  stops <- getBusStopsOf blid
  busStopTups <-
    runDB
      $ forM stops
      $ \(Entity _ bls, Entity bsid bs) -> (,,) bls bs <$> getOccupancyOfStop sy bsid
  withNavBar $(widgetFile "admin/transport/bus-enrollments-line")

getAdminBusEnrollmentsStopR :: SchoolYear -> BusStopId -> Handler Html
getAdminBusEnrollmentsStopR sy bsid = do
  busStop <- runDB $ get404 bsid
  occupancy <- runDB $ getOccupancyOfStop sy bsid
  withNavBar $(widgetFile "admin/transport/bus-enrollments-stop")

newtype Occupancy = Occupancy
  { occupancySignups :: [(Entity Child, BusPaymentStatus)]
  }

occupancyNbSignedUp :: Occupancy -> Int
occupancyNbSignedUp = length . occupancySignups

occupancyNbAllPaid :: Occupancy -> Int
occupancyNbAllPaid =
  length
    . filter
      ( ( \case
            FractionPaid n d -> n > zeroAmount && n >= d
            NoPaymentNecessary -> True
            NoPaymentsYet -> False
        )
          . snd
      )
    . occupancySignups

newtype OccasionalOccupancy = OccasionalOccupancy
  { occasionalOccupancySignups :: [(Entity Child, BusDirection, OccasionalBusPaymentStatus)]
  }

data BusPaymentStatus
  = NoPaymentNecessary
  | FractionPaid
      Amount -- Paid
      Amount -- Total to pay
  | NoPaymentsYet

statusClass :: BusPaymentStatus -> Text
statusClass NoPaymentNecessary = "success"
statusClass (FractionPaid n d)
  | n == zeroAmount || d == zeroAmount = "danger"
  | n >= d = "success"
  | otherwise = "warning"
statusClass NoPaymentsYet = "danger"

data OccasionalBusPaymentStatus
  = OccasionalNotPaid
  | OccasionalPaid
  deriving (Eq, Ord)

occasionalStatusClass :: OccasionalBusPaymentStatus -> Text
occasionalStatusClass OccasionalNotPaid = "danger"
occasionalStatusClass OccasionalPaid = "success"

getOccupancyOfStop :: (MonadIO m) => SchoolYear -> BusStopId -> ReaderT SqlBackend m Occupancy
getOccupancyOfStop sy = getChildrenEnrolledInStop sy >=> makeOccupancy

getOccupancyOfLine :: (MonadIO m) => SchoolYear -> BusLineId -> ReaderT SqlBackend m Occupancy
getOccupancyOfLine sy = getChildrenEnrolledInLine sy >=> makeOccupancy

getOccasionalOccupancy :: (MonadIO m) => Day -> BusLineId -> ReaderT SqlBackend m OccasionalOccupancy
getOccasionalOccupancy d blid = getOccasionalEnrollments d blid >>= makeOccasionalOccupancy

makeOccupancy ::
  (MonadIO m) => [(Entity Child, Entity TransportSignup)] -> ReaderT SqlBackend m Occupancy
makeOccupancy tups =
  fmap (Occupancy . sortOn (Down . busPaymentStatusFractionOrderer . snd))
    $ forM tups
    $ \(echild@(Entity cid _), Entity tsid _) -> (,) echild <$> getBusPaymentStatus cid tsid

makeOccasionalOccupancy ::
  (MonadIO m) =>
  [(Entity Child, Entity OccasionalTransportSignup)] ->
  ReaderT SqlBackend m OccasionalOccupancy
makeOccasionalOccupancy tups =
  let thd (_, _, z) = z
   in fmap (OccasionalOccupancy . sortOn (Down . thd))
        $ forM tups
        $ \(echild, Entity otsid OccasionalTransportSignup {..}) ->
          (,,) echild occasionalTransportSignupDirection <$> getOccasionalBusPaymentStatus otsid

busPaymentStatusFractionOrderer :: BusPaymentStatus -> Ratio Int
busPaymentStatusFractionOrderer NoPaymentsYet = -1
busPaymentStatusFractionOrderer NoPaymentNecessary = 1 % 1
busPaymentStatusFractionOrderer (FractionPaid n d)
  | d == zeroAmount = 0
  | otherwise = amountCents n % amountCents d

getBusPaymentStatus ::
  (MonadIO m) => ChildId -> TransportSignupId -> ReaderT SqlBackend m BusPaymentStatus
getBusPaymentStatus cid tsid = do
  menr <- selectFirst [TransportEnrollmentSignup ==. tsid] []
  case menr of
    Nothing -> do
      ps <- getParentsOfChild cid
      pure
        $ if any ((/= OtherCompany) . parentCompany . entityVal) ps
          then NoPaymentNecessary
          else NoPaymentsYet
    Just (Entity _ TransportEnrollment {..}) -> do
      mtppe <- selectFirst [TransportPaymentPlanId ==. transportEnrollmentPaymentPlan] []
      case mtppe of
        Nothing -> pure NoPaymentsYet
        Just (Entity _ TransportPaymentPlan {..}) -> do
          tps <- selectList [TransportPaymentPaymentPlan ==. transportEnrollmentPaymentPlan] []
          pure
            $ case tps of
              [] -> NoPaymentsYet
              _ ->
                FractionPaid
                  (sumAmount (map (transportPaymentAmount . entityVal) tps))
                  transportPaymentPlanTotalAmount

getOccasionalBusPaymentStatus ::
  (MonadIO m) => OccasionalTransportSignupId -> ReaderT SqlBackend m OccasionalBusPaymentStatus
getOccasionalBusPaymentStatus tsid = do
  motp <- selectFirst [OccasionalTransportPaymentSignup ==. tsid] []
  pure
    $ case motp of
      Nothing -> OccasionalNotPaid
      Just _ -> OccasionalPaid

getChildrenEnrolledInStop ::
  (MonadIO m) =>
  SchoolYear ->
  BusStopId ->
  ReaderT SqlBackend m [(Entity Child, Entity TransportSignup)]
getChildrenEnrolledInStop sy bsid =
  E.select
    $ E.from
    $ \(child `E.InnerJoin` transportSignup) -> do
      E.on (child ^. ChildId E.==. transportSignup ^. TransportSignupChild)
      E.where_ $ transportSignup ^. TransportSignupDeletedByAdmin E.==. E.val False
      E.where_
        $ (transportSignup ^. TransportSignupBusStop E.==. E.val bsid)
        E.&&. (transportSignup ^. TransportSignupSchoolYear E.==. E.val sy)
      return (child, transportSignup)

getChildrenEnrolledInLine ::
  (MonadIO m) =>
  SchoolYear ->
  BusLineId ->
  ReaderT SqlBackend m [(Entity Child, Entity TransportSignup)]
getChildrenEnrolledInLine sy bid =
  E.select
    $ E.from
    $ \(child `E.InnerJoin` transportSignup `E.InnerJoin` busLineStop) -> do
      E.on (child ^. ChildId E.==. transportSignup ^. TransportSignupChild)
      E.on $ transportSignup ^. TransportSignupBusStop E.==. busLineStop ^. BusLineStopStop
      E.where_ $ transportSignup ^. TransportSignupDeletedByAdmin E.==. E.val False
      E.where_
        $ (busLineStop ^. BusLineStopLine E.==. E.val bid)
        E.&&. (transportSignup ^. TransportSignupSchoolYear E.==. E.val sy)
      return (child, transportSignup)

getOccasionalEnrollments ::
  (MonadIO m) =>
  Day ->
  BusLineId ->
  ReaderT SqlBackend m [(Entity Child, Entity OccasionalTransportSignup)]
getOccasionalEnrollments d blid =
  E.select
    $ E.from
    $ \((child `E.InnerJoin` occasionalTransportSignup) `E.InnerJoin` busLineStop) -> do
      E.on
        ( busLineStop
            ^. BusLineStopStop
            E.==. occasionalTransportSignup
              ^. OccasionalTransportSignupBusStop
        )
      E.on (child ^. ChildId E.==. occasionalTransportSignup ^. OccasionalTransportSignupChild)
      E.where_ (busLineStop ^. BusLineStopLine E.==. E.val blid)
      E.where_ $ occasionalTransportSignup ^. OccasionalTransportSignupDay E.==. E.val d
      pure (child, occasionalTransportSignup)

data SearchBusEnrollments = SearchBusEnrollments
  { searchBusEnrollmentsSchoolYear :: SchoolYear,
    searchBusEnrollmentsDay :: Day,
    searchBusEnrollmentsBusLine :: BusLineId
  }

searchBusEnrollmentsForm :: [SchoolYear] -> [Entity BusLine] -> AForm Handler SearchBusEnrollments
searchBusEnrollmentsForm sys bls =
  SearchBusEnrollments
    <$> areq
      (selectFieldList $ map (\sy -> (schoolYearText sy, sy)) sys)
      (bfs ("School Year" :: Text))
      Nothing
    <*> areq dayField (bfs ("Day" :: Text)) Nothing
    <*> areq
      ( selectFieldList $ do
          Entity bsid bs <- bls
          let t = T.unlines [busLineName bs, unTextarea $ busLineDescription bs]
          pure (t, bsid)
      )
      (bfs ("Bus Line" :: Text))
      Nothing

getAdminBusEnrollmentsDayR :: Handler Html
getAdminBusEnrollmentsDayR = do
  csys <- liftIO getCurrentSchoolyears
  bls <- runDB $ selectList [] []
  ((result, _), _) <-
    runFormGet $ renderCustomForm BootstrapBasicForm $ searchBusEnrollmentsForm csys bls
  case result of
    FormSuccess SearchBusEnrollments {..} -> do
      occupancy <-
        runDB $ getOccupancyOfLine searchBusEnrollmentsSchoolYear searchBusEnrollmentsBusLine
      occasionalOccupancy <-
        runDB $ getOccasionalOccupancy searchBusEnrollmentsDay searchBusEnrollmentsBusLine
      busLine <- runDB $ get404 searchBusEnrollmentsBusLine
      withNavBar $(widgetFile "admin/transport/bus-enrollments-day")
    _ -> redirect $ AdminR $ AdminBusEnrollmentsR AdminBusEnrollmentsOverviewR
