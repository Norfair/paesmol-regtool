{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Admin.Transport.BusLineStop
  ( getAdminBusLineStopR,
    postAdminBusLineStopR,
  )
where

import Regtool.Handler.Import

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data EditBusLineStop = EditBusLineStop
  { editBusLineStopMorning :: Text,
    editBusLineStopEvening :: Text,
    editBusLineStopWednesday :: Text,
    editBusLineStopId :: BusLineStopId
  }

editBusLineStopForm :: Entity BusLineStop -> AForm Handler EditBusLineStop
editBusLineStopForm mce =
  EditBusLineStop
    <$> areq textField (bfs ("Morning" :: Text)) (dv busLineStopMorningTime)
    <*> areq textField (bfs ("Evening" :: Text)) (dv busLineStopEveningTime)
    <*> areq textField (bfs ("Wednesday" :: Text)) (dv busLineStopWednesdayTime)
    <*> areq hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey mce)
  where
    dv :: (BusLineStop -> a) -> Maybe a
    dv func = Just $ func $ entityVal mce

getAdminBusLineStopR :: BusLineId -> BusStopId -> Handler Html
getAdminBusLineStopR blid bsid =
  withBusLineStop blid bsid $ \ebls -> do
    makeEditForm
      "Bus Line Stop"
      (AdminR (AdminBusLineStopR blid bsid))
      (AdminR (AdminBusLineOverviewR blid))
      (editBusLineStopForm ebls)

postAdminBusLineStopR :: BusLineId -> BusStopId -> Handler Html
postAdminBusLineStopR blid bsid =
  withBusLineStop blid bsid $ \ebls@(Entity blsid _) -> do
    processEditForm
      "Bus Line Stop"
      (AdminR (AdminBusLineStopR blid bsid))
      (AdminR (AdminBusLineOverviewR blid))
      (editBusLineStopForm ebls)
      $ \EditBusLineStop {..} -> do
        runDB
          $ update
            blsid
            [ BusLineStopMorningTime =. editBusLineStopMorning,
              BusLineStopEveningTime =. editBusLineStopEvening,
              BusLineStopWednesdayTime =. editBusLineStopWednesday
            ]
        redirect $ AdminR $ AdminBusLineOverviewR blid

withBusLineStop :: BusLineId -> BusStopId -> (Entity BusLineStop -> Handler a) -> Handler a
withBusLineStop blid bsid func = do
  mbls <- runDB $ selectFirst [BusLineStopLine ==. blid, BusLineStopStop ==. bsid] []
  case mbls of
    Nothing -> notFound
    Just ebls -> func ebls
