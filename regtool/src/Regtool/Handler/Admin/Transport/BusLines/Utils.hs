module Regtool.Handler.Admin.Transport.BusLines.Utils
  ( getBusStopsOf,
  )
where

import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import Regtool.Handler.Import

getBusStopsOf :: BusLineId -> Handler [(Entity BusLineStop, Entity BusStop)]
getBusStopsOf bid =
  runDB
    $ E.select
    $ E.from
    $ \(busLine `E.InnerJoin` busLineStop `E.InnerJoin` busStop) -> do
      E.on (busLine ^. BusLineId E.==. busLineStop ^. BusLineStopLine)
      E.on $ busLineStop ^. BusLineStopStop E.==. busStop ^. BusStopId
      E.where_ $ busLine ^. BusLineId E.==. E.val bid
      return (busLineStop, busStop)
