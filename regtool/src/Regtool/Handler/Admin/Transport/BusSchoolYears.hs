{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Transport.BusSchoolYears
  ( getAdminBusSchoolYearsR,
    postAdminRegisterBusSchoolYearsR,
    postAdminBusSchoolYearStatusR,
  )
where

import Regtool.Component.Register
import Regtool.Handler.Import

data RegisterBusSchoolYear = RegisterBusSchoolYear
  { registerBusSchoolYearYear :: SchoolYear
  }

registerBusSchoolYearForm :: UTCTime -> AForm Handler RegisterBusSchoolYear
registerBusSchoolYearForm now =
  RegisterBusSchoolYear
    <$> areq
      ( selectFieldList
          $ map
            ((\sy -> (schoolYearText sy, sy)) . schoolYearNext)
            (currentSchoolYears now)
      )
      (bfs ("School Year" :: Text))
      Nothing

getAdminBusSchoolYearsR :: Handler Html
getAdminBusSchoolYearsR = do
  busSchoolYears <- runDB $ selectList [] [Asc BusSchoolYearId]
  token <- genToken
  now <- liftIO getCurrentTime
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ registerBusSchoolYearForm now
  withNavBar $(widgetFile "admin/transport/bus-schoolyears")

postAdminRegisterBusSchoolYearsR :: Handler Html
postAdminRegisterBusSchoolYearsR = do
  now <- liftIO getCurrentTime
  processRegisterForm
    "Bus School Year"
    (AdminR AdminRegisterBusSchoolYearsR)
    (registerBusSchoolYearForm now)
    $ \RegisterBusSchoolYear {..} -> do
      my <- runDB $ getBy $ UniqueSchoolYear registerBusSchoolYearYear
      case my of
        Just _ -> redirect $ AdminR AdminBusSchoolYearsR
        Nothing -> do
          runDB
            $ insertValid_
            $ BusSchoolYear {busSchoolYearYear = registerBusSchoolYearYear, busSchoolYearOpen = True}
          redirect $ AdminR AdminBusSchoolYearsR

postAdminBusSchoolYearStatusR :: BusSchoolYearId -> Bool -> Handler Html
postAdminBusSchoolYearStatusR bsyid b = do
  runDB $ update bsyid [BusSchoolYearOpen =. b]
  redirect $ AdminR AdminBusSchoolYearsR
