{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.ExtraCharge
  ( getAdminAddExtraChargeR,
    postAdminAddExtraChargeR,
  )
where

import Regtool.Handler.Import

data RegisterExtraCharge = RegisterExtraCharge
  { registerExtraChargeAmount :: Amount,
    registerExtraChargeReason :: Maybe Text
  }

registerExtraChargeForm :: AForm Handler RegisterExtraCharge
registerExtraChargeForm =
  RegisterExtraCharge
    <$> areq amountField (bfs ("Amount" :: Text)) Nothing
    <*> aopt textField (bfs ("Reason" :: Text)) Nothing

getAddExtraChargePage :: Maybe (FormResult a) -> AccountId -> Handler Html
getAddExtraChargePage mfr aid = do
  account <- runDB $ get404 aid
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm registerExtraChargeForm
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "admin/extra-charge")

getAdminAddExtraChargeR :: AccountId -> Handler Html
getAdminAddExtraChargeR = getAddExtraChargePage Nothing

postAdminAddExtraChargeR :: AccountId -> Handler Html
postAdminAddExtraChargeR aid = do
  ((result, _), _) <- runFormPost $ renderCustomForm BootstrapBasicForm registerExtraChargeForm
  case result of
    FormSuccess RegisterExtraCharge {..} -> do
      now <- liftIO getCurrentTime
      let extraCharge =
            ExtraCharge
              { extraChargeAccount = aid,
                extraChargeAmount = registerExtraChargeAmount,
                extraChargeReason = registerExtraChargeReason,
                extraChargeCheckout = Nothing, -- Not used yet.
                extraChargeTimestamp = now
              }
      runDB $ insertValid_ extraCharge
      redirect $ AdminR $ AdminAccountR aid
    fr -> getAddExtraChargePage (Just fr) aid
