{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Technical.Overview
  ( getAdminTechnicalR,
  )
where

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation

getAdminTechnicalR :: Handler Html
getAdminTechnicalR = do
  token <- genToken
  withNavBar $(widgetFile "admin/technical")
