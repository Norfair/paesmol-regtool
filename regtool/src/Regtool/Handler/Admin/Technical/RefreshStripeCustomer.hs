{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Technical.RefreshStripeCustomer
  ( postAdminTechnicalRefreshStripeCustomersR,
  )
where

import Control.Exception (throwIO)
import Regtool.Handler.Import
import Regtool.Utils.Stripe

postAdminTechnicalRefreshStripeCustomersR :: Handler Html
postAdminTechnicalRefreshStripeCustomersR = do
  scs <- runDB $ selectList [] [Asc StripeCustomerId]
  as <- runDB $ selectList [] [Asc AccountId]
  ress <-
    forM scs $ \(Entity scid StripeCustomer {..}) -> do
      Account {..} <-
        case find ((== stripeCustomerAccount) . entityKey) as of
          Nothing -> liftIO $ throwIO $ userError "should not happen."
          Just (Entity _ a) -> pure a
      gscr <- getStripeCustomer accountEmailAddress
      pure (scid, accountEmailAddress, stripeCustomerIdentifier, gscr)
  runDB
    $ forM_ ress
    $ \(scid, _, _, res) ->
      case res of
        GotStripeCustomer cid -> update scid [StripeCustomerIdentifier =. Just cid]
        NoSuchStripeCustomer -> update scid [StripeCustomerIdentifier =. Nothing]
        _ -> pure ()
  withNavBar $(widgetFile "admin/technical/refresh-stripe-customers")

statusClass :: Maybe Text -> GetStripeCustomerResult -> Text
statusClass mOldId res =
  case (mOldId, res) of
    (Nothing, NoSuchStripeCustomer) -> "info"
    (Nothing, GotStripeCustomer _) -> "success"
    (Just oldId, GotStripeCustomer newId) ->
      if oldId == newId
        then "info"
        else "success"
    (Just _, NoSuchStripeCustomer) -> "success"
    _ -> "danger"
