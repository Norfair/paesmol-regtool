{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Technical.ShoppingCart
  ( getAdminTechnicalShoppingCartsR,
    getAdminTechnicalShoppingCartR,
  )
where

import Regtool.Calculation.Costs.Cache
import Regtool.Calculation.Time
import Regtool.Handler.Checkout.Form
import Regtool.Handler.Import

getAdminTechnicalShoppingCartsR :: Handler Html
getAdminTechnicalShoppingCartsR = do
  carts <- runDB $ selectList [] [Desc ShoppingCartCreated]
  withNavBar $(widgetFile "admin/technical/shopping-carts")

getAdminTechnicalShoppingCartR :: ShoppingCartId -> Handler Html
getAdminTechnicalShoppingCartR cid = do
  sc@ShoppingCart {..} <- runDB $ get404 cid
  cartHtml <-
    case decodeShoppingCart sc of
      Nothing -> pure mempty
      Just choices -> do
        (formWidget, formEncType) <- generateFormPost $ chooseForm choices
        pure [whamlet|<form enctype=#{formEncType}>^{formWidget}|]
  withNavBar $(widgetFile "admin/technical/shopping-cart")
