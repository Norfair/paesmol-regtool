{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Config where

import qualified Data.Text as T
import Regtool.Calculation.Costs.Config
import Regtool.Core.AdminConfiguration
import Regtool.Handler.Import

getAdminConfigurationR :: Handler Html
getAdminConfigurationR = do
  cform <- makeConfigurationForm
  (formWidget, formEncType) <- generateFormPost $ renderCustomForm BootstrapBasicForm cform
  withNavBar $(widgetFile "admin/configuration")

postAdminConfigurationR :: Handler Html
postAdminConfigurationR = do
  cform <- makeConfigurationForm
  ((result, formWidget), formEncType) <- runFormPost $ renderCustomForm BootstrapBasicForm cform
  case result of
    FormSuccess newConfig ->
      case prettyValidate newConfig of
        Left err -> invalidArgs [T.unwords ["The configuration is not valid:", T.pack err]]
        Right nc -> do
          putConfiguration nc
          redirect $ AdminR AdminConfigurationR
    fr -> withFormResultNavBar fr $(widgetFile "admin/configuration")

makeConfigurationForm :: Handler (AForm Handler Configuration)
makeConfigurationForm = do
  conf <- getConfiguration
  pure $ configurationForm $ Just conf

configurationForm :: Maybe Configuration -> AForm Handler Configuration
configurationForm mc =
  Configuration
    <$> costsConfigForm (dv confCostsConfig)
    <*> aopt
      dayField
      (bfs ("Date after which parents have to re-confirm the school level of the Children" :: Text))
      (dv confChildrenLevelResetDay)
  where
    dv :: (Configuration -> a) -> Maybe a
    dv func = func <$> mc

costsConfigForm :: Maybe CostsConfig -> AForm Handler CostsConfig
costsConfigForm mcc =
  CostsConfig
    <$> areq amountField (bfs ("Yearly Membership Fee" :: Text)) (dv costsConfigAnnualFee)
    <*> yearlyBusFeeForm (dv costsConfigBusYearly)
    <*> areq
      amountField
      (bfs ("Yearly Bus fee for every child after that" :: Text))
      (dv costsConfigBusYearlyDefault)
    <*> areq amountField (bfs ("Occasional Bus Fee" :: Text)) (dv costsConfigOccasionalBus)
  where
    dv :: (CostsConfig -> a) -> Maybe a
    dv func = func <$> mcc

yearlyBusFeeForm :: Maybe [Amount] -> AForm Handler [Amount]
yearlyBusFeeForm mas =
  for [1 .. 5] $ \i ->
    areq
      amountField
      (bfs (("Yearly bus fee for child " <> T.pack (show i)) :: Text))
      ((mas >>= (`atMay` (i - 1))) `mplus` Just zeroAmount)
