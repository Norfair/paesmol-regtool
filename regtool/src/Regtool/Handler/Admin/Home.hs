{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Home where

import Regtool.Component.NavigationBar (withNavBar)
import Regtool.Core.Foundation (widgetFile)
import Regtool.Core.Foundation.Regtool
import Yesod.Core (Html)

getPanelR :: Handler Html
getPanelR = withNavBar $(widgetFile "admin/panel")
