{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Admin.Permission
  ( getAdminAccountPermissionR,
    postAdminAccountPermissionR,
  )
where

import Regtool.Handler.Import

data RegisterPermission = RegisterPermission
  { registerPermissionExportData :: Bool,
    registerPermissionReadDaycareEnrolments :: Bool,
    registerPermissionReadTransportEnrolments :: Bool,
    registerPermissionId :: Maybe PermissionId
  }

registerPermissionForm :: Maybe (Entity Permission) -> AForm Handler RegisterPermission
registerPermissionForm mp =
  RegisterPermission
    <$> areq checkBoxField (bfs ("Export Child Data" :: Text)) (dv permissionExportData)
    <*> areq checkBoxField (bfs ("Read Daycare Enrolments" :: Text)) (dv permissionReadDaycareEnrolments)
    <*> areq checkBoxField (bfs ("Read Transport Enrolments" :: Text)) (dv permissionReadTransportEnrolments)
    <*> aopt
      hiddenField
      ("Id" {fsLabel = "", fsName = Just "id"})
      (Just $ entityKey <$> mp)
  where
    dv func = func . entityVal <$> mp

getAdminAccountPermissionR :: AccountId -> Handler Html
getAdminAccountPermissionR aid = do
  mp <- runDB $ getBy $ UniquePermissionAccount aid
  makeEditForm "Permission" (AdminR $ AdminAccountPermissionR aid) (AdminR $ AdminAccountR aid) (registerPermissionForm mp)

postAdminAccountPermissionR :: AccountId -> Handler Html
postAdminAccountPermissionR aid = do
  mp <- runDB $ getBy $ UniquePermissionAccount aid
  processEditForm
    "Permission"
    (AdminR $ AdminAccountPermissionR aid)
    (AdminR $ AdminAccountR aid)
    (registerPermissionForm mp)
    $ \RegisterPermission {..} -> do
      let permission =
            Permission
              { permissionAccount = aid,
                permissionExportData = registerPermissionExportData,
                permissionReadDaycareEnrolments = registerPermissionReadDaycareEnrolments,
                permissionReadTransportEnrolments = registerPermissionReadTransportEnrolments
              }
      _ <-
        runDB
          $ upsertBy
            (UniquePermissionAccount aid)
            permission
            [ PermissionExportData =. registerPermissionExportData,
              PermissionReadDaycareEnrolments =. registerPermissionReadDaycareEnrolments,
              PermissionReadTransportEnrolments =. registerPermissionReadTransportEnrolments
            ]
      redirect $ AdminR $ AdminAccountPermissionR aid
