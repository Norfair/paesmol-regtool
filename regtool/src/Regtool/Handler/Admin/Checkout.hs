{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Checkout
  ( getAdminCheckoutR,
  )
where

import Regtool.Calculation.Costs.Checkout
import Regtool.Calculation.Costs.Input
import Regtool.Handler.Import

getAdminCheckoutR :: AccountId -> CheckoutId -> Handler Html
getAdminCheckoutR aid cid = do
  mc <- runDB $ selectFirst [CheckoutId ==. cid, CheckoutAccount ==. aid] []
  case mc of
    Nothing -> notFound
    Just (Entity _ co) -> do
      ci <- runDB $ getCostsInputForAccount aid
      let coo = deriveCheckoutOverview ci cid co
      let checkoutOverviewWidget = $(widgetFile "checkout-overview")
      withNavBar $(widgetFile "admin/checkout")
