{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-unused-pattern-binds #-}

module Regtool.Handler.Admin.Accounts
  ( getAdminAccountsR,
    getAdminAccountR,
    postAdminAccountImpersonateR,
    postAdminAccountUnregisterParentR,
    postAdminAccountUnregisterDoctorR,
    postAdminAccountUnregisterChildR,
  )
where

import qualified Data.Text as T
import Regtool.Calculation.Checkout
import Regtool.Calculation.Children
import Regtool.Handler.Import
import Regtool.Utils.Children
import Regtool.Utils.Doctors
import Regtool.Utils.Parents
import Regtool.Utils.Signup.CustomActivity (customActivityText, getAllOurCustomActivitySignups)
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.DaycareActivity
import Regtool.Utils.Signup.OccasionalDaycare
import Regtool.Utils.Signup.OccasionalTransport
import Regtool.Utils.Signup.Transport

getAdminAccountsR :: Handler Html
getAdminAccountsR = do
  accounts <- runDB $ selectList [] [Asc AccountId]
  token <- genToken
  withNavBar $(widgetFile "admin/accounts")

getAdminAccountR :: AccountId -> Handler Html
getAdminAccountR aid = do
  account <- runDB $ get404 aid
  mPerm <- runDB $ getBy $ UniquePermissionAccount aid
  let Permission _ _ _ _ = undefined
  token <- genToken
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  let lookupDoctorName did =
        case find (\(Entity i _) -> did == i) doctors of
          Nothing -> "Unknown Doctor"
          Just (Entity _ d) -> T.unwords [doctorFirstName d, doctorLastName d]
  languageSections <- runDB $ selectList [] [Asc LanguageSectionId]
  let lookupLanguageSectionName lsid =
        case find (\(Entity i _) -> lsid == i) languageSections of
          Nothing -> "Unknown Language Section"
          Just (Entity _ ls) -> languageSectionName ls
  children <- runDB $ getChildrenOf aid
  let lookupChildName cid =
        case find (\(Entity i _) -> cid == i) children of
          Nothing -> "Unknown Child"
          Just (Entity _ c) -> T.unwords [childFirstName c, childLastName c]
  busStops <- runDB $ selectList [] [Asc BusStopId]
  let lookupBusStopName bid =
        case find (\(Entity i _) -> bid == i) busStops of
          Nothing -> "Unknown BusStops"
          Just (Entity _ b) ->
            T.unwords [unTextarea $ busStopCity b, unTextarea $ busStopLocation b]
  transportSignups <- runDB $ getAllOurTransportSignups $ map entityKey children
  occasionalTransportSignups <- runDB $ getOurOccasionalTransportSignups $ map entityKey children
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  daycareSignups <- runDB $ getAllOurDaycareSignups $ map entityKey children
  let lookupDaycareTimeslot tsid = find (\(Entity i _) -> tsid == i) timeslots
  occasionalDaycareSignups <- runDB $ getAllOurOccasionalDaycareSignups $ map entityKey children
  daycareActivities <- runDB $ selectList [] [Asc DaycareActivityId]
  let lookupDaycareSemestre dsid = find (\(Entity i _) -> dsid == i) semestres
  let lookupDaycareActivity daid = find (\(Entity i _) -> daid == i) daycareActivities
  daycareActivitySignups <- runDB $ getAllOurDaycareActivitySignups $ map entityKey children
  customActivities <- runDB $ selectList [] [Asc CustomActivityId]
  let lookupCustomActivity caid = find (\(Entity i _) -> caid == i) customActivities
  customActivitySignups <- runDB $ getAllOurCustomActivitySignups $ map entityKey children
  scs <- runDB $ getStripeCustomersOf aid
  checkouts <- runDB $ selectList [CheckoutAccount ==. aid] []
  payments <- runDB $ getPaymentsOf $ map entityKey scs
  extraCharges <- runDB $ selectList [ExtraChargeAccount ==. aid] []
  discounts <- runDB $ selectList [DiscountAccount ==. aid] []
  let paidMsg = maybe "Not Paid" (const ("Paid" :: Text))
  withNavBar $(widgetFile "admin/account")

postAdminAccountImpersonateR :: AccountId -> Handler TypedContent
postAdminAccountImpersonateR = impersonateAccount

postAdminAccountUnregisterParentR :: AccountId -> ParentId -> Handler Html
postAdminAccountUnregisterParentR aid pid = do
  canDeleteParent pid >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid

postAdminAccountUnregisterDoctorR :: AccountId -> DoctorId -> Handler Html
postAdminAccountUnregisterDoctorR aid did = do
  canDeleteDoctor did >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid

postAdminAccountUnregisterChildR :: AccountId -> ChildId -> Handler Html
postAdminAccountUnregisterChildR aid cid = do
  canDeleteChild cid >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid
