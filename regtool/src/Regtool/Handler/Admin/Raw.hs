{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Handler.Admin.Raw
  ( getAdminRawDataR,
    postProfileVerifyManuallyR,
    postAdminExportRawDataR,
  )
where

import Codec.Archive.Zip
import qualified Data.ByteString.Lazy as LB
import qualified Data.Csv as Csv
import Data.Proxy
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Regtool.Calculation.Time
import Regtool.Handler.Import

getAdminRawDataR :: [Text] -> Handler Html
getAdminRawDataR ts = do
  let options =
        [ getTableOptionFor @Account,
          getTableOptionFor @Permission,
          getTableOptionFor @BusLine,
          getTableOptionFor @BusLineStop,
          getTableOptionFor @BusStop,
          getTableOptionFor @Checkout,
          getTableOptionFor @Child,
          getTableOptionFor @ChildOf,
          getTableOptionFor @ShoppingCart,
          getTableOptionFor @CustomActivity,
          getTableOptionFor @CustomActivityPayment,
          getTableOptionFor @CustomActivitySignup,
          getTableOptionFor @DaycareActivity,
          getTableOptionFor @DaycareActivityPayment,
          getTableOptionFor @DaycareActivitySignup,
          getTableOptionFor @DaycarePayment,
          getTableOptionFor @DaycareSemestre,
          getTableOptionFor @DaycareSignup,
          getTableOptionFor @DaycareTimeslot,
          getTableOptionFor @ExtraCharge,
          getTableOptionFor @Discount,
          getTableOptionFor @Doctor,
          getTableOptionFor @Email,
          getTableOptionFor @LanguageSection,
          getTableOptionFor @OccasionalDaycarePayment,
          getTableOptionFor @OccasionalDaycareSignup,
          getTableOptionFor @OccasionalTransportPayment,
          getTableOptionFor @OccasionalTransportSignup,
          getTableOptionFor @Parent,
          getTableOptionFor @PasswordResetEmail,
          getTableOptionFor @PaymentReceivedEmail,
          getTableOptionFor @AdminNotificationEmail,
          getTableOptionFor @DaycareEmail,
          getTableOptionFor @TransportEmail,
          getTableOptionFor @OccasionalDaycareEmail,
          getTableOptionFor @OccasionalTransportEmail,
          getTableOptionFor @StripeCustomer,
          getTableOptionFor @StripePayment,
          getTableOptionFor @TransportEnrollment,
          getTableOptionFor @TransportPayment,
          getTableOptionFor @TransportPaymentPlan,
          getTableOptionFor @TransportSignup,
          getTableOptionFor @VerificationEmail,
          getTableOptionFor @YearlyFeePayment
        ]
  case ts of
    [] -> do
      token <- genToken
      withNavBar $(widgetFile "admin/raw")
    _ ->
      let dbn = T.unwords ts
       in case lookup dbn options of
            Nothing -> notFound
            Just tableWidgetHandler -> do
              tableWidget <- tableWidgetHandler
              withNavBar $(widgetFile "admin/raw/table-page")

postProfileVerifyManuallyR :: AccountId -> Handler Html
postProfileVerifyManuallyR aid = do
  maid <- runDB $ get aid
  case maid of
    Nothing -> invalidArgs ["Unknown account"]
    Just acc -> do
      verifyAccount $ Entity aid acc
      redirect $ AdminR $ AdminRawDataR []

getTableOptionFor ::
  forall record.
  ( ToBackendKey SqlBackend record,
    Validity record
  ) =>
  (Text, Handler Widget)
getTableOptionFor =
  let def = entityDef (Proxy :: Proxy record)
      dbn = unEntityNameDB $ getEntityDBName def
      wFunc = do
        values <- runDB $ selectList [] [Asc (persistIdField :: EntityField record (Key record))]
        let pvalues =
              map (\(Entity i r) -> (i, isValid r, map toPersistValue $ toPersistFields r)) values
        pure $(widgetFile "admin/raw/table")
   in (dbn, wFunc)

fieldText :: PersistValue -> Text
fieldText (PersistText t) = t
fieldText (PersistByteString bs) = tshow bs
fieldText (PersistInt64 i) = tshow i
fieldText (PersistDouble d) = tshow d
fieldText (PersistRational r) = tshow r
fieldText (PersistBool b) = tshow b
fieldText (PersistDay d) = tshow d
fieldText (PersistTimeOfDay tod) = tshow tod
fieldText (PersistUTCTime utct) = T.pack $ adminTime utct
fieldText PersistNull = ""
fieldText (PersistList ls) = tshow ls
fieldText (PersistArray ls) = tshow ls
fieldText (PersistMap m) = tshow m
fieldText (PersistObjectId bs) = tshow bs
fieldText (PersistLiteral_ _ bs) = tshow bs

tshow :: (Show v) => v -> Text
tshow = T.pack . show

postAdminExportRawDataR :: Handler TypedContent
postAdminExportRawDataR = do
  csvs <-
    sequence
      [ getCsvFor @Account,
        getCsvFor @BusLine,
        getCsvFor @BusLineStop,
        getCsvFor @BusStop,
        getCsvFor @Checkout,
        getCsvFor @Child,
        getCsvFor @ChildOf,
        getCsvFor @ShoppingCart,
        getCsvFor @CustomActivity,
        getCsvFor @CustomActivityPayment,
        getCsvFor @CustomActivitySignup,
        getCsvFor @DaycareActivity,
        getCsvFor @DaycareActivityPayment,
        getCsvFor @DaycareActivitySignup,
        getCsvFor @DaycarePayment,
        getCsvFor @DaycareSemestre,
        getCsvFor @DaycareSignup,
        getCsvFor @DaycareTimeslot,
        getCsvFor @ExtraCharge,
        getCsvFor @Discount,
        getCsvFor @Doctor,
        getCsvFor @Email,
        getCsvFor @LanguageSection,
        getCsvFor @OccasionalDaycarePayment,
        getCsvFor @OccasionalDaycareSignup,
        getCsvFor @OccasionalTransportPayment,
        getCsvFor @OccasionalTransportPayment,
        getCsvFor @OccasionalTransportSignup,
        getCsvFor @OccasionalTransportSignup,
        getCsvFor @Parent,
        getCsvFor @PasswordResetEmail,
        getCsvFor @PaymentReceivedEmail,
        getCsvFor @AdminNotificationEmail,
        getCsvFor @DaycareEmail,
        getCsvFor @TransportEmail,
        getCsvFor @OccasionalDaycareEmail,
        getCsvFor @OccasionalTransportEmail,
        getCsvFor @StripeCustomer,
        getCsvFor @StripePayment,
        getCsvFor @TransportEnrollment,
        getCsvFor @TransportPayment,
        getCsvFor @TransportPaymentPlan,
        getCsvFor @TransportSignup,
        getCsvFor @VerificationEmail,
        getCsvFor @YearlyFeePayment
      ]
  let archive =
        foldl' (flip addEntryToArchive) emptyArchive (map (\(fp, bs) -> toEntry fp 0 bs) csvs)
  addHeader "Content-Disposition" $ T.concat ["attachment; filename=\"", "regtool-data.zip", "\""]
  sendResponse (TE.encodeUtf8 "application/zip", toContent $ fromArchive archive)

getCsvFor ::
  forall record.
  (ToBackendKey SqlBackend record) =>
  Handler (FilePath, LB.ByteString)
getCsvFor = do
  values <- runDB $ selectList [] [Asc (persistIdField :: EntityField record (Key record))]
  let def = entityDef (Proxy :: Proxy record)
  let pvalues =
        map
          ( \(Entity i r) ->
              tshow (fromSqlKey i) : map (fieldText . toPersistValue) (toPersistFields r)
          )
          values
  pure (T.unpack (unEntityNameDB $ getEntityDBName def) ++ ".csv", Csv.encode pvalues)
