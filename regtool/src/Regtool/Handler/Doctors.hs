{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Handler.Doctors
  ( RegisterDoctor (..),
    getDoctorsR,
    getDoctorR,
    getRegisterDoctorR,
    postRegisterDoctorR,
    postDeregisterDoctorR,
  )
where

import Regtool.Component.Register
import Regtool.Handler.Import
import Regtool.Utils.Doctors

getDoctorsR :: Handler Html
getDoctorsR = do
  accid <- requireAuthId
  doctors <- runDB $ selectList [DoctorAccount ==. accid] []
  token <- genToken
  withNavBar $(widgetFile "doctors")

data RegisterDoctor = RegisterDoctor
  { registerDoctorFirstName :: Text,
    registerDoctorLastName :: Text,
    registerDoctorAddressLine1 :: Text,
    registerDoctorAddressLine2 :: Maybe Text,
    registerDoctorPostalCode :: Text,
    registerDoctorCity :: Text,
    registerDoctorCountry :: Country,
    registerDoctorPhoneNumber1 :: Text,
    registerDoctorPhoneNumber2 :: Maybe Text,
    registerDoctorEmail1 :: EmailAddress,
    registerDoctorEmail2 :: Maybe EmailAddress,
    registerDoctorComments :: Maybe Textarea,
    registerDoctorId :: Maybe DoctorId
  }
  deriving (Show)

registerDoctorForm :: Maybe (Entity Doctor) -> AForm Handler RegisterDoctor
registerDoctorForm mce =
  RegisterDoctor
    <$> areq
      textField
      ((bfs ("First Name" :: Text)) {fsName = Just "first-name"})
      (dv doctorFirstName)
    <*> areq
      textField
      ((bfs ("Last Name" :: Text)) {fsName = Just "last-name"})
      (dv doctorLastName)
    <*> areq
      textField
      ((bfs ("Address line 1 (Street and number)" :: Text)) {fsName = Just "address-1"})
      (dv doctorAddressLine1)
    <*> aopt
      textField
      ((bfs ("Address line 2" :: Text)) {fsName = Just "address-2"})
      (dv doctorAddressLine2)
    <*> areq
      textField
      ((bfs ("Post code" :: Text)) {fsName = Just "post-code"})
      (dv doctorPostalCode)
    <*> areq
      textField
      ((bfs ("City" :: Text)) {fsName = Just "city"})
      (dv doctorCity)
    <*> standardOrRawForm
      ((bfs ("Country" :: Text)) {fsName = Just "country"})
      (dv doctorCountry)
    <*> areq
      textField
      ((bfs ("Phone number (primary)" :: Text)) {fsName = Just "phone-number-1"})
      (dv doctorPhoneNumber1)
    <*> aopt
      textField
      ((bfs ("Phone number (secondary)" :: Text)) {fsName = Just "phone-number-2"})
      (dv doctorPhoneNumber2)
    <*> areq
      emailAddressField
      ((bfs ("Email address (primary)" :: Text)) {fsName = Just "email-1"})
      (dv doctorEmail1)
    <*> aopt
      emailAddressField
      ((bfs ("Email address (secondary)" :: Text)) {fsName = Just "email-2"})
      (dv doctorEmail2)
    <*> aopt
      textareaField
      ((bfs ("Extra Comments" :: Text)) {fsName = Just "comments"})
      (dv doctorComments)
    <*> aopt
      hiddenField
      ("Id" {fsLabel = "", fsName = Just "id"})
      (Just $ entityKey <$> mce)
  where
    dv :: (Doctor -> a) -> Maybe a
    dv func = func . entityVal <$> mce

getRegisterDoctorR :: Handler Html
getRegisterDoctorR = do
  void requireAuthId
  makeRegisterForm "Doctor" RegisterDoctorR $ registerDoctorForm Nothing

postRegisterDoctorR :: Handler Html
postRegisterDoctorR = do
  aid <- requireAuthId
  processRegisterForm "Doctor" RegisterDoctorR (registerDoctorForm Nothing) $ \RegisterDoctor {..} -> do
    now <- liftIO getCurrentTime
    let doctor_ =
          Doctor
            { doctorAccount = aid,
              doctorFirstName = registerDoctorFirstName,
              doctorLastName = registerDoctorLastName,
              doctorAddressLine1 = registerDoctorAddressLine1,
              doctorAddressLine2 = registerDoctorAddressLine2,
              doctorPostalCode = registerDoctorPostalCode,
              doctorCity = registerDoctorCity,
              doctorCountry = registerDoctorCountry,
              doctorPhoneNumber1 = registerDoctorPhoneNumber1,
              doctorPhoneNumber2 = registerDoctorPhoneNumber2,
              doctorEmail1 = registerDoctorEmail1,
              doctorEmail2 = registerDoctorEmail2,
              doctorComments = registerDoctorComments,
              doctorRegistrationTime = now
            }
    case registerDoctorId of
      Just i ->
        withOwnDoctor i $ do
          runDB $ replaceValid i doctor_
          redirect DoctorsR
      Nothing -> do
        runDB $ insertValid_ doctor_
        redirect DoctorsR

getDoctorR :: DoctorId -> Handler Html
getDoctorR did =
  withOwnDoctor did $ do
    doctor_ <- runDB $ get404 did
    makeEditForm "Doctor" RegisterDoctorR DoctorsR $ registerDoctorForm $ Just (Entity did doctor_)

isDoctorOf :: DoctorId -> AccountId -> Handler Bool
isDoctorOf pid aid = (== Just True) . fmap ((== aid) . doctorAccount) <$> runDB (get pid)

withOwnDoctor :: DoctorId -> Handler Html -> Handler Html
withOwnDoctor cid func = do
  aid <- requireAuthId
  isOwnDoctor <- isDoctorOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnDoctor || isAdminAcc
    then func
    else permissionDenied "Not your doctor."

postDeregisterDoctorR :: DoctorId -> Handler Html
postDeregisterDoctorR did =
  withOwnDoctor did $ do
    canDeleteDoctor did >>= deleteOrError
    redirect DoctorsR
