{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Checkout.Page
  ( getCheckoutChoosePage,
  )
where

import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Core.AdminConfiguration as Admin
import Regtool.Handler.Checkout.Form
import Regtool.Handler.Import
import Regtool.Utils.Costs.Input
import Regtool.Utils.Stripe

getCheckoutChoosePage :: Maybe (FormResult a) -> Handler Html
getCheckoutChoosePage mfr = do
  ci <- getCostsInput
  env <- regtoolEnvironment <$> getYesod
  let ac = makeAbstractCosts ci
  conf <- confCostsConfig <$> Admin.getConfiguration
  let cc = calculateCosts conf ac
  let choices = deriveChoices cc
      invoice = deriveInvoice choices
  let emptyChoices = choices == mempty
  (formWidget, formEncType) <- generateFormPost $ chooseForm choices
  now <- liftIO getCurrentTime

  let -- Only for debugging
      proposedConsequences = paymentConsequences (costsInputAccount ci) (toSqlKey (-1)) now choices
      proposedAmount = makeLineItemAmount invoice
  let errors =
        let e :: (Show a, Validity a) => String -> a -> Maybe (String, String, String)
            e n v =
              case prettyValidate v of
                Left err -> Just (n, ppShow v, err)
                Right _ -> Nothing
         in catMaybes
              [ e "Costs Input" ci,
                e "Abstract Costs" ac,
                e "Environment" env,
                e "Costs Config" conf,
                e "Calculated Costs" cc,
                e "Choices" choices,
                e "Invoice" invoice,
                e "Proposed payment consequences" proposedConsequences,
                e "Proposed amount" proposedAmount
              ]
  maybe withNavBar withFormResultNavBar mfr
    $ case errors of
      [] -> mconcat [$(widgetFile "checkout-choose"), $(widgetFile "checkout-choose-debug")]
      _ -> $(widgetFile "checkout-choose-error")
