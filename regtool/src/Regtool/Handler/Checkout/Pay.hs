{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.Checkout.Pay
  ( postCheckoutPaymentNotNecessaryR,
    getCheckoutSuccessR,
    getCheckoutCancelR,
  )
where

import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Cache
import Regtool.Calculation.Costs.Invoice
import Regtool.Handler.Checkout.Page
import Regtool.Handler.Import

postCheckoutPaymentNotNecessaryR :: ShoppingCartId -> Handler Html
postCheckoutPaymentNotNecessaryR cartId = do
  aid <- requireAuthId
  mChoices <- runDB $ getShoppingCartOfAccount aid cartId
  case mChoices of
    Nothing -> do
      setMessage "No choices found"
      redirect CheckoutChooseR
    Just (cart, chosenChoices) -> do
      paymentNotNecessaryResult <- runInputPostResult paymentNotNecessaryForm
      let total = invoiceTotal $ deriveInvoice chosenChoices
      now <- liftIO getCurrentTime
      case paymentNotNecessaryResult of
        FormSuccess PaymentNotNecessary -> do
          when (total > zeroAmount) $ invalidArgs ["Internal error: should have had to pay"]
          when (maybe False cartStatusDone $ shoppingCartStatus cart)
            $ invalidArgs ["These choices have already been checked out"]
          runDB $ do
            Entity cid _ <- makeCheckout aid chosenChoices Nothing
            let pcs = paymentConsequences aid cid now chosenChoices
            realisePaymentConsequences pcs
            update cartId [ShoppingCartStatus =. Just CartPaymentNotNecessary]
          setMessage "Success!"
          redirect PaymentsR
        fr -> getCheckoutChoosePage $ Just fr

getCheckoutSuccessR :: ShoppingCartId -> Handler Html
getCheckoutSuccessR cartId = do
  setMessage "Payment success, please wait for confirmation. This may take up to five minutes."
  -- TODO try to fetch the success event already, instead of waiing for the looper to run.
  runDB $ update cartId [ShoppingCartStatus =. Just CartPaymentInitiated]
  redirect PaymentsR

getCheckoutCancelR :: ShoppingCartId -> Handler Html
getCheckoutCancelR cartId = do
  setMessage "Payment Cancelled"
  runDB $ update cartId [ShoppingCartStatus =. Just CartPaymentCancelled]
  redirect CheckoutChooseR

data PaymentNotNecessary
  = PaymentNotNecessary

paymentNotNecessaryForm :: FormInput Handler PaymentNotNecessary
paymentNotNecessaryForm =
  PaymentNotNecessary <$ iopt (hiddenField :: Field Handler ()) "payment-not-necessary"
