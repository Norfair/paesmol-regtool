module Regtool.Handler.Checkout.Choose
  ( getCheckoutChooseR,
  )
where

import Regtool.Handler.Checkout.Page
import Regtool.Handler.Import

getCheckoutChooseR :: Handler Html
getCheckoutChooseR = getCheckoutChoosePage Nothing
