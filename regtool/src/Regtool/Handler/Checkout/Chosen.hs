{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Checkout.Chosen
  ( postCheckoutChooseR,
  )
where

import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Cache
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Core.AdminConfiguration as Admin
import Regtool.Handler.Checkout.Form
import Regtool.Handler.Checkout.Page
import Regtool.Handler.Import
import Regtool.Utils.Costs.Input
import Regtool.Utils.Stripe
import Text.Julius

postCheckoutChooseR :: Handler Html
postCheckoutChooseR = do
  aid <- requireAuthId
  env <- regtoolEnvironment <$> getYesod
  ci <- getCostsInput
  let ac = makeAbstractCosts ci
  conf <- confCostsConfig <$> Admin.getConfiguration
  let cc = calculateCosts conf ac
  let choices = deriveChoices cc
  ((result, _), _) <- runFormPost $ chooseForm choices
  now <- liftIO getCurrentTime
  let proposedConsequences =
        -- Only for debugging
        paymentConsequences (costsInputAccount ci) (toSqlKey (-1)) now choices
  case result of
    FormSuccess chosenChoices -> do
      let i = deriveInvoice chosenChoices
      let proposedAmountResult = makeLineItemAmount i
      token <- genToken
      let total = invoiceTotal i
      case proposedAmountResult of
        NegativeOrZeroAmount -> do
          cartId <- runDB $ startShoppingCart aid chosenChoices
          let invoiceWidget = $(widgetFile "invoice")
          let checkoutForm = $(widgetFile "confirm-form")
          withNavBar
            $ mconcat [$(widgetFile "checkout-chosen"), $(widgetFile "checkout-chosen-debug")]
        StrictlyPositiveAmount proposedAmount -> do
          cartId <- runDB $ startShoppingCart aid chosenChoices
          sessionId <- makeStripeSession cartId proposedAmount
          runDB $ update cartId [ShoppingCartSession =. Just sessionId] -- Two-phase because the stripe call can fail.
          mspk <- fmap stripeSettingPublishableKey . regtoolStripeSettings <$> getYesod
          let checkoutForm =
                case mspk of
                  Just publishableKey -> do
                    addScriptRemote "https://js.stripe.com/v3/"
                    $(widgetFile "stripe-form")
                  Nothing -> "There must be no interaction with stripe during automated testing."
          let invoiceWidget = $(widgetFile "invoice")
          withNavBar
            $ mconcat [$(widgetFile "checkout-chosen"), $(widgetFile "checkout-chosen-debug")]
    fr -> getCheckoutChoosePage $ Just fr

makeStripeSession ::
  ShoppingCartId ->
  Amount ->
  Handler Text -- Stripe session id
makeStripeSession cartId =
  stripeCreateSession (CheckoutSuccessR cartId) (CheckoutCancelR cartId)
