{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Checkout.Form
  ( chooseForm,
  )
where

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Regtool.Calculation.Children
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Time (formatTimestamp)
import Regtool.Handler.Import

chooseForm :: Choices -> Html -> MForm Handler (FormResult Choices, Widget)
chooseForm Choices {..} extra = do
  tups <-
    forM (M.toAscList choicesYearlyMap) $ \(sy, yc) -> do
      (fryc, w) <- yearlyChoicesForm sy yc
      pure ((sy, fryc), (sy, w))
  (otcsRes, otcsWidget) <- occasionalTransportChoicesForm choicesOccasionalTransport
  (odcsRes, odcsWidget) <- occasionalDaycareChoicesForm choicesOccasionalDaycare
  (cacsRes, cacsWidget) <- customActivityChoicesForm choicesCustomActivity
  (ecsRes, ecsWidget) <- extraChargesChoicesForm choicesExtraCharges
  (dsRes, dsWidget) <- discountsChoicesForm choicesDiscounts
  let res =
        Choices
          <$> (M.fromList <$> traverse ((\(sy, fryc) -> (,) sy <$> fryc) . fst) tups)
          <*> otcsRes
          <*> odcsRes
          <*> cacsRes
          <*> ecsRes
          <*> dsRes
  let widget =
        [whamlet|
                #{extra}
                <ul .list-group>
                    $forall (_, w) <- map snd tups
                        <li .list-group-item>
                            ^{w}
                    $if (not (null choicesOccasionalTransport))
                        <li .list-group-item>
                            ^{otcsWidget}
                    $if (not (null choicesOccasionalDaycare))
                        <li .list-group-item>
                            ^{odcsWidget}
                    $if (not (null choicesCustomActivity))
                        <li .list-group-item>
                            ^{cacsWidget}
                    $if (not (null choicesExtraCharges))
                        <li .list-group-item>
                            ^{ecsWidget}
                    $if (not (null choicesDiscounts))
                        <li .list-group-item>
                            ^{dsWidget}
                    |]
  pure (res, widget)

yearlyChoicesForm ::
  BusSchoolYear -> YearlyChoices -> MForm Handler (FormResult YearlyChoices, Widget)
yearlyChoicesForm bsy YearlyChoices {..} = do
  myfp <-
    case yearlyChoicesYearlyFee of
      Nothing -> pure Nothing
      Just a -> Just <$> yearlyFeeChoiceForm (busSchoolYearYear bsy) a
  tups <-
    forM (M.toList yearlyChoicesTransportInstalments) $ \(c, tics) ->
      (,) c <$> transportInstalmentsForm bsy c tics
  scss <-
    forM (M.toList yearlyChoicesSemestrelyMap) $ \(s, sc) -> (,) s <$> semestrelyChoicesForm s sc
  let res =
        YearlyChoices
          <$> maybe (pure Nothing) fst myfp
          <*> (M.mapMaybe id . M.fromList <$> traverse (\(c, (tics, _)) -> (,) c <$> tics) tups)
          <*> (M.fromList <$> traverse (\(c, (scs, _)) -> (,) c <$> scs) scss)
  let widget =
        [whamlet|
            <h2>
                #{schoolYearText (busSchoolYearYear bsy)}
            <table .table>
                <tr>
                    <th>
                        Service
                    <th .col-md-1>
                        Amount
                    <th .col-md-1>
                        Select
                $maybe (_, yfpw) <- myfp
                    ^{yfpw}
                $forall (_, (_, tisw)) <- tups
                    ^{tisw}
                $forall (_, (_, scw)) <- scss
                    ^{scw}
        |]
  pure (res, widget)

semestrelyChoicesForm ::
  Entity DaycareSemestre ->
  SemestrelyChoices ->
  MForm Handler (FormResult SemestrelyChoices, Widget)
semestrelyChoicesForm dcs SemestrelyChoices {..} = do
  tups <- forM semestrelyChoicesDaycareChoices $ daycareChoiceForm dcs
  activityTups <- forM semestrelyChoicesDaycareActivityChoices $ daycareActivityChoiceForm dcs
  let res =
        SemestrelyChoices
          <$> fmap catMaybes (traverse fst tups)
          <*> fmap catMaybes (traverse fst activityTups)
  let widget = mconcat $ map snd tups ++ map snd activityTups
  pure (res, widget)

yearlyFeeChoiceForm :: SchoolYear -> Amount -> MForm Handler (FormResult (Maybe Amount), Widget)
yearlyFeeChoiceForm sy a = do
  (r, fv) <- unUnselectableSelectTheThingForm a True "unused1"
  let widget =
        [whamlet|
                <tr>
                    <td>
                        Yearly fee for #{schoolYearText sy}
                    <td>
                        #{fmtAmount a}
                        €
                    <td>
                        ^{fvInput fv}
            |]
  pure (r, widget)

transportInstalmentsForm ::
  BusSchoolYear ->
  Entity Child ->
  TransportInstallmentChoices ->
  MForm Handler (FormResult (Maybe TransportInstallmentChoices), Widget)
transportInstalmentsForm sy c tics@TransportInstallmentChoices {..} = do
  tups <-
    case transportInstallmentChoicesInstalments of
      (a :| as) ->
        let f = transportInstallmentForm sy c $ entityVal transportInstallmentChoicesTransportSignup
         in sequenceA $ f True a : map (f False) as
  let mres =
        fmap (\as -> tics {transportInstallmentChoicesInstalments = as})
          . NE.nonEmpty
          . catMaybes
          <$> traverse fst tups
  let widget = mconcat $ map snd tups
  pure (mres, widget)

transportInstallmentForm ::
  BusSchoolYear ->
  Entity Child ->
  TransportSignup ->
  Bool ->
  Amount ->
  MForm Handler (FormResult (Maybe Amount), Widget)
transportInstallmentForm BusSchoolYear {..} (Entity _ c) ts b a = do
  (r, fv) <-
    if busSchoolYearOpen
      then selectTheThingForm a True "unused2"
      else unUnselectableSelectTheThingForm a b "unused2"
  let widget =
        [whamlet|
                <tr>
                    <td>
                        $if transportSignupInstalments ts <= 1
                            Bus services for #{childName c}
                        $else
                            Bus services for #{childName c} installment
                    <td>
                        #{fmtAmount a}
                        €
                    <td>
                        ^{fvInput fv}
            |]
  pure (r, widget)

occasionalTransportChoicesForm ::
  [OccasionalTransportChoice] -> MForm Handler (FormResult [OccasionalTransportChoice], Widget)
occasionalTransportChoicesForm [] = pure (pure [], mempty)
occasionalTransportChoicesForm otcs = do
  tups <- mapM occasionalTransportChoiceForm otcs
  let res = catMaybes <$> traverse fst tups
  let widget =
        [whamlet|
                <h2>
                    Occasional Transport Service
                <table .table>
                    <tr>
                        <th>
                            Child
                        <th>
                            Day
                        <th>
                            Amount
                        <th>
                            Select
                    ^{mconcat $ map snd tups}
                |]
  pure (res, widget)

occasionalTransportChoiceForm ::
  OccasionalTransportChoice ->
  MForm Handler (FormResult (Maybe OccasionalTransportChoice), Widget)
occasionalTransportChoiceForm otc = do
  (r, fv) <- unUnselectableSelectTheThingForm otc True "unused3"
  let OccasionalTransportChoice {..} = otc
  let widget =
        [whamlet|
                <tr>
                    <td>
                        #{childName $ entityVal occasionalTransportChoiceChild}
                    <td>
                        #{prettyFormatDay $ occasionalTransportSignupDay $ entityVal occasionalTransportChoiceSignup}
                    <td>
                        #{fmtAmount occasionalTransportChoiceAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

occasionalDaycareChoicesForm ::
  [OccasionalDaycareChoice] -> MForm Handler (FormResult [OccasionalDaycareChoice], Widget)
occasionalDaycareChoicesForm [] = pure (pure [], mempty)
occasionalDaycareChoicesForm otcs = do
  tups <- mapM occasionalDaycareChoiceForm otcs
  let res = catMaybes <$> traverse fst tups
  let widget =
        [whamlet|
                <h2>
                    Occasional Daycare Service
                <table .table>
                    <tr>
                        <th>
                            Child
                        <th>
                            Day
                        <th>
                            Amount
                        <th>
                            Select
                    ^{mconcat $ map snd tups}
                |]
  pure (res, widget)

occasionalDaycareChoiceForm ::
  OccasionalDaycareChoice -> MForm Handler (FormResult (Maybe OccasionalDaycareChoice), Widget)
occasionalDaycareChoiceForm otc = do
  (r, fv) <- unUnselectableSelectTheThingForm otc True "unused3"
  let OccasionalDaycareChoice {..} = otc
  let widget =
        [whamlet|
                <tr>
                    <td>
                        #{childName $ entityVal occasionalDaycareChoiceChild}
                    <td>
                        #{prettyFormatDay $ occasionalDaycareSignupDay $ entityVal occasionalDaycareChoiceSignup}
                    <td>
                        #{fmtAmount occasionalDaycareChoiceAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

customActivityChoicesForm ::
  [CustomActivityChoice] -> MForm Handler (FormResult [CustomActivityChoice], Widget)
customActivityChoicesForm [] = pure (pure [], mempty)
customActivityChoicesForm otcs = do
  tups <- mapM customActivityChoiceForm otcs
  let res = catMaybes <$> traverse fst tups
  let widget =
        [whamlet|
                <h2>
                    Custom Activities
                <table .table>
                    <tr>
                        <th>
                            Child
                        <th>
                            Day
                        <th>
                            Time
                        <th>
                            Amount
                        <th>
                            Select
                    ^{mconcat $ map snd tups}
                |]
  pure (res, widget)

customActivityChoiceForm ::
  CustomActivityChoice -> MForm Handler (FormResult (Maybe CustomActivityChoice), Widget)
customActivityChoiceForm cac = do
  (r, fv) <- unUnselectableSelectTheThingForm cac True "unused3"
  let CustomActivityChoice {..} = cac
  let widget =
        [whamlet|
                <tr>
                    <td>
                        #{childName $ entityVal customActivityChoiceChild}
                    <td>
                        #{prettyFormatDay $ customActivityDate $ entityVal customActivityChoiceActivity}
                    <td>
                        #{formatTimestamp $ customActivityStart $ entityVal customActivityChoiceActivity}
                    <td>
                        #{fmtAmount $ customActivityFee $ entityVal customActivityChoiceActivity}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

daycareChoiceForm ::
  Entity DaycareSemestre ->
  DaycareChoice ->
  MForm Handler (FormResult (Maybe DaycareChoice), Widget)
daycareChoiceForm (Entity _ dcs) dc = do
  let DaycareSemestre {..} = dcs
  (r, fv) <- unUnselectableSelectTheThingForm dc True "unused4"
  let DaycareChoice {..} = dc
  let (Entity _ DaycareTimeslot {..}) = daycareChoiceTimeslot
  let widget =
        [whamlet|
                <tr>
                    <td>
                        Daycare for #{childName $ entityVal daycareChoiceChild} for
                        #{semestreText daycareSemestreSemestre}
                        #{schoolYearText daycareSemestreYear}
                        on
                        #{schoolDayText daycareTimeslotSchoolDay}
                        at
                        #{show daycareTimeslotStart} - #{show daycareTimeslotEnd}
                    <td>
                        #{fmtAmount daycareChoiceAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

daycareActivityChoiceForm ::
  Entity DaycareSemestre ->
  DaycareActivityChoice ->
  MForm Handler (FormResult (Maybe DaycareActivityChoice), Widget)
daycareActivityChoiceForm (Entity _ dcs) dc = do
  (r, fv) <- unUnselectableSelectTheThingForm dc True "unused4"
  let DaycareSemestre {..} = dcs
  let DaycareActivityChoice {..} = dc
  let (Entity _ DaycareActivity {..}) = daycareActivityChoiceActivity
  let widget =
        [whamlet|
                <tr>
                    <td>
                        Daycare activity for #{childName $ entityVal daycareActivityChoiceChild} for
                        #{semestreText daycareSemestreSemestre}
                        #{schoolYearText daycareSemestreYear}
                        on
                        #{schoolDayText daycareActivitySchoolDay}
                        at
                        #{show daycareActivityStart} - #{show daycareActivityEnd}
                        : #{daycareActivityName}
                    <td>
                        #{fmtAmount daycareActivityChoiceAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

extraChargesChoicesForm ::
  [Entity ExtraCharge] -> MForm Handler (FormResult [Entity ExtraCharge], Widget)
extraChargesChoicesForm ds = do
  tups <- mapM extraChargeChoiceForm ds
  let res = catMaybes <$> traverse fst tups
  let widget =
        [whamlet|
                <h2>
                    Extra Charges
                <table .table>
                    <tr>
                        <th>
                            Reason
                        <th>
                            Amount
                        <th>
                            Select
                    ^{mconcat $ map snd tups}
                |]
  pure (res, widget)

extraChargeChoiceForm ::
  Entity ExtraCharge -> MForm Handler (FormResult (Maybe (Entity ExtraCharge)), Widget)
extraChargeChoiceForm extraCharge = do
  (r, fv) <- unUnselectableSelectTheThingForm extraCharge True "unused_extra_charge"
  let (Entity _ ExtraCharge {..}) = extraCharge
  let widget =
        [whamlet|
                <tr>
                    <td>
                        #{fromMaybe "Extra charge" extraChargeReason}
                    <td>
                        #{fmtAmount extraChargeAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

discountsChoicesForm :: [Entity Discount] -> MForm Handler (FormResult [Entity Discount], Widget)
discountsChoicesForm ds = do
  tups <- mapM discountChoiceForm ds
  let res = catMaybes <$> traverse fst tups
  let widget =
        [whamlet|
                <h2>
                    Discounts
                <table .table>
                    <tr>
                        <th>
                            Reason
                        <th>
                            Amount
                        <th>
                            Discount
                    ^{mconcat $ map snd tups}
                |]
  pure (res, widget)

discountChoiceForm ::
  Entity Discount -> MForm Handler (FormResult (Maybe (Entity Discount)), Widget)
discountChoiceForm discount = do
  (r, fv) <- unUnselectableSelectTheThingForm discount True "unused_discount"
  let (Entity _ Discount {..}) = discount
  let widget =
        [whamlet|
                <tr .success>
                    <td>
                        #{fromMaybe "Discount" discountReason}
                    <td>
                        #{fmtAmount discountAmount}
                        €
                    <td>
                        ^{fvInput fv}
                |]
  pure (r, widget)

selectTheThingForm ::
  a -> Bool -> FieldSettings Regtool -> MForm Handler (FormResult (Maybe a), FieldView Regtool)
selectTheThingForm a b fs = do
  (r, fv) <-
    mreq
      checkBoxField
      fs
      ( if b
          then Just True
          else Nothing
      )
  let vr =
        ( \b_ ->
            if b_
              then Just a
              else Nothing
        )
          <$> r
  pure (vr, fv)

unUnselectableSelectTheThingForm ::
  a -> Bool -> FieldSettings Regtool -> MForm Handler (FormResult (Maybe a), FieldView Regtool)
unUnselectableSelectTheThingForm a b fs = do
  (r, fv) <-
    mreq
      disabledCheckboxField
      fs
      ( if b
          then Just True
          else Nothing
      )
  let vr =
        ( \_b -> Just a -- No choice
        )
          <$> r
  pure (vr, fv)
  where
    disabledCheckboxField :: (Monad m) => Field m Bool
    disabledCheckboxField =
      Field
        { fieldParse = \_e _ -> pure (Right (Just True)),
          fieldView =
            \theId name attrs val _ ->
              [whamlet|
  $newline never
  <input id=#{theId} *{attrs} type=checkbox name=#{name} value=yes :showVal val:checked disabled>
  |],
          fieldEnctype = UrlEncoded
        }
    showVal e = case e of
      Left _ -> False
      Right b_ -> b_
