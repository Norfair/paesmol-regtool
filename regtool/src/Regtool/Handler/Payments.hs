{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Payments
  ( getPaymentsR,
  )
where

import qualified Data.Map as M
import Regtool.Calculation.Costs.Checkout
import Regtool.Handler.Import
import Regtool.Utils.Costs.Input

getPaymentsR :: Handler Html
getPaymentsR = do
  ci <- getCostsInput
  let co = deriveCheckoutsOverview ci
      checkoutOverviewWidget coo = $(widgetFile "checkout-overview")
  withNavBar $(widgetFile "payments")
