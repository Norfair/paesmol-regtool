{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

-- At least until the infinite compile-time is fixed.
module Regtool.Handler.Parents
  ( RegisterParent (..),
    getParentsR,
    getParentR,
    getRegisterParentR,
    postRegisterParentR,
    postDeregisterParentR,
  )
where

import Regtool.Component.Register
import Regtool.Handler.Import hiding (isParentOf)
import Regtool.Utils.Parents

getParentsR :: Handler Html
getParentsR = do
  accid <- requireAuthId
  parents <- runDB $ selectList [ParentAccount ==. accid] []
  token <- genToken
  withNavBar $(widgetFile "parents")

data RegisterParent = RegisterParent
  { registerParentFirstName :: Text,
    registerParentLastName :: Text,
    registerParentAddressLine1 :: Text,
    registerParentAddressLine2 :: Maybe Text,
    registerParentPostalCode :: Text,
    registerParentCity :: Text,
    registerParentCountry :: Country,
    registerParentPhoneNumber1 :: Text,
    registerParentPhoneNumber2 :: Maybe Text,
    registerParentEmail1 :: EmailAddress,
    registerParentEmail2 :: Maybe EmailAddress,
    registerParentCompany :: Company,
    registerParentRelationShip :: RelationShip,
    registerParentComments :: Maybe Textarea,
    registerParentId :: Maybe ParentId
  }
  deriving (Show)

registerParentForm :: Maybe (Entity Parent) -> AForm Handler RegisterParent
registerParentForm mce =
  RegisterParent
    <$> areq
      textField
      ((bfs ("First Name" :: Text)) {fsName = Just "first-name"})
      (dv parentFirstName)
    <*> areq
      textField
      ((bfs ("Last Name" :: Text)) {fsName = Just "last-name"})
      (dv parentLastName)
    <*> areq
      textField
      ((bfs ("Address line 1 (Street and number)" :: Text)) {fsName = Just "address-1"})
      (dv parentAddressLine1)
    <*> aopt
      textField
      ((bfs ("Address line 2" :: Text)) {fsName = Just "address-2"})
      (dv parentAddressLine2)
    <*> areq
      textField
      ((bfs ("Post code" :: Text)) {fsName = Just "post-code"})
      (dv parentPostalCode)
    <*> areq
      textField
      ((bfs ("City" :: Text)) {fsName = Just "city"})
      (dv parentCity)
    <*> standardOrRawForm
      ((bfs ("Country" :: Text)) {fsName = Just "country"})
      (dv parentCountry)
    <*> areq
      textField
      ((bfs ("Phone number (primary)" :: Text)) {fsName = Just "phone-number-1"})
      (dv parentPhoneNumber1)
    <*> aopt
      textField
      ((bfs ("Phone number (secondary)" :: Text)) {fsName = Just "phone-number-2"})
      (dv parentPhoneNumber2)
    <*> areq
      emailAddressField
      ((bfs ("Email address (primary)" :: Text)) {fsName = Just "email-1"})
      (dv parentEmail1)
    <*> aopt
      emailAddressField
      ((bfs ("Email address (secondary)" :: Text)) {fsName = Just "email-2"})
      (dv parentEmail2)
    <*> areq
      (selectField $ optionsPairs $ map (\c -> (companyText c, c)) [minBound .. maxBound])
      ((bfs ("Company" :: Text)) {fsName = Just "company"})
      (dv parentCompany `mplus` Just OtherCompany)
    <*> standardOrRawForm
      ((bfs ("Relation to the children" :: Text)) {fsName = Just "relationship"})
      (dv parentRelationShip)
    <*> aopt
      textareaField
      ((bfs ("Extra Comments" :: Text)) {fsName = Just "comments"})
      (dv parentComments)
    <*> aopt
      hiddenField
      ("Id" {fsLabel = "", fsName = Just "id"})
      (Just $ entityKey <$> mce)
  where
    dv :: (Parent -> a) -> Maybe a
    dv func = func . entityVal <$> mce

getRegisterParentR :: Handler Html
getRegisterParentR = do
  void requireAuthId
  makeRegisterForm "Parent" RegisterParentR $ registerParentForm Nothing

postRegisterParentR :: Handler Html
postRegisterParentR = do
  aid <- requireAuthId
  processRegisterForm "Parent" RegisterParentR (registerParentForm Nothing) $ \RegisterParent {..} -> do
    now <- liftIO getCurrentTime
    let parent_ =
          Parent
            { parentAccount = aid,
              parentFirstName = registerParentFirstName,
              parentLastName = registerParentLastName,
              parentAddressLine1 = registerParentAddressLine1,
              parentAddressLine2 = registerParentAddressLine2,
              parentPostalCode = registerParentPostalCode,
              parentCity = registerParentCity,
              parentCountry = registerParentCountry,
              parentPhoneNumber1 = registerParentPhoneNumber1,
              parentPhoneNumber2 = registerParentPhoneNumber2,
              parentEmail1 = registerParentEmail1,
              parentEmail2 = registerParentEmail2,
              parentCompany = registerParentCompany,
              parentRelationShip = registerParentRelationShip,
              parentComments = registerParentComments,
              parentRegistrationTime = now
            }
    case registerParentId of
      Just i ->
        withOwnParent i $ do
          runDB $ replaceValid i parent_
          redirect ParentsR
      Nothing -> do
        runDB $ insertValid_ parent_
        redirect ParentsR

getParentR :: ParentId -> Handler Html
getParentR cid =
  withOwnParent cid $ do
    parent_ <- runDB $ get404 cid
    makeEditForm "Parent" RegisterParentR ParentsR $ registerParentForm $ Just (Entity cid parent_)

isParentOf :: ParentId -> AccountId -> Handler Bool
isParentOf pid aid = (== Just True) . fmap ((== aid) . parentAccount) <$> runDB (get pid)

withOwnParent :: ParentId -> Handler Html -> Handler Html
withOwnParent cid func = do
  aid <- requireAuthId
  isOwnParent <- isParentOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnParent || isAdminAcc
    then func
    else permissionDenied "Not your parent."

postDeregisterParentR :: ParentId -> Handler Html
postDeregisterParentR pid =
  withOwnParent pid $ do
    canDeleteParent pid >>= deleteOrError
    redirect ParentsR
