{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Profile where

import Database.Esqueleto.Legacy ((^.))
import qualified Database.Esqueleto.Legacy as E
import qualified Database.Persist as DB
import Regtool.Handler.Import

getProfileR :: Handler Html
getProfileR = do
  Entity _ acc <- getAccount
  token <- genToken
  verEmails <-
    runDB
      $ selectList
        [VerificationEmailTo ==. accountEmailAddress acc, VerificationEmailEmail ==. Nothing]
        [Desc VerificationEmailTimestamp]
  triedVerEmails <-
    runDB
      $ E.select
      $ E.from
      $ \(verEmail `E.InnerJoin` email) -> do
        E.on
          $ (verEmail ^. VerificationEmailEmail E.==. E.just (email ^. EmailId))
          E.&&. (verEmail ^. VerificationEmailTo E.==. E.val (accountEmailAddress acc))
        return (email ^. EmailStatus, verEmail ^. VerificationEmailTimestamp)
  let userNamePlaceholder = fromMaybe "Your username" $ accountUsername acc
  withNavBar $(widgetFile "profile")

newtype SetUserNameRequest = SetUserNameRequest
  { setUserName :: Text
  }

setUserNameIForm :: FormInput Handler SetUserNameRequest
setUserNameIForm = SetUserNameRequest <$> ireq textField "user-name"

postProfileSetUserNameR :: Handler Html
postProfileSetUserNameR = do
  Entity aid acc <- getAccount
  if accountVerified acc
    then do
      result <- runInputPostResult setUserNameIForm
      case result of
        FormFailure _ -> setMessage "Failed to set display name."
        FormMissing -> setMessage "Missing data"
        FormSuccess (SetUserNameRequest newUserName) -> do
          setMessage "Set new user name."
          ma <- runDB $ getBy $ UniqueAccountUsername $ Just newUserName
          case ma of
            Just _ -> setMessage "This username has already been claimed"
            Nothing -> runDB $ DB.update aid [AccountUsername DB.=. Just newUserName]
      redirect ProfileR
    else notFound
