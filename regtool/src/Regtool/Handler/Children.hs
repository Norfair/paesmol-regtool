{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Handler.Children
  ( RegisterChild (..),
    getChildrenR,
    getChildR,
    getRegisterChildR,
    postRegisterChildR,
    postDeregisterChildR,
    getChildrenConfirmLevelR,
    postChildrenConfirmLevelR,
  )
where

import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Component.Register
import Regtool.Core.AdminConfiguration as Admin
import Regtool.Handler.Import
import Regtool.Utils.Children

getChildrenR :: Handler Html
getChildrenR = do
  accid <- requireAuthId
  children <- runDB $ getChildrenOf accid
  token <- genToken
  mResetDay <- confChildrenLevelResetDay <$> Admin.getConfiguration
  withNavBar $(widgetFile "children")

data RegisterChild = RegisterChild
  { registerChildFirstName :: Text,
    registerChildLastName :: Text,
    registerChildBirthDate :: Day,
    registerChildSchoolLevel :: SchoolLevel,
    registerChildLanguageSection :: LanguageSectionId,
    registerChildNationality :: Nationality,
    registerChildAllergies :: Maybe Textarea,
    registerChildHealthInfo :: Maybe Textarea,
    registerChildMedication :: Maybe Textarea,
    registerChildComments :: Maybe Textarea,
    registerChildPickupAllowed :: Maybe Textarea,
    registerChildPickupDisallowed :: Maybe Textarea,
    registerChildPicturePermission :: Bool,
    registerChildDelijnBus :: Bool,
    registerChildDoctor :: DoctorId,
    registerChildId :: Maybe ChildId
  }
  deriving (Show)

registerChildForm ::
  Maybe (Entity Child) ->
  [Entity Doctor] ->
  [Entity LanguageSection] ->
  AForm Handler RegisterChild
registerChildForm mce docOpts lsOpts =
  RegisterChild
    <$> areq
      textField
      ((bfs ("First Name" :: Text)) {fsName = Just "first-name"})
      (dv childFirstName)
    <*> areq
      textField
      ((bfs ("Last Name" :: Text)) {fsName = Just "last-name"})
      (dv childLastName)
    <*> areq
      dayField
      ((bfs ("Date of Birth" :: Text)) {fsName = Just "birth-date"})
      (dv childBirthdate)
    <*> areq
      schoolLevelField
      ((bfs ("Level and Grade" :: Text)) {fsName = Just "school-level"})
      (dv childLevel)
    <*> areq
      (selectFieldList $ map (\(Entity lsid l) -> (languageSectionName l, lsid)) lsOpts)
      ((bfs ("Language Section" :: Text)) {fsName = Just "language-section"})
      (dv childLanguageSection)
    <*> standardOrRawForm
      ((bfs ("Nationality" :: Text)) {fsName = Just "nationality"})
      (dv childNationality)
    <*> aopt
      textareaField
      ((bfs ("Allergy Information" :: Text)) {fsName = Just "allergy-info"})
      (dv childAllergies)
    <*> aopt
      textareaField
      ((bfs ("Other Health Information" :: Text)) {fsName = Just "other-health-info"})
      (dv childHealthInfo)
    <*> aopt
      textareaField
      ((bfs ("Medication" :: Text)) {fsName = Just "medication"})
      (dv childMedication)
    <*> aopt
      textareaField
      ((bfs ("Extra Comments" :: Text)) {fsName = Just "extra-comments"})
      (dv childComments)
    <*> aopt
      textareaField
      ((bfs ("People who are allowed to pick up this child" :: Text)) {fsName = Just "pickup-allowed"})
      (dv childPickupAllowed)
    <*> aopt
      textareaField
      ((bfs ("People who are not allowed to pick up this child" :: Text)) {fsName = Just "pickup-disallowed"})
      (dv childPickupDisallowed)
    <*> areq
      checkBoxField
      ((bfs ("Permission to use pictures (without names)" :: Text)) {fsName = Just "picture-permission"})
      (dv childPicturePermission)
    <*> areq
      checkBoxField
      ((bfs ("Needs to be brought to the bus" :: Text)) {fsName = Just "brought-to-bus"})
      (dv childDelijnBus)
    <*> areq
      ( selectFieldList
          $ map (\(Entity did d) -> (T.unwords [doctorFirstName d, doctorLastName d], did)) docOpts
      )
      ((bfs ("Doctor" :: Text)) {fsName = Just "doctor"})
      (dv childDoctor)
    <*> aopt
      hiddenField
      ("Id" {fsLabel = "", fsName = Just "id"})
      (Just $ entityKey <$> mce)
  where
    dv :: (Child -> a) -> Maybe a
    dv func = func . entityVal <$> mce

makeRegisterChildForm :: Maybe (Entity Child) -> Handler (AForm Handler RegisterChild)
makeRegisterChildForm mcid = do
  aid <- requireAuthId
  ds <- runDB $ selectList [DoctorAccount ==. aid] []
  lss <- runDB $ selectList [] [Asc LanguageSectionId]
  pure $ registerChildForm mcid ds lss

getRegisterChildR :: Handler Html
getRegisterChildR = do
  void requireAuthId
  childForm <- makeRegisterChildForm Nothing
  makeRegisterForm "Child" RegisterChildR childForm

postRegisterChildR :: Handler Html
postRegisterChildR = do
  aid <- requireAuthId
  childForm <- makeRegisterChildForm Nothing
  processRegisterForm "Child" RegisterChildR childForm $ \RegisterChild {..} -> do
    now <- liftIO getCurrentTime
    case registerChildId of
      Just i ->
        withOwnChild i $ do
          runDB
            $ update
              i
              [ ChildFirstName =. registerChildFirstName,
                ChildLastName =. registerChildLastName,
                ChildBirthdate =. registerChildBirthDate,
                ChildLevel =. registerChildSchoolLevel,
                ChildLanguageSection =. registerChildLanguageSection,
                ChildNationality =. registerChildNationality,
                ChildAllergies =. registerChildAllergies,
                ChildHealthInfo =. registerChildHealthInfo,
                ChildMedication =. registerChildMedication,
                ChildComments =. registerChildComments,
                ChildPickupAllowed =. registerChildPickupAllowed,
                ChildPickupDisallowed =. registerChildPickupDisallowed,
                ChildPicturePermission =. registerChildPicturePermission,
                ChildDelijnBus =. registerChildDelijnBus,
                ChildDoctor =. registerChildDoctor
                -- childRegistrationTime =. now, DON'T update registration time
                --
                -- Don't update confirmation time because people won't be paying attention to the school level.
                -- ChildLatestConfirmationTime =. Just now
              ]
          redirect ChildrenR
      Nothing -> do
        let child =
              Child
                { childFirstName = registerChildFirstName,
                  childLastName = registerChildLastName,
                  childBirthdate = registerChildBirthDate,
                  childLevel = registerChildSchoolLevel,
                  childLanguageSection = registerChildLanguageSection,
                  childNationality = registerChildNationality,
                  childAllergies = registerChildAllergies,
                  childHealthInfo = registerChildHealthInfo,
                  childMedication = registerChildMedication,
                  childComments = registerChildComments,
                  childPickupAllowed = registerChildPickupAllowed,
                  childPickupDisallowed = registerChildPickupDisallowed,
                  childPicturePermission = registerChildPicturePermission,
                  childDelijnBus = registerChildDelijnBus,
                  childDoctor = registerChildDoctor,
                  childRegistrationTime = now,
                  childLatestConfirmationTime = Just now
                }
        runDB $ do
          cid <- insertValid child
          insertValid_ $ ChildOf cid aid
        redirect ChildrenR

getChildR :: ChildId -> Handler Html
getChildR cid =
  withOwnChild cid $ do
    child <- runDB $ get404 cid
    childForm <- makeRegisterChildForm (Just (Entity cid child))
    makeEditForm "Child" RegisterChildR ChildrenR childForm

postDeregisterChildR :: ChildId -> Handler Html
postDeregisterChildR cid =
  withOwnChild cid $ do
    canDeleteChild cid >>= deleteOrError
    redirect ChildrenR

data ConfirmChild = ConfirmChild
  { confirmChildId :: ChildId,
    confirmChildLevel :: SchoolLevel
  }
  deriving (Show)

confirmChildForm :: Entity Child -> AForm Handler ConfirmChild
confirmChildForm (Entity cid Child {..}) =
  ConfirmChild cid
    <$> areq
      schoolLevelField
      ( (bfs (childFirstName <> "'s Level and Grade" :: Text))
          { fsName = Just $ T.pack (show (fromSqlKey cid)) <> "school-level"
          }
      )
      (Just childLevel)

confirmChildrenForm ::
  [Entity Child] ->
  AForm Handler [ConfirmChild]
confirmChildrenForm = traverse confirmChildForm

getChildrenConfirmPage :: [Entity Child] -> Maybe (FormResult [ConfirmChild]) -> Handler Html
getChildrenConfirmPage children mfr = do
  (formWidget, formEnctype) <- generateFormPost $ renderCustomForm BootstrapBasicForm $ confirmChildrenForm children
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "children-confirm-level")

getChildrenConfirmLevelR :: Handler Html
getChildrenConfirmLevelR = do
  accid <- requireAuthId
  children <- runDB $ getChildrenOf accid
  getChildrenConfirmPage children Nothing

postChildrenConfirmLevelR :: Handler Html
postChildrenConfirmLevelR = do
  accid <- requireAuthId
  children <- runDB $ getChildrenOf accid
  ((result, _), _) <- runFormPost $ renderCustomForm BootstrapBasicForm $ confirmChildrenForm children
  liftIO $ print result
  case result of
    FormSuccess ccs ->
      withOwnChildren (map confirmChildId ccs) $ do
        now <- liftIO getCurrentTime
        runDB
          $ forM_ ccs
          $ \ConfirmChild {..} ->
            update
              confirmChildId
              [ ChildLevel =. confirmChildLevel,
                ChildLatestConfirmationTime =. Just now
              ]
        redirect ChildrenR
    _ -> getChildrenConfirmPage children $ Just result
