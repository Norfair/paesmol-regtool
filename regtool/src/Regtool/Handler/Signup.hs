{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup
  ( getSignupR,
    module Regtool.Handler.Signup.CustomActivity,
    module Regtool.Handler.Signup.Daycare,
    module Regtool.Handler.Signup.DaycareActivity,
    module Regtool.Handler.Signup.OccasionalDaycare,
    module Regtool.Handler.Signup.OccasionalTransport,
    module Regtool.Handler.Signup.Transport,
  )
where

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Handler.Signup.CustomActivity
import Regtool.Handler.Signup.Daycare
import Regtool.Handler.Signup.DaycareActivity
import Regtool.Handler.Signup.OccasionalDaycare
import Regtool.Handler.Signup.OccasionalTransport
import Regtool.Handler.Signup.Transport

getSignupR :: Handler Html
getSignupR = withNavBar $(widgetFile "signup")
