{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.Transport
  ( SignupChildTransport (..),
    getSignupTransportR,
    postSignupTransportR,
  )
where

import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Handler.Import
import Regtool.Utils.Signup.Transport

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildTransport = SignupChildTransport
  { signupChildTransportChild :: ChildId,
    signupChildTransportBusStop :: BusStopId,
    signupChildTransportSchoolYear :: BusSchoolYearId,
    signupChildTransportInstalments :: Int
  }

signupChildTransportForm ::
  [Entity BusSchoolYear] ->
  [Entity Child] ->
  [(Entity BusLine, [Entity BusStop])] ->
  AForm Handler SignupChildTransport
signupChildTransportForm bsys cs bls =
  SignupChildTransport
    <$> areq
      ( selectFieldList
          $ map (\(Entity cid Child {..}) -> (T.unwords [childFirstName, childLastName], cid)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      ( selectFieldList $ do
          (Entity _ bl, Entity bsid bs) <- unTup bls
          let t =
                T.unwords
                  [ "Line:",
                    busLineName bl,
                    "; Stop:",
                    unTextarea $ busStopCity bs,
                    unTextarea $ busStopLocation bs
                  ]
          pure (t, bsid)
      )
      (bfs ("Bus Stop" :: Text))
      Nothing
    <*> areq
      ( selectFieldList
          $ map (\(Entity bsyid bsy) -> (schoolYearText $ busSchoolYearYear bsy, bsyid)) bsys
      )
      (bfs ("School Year" :: Text))
      Nothing
    <*> pure 3
  where
    unTup :: [(a, [b])] -> [(a, b)]
    unTup = concatMap (\(a, bs) -> (,) a <$> bs)

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  busSchoolYears <- runDB $ selectList [BusSchoolYearOpen ==. True] [Asc BusSchoolYearYear]
  busLines <- runDB $ selectList [] [Asc BusLineId]
  busStops <- runDB $ selectList [BusStopArchived ==. False] [Asc BusStopId]
  busLineTups <- getBusLines
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurTransportSignups $ map entityKey children
  let ets = signupTrips signups children busStops
  (formWidget, formEnctype) <-
    generateFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildTransportForm busSchoolYears children busLineTups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/transport")

getSignupTransportR :: Handler Html
getSignupTransportR = getSignupPage Nothing

getBusLines :: Handler [(Entity BusLine, [Entity BusStop])]
getBusLines = do
  busLines <- runDB $ selectList [] [Asc BusLineId]
  busStops <- runDB $ selectList [BusStopArchived ==. False] [Asc BusStopId]
  attachments <- runDB $ selectList [] [Asc BusLineStopId]
  let go blid (Entity bsid _) =
        any
          (\(Entity _ bls) -> busLineStopLine bls == blid && busLineStopStop bls == bsid)
          attachments
  pure $ flip map busLines $ \bl@(Entity blid _) -> (bl, filter (go blid) busStops)

postSignupTransportR :: Handler Html
postSignupTransportR = do
  aid <- requireAuthId
  busSchoolYears <- runDB $ selectList [BusSchoolYearOpen ==. True] [Asc BusSchoolYearYear]
  busLineTups <- getBusLines
  children <- runDB $ getChildrenOf aid
  ((result, _), _) <-
    runFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildTransportForm busSchoolYears children busLineTups
  case result of
    FormSuccess SignupChildTransport {..} -> do
      now <- liftIO getCurrentTime
      case find (\(Entity bsyid _) -> bsyid == signupChildTransportSchoolYear) busSchoolYears of
        Nothing -> invalidArgs ["This schoolyear is closed"]
        Just (Entity _ bsy) -> do
          let signup =
                TransportSignup
                  { transportSignupChild = signupChildTransportChild,
                    transportSignupBusStop = signupChildTransportBusStop,
                    transportSignupSchoolYear = busSchoolYearYear bsy,
                    transportSignupInstalments = signupChildTransportInstalments,
                    transportSignupTime = now,
                    transportSignupDeletedByAdmin = False
                  }
          void $ runDB $ insertValidUnique signup
          setMessage "Succesfully signed up for transport services. Do not forget to pay for them!"
          redirect SignupTransportR
    fr -> getSignupPage (Just fr)
