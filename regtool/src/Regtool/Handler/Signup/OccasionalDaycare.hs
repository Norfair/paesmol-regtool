{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.OccasionalDaycare
  ( SignupChildOccasionalDaycare (..),
    getSignupOccasionalDaycareR,
    postSignupOccasionalDaycareR,
  )
where

import qualified Data.Set as S
import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Calculation.Time
import Regtool.Handler.Import
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.OccasionalDaycare

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildOccasionalDaycare = SignupChildOccasionalDaycare
  { signupChildOccasionalDaycareChild :: Entity Child,
    signupChildOccasionalDaycareTimeslot :: Entity DaycareTimeslot,
    signupChildOccasionalDaycareDay :: Day
  }

signupChildOccasionalDaycareForm ::
  [Entity Child] ->
  [(Entity DaycareSemestre, Entity DaycareTimeslot)] ->
  AForm Handler SignupChildOccasionalDaycare
signupChildOccasionalDaycareForm cs tups =
  SignupChildOccasionalDaycare
    <$> areq
      ( selectFieldList
          $ map (\ce@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], ce)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      ( selectFieldList
          $ map (\(Entity _ s, e@(Entity _ ts)) -> (occasionalTimeslotDescription s ts, e)) tups
      )
      (bfs ("Timeslot" :: Text))
      Nothing
    <*> areq dayField (bfs ("Day" :: Text)) Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurOccasionalDaycareSignups $ map entityKey children
  payments <- runDB $ getOurOccasionalDaycarePayments $ map entityKey signups
  let tups = timeslotsTups semestres timeslots
  let ets = occasionalSignupTrips signups children semestres timeslots payments
  (formWidget, formEnctype) <-
    generateFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildOccasionalDaycareForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/occasional-daycare")

getSignupOccasionalDaycareR :: Handler Html
getSignupOccasionalDaycareR = getSignupPage Nothing

postSignupOccasionalDaycareR :: Handler Html
postSignupOccasionalDaycareR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  let tups = timeslotsTups semestres timeslots
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildOccasionalDaycareForm children tups
  case result of
    FormSuccess SignupChildOccasionalDaycare {..} -> do
      let Entity cid c = signupChildOccasionalDaycareChild
      let Entity otsid ts = signupChildOccasionalDaycareTimeslot
      when (not (childLevel c `S.member` daycareTimeslotAccessible ts))
        $ invalidArgs ["This time slot is not accessible to a child in that level"]
      when (not (dayMatchesSchoolDay signupChildOccasionalDaycareDay (daycareTimeslotSchoolDay ts)))
        $ invalidArgs ["This timeslot does not occur on the selected day."]
      let signup =
            OccasionalDaycareSignup
              { occasionalDaycareSignupChild = cid,
                occasionalDaycareSignupTimeslot = otsid,
                occasionalDaycareSignupDay = signupChildOccasionalDaycareDay,
                occasionalDaycareSignupCreationTime = now,
                occasionalDaycareSignupDeletedByAdmin = False
              }
      msId <- runDB $ insertValidUnique signup
      signupId <-
        case msId of
          Nothing -> do
            setMessage
              "Your child is already signed up for occasional daycare on the requested date."
            redirect SignupOccasionalDaycareR
          Just signupId -> return signupId
      sendOccasionalDaycareEmail signupId
      setMessage
        "Succesfully signed up for occasional daycare services. Do not forget to pay for them!"
      redirect SignupOccasionalDaycareR
    fr -> getSignupPage (Just fr)

sendOccasionalDaycareEmail :: OccasionalDaycareSignupId -> Handler ()
sendOccasionalDaycareEmail signup = do
  now <- liftIO getCurrentTime
  let email =
        OccasionalDaycareEmail
          { occasionalDaycareEmailSignup = signup,
            occasionalDaycareEmailTimestamp = now,
            occasionalDaycareEmailEmail = Nothing
          }
  runDB $ insertValid_ email
