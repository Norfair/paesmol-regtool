{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.Daycare
  ( SignupChildDaycare (..),
    getSignupDaycareR,
    postSignupDaycareR,
  )
where

import qualified Data.Set as S
import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Calculation.Time
import Regtool.Handler.Import
import Regtool.Utils.Signup.Daycare

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildDaycare = SignupChildDaycare
  { signupChildDaycareChild :: Entity Child,
    signupChildDaycareTimeslot :: Entity DaycareTimeslot
  }

signupChildDaycareForm ::
  [Entity Child] ->
  [(Entity DaycareSemestre, Entity DaycareTimeslot)] ->
  AForm Handler SignupChildDaycare
signupChildDaycareForm cs tups =
  SignupChildDaycare
    <$> areq
      ( selectFieldList
          $ map (\e@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], e)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      (selectFieldList $ map (\(Entity _ s, e@(Entity _ ts)) -> (timeslotDescription s ts, e)) tups)
      (bfs ("Timeslot" :: Text))
      Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurDaycareSignups $ map entityKey children
  payments <- runDB $ getOurDaycarePayments $ map entityKey signups
  let tups = timeslotsTups semestres timeslots
  let ets = signupTrips signups children semestres timeslots payments
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/daycare")

getSignupDaycareR :: Handler Html
getSignupDaycareR = getSignupPage Nothing

postSignupDaycareR :: Handler Html
postSignupDaycareR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  let tups = timeslotsTups semestres timeslots
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareForm children tups
  case result of
    FormSuccess SignupChildDaycare {..} -> do
      let Entity cid c = signupChildDaycareChild
      let Entity tsid ts = signupChildDaycareTimeslot
      when (not (childLevel c `S.member` daycareTimeslotAccessible ts))
        $ invalidArgs ["This time slot is not accessible to a child in that level"]
      let signup =
            DaycareSignup
              { daycareSignupChild = cid,
                daycareSignupTimeslot = tsid,
                daycareSignupCreationTime = now,
                daycareSignupDeletedByAdmin = False
              }
      mr <- runDB $ insertValidUnique signup
      liftIO $ print mr
      case mr of
        Nothing -> invalidArgs ["This child has already been signed up for this timeslot"]
        Just _ -> do
          setMessage "Succesfully signed up for daycare services. Do not forget to pay for them!"
          redirect SignupDaycareR
    fr -> getSignupPage (Just fr)
