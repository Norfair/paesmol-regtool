{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.DaycareActivity
  ( SignupChildDaycareActivity (..),
    getSignupDaycareActivityR,
    postSignupDaycareActivityR,
  )
where

import qualified Data.Set as S
import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Calculation.Time
import Regtool.Handler.Import
import Regtool.Utils.Signup.DaycareActivity
import Text.Printf

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildDaycareActivity = SignupChildDaycareActivity
  { signupChildDaycareActivityChild :: Entity Child,
    signupChildDaycareActivity :: Entity DaycareActivity
  }

signupChildDaycareActivityForm ::
  [Entity Child] ->
  [(Entity DaycareSemestre, Entity DaycareActivity, String)] ->
  AForm Handler SignupChildDaycareActivity
signupChildDaycareActivityForm cs tups =
  SignupChildDaycareActivity
    <$> areq
      ( selectFieldList
          $ map (\e@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], e)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      (selectFieldList $ map (\(Entity _ s, e@(Entity _ ts), _) -> (activityDescription s ts, e)) tups)
      (bfs ("Activity" :: Text))
      Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]

  activityTups <- getActivityTups
  let activities = map fst activityTups

  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurDaycareActivitySignups $ map entityKey children
  payments <- runDB $ getOurDaycareActivityPayments $ map entityKey signups
  let tups = activitiesTups semestres activityTups
  let ets = signupTrips signups children semestres activities payments
  (formWidget, formEnctype) <-
    generateFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildDaycareActivityForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/daycare-activity")

getActivityTups :: Handler [(Entity DaycareActivity, String)]
getActivityTups = runDB $ do
  das <- selectList [] [Asc DaycareActivityId]
  fmap (map snd . filter fst) . forM das $ \dae@(Entity daid da) -> do
    spacesTaken <- count [DaycareActivitySignupActivity ==. daid]
    let (available, spaces) = case daycareActivitySpaces da of
          Nothing -> (True, "yes" :: String)
          Just totalSpaces ->
            let spacesLeft = fromIntegral totalSpaces - spacesTaken :: Int
             in (spacesLeft > 0, printf "%d / %d" spacesLeft totalSpaces)

    pure (available, (dae, spaces))

getSignupDaycareActivityR :: Handler Html
getSignupDaycareActivityR = getSignupPage Nothing

postSignupDaycareActivityR :: Handler Html
postSignupDaycareActivityR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [DaycareSemestreOpen ==. True] [Asc DaycareSemestreId]

  activityTups <- getActivityTups

  children <- runDB $ getChildrenOf aid
  let tups = activitiesTups semestres activityTups
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareActivityForm children tups
  case result of
    FormSuccess SignupChildDaycareActivity {..} -> do
      let Entity cid c = signupChildDaycareActivityChild
      let Entity dcaid da = signupChildDaycareActivity
      when (not (childLevel c `S.member` daycareActivityAccessible da))
        $ invalidArgs
          [ "This time slot is not accessible to a child in that level, this activity is only available to the following school levels: "
              <> T.unwords (map schoolLevelText (S.toList (daycareActivityAccessible da)))
          ]
      mdse <- runDB $ getBy (UniqueDaycareSignup cid (daycareActivityTimeslot da) False)
      case mdse of
        Nothing -> invalidArgs ["Please sign up for the corresponding daycare timeslot first"]
        Just _ -> pure ()
      let signup =
            DaycareActivitySignup
              { daycareActivitySignupChild = cid,
                daycareActivitySignupActivity = dcaid,
                daycareActivitySignupCreationTime = now,
                daycareActivitySignupDeletedByAdmin = False
              }
      void $ runDB $ insertValidUnique signup
      setMessage
        "Succesfully signed up for daycareActivity services. Do not forget to pay for them!"
      redirect SignupDaycareActivityR
    fr -> getSignupPage (Just fr)
