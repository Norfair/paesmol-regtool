{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.CustomActivity where

import Data.Text (unwords)
import Regtool.Calculation.Children (childName, getChildrenOf)
import Regtool.Calculation.Time (formatDay, formatTimestamp)
import Regtool.Handler.Import hiding (unwords)
import Regtool.Utils.Signup.CustomActivity
  ( customActivityText,
    getOurCustomActivityPayments,
    getOurCustomActivitySignups,
    signupTrips,
  )
import Text.Printf (printf)

data SignupChildCustomActivity = SignupChildCustomActivity
  { signupChildCustomActivityChild :: Entity Child,
    signupChildCustomActivity :: Entity CustomActivity
  }

signupChildCustomActivityForm ::
  [Entity Child] -> [Entity CustomActivity] -> AForm Handler SignupChildCustomActivity
signupChildCustomActivityForm cs activities =
  SignupChildCustomActivity
    <$> areq
      ( selectFieldList
          $ map (\e@(Entity _ Child {..}) -> (unwords [childFirstName, childLastName], e)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      (selectFieldList $ map (\e@(Entity _ activity) -> (customActivityText activity, e)) activities)
      (bfs ("Activity" :: Text))
      Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  activityTups <- runDB getActivityTups
  let activities = map fst activityTups

  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurCustomActivitySignups $ map entityKey children
  payments <- runDB $ getOurCustomActivityPayments $ map entityKey signups
  let ets = signupTrips signups children activities payments
  (formWidget, formEnctype) <-
    generateFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildCustomActivityForm children activities
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/custom-activity")

getSignupCustomActivityR :: Handler Html
getSignupCustomActivityR = getSignupPage Nothing

postSignupCustomActivityR :: Handler Html
postSignupCustomActivityR = do
  aid <- requireAuthId
  activityTups <- runDB getActivityTups
  let activities = map fst activityTups
  children <- runDB $ getChildrenOf aid
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildCustomActivityForm children activities
  case result of
    FormSuccess SignupChildCustomActivity {..} -> do
      let Entity caid ca = signupChildCustomActivity
      case find (\(Entity i _) -> i == caid) activities of
        Nothing -> invalidArgs ["This activity is no longer open."]
        Just _ -> do
          let Entity cid _ = signupChildCustomActivityChild
          let signup =
                CustomActivitySignup
                  { customActivitySignupChild = cid,
                    customActivitySignupActivity = caid,
                    customActivitySignupCreationTime = now,
                    customActivitySignupDeletedByAdmin = False
                  }
          void $ runDB $ insertValidUnique signup
          setMessage
            $ "Succesfully signed up for the custom activity on "
            <> toHtml (formatDay (customActivityDate ca))
            <> ". Do not forget to pay for it!"
          redirect SignupCustomActivityR
    fr -> getSignupPage (Just fr)

getActivityTups :: (MonadIO m) => SqlPersistT m [(Entity CustomActivity, String)]
getActivityTups = do
  cas <- selectList [CustomActivityOpen ==. True] [Asc CustomActivityId]
  fmap (map snd . filter fst) . forM cas $ \cae@(Entity caid ca) -> do
    spacesTaken <- count [CustomActivitySignupActivity ==. caid]
    let (available, spaces) = case customActivitySpaces ca of
          Nothing -> (True, "yes" :: String)
          Just totalSpaces ->
            let spacesLeft = fromIntegral totalSpaces - spacesTaken :: Int
             in (spacesLeft > 0, printf "%d / %d" spacesLeft totalSpaces)

    pure (available, (cae, spaces))
