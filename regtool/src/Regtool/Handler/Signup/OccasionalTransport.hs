{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Signup.OccasionalTransport
  ( SignupChildOccasionalTransport (..),
    getSignupOccasionalTransportR,
    postSignupOccasionalTransportR,
  )
where

import qualified Data.Text as T
import Regtool.Calculation.Children
import Regtool.Handler.Import
import Regtool.Utils.Signup.OccasionalTransport

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildOccasionalTransport = SignupChildOccasionalTransport
  { signupChildOccasionalTransportChild :: ChildId,
    signupChildOccasionalTransportBusStop :: BusStopId,
    signupChildOccasionalTransportDay :: Day,
    signupChildOccasionalTransportDirection :: BusDirection
  }

signupChildOccasionalTransportForm ::
  [Entity Child] -> [Entity BusStop] -> AForm Handler SignupChildOccasionalTransport
signupChildOccasionalTransportForm cs bss =
  SignupChildOccasionalTransport
    <$> areq
      ( selectFieldList
          $ map (\(Entity cid Child {..}) -> (T.unwords [childFirstName, childLastName], cid)) cs
      )
      (bfs ("Child" :: Text))
      Nothing
    <*> areq
      ( selectFieldList $ do
          Entity bsid bs <- bss
          let t = T.unlines [unTextarea $ busStopCity bs, unTextarea $ busStopLocation bs]
          pure (t, bsid)
      )
      (bfs ("Bus Stop" :: Text))
      Nothing
    <*> areq dayField (bfs ("Day" :: Text)) Nothing
    <*> areq (selectField optionsEnum) (bfs ("Direction" :: Text)) Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  busStops <- runDB $ selectList [] [Asc BusStopId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurOccasionalTransportSignups $ map entityKey children
  let ets = signupTrips signups children busStops
  (formWidget, formEnctype) <-
    generateFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildOccasionalTransportForm children busStops
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/occasional-transport")

getSignupOccasionalTransportR :: Handler Html
getSignupOccasionalTransportR = getSignupPage Nothing

postSignupOccasionalTransportR :: Handler Html
postSignupOccasionalTransportR = do
  aid <- requireAuthId
  busStops <- runDB $ selectList [] [Asc BusStopId]
  children <- runDB $ getChildrenOf aid
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost
      $ renderCustomForm BootstrapBasicForm
      $ signupChildOccasionalTransportForm children busStops
  case result of
    FormSuccess SignupChildOccasionalTransport {..} -> do
      let mBusStop = find ((== signupChildOccasionalTransportBusStop) . entityKey) busStops
          mChild = find ((== signupChildOccasionalTransportChild) . entityKey) children
          signup =
            OccasionalTransportSignup
              { occasionalTransportSignupChild = signupChildOccasionalTransportChild,
                occasionalTransportSignupBusStop = signupChildOccasionalTransportBusStop,
                occasionalTransportSignupDay = signupChildOccasionalTransportDay,
                occasionalTransportSignupDirection = signupChildOccasionalTransportDirection,
                occasionalTransportSignupDeletedByAdmin = False,
                occasionalTransportSignupTime = now
              }
      case (mBusStop, mChild) of
        (Just (Entity _ _busStop), Just (Entity _ _child)) -> do
          mId <- runDB $ insertValidUnique signup
          signupId <-
            case mId of
              Nothing -> do
                setMessage
                  "Your child is already signed up for occasional tranport on the requested date."
                redirect SignupOccasionalTransportR
              Just sId -> return sId
          sendOccasionalTransportEmail signupId
          setMessage
            "Succesfully signed up for occasional transport services. Do not forget to pay for them!"
          redirect SignupOccasionalTransportR
        _ -> invalidArgs ["Invalid entity keys"]
    fr -> getSignupPage (Just fr)

sendOccasionalTransportEmail :: OccasionalTransportSignupId -> Handler ()
sendOccasionalTransportEmail signup = do
  now <- liftIO getCurrentTime
  let email =
        OccasionalTransportEmail
          { occasionalTransportEmailSignup = signup,
            occasionalTransportEmailTimestamp = now,
            occasionalTransportEmailEmail = Nothing
          }
  runDB $ insertValid_ email
