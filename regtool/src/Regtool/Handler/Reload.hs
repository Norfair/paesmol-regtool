module Regtool.Handler.Reload where

import Regtool.Handler.Import
import Yesod.AutoReload

getReloadR :: Handler ()
getReloadR = getAutoReloadR
