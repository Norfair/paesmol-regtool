{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Home where

import Regtool.Handler.Import

getHomeR :: Handler Html
getHomeR = do
  aid <- maybeAuthId
  case aid of
    Nothing ->
      withNavBar $ do
        setTitle "PAESMOL Registration"
        $(widgetFile "home")
    Just id_ -> do
      b <- accIsAdmin id_
      if b
        then redirect $ AdminR PanelR
        else redirect OverviewR
