{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.OptParse
  ( getSettings,
    Settings (..),
    LoopersSettings (..),
    RunVerificationSettings (..),
  )
where

import Control.Monad.Logger
import Import hiding (reader)
import Looper
import Necrork
import OptEnvConf
import Paths_regtool
import Regtool.Core.Foundation.Regtool
import Regtool.Data
import Regtool.Logging ()

getSettings :: IO Settings
getSettings = runSettingsParser version "PAESMOL registration tool"

data Settings = Settings
  { settingEnv :: !RegtoolEnvironment,
    settingDbFile :: !(Path Abs File),
    settingHost :: !Text,
    settingPort :: !Int,
    settingLogLevel :: !LogLevel,
    settingEmailAddress :: !EmailAddress,
    settingLoopers :: !LoopersSettings,
    settingStripeSettings :: !(Maybe StripeSettings),
    settingVerification :: !RunVerificationSettings,
    settingNecrorkNotifierSettings :: !(Maybe Necrork.NotifierSettings),
    settingNotPaid :: !(Maybe Day),
    settingMaintenance :: Bool
  }
  deriving (Show)

instance HasParser Settings where
  settingsParser = parseSettings

{-# ANN parseSettings ("NOCOVER" :: String) #-}
parseSettings :: Parser Settings
parseSettings = subEnv_ "regtool"
  $ withLocalYamlConfig
  $ checkMapEither
    ( \s ->
        if isJust (settingStripeSettings s) && settingEnv s == EnvAutomatedTesting
          then Left "Must not configure stripe settings during automated testing."
          else Right s
    )
  $ do
    settingEnv <- settingsParser
    settingDbFile <-
      mapIO resolveFile'
        $ setting
          [ help "database-file",
            reader str,
            name "db-file",
            name "database-file",
            value "regtool.db",
            metavar "FILE"
          ]
    settingHost <-
      setting
        [ help "Host",
          reader str,
          name "host",
          value "localhost",
          metavar "HOST"
        ]
    settingPort <-
      setting
        [ help "Port",
          reader auto,
          name "port",
          value 8080,
          metavar "PORT"
        ]
    settingLogLevel <- settingsParser
    --   | otherwise = fromMaybe RunVerificationAndFail envSets
    settingEmailAddress <-
      setting
        [ help "The email address to send emails from",
          reader $ eitherReader emailValidateFromString,
          name "email-address",
          value $ unsafeEmailAddress "registration" "paesmol.eu",
          metavar "EMAIL_ADDRESS"
        ]
    settingLoopers <- subSettings "looper"
    settingStripeSettings <- optional $ subSettings "stripe"
    settingVerification <- settingsParser
    settingNecrorkNotifierSettings <- optional $ subSettings "necrork"
    settingNotPaid <-
      optional
        $ setting
          [ help "The due date for an unpaid payment",
            reader auto,
            name "not-paid",
            metavar "DATE"
          ]
    settingMaintenance <-
      setting
        [ help "maintenance mode",
          switch True,
          reader exists,
          long "maintenance",
          env "MAINTENANCE",
          metavar "ANY",
          conf "maintenance",
          value False
        ]
    pure Settings {..}

data LoopersSettings = LoopersSettings
  { loopersSetsEmailer :: LooperSettings,
    loopersSetsVerifier :: LooperSettings,
    loopersSetsGarbageCollector :: LooperSettings
  }
  deriving (Show)

instance HasParser LoopersSettings where
  settingsParser = parseLoopersSettings

{-# ANN parseLoopersSettings ("NOCOVER" :: String) #-}
parseLoopersSettings :: Parser LoopersSettings
parseLoopersSettings = do
  loopersSetsEmailer <-
    parseLooperSettings
      "emailer"
      (seconds 5)
      (seconds 5)
  loopersSetsVerifier <-
    parseLooperSettings
      "verifier"
      (seconds 10)
      (hours 12)
  loopersSetsGarbageCollector <-
    parseLooperSettings
      "garbage-collector"
      (seconds 0)
      (hours 24)
  pure LoopersSettings {..}

data RunVerificationSettings
  = RunVerificationAndFail
  | RunVerificationButDoNotFail
  | DoNotRunVerification
  deriving (Show, Read)

instance HasParser RunVerificationSettings where
  settingsParser = parseRunVerificationSettings

{-# ANN parseRunVerificationSettings ("NOCOVER" :: String) #-}
parseRunVerificationSettings :: Parser RunVerificationSettings
parseRunVerificationSettings =
  choice
    [ setting
        [ switch RunVerificationAndFail,
          long "verify-and-fail",
          help "Verify and fail if necessary"
        ],
      setting
        [ switch RunVerificationButDoNotFail,
          long "verify-but-do-not-fail",
          help "Verifiy but do not fail if verification fails"
        ],
      setting
        [ switch DoNotRunVerification,
          long "no-verification",
          help "Do not run verification"
        ],
      setting
        [ help "whether and how to run verification",
          reader auto,
          env "VERIFICATION",
          metavar "VERIFICATION",
          value RunVerificationAndFail,
          shownExample RunVerificationAndFail,
          shownExample RunVerificationButDoNotFail,
          shownExample DoNotRunVerification,
          shownExample RunVerificationAndFail
        ]
    ]
