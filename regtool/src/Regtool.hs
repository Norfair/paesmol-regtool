{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool where

import Control.Lens
import Control.Monad.Logger
import Control.Monad.Trans.Resource (runResourceT)
import Data.Pool
import qualified Data.Text as T
import qualified Database.Persist.Sqlite as DB
import Import
import Necrork
import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Client.TLS as Http
import qualified Network.HTTP.Types as Http
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as Wai
import Regtool.Application ()
import Regtool.Core.Constants
import Regtool.Core.Foundation
import Regtool.Core.Migrations
import Regtool.Logging
import Regtool.Looper
import Regtool.Looper.Types
import Regtool.Looper.Verifier
import Regtool.OptParse
import UnliftIO

regtool :: IO ()
regtool = getSettings >>= run

run :: Settings -> IO ()
run ss@Settings {..} = do
  httpManager <- Http.newManager Http.tlsManagerSettings
  runMyLoggingT $ filterLogger (\_ ll -> ll >= settingLogLevel) $ do
    logInfoN $ T.unlines ["Running Regtool with these settings:", T.pack (ppShow ss)]
    DB.withSqlitePoolInfo
      ( DB.mkSqliteConnectionInfo (T.pack $ toFilePath settingDbFile)
          & DB.fkEnabled .~ False
      )
      1
      $ \pool -> do
        setupDB pool ss
        regtoolClientSessionKeyFile <- resolveFile' "client_session_key.aes"
        regtoolAdminConfigFile <- resolveFile' "config.yaml"
        let ar =
              T.concat
                [ case settingEnv of
                    EnvManualTesting -> "http://"
                    _ -> "https://",
                  settingHost,
                  case settingEnv of
                    EnvManualTesting -> ":" <> T.pack (show settingPort)
                    _ -> ""
                ]
            rt =
              Regtool
                { regtoolEnvironment = settingEnv,
                  regtoolApproot = Just ar,
                  regtoolConnectionPool = pool,
                  regtoolStatic = regtoolEmbeddedStatic,
                  regtoolHttpManager = httpManager,
                  regtoolStripeSettings = settingStripeSettings,
                  regtoolClientSessionKeyFile = regtoolClientSessionKeyFile,
                  regtoolAdminConfigFile = regtoolAdminConfigFile,
                  regtoolNotPaid = settingNotPaid
                }

        let looperEnv =
              LooperEnv
                { looperEnvironment = settingEnv,
                  looperDatabaseFilename = settingDbFile,
                  looperConnectionPool = pool,
                  looperRegtool = rt,
                  looperApproot = ar,
                  looperEmailAddress = settingEmailAddress
                }
        let defMiddles = defaultMiddlewaresNoLogging
        let extraMiddles =
              if development
                then Wai.logStdoutDev
                else Wai.logStdout
        let middle = extraMiddles . defMiddles
        plainApp <- liftIO $ toWaiAppPlain rt
        let app_ =
              if settingMaintenance
                then maintenanceApp
                else middle plainApp
        logInfoN $ "Serving at " <> ar
        Necrork.withMNotifier settingNecrorkNotifierSettings $ do
          concurrently_ (liftIO (Warp.run settingPort app_)) (runAllLoopers ss looperEnv)

maintenanceApp :: Wai.Application
maintenanceApp _ resp =
  resp
    $ Wai.responseLBS
      Http.ok200
      []
      "<html><body><p>The registration tool is in maintenance mode at the moment.</p><p>Please come back later.</p></body></html>"

setupDB :: (MonadLoggerIO m, MonadUnliftIO m) => Pool SqlBackend -> Settings -> m ()
setupDB pool Settings {..} =
  runResourceT
    $ flip DB.runSqlPool pool
    $ do
      logInfoN "Running persistent migrations"
      DB.runMigration migrateAll
      logInfoN "Persistent migrations done"
      logInfoN "Running Regtool migrations"
      regtoolMigrations
      logInfoN "Regtool migrations done"
      case settingVerification of
        RunVerificationAndFail -> do
          logInfoN "Verifying database. Will fail if verification fails."
          res <- verifyDatabaseWithResult
          failIfVerificationFailed res
          logInfoN "Database verification done."
        RunVerificationButDoNotFail -> do
          logInfoN "Verifying database. Will NOT fail if verification fails."
          res <- verifyDatabaseWithResult
          case res of
            VerifiedFine -> logInfoN "Verification succeeded."
            VerificationErrors es -> do
              mapM_ logVerificationError es
              logWarnN $ T.unwords ["Verification failed with", T.pack (show (length es)), "errors"]
        DoNotRunVerification -> logWarnN "Not running verification at all."
