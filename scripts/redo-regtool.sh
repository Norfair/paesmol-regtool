#!/usr/bin/env bash

set -e
set -x

cd regtool

killall regtool || true

# regtool verify-db --db-file regtool.db
export REGTOOL_DATABASE_FILE=../${1:-regtool/regtool.db}
export REGTOOL_ENVIRONMENT=EnvManualTesting
export REGTOOL_LOG_LEVEL=Debug
# export REGTOOL_VERIFICATION=RunVerificationAndFail
# export REGTOOL_VERIFICATION=RunVerificationButDoNotFail
export REGTOOL_VERIFICATION=DoNotRunVerification
export REGTOOL_STRIPE_SECRET_KEY="sk_test_tWfqF4zGROtKISni9BvrB5bV00h0vzX0YS"
export REGTOOL_STRIPE_PUBLISHABLE_KEY="pk_test_zV5qVP1IQTjE9QYulRZpfD8C00cqGOnQ91"
export REGTOOL_LOOPER_EMAILER_ENABLE=False
export REGTOOL_LOOPER_STRIPE_EVENTS_FETCHER_ENABLE=False
export REGTOOL_LOOPER_VERIFIER_ENABLE=False
export REGTOOL_LOOPER_GARBAGE_COLLECTOR_ENABLE=False
export REGTOOL_LOOPER_GARBAGE_COLLECTOR_PERIOD=10
regtool &
