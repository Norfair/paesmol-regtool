#!/usr/bin/env bash

echo ".backup backup.db" | ssh paesmol 'cd /www/regtool/production/data && sqlite3 regtool.db'

scp -O paesmol:/www/regtool/production/data/backup.db ~/regtool-prod.db
cp ~/regtool-prod.db regtool/regtool.db
