#!/usr/bin/env bash

set -e
set -x

nice -n 19 stack install :regtool \
  --file-watch-poll \
  --watch-all \
  --exec="./scripts/redo-regtool.sh" \
  --test --no-run-tests \
  $@
