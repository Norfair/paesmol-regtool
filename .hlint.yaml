- ignore: { name: "Use void" }
- ignore: { name: "Use bimap" }
- ignore: { name: "Use ++" }
- ignore: { name: "Use uncurry" }
- ignore: { name: "Use Just" }
- ignore: { name: "Use newtype instead of data" }
- ignore: { name: "Use ?~" }
- ignore: { name: "Use const" }
- ignore: { name: "Use unless" }
- ignore: { name: "Use record patterns" }
- ignore: { name: "Use <$>" }
- ignore: { name: "Replace case with maybe" }
- ignore: { name: "Redundant if" }
- ignore: { name: "Reduce duplication" }
- functions:
  - {name: unsafeDupablePerformIO, within: []} # Unsafe
  - {name: unsafeInterleaveIO, within: []} # Unsafe
  - {name: unsafeFixIO, within: []} # Unsafe
  - {name: unsafePerformIO, within: []} # Unsafe

  # _VERY_ hard to get right, use the async library instead.
  # See also https://github.com/informatikr/hedis/issues/165
  - {name: forkIO, within: []}
  # Mostly impossible to get right, rethink what you're doing entirely.
  # See also https://www.reddit.com/r/haskell/comments/jsap9r/how_dangerous_is_forkprocess/
  - {name: forkProcess, within: []}

  # Purposely fails. Deal with errors appropriately instead.
  - name: undefined
    # Ignores for the undefined trick
    within:
      - Regtool.Handler.Admin.Accounts.getAdminAccountR 

  - {name: throw, within: []} # Don't throw from pure code, use throwIO instead.
  - name: Prelude.error
    within:
      - Regtool.TestUtils.Admin.Semestre.registerDaycareSemestre
  

  - name: Data.List.head # Partial, use `listToMaybe` instead.
    within:
      - Regtool.Calculation.Costs.Calculate.calculateTransportPayments
  - name: Data.List.tail
    within:
      - Regtool.Calculation.Costs.Calculate.calculateTransportPayments
  - {name: Data.List.init, within: []} # Partial
  - {name: Data.List.last, within: []} # Partial
  - {name: 'Data.List.!!', within: []} # Partial
  - {name: Data.List.genericIndex, within: []} # Partial

  - {name: minimum, within: []} # Partial
  - {name: minimumBy, within: []} # Partial
  - {name: maximum, within: []} # Partial
  - {name: maximumBy, within: []} # Partial

  - {name: GHC.Enum.pred, within: []} # Partial
  - {name: GHC.Enum.suc, within: []} # Partial
  - {name: GHC.Enum.toEnum, within: []} # Partial
  - {name: GHC.Enum.fromEnum, within: []} # Does not do what you think it does.
  - {name: GHC.Enum.enumFrom, within: []} # Does not do what you think it does, depending on the type.
  - {name: GHC.Enum.enumFromThen, within: []} # Does not do what you think it does, depending on the type.
  - {name: GHC.Enum.enumFromTo, within: []} # Does not do what you think it does, depending on the type.
  - {name: GHC.Enum.enumFromThenTo, within: []} # Does not do what you think it does, depending on the type.

  - {name: unless, within: []} # Really confusing, use 'when' instead.
  - {name: either, within: []} # Really confusing, just use a case-match.

  - {name: foldl, within: []} # Lazy. Use foldl' instead.

  # Does unexpected things, see
  # https://github.com/NorfairKing/haskell-WAT#real-double
  - {name: realToFrac, within: []}

  # Don't use string for command-line output.
  - {name: System.IO.putChar, within: []}
  - {name: System.IO.putStr, within: []}
  - {name: System.IO.putStrLn, within: []}
  - {name: System.IO.print, within: []}

  # Don't use string for command-line input either.
  - {name: System.IO.getChar, within: []}
  - {name: System.IO.getLine, within: []}
  - {name: System.IO.getContents, within: []} # Does lazy IO.
  - {name: System.IO.interact, within: []}
  - {name: System.IO.readIO, within: []}
  - {name: System.IO.readLn, within: []}

  # Don't use strings to interact with files
  - name: System.IO.readFile
    within:
      - Regtool.Data.TH # Executed at compile-time
  - {name: System.IO.writeFile, within: []}
  - {name: System.IO.appendFile, within: []}

  # Can succeed in dev, but fail in prod, because of encoding guessing
  # It's also Lazy IO.
  # See https://www.snoyman.com/blog/2016/12/beware-of-readfile/ for more info.
  - {name: Data.Text.IO.readFile, within: []}
  - {name: Data.Text.IO.Lazy.readFile, within: []}

  - name: fromJust
    within:
      - Regtool.Calculation.Gen.Costs.Input.shuffleNE
      - Regtool.TestUtils.Parents.registerParent

  - {name: 'read', within: []} # Partial, use `Text.Read.readMaybe` instead.
