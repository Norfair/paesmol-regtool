{ pkgs
, regtool-nixos-module-factory
}:
let
  regtool-production = regtool-nixos-module-factory {
    envname = "production";
  };

  web-port = 8080;
in
pkgs.nixosTest (
  { lib, pkgs, ... }: {
    name = "regtool-module-test";
    nodes = {
      server = {
        imports = [
          regtool-production
        ];
        services.regtool.production = {
          enable = true;
          envname = "EnvAutomatedTesting";
          config = {
            port = web-port;
            log-level = "Debug";
          };
        };
      };
    };
    testScript = ''
      server.start()

      server.wait_for_unit("multi-user.target")
      server.wait_for_unit("regtool-production.service")

      server.wait_for_open_port(${builtins.toString web-port})
    '';
  }
)
