final: prev:
with final.lib;
with final.haskell.lib;
let
  stripe-spec = builtins.fetchGit {
    url = "https://github.com/stripe/openapi";
    rev = "88c60c84c2a27840508261e7b06ac4b7a43e2c99";
  };
  generatedRegtoolStripe = final.generateOpenAPIClient {
    name = "regtool-stripe-client";
    configFile = ../stripe-client-gen.yaml;
    src = stripe-spec + "/openapi/spec3.yaml";
  };

in
{
  regtoolReleasePackages = mapAttrs
    (_: pkg: justStaticExecutables pkg)
    final.haskellPackages.regtoolPackages;

  regtoolRelease =
    final.symlinkJoin {
      name = "regtool-release";
      paths = attrValues final.regtoolReleasePackages;
    };

  generatedRegtoolStripeCode = generatedRegtoolStripe;

  haskellPackages =
    prev.haskellPackages.override (old: {
      overrides = final.lib.composeExtensions (old.overrides or (_: _: { }))
        (
          self: super:
            let
              regtoolPackages =
                let
                  pathFor = name: ../${name};
                  regtoolPkg =
                    name:
                    overrideCabal
                      (failOnAllWarnings (self.callPackage (../${name}) { }))
                      (old: {
                        doBenchmark = true;
                        doHaddock = false;
                        doCoverage = false;
                        doHoogle = false;
                        doCheck = false; # Only for coverage
                        hyperlinkSource = false;
                        enableLibraryProfiling = false;
                        enableExecutableProfiling = false;
                        buildDepends = (old.buildDepends or [ ]) ++ [ final.haskellPackages.autoexporter ];
                        configureFlags = (old.configureFlags or [ ]) ++ [
                          # Extra warnings
                          "--ghc-option=-Wall"
                          "--ghc-option=-Wincomplete-uni-patterns"
                          "--ghc-option=-Wincomplete-record-updates"
                          "--ghc-option=-Wpartial-fields"
                          "--ghc-option=-Widentities"
                          "--ghc-option=-Wredundant-constraints"
                          "--ghc-option=-Wcpp-undef"
                          "--ghc-option=-Werror"
                          "--ghc-option=-Wunused-packages"
                        ];
                        # Ugly hack because we can't just add flags to the 'test' invocation.
                        # Show test output as we go, instead of all at once afterwards.
                        testTarget = (old.testTarget or "") + " --show-details=direct";
                      });
                  bootstrap-css = builtins.fetchurl {
                    url = https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css;
                    sha256 = "sha256:0p33lbv1pkdc35xjydqxs61m5j0vyd2imqmiyhr19l9vr1n88ppp";
                  };
                  jquery-js = builtins.fetchurl {
                    url = https://code.jquery.com/jquery-3.2.1.min.js;
                    sha256 = "sha256:1pl2imaca804jkq29flhbkfga5qqk39rj6j1n179h5b0rj13h247";
                  };
                  bootstrap-js = builtins.fetchurl {
                    url = https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js;
                    sha256 = "sha256:1vw66ik7zxmc3gg0cfcrwkf01384v0yk1k2fsgdfhd66lxw495jk";
                  };
                  font-awesome-css = builtins.fetchurl {
                    url = https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css;
                    sha256 = "sha256:1gch64hq7xc9jqvs7npsil2hwsigdjnvf78v1vpgswq3rhjyp6kr";
                  };
                  font-awesome-webfont-ttf = builtins.fetchurl {
                    url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf;
                    sha256 = "sha256:1a6nq3nx9mpcjik80d9k16darafp8g04av3sbhpv03ws4czz6n5a";
                  };
                  font-awesome-webfont-woff = builtins.fetchurl {
                    url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff;
                    sha256 = "sha256:01sg8wrpb2la7qigmpbpb10rbnfhwag614rz3fs5q3s5npg5j35s";
                  };
                  font-awesome-webfont-woff2 = builtins.fetchurl {
                    url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2;
                    sha256 = "sha256:1zpk2gd37kmnqjivfxfncjm9g6d0qnfqf5ylyby1hz8y0jygrpia";
                  };
                  regtool =
                    overrideCabal (regtoolPkg "regtool") (old: {
                      preConfigure = ''
                        ${old.preConfigure or ""}

                        mkdir -p static/
                        ln -s ${bootstrap-css} static/bootstrap.css
                        ln -s ${jquery-js} static/jquery.js
                        ln -s ${bootstrap-js} static/bootstrap.js
                        ln -s ${font-awesome-css} static/font-awesome.css

                        mkdir -p fonts/
                        ln -s ${font-awesome-webfont-ttf} fonts/fontawesome-webfont.ttf
                        ln -s ${font-awesome-webfont-woff} fonts/fontawesome-webfont.woff
                        ln -s ${font-awesome-webfont-woff2} fonts/fontawesome-webfont.woff2
                      '';
                    });
                  generatedRegtoolStripePackage = self.callPackage (generatedRegtoolStripe + "/default.nix") { };
                in
                genAttrs [
                  "regtool-data"
                  "regtool-data-gen"
                  "regtool-calculate"
                  "regtool-calculate-gen"
                  "regtool-gen"
                ]
                  regtoolPkg // {
                  inherit regtool;
                  regtool-stripe-client = generatedRegtoolStripePackage;
                };

            in
            regtoolPackages // { inherit regtoolPackages; }
        );
    });
}
