{ regtool
, opt-env-conf
}:
{ envname }:
{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.regtool."${envname}";
  concatAttrs =
    attrList:
    fold (x: y: x // y) { } attrList;
in
{
  options.services.regtool."${envname}" = {
    enable = mkEnableOption "Regtool Services";
    config = mkOption {
      default = { };
      type = types.submodule {
        options = import ../regtool-gen/options.nix { inherit lib; };
      };
    };
    extraConfig = mkOption {
      description = "The contents of the config file, as an attribute set. This will be translated to Yaml and put in the right place along with the rest of the options defined in this submodule.";
      default = { };
    };
    maintenance = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "whether the app is in maintenance mode";
    };
    envname = mkOption {
      type = types.nullOr types.str;
      example = "production";
      default = envname;
      description = "The name of the environment";
    };
    localBackup = mkOption {
      type = types.submodule {
        options = {
          enable = mkEnableOption "Regtool Local Backup Service";
        };
      };
      default = { };
    };
    s3Backup = mkOption {
      default = null;
      type = types.nullOr (types.submodule {
        options = {
          enable = mkEnableOption "Regtool S3 Backup Service";
          bucket = mkOption {
            type = types.str;
            example = "arn:aws:s3:::paesmol-backup";
            description = "The s3 bucket ARN";
          };
        };
      }
      );
    };
  };
  config =
    let
      workingDir = "/www/regtool/${envname}/data/";
      databaseFile = workingDir + "regtool.db";
      backupDir = workingDir + "backups/";
      web-server-config = concatAttrs [
        cfg.config
        cfg.extraConfig
      ];
      web-server-config-file = (pkgs.formats.yaml { }).generate "regtool-config.yaml" web-server-config;
      web-server-service = opt-env-conf.addSettingsCheckToService {
        description = "Regtool ${envname} Service";
        wantedBy = [
          "multi-user.target"
        ];
        environment = {
          "REGTOOL_CONFIG_FILE" = web-server-config-file;
        };
        script = ''
          mkdir -p "${workingDir}"
          cd "${workingDir}"

          mkdir -p ${backupDir}
          file="${backupDir}deploy-''$(date +%F_%T).db"
          ${pkgs.sqlite}/bin/sqlite3 ${databaseFile} ".backup ''${file}"

          ${regtool}/bin/regtool
        '';
        serviceConfig = {
          Restart = "always";
          RestartSec = 1;
          Nice = 15;
        };
        unitConfig = {
          StartLimitIntervalSec = 0;
          # ensure Restart=always is always honoured
        };
      };
      local-backup-service = {
        description = "Backup regtool database locally for ${envname}";
        wantedBy = [ ];
        script = ''
          mkdir -p ${backupDir}
          file="${backupDir}''$(date +%F_%T).db"
          ${pkgs.sqlite}/bin/sqlite3 ${databaseFile} ".backup ''${file}"
        '';
        serviceConfig = {
          Type = "oneshot";
        };
      };
      local-backup-timer = {
        description = "Backup regtool database locally for ${envname} every twelve hours.";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "00/12:00";
          Persistent = true;
        };
      };
      s3-backup-service = {
        description = "Backup regtool database to s3 for ${envname}";
        wantedBy = [ ];
        script = ''
          mkdir -p ${backupDir}
          file="${backupDir}''$(date +%F_%T).db"
          ${pkgs.sqlite}/bin/sqlite3 ${databaseFile} ".backup ''${file}"
          ${pkgs.awscli}/bin/aws s3 cp ''${file} s3://${cfg.s3Backup.bucket}/
          rm ''${file}
        '';
        serviceConfig = {
          Type = "oneshot";
        };
      };
      s3-backup-timer = {
        description = "Backup regtool database to s3 for ${envname} every day.";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = "daily";
          Persistent = true;
        };
      };
    in
    mkIf cfg.enable {
      systemd.services = concatAttrs [
        { "regtool-${envname}" = web-server-service; }
        (optionalAttrs (cfg.localBackup.enable or false) { "backup-regtool-local-${envname}" = local-backup-service; })
        (optionalAttrs (cfg.s3Backup.enable or false) { "backup-regtool-s3-${envname}" = s3-backup-service; })
      ];
      systemd.timers = concatAttrs [
        (optionalAttrs (cfg.localBackup.enable or false) { "backup-regtool-local-${envname}" = local-backup-timer; })
        (optionalAttrs (cfg.s3Backup.enable or false) { "backup-regtool-s3-${envname}" = s3-backup-timer; })
      ];
      networking.firewall.allowedTCPPorts = [ cfg.config.port ];
      services.nginx.virtualHosts = optionalAttrs ((cfg.config.host or null) != null) ({
        "${cfg.config.host}" = {
          enableACME = true;
          forceSSL = true;
          locations."/".proxyPass =
            "http://localhost:${builtins.toString (cfg.config.port)}";
        };
      });
    };
}
