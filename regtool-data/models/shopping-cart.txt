ShoppingCart sql=choices_cache
  account AccountId
  json ByteString
  session Text Maybe default=NULL -- Stripe session id
  created UTCTime Maybe default=NULL
  status CartStatus Maybe default=NULL

  UniqueShoppingCartSession session !force

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable
