Account
    username              Text         Maybe
    emailAddress          EmailAddress
    verified              Bool             
    verificationKey       Text         Maybe -- INVARIANT: if verified is False, then this must satisfy isJust
    saltedPassphraseHash  Text
    resetPassphraseKey    Text         Maybe
    admin                 Bool         default=0 -- True means admin, default: False
    creationTime          UTCTime

    UniqueAccountUsername username     !force
    UniqueAccountEmail    emailAddress

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable


Permission
  account AccountId
  -- One column per possible permission
  exportData Bool default=0 -- False
  readDaycareEnrolments Bool default=0 -- False
  readTransportEnrolments Bool default=0 -- False


  UniquePermissionAccount account

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable
