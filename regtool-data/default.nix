{ mkDerivation, aeson, autodocodec, base, blaze-markup, bytestring
, cassava, containers, deepseq, email-validate, lib, mtl, path
, path-io, persistent, safe, template-haskell, text, time, validity
, validity-bytestring, validity-containers, validity-path
, validity-persistent, validity-text, validity-time, yesod-core
, yesod-form
}:
mkDerivation {
  pname = "regtool-data";
  version = "0.0.0";
  src = ./.;
  libraryHaskellDepends = [
    aeson autodocodec base blaze-markup bytestring cassava containers
    deepseq email-validate mtl path path-io persistent safe
    template-haskell text time validity validity-bytestring
    validity-containers validity-path validity-persistent validity-text
    validity-time yesod-core yesod-form
  ];
  license = "unknown";
}
