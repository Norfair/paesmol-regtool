{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Data
  ( module Regtool.Data,
    module Regtool.Data.Amount,
    module Regtool.Data.BusDirection,
    module Regtool.Data.CartStatus,
    module Regtool.Data.Company,
    module Regtool.Data.Country,
    module Regtool.Data.DB,
    module Regtool.Data.EmailAddress,
    module Regtool.Data.EmailStatus,
    module Regtool.Data.Nationality,
    module Regtool.Data.RelationShip,
    module Regtool.Data.SchoolDay,
    module Regtool.Data.SchoolLevel,
    module Regtool.Data.SchoolYear,
    module Regtool.Data.Semestre,
    module Regtool.Data.StandardOrRaw,
    module Regtool.Data.Utils,
  )
where

import Data.Aeson
import Data.Set (Set)
import Data.Time
import Data.Typeable
import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH
import Import
import Regtool.Data.Amount
import Regtool.Data.BusDirection
import Regtool.Data.CartStatus
import Regtool.Data.Company
import Regtool.Data.Country
import Regtool.Data.DB
import Regtool.Data.EmailAddress
import Regtool.Data.EmailStatus
import Regtool.Data.Nationality
import Regtool.Data.RelationShip
import Regtool.Data.SchoolDay
import Regtool.Data.SchoolLevel
import Regtool.Data.SchoolYear
import Regtool.Data.Semestre
import Regtool.Data.StandardOrRaw
import Regtool.Data.TH
import Regtool.Data.Utils
import Yesod.Form

instance (ToBackendKey SqlBackend a) => NFData (Regtool.Data.DB.Key a) where
  rnf = rnf . fromSqlKey

instance (NFData a) => NFData (Entity a) where
  rnf (Entity k v) = seq k $ rnf v

instance (PersistEntity a, FromJSON a) => FromJSON (Entity a) where
  parseJSON = withObject "Entity" $ \o -> Entity <$> o .: "key" <*> o .: "value"

instance (ToJSON (Regtool.Data.DB.Key a), ToJSON a) => ToJSON (Entity a) where
  toJSON (Entity k v) = object ["key" .= k, "value" .= v]

instance (ToJSON (Regtool.Data.DB.Key a), ToJSON a) => ToJSONKey (Entity a)

instance (PersistEntity a, FromJSON a) => FromJSONKey (Entity a)

type Userkey = Text

type Username = Text

instance NFData Textarea where
  rnf = rnf . unTextarea

instance Validity Textarea where
  validate = validate . unTextarea

share [mkPersist sqlSettings, mkMigrate "migrateAll"] $allModels

instance NFData Account

instance Validity Account where
  validate a@Account {..} =
    mconcat
      [ genericValidate a,
        declare "the verification key must exist if the account is not verified"
          $ isJust accountVerificationKey
          || accountVerified
      ]

instance FromJSON Account

instance ToJSON Account

instance NFData Permission

instance Validity Permission

instance FromJSON Permission

instance ToJSON Permission

instance NFData Email

instance Validity Email

instance FromJSON Email

instance ToJSON Email

instance NFData LanguageSection

instance Validity LanguageSection

instance FromJSON LanguageSection

instance ToJSON LanguageSection

instance NFData TransportSignup

instance Validity TransportSignup where
  validate ts@TransportSignup {..} =
    mconcat
      [ genericValidate ts,
        declare
          ( unwords
              ["the number of instalments is strictly positive: ", show transportSignupInstalments]
          )
          $ transportSignupInstalments
          >= 1
      ]

instance FromJSON TransportSignup

instance ToJSON TransportSignup

instance NFData TransportPayment

instance Validity TransportPayment where
  validate tp@TransportPayment {..} =
    mconcat [genericValidate tp, validateStrictlyPositiveAmount transportPaymentAmount]

instance FromJSON TransportPayment

instance ToJSON TransportPayment

instance NFData TransportPaymentPlan

instance Validity TransportPaymentPlan where
  validate tpp@TransportPaymentPlan {..} =
    mconcat [genericValidate tpp, validateStrictlyPositiveAmount transportPaymentPlanTotalAmount]

instance FromJSON TransportPaymentPlan

instance ToJSON TransportPaymentPlan

instance NFData TransportEnrollment

instance Validity TransportEnrollment

instance FromJSON TransportEnrollment

instance ToJSON TransportEnrollment

instance NFData OccasionalTransportSignup

instance Validity OccasionalTransportSignup

instance FromJSON OccasionalTransportSignup

instance ToJSON OccasionalTransportSignup

instance NFData OccasionalTransportPayment

instance Validity OccasionalTransportPayment where
  validate dcp@OccasionalTransportPayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount occasionalTransportPaymentAmount]

instance FromJSON OccasionalTransportPayment

instance ToJSON OccasionalTransportPayment

instance NFData YearlyFeePayment

instance Validity YearlyFeePayment where
  validate yfp@YearlyFeePayment {..} =
    mconcat [genericValidate yfp, validatePositiveAmount yearlyFeePaymentAmount]

instance FromJSON YearlyFeePayment

instance ToJSON YearlyFeePayment

instance NFData Checkout

instance Validity Checkout where
  validate co@Checkout {..} =
    mconcat
      [ genericValidate co,
        declare
          "If the checkout has a payment, then the amount must have been strictly positive otherwise it must have been negative"
          $ case checkoutPayment of
            Nothing -> checkoutAmount <= zeroAmount
            Just _ -> checkoutAmount > zeroAmount
      ]

instance FromJSON Checkout

instance ToJSON Checkout

instance NFData ExtraCharge

instance Validity ExtraCharge where
  validate d@ExtraCharge {..} =
    mconcat [genericValidate d, validateStrictlyPositiveAmount extraChargeAmount]

instance FromJSON ExtraCharge

instance ToJSON ExtraCharge

instance NFData Discount

instance Validity Discount where
  validate d@Discount {..} =
    mconcat [genericValidate d, validateStrictlyPositiveAmount discountAmount]

instance FromJSON Discount

instance ToJSON Discount

instance NFData Doctor

instance Validity Doctor

instance FromJSON Doctor

instance ToJSON Doctor

instance NFData Child

instance Validity Child

instance FromJSON Child

instance ToJSON Child

instance NFData ChildOf

instance Validity ChildOf

instance FromJSON ChildOf

instance ToJSON ChildOf

instance NFData Parent

instance Validity Parent

instance FromJSON Parent

instance ToJSON Parent

instance NFData PasswordResetEmail

instance Validity PasswordResetEmail

instance FromJSON PasswordResetEmail

instance ToJSON PasswordResetEmail

instance NFData VerificationEmail

instance Validity VerificationEmail

instance FromJSON VerificationEmail

instance ToJSON VerificationEmail

instance NFData PaymentReceivedEmail

instance Validity PaymentReceivedEmail

instance FromJSON PaymentReceivedEmail

instance ToJSON PaymentReceivedEmail

instance NFData AdminNotificationEmail

instance Validity AdminNotificationEmail

instance FromJSON AdminNotificationEmail

instance ToJSON AdminNotificationEmail

instance NFData DaycareEmail

instance Validity DaycareEmail

instance FromJSON DaycareEmail

instance ToJSON DaycareEmail

instance NFData TransportEmail

instance Validity TransportEmail

instance FromJSON TransportEmail

instance ToJSON TransportEmail

instance NFData OccasionalDaycareEmail

instance Validity OccasionalDaycareEmail

instance FromJSON OccasionalDaycareEmail

instance ToJSON OccasionalDaycareEmail

instance NFData OccasionalTransportEmail

instance Validity OccasionalTransportEmail

instance FromJSON OccasionalTransportEmail

instance ToJSON OccasionalTransportEmail

instance NFData StripeCustomer

instance Validity StripeCustomer

instance FromJSON StripeCustomer

instance ToJSON StripeCustomer

instance NFData StripePayment

instance Validity StripePayment where
  validate sp@StripePayment {..} =
    mconcat [genericValidate sp, validateStrictlyPositiveAmount stripePaymentAmount]

instance FromJSON StripePayment

instance ToJSON StripePayment

instance NFData BusSchoolYear

instance Validity BusSchoolYear

instance FromJSON BusSchoolYear

instance ToJSON BusSchoolYear

instance FromJSONKey BusSchoolYear

instance ToJSONKey BusSchoolYear

instance NFData BusLine

instance Validity BusLine where
  validate bl@BusLine {..} =
    mconcat
      [ genericValidate bl,
        declare (unwords ["the number of seats must be strictly positive: ", show busLineSeats])
          $ busLineSeats
          >= 1
      ]

instance FromJSON BusLine

instance ToJSON BusLine

instance NFData BusStop

instance Validity BusStop

instance FromJSON BusStop

instance ToJSON BusStop

instance NFData BusLineStop

instance Validity BusLineStop

instance FromJSON BusLineStop

instance ToJSON BusLineStop

instance NFData DaycareSemestre

instance Validity DaycareSemestre

instance FromJSON DaycareSemestre

instance ToJSON DaycareSemestre

instance NFData DaycareTimeslot

instance Validity DaycareTimeslot where
  validate dcts@DaycareTimeslot {..} =
    mconcat
      [ genericValidate dcts,
        declare "The end time is after the start time" $ daycareTimeslotEnd >= daycareTimeslotStart,
        validatePositiveAmount daycareTimeslotFee,
        validatePositiveAmount daycareTimeslotOccasionalFee
      ]

instance FromJSON DaycareTimeslot

instance ToJSON DaycareTimeslot

instance NFData DaycareSignup

instance Validity DaycareSignup

instance FromJSON DaycareSignup

instance ToJSON DaycareSignup

instance NFData DaycarePayment

instance Validity DaycarePayment where
  validate dcp@DaycarePayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount daycarePaymentAmount]

instance FromJSON DaycarePayment

instance ToJSON DaycarePayment

instance NFData DaycareActivity

instance Validity DaycareActivity where
  validate dcp@DaycareActivity {..} =
    mconcat
      [ genericValidate dcp,
        declare "The end time is after the start time" $ daycareActivityEnd >= daycareActivityStart,
        validatePositiveAmount daycareActivityFee
      ]

instance FromJSON DaycareActivity

instance ToJSON DaycareActivity

instance NFData DaycareActivitySignup

instance Validity DaycareActivitySignup

instance FromJSON DaycareActivitySignup

instance ToJSON DaycareActivitySignup

instance NFData DaycareActivityPayment

instance Validity DaycareActivityPayment where
  validate dcp@DaycareActivityPayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount daycareActivityPaymentAmount]

instance FromJSON DaycareActivityPayment

instance ToJSON DaycareActivityPayment

instance NFData OccasionalDaycareSignup

instance Validity OccasionalDaycareSignup

instance FromJSON OccasionalDaycareSignup

instance ToJSON OccasionalDaycareSignup

instance NFData OccasionalDaycarePayment

instance Validity OccasionalDaycarePayment where
  validate dcp@OccasionalDaycarePayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount occasionalDaycarePaymentAmount]

instance FromJSON OccasionalDaycarePayment

instance ToJSON OccasionalDaycarePayment

instance Validity ShoppingCart

instance NFData CustomActivity

instance Validity CustomActivity where
  validate ca@CustomActivity {..} =
    mconcat
      [ genericValidate ca,
        declare "The end time is after the start time" $ customActivityEnd >= customActivityStart,
        validatePositiveAmount customActivityFee
      ]

instance FromJSON CustomActivity

instance ToJSON CustomActivity

instance NFData CustomActivitySignup

instance Validity CustomActivitySignup

instance FromJSON CustomActivitySignup

instance ToJSON CustomActivitySignup

instance NFData CustomActivityPayment

instance Validity CustomActivityPayment

instance FromJSON CustomActivityPayment

instance ToJSON CustomActivityPayment
