{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.CartStatus
  ( CartStatus (..),
    cartStatusText,
    parseCartStatus,
    cartStatusDone,
  )
where

import Database.Persist
import Database.Persist.Sql
import Import

data CartStatus
  = CartChosen
  | CartPaymentInitiated
  | CartPaymentCancelled
  | CartPaymentSucceeded
  | CartPaymentNotNecessary
  | CartPaymentReceived
  | CartPaymentFailed
  deriving (Show, Eq, Ord, Generic)

instance Validity CartStatus

instance NFData CartStatus

instance PersistField CartStatus where
  toPersistValue = toPersistValue . cartStatusText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseCartStatus t of
      Nothing -> Left $ "Couldn't decode cart status: " <> t
      Just cs -> pure cs

instance PersistFieldSql CartStatus where
  sqlType Proxy = sqlType (Proxy :: Proxy String)

cartStatusText :: CartStatus -> Text
cartStatusText =
  \case
    CartChosen -> "chosen"
    CartPaymentInitiated -> "initiated"
    CartPaymentCancelled -> "cancelled"
    CartPaymentSucceeded -> "succeeded"
    CartPaymentNotNecessary -> "not-necessary"
    CartPaymentReceived -> "received"
    CartPaymentFailed -> "failed"

parseCartStatus :: Text -> Maybe CartStatus
parseCartStatus =
  \case
    "chosen" -> Just CartChosen
    "initiated" -> Just CartPaymentInitiated
    "cancelled" -> Just CartPaymentCancelled
    "succeeded" -> Just CartPaymentSucceeded
    "not-necessary" -> Just CartPaymentNotNecessary
    "received" -> Just CartPaymentReceived
    "failed" -> Just CartPaymentFailed
    _ -> Nothing

cartStatusDone :: CartStatus -> Bool
cartStatusDone =
  \case
    CartChosen -> False
    CartPaymentInitiated -> False
    CartPaymentCancelled -> False
    CartPaymentSucceeded -> False
    CartPaymentNotNecessary -> True
    CartPaymentReceived -> True
    CartPaymentFailed -> True
