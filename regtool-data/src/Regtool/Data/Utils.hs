{-# LANGUAGE FlexibleContexts #-}

module Regtool.Data.Utils where

import Database.Persist
import Database.Persist.Sql (SqlBackend, fromSqlKey)
import Import

declareForeignKeyConstraint ::
  ( ToBackendKey SqlBackend a,
    ToBackendKey SqlBackend b
  ) =>
  String ->
  [Entity a] ->
  (a -> Key b) ->
  Key b ->
  Validation
declareForeignKeyConstraint n vals func foreignKey =
  decorate n
    $ decorateList vals
    $ \val ->
      let k = func (entityVal val)
          s =
            unwords
              [ "The foreign key",
                show $ fromSqlKey k,
                "of entity with key",
                show $ fromSqlKey (entityKey val),
                "is equal to the expected foreign key",
                show $ fromSqlKey foreignKey
              ]
       in declare s $ k == foreignKey

declareForeignKeysConstraint ::
  ( ToBackendKey SqlBackend a,
    ToBackendKey SqlBackend b
  ) =>
  String ->
  [Entity a] ->
  (a -> Key b) ->
  [Entity b] ->
  Validation
declareForeignKeysConstraint n vals func = declareForeignKeysConstraintM n vals (Just . func)

validateForeignKeys :: (Eq (Key b)) => [Entity a] -> (a -> Key b) -> [Entity b] -> Bool
validateForeignKeys vals func = validateForeignKeysM vals (Just . func)

declareForeignKeysConstraintM ::
  ( ToBackendKey SqlBackend a,
    ToBackendKey SqlBackend b
  ) =>
  String ->
  [Entity a] ->
  (a -> Maybe (Key b)) ->
  [Entity b] ->
  Validation
declareForeignKeysConstraintM n vals func foreignVals =
  decorate n
    $ decorateList vals
    $ \val ->
      case func $ entityVal val of
        Nothing -> valid
        Just k ->
          let s =
                unwords
                  [ "The foreign key",
                    show $ fromSqlKey k,
                    "of entity with key",
                    show $ fromSqlKey (entityKey val),
                    "is found in the other table:",
                    show (map (fromSqlKey . entityKey) foreignVals)
                  ]
           in declare s $ k `elem` map entityKey foreignVals

validateForeignKeysM :: (Eq (Key b)) => [Entity a] -> (a -> Maybe (Key b)) -> [Entity b] -> Bool
validateForeignKeysM vals func foreignVals =
  all (`elem` map entityKey foreignVals) (mapMaybe (func . entityVal) vals)

declareUniquenessConstraint :: (Show b, Ord b) => String -> [a] -> (a -> b) -> Validation
declareUniquenessConstraint n ls func =
  declare (unlines (n : explanation)) $ validateUniquenessConstraint ls func
  where
    explanation = map show . group . sort . map func $ ls

validateUniquenessConstraint :: (Eq b) => [a] -> (a -> b) -> Bool
validateUniquenessConstraint ls func = distinct $ map func ls

seperateIds :: (Eq (Key a)) => [Entity a] -> Bool
seperateIds = distinct . map entityKey

distinct :: (Eq a) => [a] -> Bool
distinct ls = length (nub ls) == length ls
