{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.Nationality where

import Data.Aeson
import Import
import Regtool.Data.StandardOrRaw

data StandardNationality
  = NatBelgian
  | NatDutch
  | NatGerman
  | NatFrench
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity StandardNationality

instance NFData StandardNationality

instance FromJSON StandardNationality

instance ToJSON StandardNationality

standardNationalityText :: StandardNationality -> Text
standardNationalityText r =
  case r of
    NatBelgian -> "Belgian"
    NatDutch -> "Dutch"
    NatGerman -> "German"
    NatFrench -> "French"

parseStandardNationality :: Text -> Maybe StandardNationality
parseStandardNationality t =
  case t of
    "Belgian" -> Just NatBelgian
    "Dutch" -> Just NatDutch
    "German" -> Just NatGerman
    "French" -> Just NatFrench
    _ -> Nothing

instance StandardField StandardNationality where
  standardToText = standardNationalityText
  standardFromText = parseStandardNationality

type Nationality = StandardOrRaw StandardNationality

nationalityText :: Nationality -> Text
nationalityText = standardOrRawText standardNationalityText
