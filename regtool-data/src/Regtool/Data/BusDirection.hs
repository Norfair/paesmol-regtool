{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.BusDirection where

import Data.Aeson
import Database.Persist
import Database.Persist.Sql
import Import

data BusDirection
  = ToSchool
  | FromSchool
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity BusDirection

instance NFData BusDirection

instance FromJSON BusDirection

instance ToJSON BusDirection

busDirectionText :: BusDirection -> Text
busDirectionText r =
  case r of
    ToSchool -> "ToSchool"
    FromSchool -> "FromSchool"

parseBusDirection :: Text -> Maybe BusDirection
parseBusDirection t =
  case t of
    "ToSchool" -> Just ToSchool
    "FromSchool" -> Just FromSchool
    _ -> Nothing

instance PersistField BusDirection where
  toPersistValue = toPersistValue . busDirectionText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseBusDirection t of
      Nothing -> Left "Could not decode bus direction."
      Just c -> pure c

instance PersistFieldSql BusDirection where
  sqlType Proxy = sqlType (Proxy :: Proxy String)

prettyBusDirection :: BusDirection -> Text
prettyBusDirection ToSchool = "to school"
prettyBusDirection FromSchool = "from school"
