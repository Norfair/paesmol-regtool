{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.Company where

import Data.Aeson
import Database.Persist
import Database.Persist.Sql
import Import

data Company
  = EU
  | EATC
  | AEA
  | OtherCompany
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity Company

instance NFData Company

instance FromJSON Company

instance ToJSON Company

instance ToField Company where
  toField = toField . companyText

companyText :: Company -> Text
companyText r =
  case r of
    EU -> "EU"
    EATC -> "EATC"
    AEA -> "AEA"
    OtherCompany -> "Other"

parseCompany :: Text -> Maybe Company
parseCompany t =
  case t of
    "EU" -> Just EU
    "EATC" -> Just EATC
    "AEA" -> Just AEA
    "Other" -> Just OtherCompany
    _ -> Just OtherCompany

instance PersistField Company where
  toPersistValue = toPersistValue . companyText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseCompany t of
      Nothing -> Left "Could not decode company."
      Just c -> pure c

instance PersistFieldSql Company where
  sqlType Proxy = sqlType (Proxy :: Proxy String)
