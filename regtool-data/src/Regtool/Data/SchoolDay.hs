{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolDay where

import Data.Aeson
import Data.Time
import Database.Persist
import Database.Persist.Sql
import Import

data SchoolDay
  = SchoolMonday
  | SchoolTuesday
  | SchoolWednesday
  | SchoolThursday
  | SchoolFriday
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity SchoolDay

instance NFData SchoolDay

instance FromJSON SchoolDay

instance ToJSON SchoolDay

schoolDayText :: SchoolDay -> Text
schoolDayText r =
  case r of
    SchoolMonday -> "Monday"
    SchoolTuesday -> "Tuesday"
    SchoolWednesday -> "Wednesday"
    SchoolThursday -> "Thursday"
    SchoolFriday -> "Friday"

parseSchoolDay :: Text -> Maybe SchoolDay
parseSchoolDay t =
  case t of
    "Monday" -> Just SchoolMonday
    "Tuesday" -> Just SchoolTuesday
    "Wednesday" -> Just SchoolWednesday
    "Thursday" -> Just SchoolThursday
    "Friday" -> Just SchoolFriday
    _ -> Nothing

instance PersistField SchoolDay where
  toPersistValue = toPersistValue . schoolDayText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseSchoolDay t of
      Nothing -> Left "Could not decode schoolDay."
      Just c -> pure c

instance PersistFieldSql SchoolDay where
  sqlType Proxy = sqlType (Proxy :: Proxy String)

dayMatchesSchoolDay :: Day -> SchoolDay -> Bool
dayMatchesSchoolDay d sd =
  case (dayOfWeek d, sd) of
    (Monday, SchoolMonday) -> True
    (Tuesday, SchoolTuesday) -> True
    (Wednesday, SchoolWednesday) -> True
    (Thursday, SchoolThursday) -> True
    (Friday, SchoolFriday) -> True
    _ -> False
