{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolYear
  ( SchoolYear (..),
    schoolYearStartYear,
    schoolYearEndYear,
    schoolYearNext,
    schoolYearText,
    schoolYearString,
    currentSchoolYears,
    currentSchoolYear,
    getCurrentSchoolyear,
    getCurrentSchoolyears,
    getCurrentAndNextSchoolYears,
    schoolYearSemestreString,
    schoolYearSemestreText,
  )
where

import Data.Aeson
import qualified Data.Text as T
import Data.Time
import Database.Persist
import Database.Persist.Sql
import Import
import Regtool.Data.Semestre
import Yesod.Core.Dispatch (PathPiece (..))

newtype SchoolYear = SchoolYear
  { unSchoolYear :: Int
  }
  deriving (Show, Read, Eq, Ord, Enum, Generic, PersistField, PersistFieldSql, PathPiece)

instance Validity SchoolYear

instance NFData SchoolYear

instance FromJSON SchoolYear

instance ToJSON SchoolYear

schoolYearStartYear :: SchoolYear -> Int
schoolYearStartYear = unSchoolYear

schoolYearEndYear :: SchoolYear -> Int
schoolYearEndYear = (+ 1) . schoolYearStartYear

schoolYearNext :: SchoolYear -> SchoolYear
schoolYearNext (SchoolYear i) = SchoolYear $ succ i

schoolYearText :: SchoolYear -> Text
schoolYearText = T.pack . schoolYearString

schoolYearString :: SchoolYear -> String
schoolYearString sy = unwords [show $ schoolYearStartYear sy, "-", show $ schoolYearEndYear sy]

currentSchoolYears :: UTCTime -> [SchoolYear]
currentSchoolYears now = [SchoolYear (y - 1), SchoolYear y]
  where
    y = fromInteger yi
    (yi, _, _) = toGregorian d
    d = utctDay now

currentSchoolYear :: UTCTime -> SchoolYear
currentSchoolYear now =
  if m >= 6
    then SchoolYear y
    else SchoolYear (y - 1)
  where
    y = fromInteger yi
    (yi, m, _) = toGregorian d
    d = utctDay now

getCurrentSchoolyear :: IO SchoolYear
getCurrentSchoolyear = currentSchoolYear <$> getCurrentTime

getCurrentSchoolyears :: IO [SchoolYear]
getCurrentSchoolyears = currentSchoolYears <$> getCurrentTime

getCurrentAndNextSchoolYears :: IO [SchoolYear]
getCurrentAndNextSchoolYears = do
  sy <- getCurrentSchoolyear
  pure [sy, succ sy, succ $ succ sy]

schoolYearSemestreString :: SchoolYear -> Semestre -> String
schoolYearSemestreString (SchoolYear y) sem =
  let y's = show (y + 1)
      ys = show y
   in case sem of
        Spring -> concat ["Feb ", y's, " - Jul ", y's]
        Fall -> concat ["Sep ", ys, " - Jan ", y's]

schoolYearSemestreText :: SchoolYear -> Semestre -> Text
schoolYearSemestreText sy sem = T.pack $ schoolYearSemestreString sy sem
