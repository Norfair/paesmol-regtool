{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Data.EmailStatus
  ( EmailStatus (..),
    renderEmailStatus,
    parseEmailStatus,
  )
where

import Data.Aeson as JSON
import qualified Data.Text as T
import Database.Persist
import Database.Persist.Sql
import Import

data EmailStatus
  = EmailStatusUnsent
  | EmailStatusSent
  | EmailStatusError
  deriving (Show, Eq, Ord, Generic)

instance Validity EmailStatus

instance NFData EmailStatus

instance PersistField EmailStatus where
  toPersistValue = toPersistValue . renderEmailStatus
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseEmailStatus t of
      Nothing -> Left "Could not decode company."
      Just c -> pure c

instance PersistFieldSql EmailStatus where
  sqlType Proxy = sqlType (Proxy :: Proxy String)

instance ToJSON EmailStatus where
  toJSON = toJSON . renderEmailStatus

instance FromJSON EmailStatus where
  parseJSON =
    withText "EmailStatus" $ \t ->
      case parseEmailStatus t of
        Nothing -> fail $ "Unknown EmailStatus: " <> T.unpack t
        Just es -> pure es

renderEmailStatus :: EmailStatus -> Text
renderEmailStatus =
  \case
    EmailStatusUnsent -> "Unsent"
    EmailStatusSent -> "Sent"
    EmailStatusError -> "Error"

parseEmailStatus :: Text -> Maybe EmailStatus
parseEmailStatus =
  \case
    "Unsent" -> Just EmailStatusUnsent
    "Sending" -> Just EmailStatusUnsent
    "Sent" -> Just EmailStatusSent
    "Error" -> Just EmailStatusError
    _ -> Nothing
