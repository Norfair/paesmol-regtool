{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.Semestre where

import Data.Aeson
import Database.Persist
import Database.Persist.Sql
import Import

data Semestre
  = Fall
  | Spring
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity Semestre

instance NFData Semestre

instance FromJSON Semestre

instance ToJSON Semestre

semestreDBText :: Semestre -> Text
semestreDBText r =
  case r of
    Spring -> "Spring"
    Fall -> "Fall"

semestreText :: Semestre -> Text
semestreText r =
  case r of
    Spring -> "Feb - Jul"
    Fall -> "Sep - Jan"

parseSemestre :: Text -> Maybe Semestre
parseSemestre t =
  case t of
    "Spring" -> Just Spring
    "Fall" -> Just Fall
    _ -> Nothing

instance PersistField Semestre where
  toPersistValue = toPersistValue . semestreDBText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseSemestre t of
      Nothing -> Left "Could not decode semestre."
      Just c -> pure c

instance PersistFieldSql Semestre where
  sqlType Proxy = sqlType (Proxy :: Proxy String)
