{-# LANGUAGE TemplateHaskell #-}

module Regtool.Data.TH
  ( allModels,
  )
where

import Data.List
import Database.Persist.Quasi
import Database.Persist.TH
import Import
import Language.Haskell.TH
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax
import System.IO (readFile)

allModels :: Q Exp
allModels = do
  fs <- sort . filter (not . hidden) <$> runIO (snd <$> listDirRecur $(mkRelDir "models"))
  mapM_ (qAddDependentFile . toFilePath) fs
  contents <- runIO $ unlines <$> mapM (readFile . toFilePath) fs
  quoteExp (persistWith lowerCaseSettings) contents

-- TODO filter all hidden files, not just vim swp files
hidden :: Path Abs File -> Bool
hidden f =
  case fileExtension f of
    Nothing -> False
    Just e -> e == ".swp"
