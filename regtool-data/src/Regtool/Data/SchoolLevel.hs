{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolLevel
  ( SchoolLevel (..),
    SchoolLevelType (..),
    schoolLevelDecompose,
    schoolLevelTypeText,
    schoolLevelText,
    schoolLevelInt,
    showableSchoolLevels,
    schoolLevelField,
  )
where

import Data.Aeson
import qualified Data.Text as T
import Database.Persist
import Database.Persist.Sql
import Import
import Text.Read
import Yesod.Core
import Yesod.Form

data SchoolLevel
  = Nursery1
  | Nursery2
  | Nursery3
  | Primary1
  | Primary2
  | Primary3
  | Primary4
  | Primary5
  | Secondary1
  | Secondary2
  | Secondary3
  | Secondary4
  | Secondary5
  | Secondary6
  | Secondary7
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity SchoolLevel

instance NFData SchoolLevel

instance FromJSON SchoolLevel

instance ToJSON SchoolLevel

instance ToField SchoolLevel where
  toField = toField . schoolLevelText

data SchoolLevelType
  = Nursery
  | Primary
  | Secondary

schoolLevelDecompose :: SchoolLevel -> (SchoolLevelType, Int)
schoolLevelDecompose s =
  case s of
    Nursery1 -> (Nursery, 1)
    Nursery2 -> (Nursery, 2)
    Nursery3 -> (Nursery, 3)
    Primary1 -> (Primary, 1)
    Primary2 -> (Primary, 2)
    Primary3 -> (Primary, 3)
    Primary4 -> (Primary, 4)
    Primary5 -> (Primary, 5)
    Secondary1 -> (Secondary, 1)
    Secondary2 -> (Secondary, 2)
    Secondary3 -> (Secondary, 3)
    Secondary4 -> (Secondary, 4)
    Secondary5 -> (Secondary, 5)
    Secondary6 -> (Secondary, 6)
    Secondary7 -> (Secondary, 7)

schoolLevelTypeText :: SchoolLevelType -> Text
schoolLevelTypeText Nursery = "Nursery"
schoolLevelTypeText Primary = "Primary"
schoolLevelTypeText Secondary = "Secondary"

schoolLevelText :: SchoolLevel -> Text
schoolLevelText s =
  case schoolLevelDecompose s of
    (t, i) -> T.unwords [schoolLevelTypeText t, T.pack $ show i]

schoolLevelInt :: SchoolLevel -> Int
schoolLevelInt = fromEnum

parseSchoolLevelInt :: Int -> Either Text SchoolLevel
parseSchoolLevelInt i
  | i >= schoolLevelInt minBound && i <= schoolLevelInt maxBound = Right $ toEnum i
  | otherwise = Left $ "Unknown school level: " <> T.pack (show i)

instance PersistField SchoolLevel where
  toPersistValue = toPersistValue . schoolLevelInt
  fromPersistValue pv =
    case pv of
      PersistInt64 i -> parseSchoolLevelInt $ fromIntegral i
      _ -> Left "Invalid persist value type for parsing SchoolLint"

instance PersistFieldSql SchoolLevel where
  sqlType Proxy = SqlInt64

showableSchoolLevels :: [SchoolLevel]
showableSchoolLevels =
  [ Nursery1,
    Nursery2,
    -- N3 is not used anymore, but still in the database
    -- Nursery3,
    Primary1,
    Primary2,
    Primary3,
    Primary4,
    Primary5,
    Secondary1,
    Secondary2,
    Secondary3,
    Secondary4,
    Secondary5,
    Secondary6,
    Secondary7
  ]

schoolLevelField :: (RenderMessage site FormMessage) => Field (HandlerFor site) SchoolLevel
schoolLevelField =
  selectField
    $ pure
      OptionList
        { olOptions =
            map
              ( \sl ->
                  Option
                    { optionDisplay = schoolLevelText sl,
                      optionInternalValue = sl,
                      optionExternalValue = T.pack $ show $ schoolLevelInt sl
                    }
              )
              showableSchoolLevels,
          olReadExternal = \t ->
            readMaybe (T.unpack t) >>= \i ->
              case parseSchoolLevelInt i of
                Left _ -> Nothing
                Right sl -> Just sl
        }
