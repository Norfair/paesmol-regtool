{ mkDerivation, base, bytestring, containers, criterion
, genvalidity, genvalidity-bytestring, genvalidity-containers
, genvalidity-criterion, genvalidity-path, genvalidity-persistent
, genvalidity-sydtest, genvalidity-sydtest-aeson, genvalidity-text
, genvalidity-time, lib, path, path-io, persistent
, persistent-sqlite, pretty-show, QuickCheck, regtool-calculate
, regtool-data, regtool-data-gen, safe, sydtest, sydtest-discover
, text, time, validity-path, validity-text
}:
mkDerivation {
  pname = "regtool-calculate-gen";
  version = "0.0.0";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring containers genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-persistent
    genvalidity-sydtest genvalidity-sydtest-aeson genvalidity-text
    genvalidity-time path path-io persistent pretty-show QuickCheck
    regtool-calculate regtool-data regtool-data-gen safe sydtest text
    validity-path validity-text
  ];
  testHaskellDepends = [
    base bytestring containers genvalidity genvalidity-bytestring
    genvalidity-containers genvalidity-path genvalidity-sydtest
    genvalidity-sydtest-aeson genvalidity-text genvalidity-time path
    path-io persistent persistent-sqlite pretty-show QuickCheck
    regtool-calculate regtool-data regtool-data-gen safe sydtest text
    time validity-path validity-text
  ];
  testToolDepends = [ sydtest-discover ];
  benchmarkHaskellDepends = [
    base criterion genvalidity-criterion regtool-calculate
    regtool-data-gen
  ];
  license = lib.licenses.unfree;
  hydraPlatforms = lib.platforms.none;
}
