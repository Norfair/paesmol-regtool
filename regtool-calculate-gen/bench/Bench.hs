{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Criterion.Main as Criterion
import Data.GenValidity.Criterion
import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Checkout
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Config
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Calculation.Gen ()
import Regtool.Data.Gen ()

main :: IO ()
main =
  Criterion.defaultMain
    [ bgroup "Input" [genValidBench @CostsInput],
      bgroup
        "Abstract"
        [ genValidBench @YearlyFeeStatus,
          genValidBench @TransportFeeStatus,
          genValidBench @OccasionalTransportFeeStatus,
          genValidBench @CustomActivityFeeStatus,
          genValidBench @DaycareFeeStatus,
          genValidBench @DaycareActivityFeeStatus,
          genValidBench @OccasionalDaycareFeeStatus,
          genValidBench @AbstractSemestrelyCosts,
          genValidBench @AbstractYearlyCosts,
          genValidBench @AbstractCosts
        ],
      bgroup
        "Calculated"
        [ genValidBench @YearlyFeePaymentStatus,
          genValidBench @TransportPaymentStatus,
          genValidBench @OccasionalTransportPaymentStatus,
          genValidBench @CustomActivityPaymentStatus,
          genValidBench @DaycarePaymentStatus,
          genValidBench @DaycareActivityPaymentStatus,
          genValidBench @OccasionalDaycarePaymentStatus,
          genValidBench @CalculatedSemestrelyCosts,
          genValidBench @CalculatedYearlyCosts,
          genValidBench @CalculatedCosts
        ],
      bgroup "Checkout" [genValidBench @CheckoutsOverview, genValidBench @CheckoutOverview],
      bgroup
        "Choice"
        [ genValidBench @Choices,
          genValidBench @YearlyChoices,
          genValidBench @SemestrelyChoices,
          genValidBench @TransportInstallmentChoices,
          genValidBench @OccasionalTransportChoice,
          genValidBench @DaycareChoice,
          genValidBench @DaycareActivityChoice,
          genValidBench @OccasionalDaycareChoice,
          genValidBench @CustomActivityChoice
        ],
      bgroup "Config" [genValidBench @CostsConfig],
      bgroup
        "Invoice"
        [ genValidBench @CostLine,
          genValidBench @DiscountLine,
          genValidBench @Invoice
        ],
      bgroup
        "Consequences"
        [ genValidBench @DaycareActivityPaymentConsequences,
          genValidBench @DaycarePaymentConsequences,
          genValidBench @TransportPaymentConsequences,
          genValidBench @PaymentConsequences
        ]
    ]
