{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Abstract where

import qualified Data.Map as M
import GenImport
import Regtool.Calculation.Costs.Abstract
import Regtool.Data
import Regtool.Data.Gen ()
import Regtool.Data.Gen.Entity

instance GenValid YearlyFeeStatus where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid TransportFeeStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportFeeStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
    oneof
      [ pure $ OccasionalTransportFeeUnpaid ec eots,
        do
          eotp <-
            modEnt (\otp -> otp {occasionalTransportPaymentSignup = entityKey eots}) <$> genValid
          pure $ OccasionalTransportFeePaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid CustomActivityFeeStatus where
  genValid = do
    ec <- genValid
    eca <- genValid
    ecas <-
      modEnt
        ( \cas ->
            cas
              { customActivitySignupChild = entityKey ec,
                customActivitySignupActivity = entityKey eca
              }
        )
        <$> genValid
    oneof
      [ pure $ CustomActivityFeeUnpaid ec eca ecas,
        do
          ecap <- modEnt (\cap -> cap {customActivityPaymentSignup = entityKey ecas}) <$> genValid
          pure $ CustomActivityFeePaid ec ecas ecap
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        (\ds -> ds {daycareSignupChild = entityKey ec, daycareSignupTimeslot = entityKey edcts})
        <$> genValid
    oneof
      [ pure $ DaycareFeeUnpaid ec edcts eds,
        do
          edp <- modEnt (\dp -> dp {daycarePaymentSignup = entityKey eds}) <$> genValid
          pure $ DaycareFeePaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        ( \ds ->
            ds
              { daycareActivitySignupChild = entityKey ec,
                daycareActivitySignupActivity = entityKey edcts
              }
        )
        <$> genValid
    oneof
      [ pure $ DaycareActivityFeeUnpaid ec edcts eds,
        do
          edp <- modEnt (\dp -> dp {daycareActivityPaymentSignup = entityKey eds}) <$> genValid
          pure $ DaycareActivityFeePaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycareFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eots <-
      modEnt
        ( \ots ->
            ots
              { occasionalDaycareSignupChild = entityKey ec,
                occasionalDaycareSignupTimeslot = entityKey edcts
              }
        )
        <$> genValid
    oneof
      [ pure $ OccasionalDaycareFeeUnpaid ec edcts eots,
        do
          eotp <-
            modEnt (\otp -> otp {occasionalDaycarePaymentSignup = entityKey eots}) <$> genValid
          pure $ OccasionalDaycareFeePaid ec edcts eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid AbstractSemestrelyCosts where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      AbstractSemestrelyCosts <$> resize a genValid <*> resize b genValid
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid AbstractYearlyCosts where
  genValid =
    (`suchThat` isValid)
      $ sized
      $ \n -> do
        (a, b, c) <- genSplit3 n
        aycYearlyFee <- resize a genValid
        aycYearlyBus <- resize b genValid
        aycYearlySemestrely <-
          resize c
            $ M.fromList
            <$> genListOf ((,) <$> genValid <*> (genValid `suchThat` (not . nullAbstractSemestrelyCosts)))
        pure AbstractYearlyCosts {..}
  shrinkValid = shrinkValidStructurally

instance GenValid AbstractCosts where
  genValid =
    sized $ \n -> do
      (a, b, c, d, e) <- genSplit5 n
      abstractCostsMap <-
        resize a
          $ M.fromList
          <$> genListOf ((,) <$> genValid <*> (genValid `suchThat` (not . nullAbstractYearlyCosts)))
      abstractCostsOccasionalTransport <-
        resize b $ genValid `suchThat` (distinct . map occasionalTransportFeeStatusSignup)
      abstractCostsOccasionalDaycare <-
        resize c $ genValid `suchThat` (distinct . map occasionalDaycareFeeStatusSignup)
      abstractCostsCustomActivity <-
        resize d $ genValid `suchThat` (distinct . map customActivityFeeStatusSignup)
      abstractCostsExtraCharges <-
        resize e $ modEntL (\d_ -> d_ {extraChargeCheckout = Nothing}) <$> genValid
      abstractCostsDiscounts <-
        resize e $ modEntL (\d_ -> d_ {discountCheckout = Nothing}) <$> genValid
      pure AbstractCosts {..}
  shrinkValid = shrinkValidStructurally
