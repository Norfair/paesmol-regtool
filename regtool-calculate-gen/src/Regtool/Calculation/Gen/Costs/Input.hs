{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Input where

import Data.Foldable
import Data.GenValidity.Containers
import Data.GenValidity.Persist
import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NE
import Database.Persist
import GenImport
import Regtool.Calculation.Costs.Input
import Regtool.Data
import Regtool.Data.Gen

genCostsInputWithOnlyUnpaidTransport :: Gen CostsInput
genCostsInputWithOnlyUnpaidTransport = scale (* 4) $ go <$> genValid
  where
    go ci =
      ci
        { costsInputYearlyFeePayments = [],
          costsInputTransportPaymentPlans = [],
          costsInputTransportPayments = [],
          costsInputTransportEnrollments = [],
          costsInputOccasionalTransportSignups = [],
          costsInputOccasionalTransportPayments = [],
          costsInputDaycareSemestres = [],
          costsInputDaycareTimeslots = [],
          costsInputDaycareSignups = [],
          costsInputDaycarePayments = [],
          costsInputOccasionalDaycareSignups = [],
          costsInputOccasionalDaycarePayments = [],
          costsInputDaycareActivities = [],
          costsInputDaycareActivitySignups = [],
          costsInputDaycareActivityPayments = [],
          costsInputCustomActivities = [],
          costsInputCustomActivitySignups = [],
          costsInputCustomActivityPayments = [],
          costsInputCheckouts = [],
          costsInputStripePayments = []
        }

instance GenValid CostsInput where
  genValid =
    sized $ \n0 -> do
      (n1, n2, n3, n4) <- genSplit4 n0
      (a, b, c, d, e, f, g) <- genSplit7 n1
      (h, i, j, k, l, m, n) <- genSplit7 n2
      (o, p, q, r, s, t, u) <- genSplit7 n3
      (v, w, x, y, z) <- genSplit5 n4
      costsInputAccount <- resize a genValid
      costsInputParents <- resize b $ parentsWithAccount costsInputAccount
      costsInputChildren <- resize c validsWithSeperateIDs
      costsInputTransportSignups <-
        resize d $ transportSignupsForChildren $ map entityKey costsInputChildren
      costsInputOccasionalTransportSignups <-
        resize e $ occasionalTransportSignupsForChildren $ map entityKey costsInputChildren
      costsInputDaycareSemestres <- resize f daycareSemestres
      costsInputBusSchoolYears <-
        busSchoolYearsFor costsInputTransportSignups costsInputDaycareSemestres
      costsInputDaycareTimeslots <-
        resize g $ daycareTimeslotsFor $ map entityKey costsInputDaycareSemestres
      costsInputDaycareActivities <-
        resize h
          $ daycareActivitiesFor
            (map entityKey costsInputDaycareSemestres)
            (map entityKey costsInputDaycareTimeslots)
      costsInputDaycareSignups <-
        resize i
          $ daycareSignupsFor
            (map entityKey costsInputChildren)
            (map entityKey costsInputDaycareTimeslots)
      costsInputOccasionalDaycareSignups <-
        resize j $ occasionalDaycareSignupsForChildren $ map entityKey costsInputChildren
      costsInputDaycareActivitySignups <-
        resize k
          $ daycareActivitySignupsFor
            (map entityKey costsInputChildren)
            (map entityKey costsInputDaycareActivities)
      costsInputCustomActivities <- resize l $ genValid >>= genSeperateIdsFor
      costsInputCustomActivitySignups <-
        resize m
          $ customActivitySignupsFor
            (map entityKey costsInputChildren)
            (map entityKey costsInputCustomActivities)
      (costsInputTransportPaymentPlans, costsInputTransportEnrollments) <-
        resize n $ enrollmentsAndPlansFor $ map entityKey costsInputTransportSignups
      -- We'll generate the following seemingly backward.
      -- First we make a stripe customer.
      costsInputStripeCustomers <- resize o $ stripeCustomersWithAccount costsInputAccount
      -- If there is no stripe customer, then there cannot have been any payments
      -- If there is a stripe customer, then there can have been payments.
      costsInputStripePayments <-
        resize p $ stripePaymentsWithCustomers $ map entityKey costsInputStripeCustomers
      -- Each payment belongs to a checkout, but there can also checkouts without a payment
      costsInputCheckouts <- resize q $ checkoutsFor costsInputAccount costsInputStripePayments
      let cs = map (\(Entity cid c_) -> (cid, checkoutAmount c_)) costsInputCheckouts
      case NE.nonEmpty cs of
        Nothing -> do
          let costsInputTransportPayments = []
              costsInputOccasionalTransportPayments = []
              costsInputYearlyFeePayments = []
              costsInputDaycarePayments = []
              costsInputOccasionalDaycarePayments = []
              costsInputDaycareActivityPayments = []
              costsInputCustomActivityPayments = []
          (_, costsInputExtraCharges) <- resize r $ extraChargesWithAccount costsInputAccount []
          costsInputDiscounts <- resize r $ discountsWithAccount costsInputAccount []
          pure CostsInput {..}
        Just ne ->
          -- Each checkout can contain a number of transport payments
          do
            (cs', costsInputTransportPayments) <-
              resize s $ transportPaymentsFor ne (map entityKey costsInputTransportPaymentPlans)
            -- ... and occasional transport payments ...
            (cs'', costsInputOccasionalTransportPayments) <-
              resize t
                $ occasionalTransportPayments cs' (map entityKey costsInputOccasionalTransportSignups)
            -- ... and daycare payments
            (cs''', costsInputDaycarePayments) <-
              resize u $ daycarePayments cs'' (map entityKey costsInputDaycareSignups)
            -- ... and occasional daycare payments ...
            (cs'''', costsInputOccasionalDaycarePayments) <-
              resize v
                $ occasionalDaycarePayments cs''' (map entityKey costsInputOccasionalDaycareSignups)
            -- ... and daycare activity payments ...
            (cs''''', costsInputDaycareActivityPayments) <-
              resize w
                $ daycareActivityPayments cs'''' (map entityKey costsInputDaycareActivitySignups)
            -- ... and custom activity payments ...
            (cs'''''', costsInputCustomActivityPayments) <-
              resize x
                $ customActivityPayments cs''''' (map entityKey costsInputCustomActivitySignups)
            -- ... and yearly fee payments
            (cs''''''', costsInputYearlyFeePayments) <-
              resize y $ yearlyFeePayments costsInputAccount cs''''''
            -- ... and extra charges
            (cs'''''''', costsInputExtraCharges) <-
              resize y $ extraChargesWithAccount costsInputAccount $ NE.toList cs'''''''
            -- Whatever amount was left, must have been discounts.
            costsInputDiscounts <- resize z $ discountsWithAccount costsInputAccount cs''''''''
            pure CostsInput {..}
  shrinkValid ci =
    filter isValid $ do
      costsInputAccount <- shrinkValid $ costsInputAccount ci
      costsInputParents <- shrinkValidWithSeperateIds $ costsInputParents ci
      costsInputChildren <- shrinkValidWithSeperateIds $ costsInputChildren ci
      costsInputBusSchoolYears <- shrinkValidWithSeperateIds $ costsInputBusSchoolYears ci
      costsInputTransportSignups <- shrinkValidWithSeperateIds $ costsInputTransportSignups ci
      costsInputCheckouts <- shrinkValidWithSeperateIds $ costsInputCheckouts ci
      costsInputStripePayments <- shrinkValidWithSeperateIds $ costsInputStripePayments ci
      costsInputYearlyFeePayments <- shrinkValidWithSeperateIds $ costsInputYearlyFeePayments ci
      costsInputTransportPaymentPlans <-
        shrinkValidWithSeperateIds $ costsInputTransportPaymentPlans ci
      costsInputTransportPayments <- shrinkValidWithSeperateIds $ costsInputTransportPayments ci
      costsInputTransportEnrollments <-
        shrinkValidWithSeperateIds $ costsInputTransportEnrollments ci
      costsInputOccasionalTransportSignups <-
        shrinkValidWithSeperateIds $ costsInputOccasionalTransportSignups ci
      costsInputOccasionalTransportPayments <-
        shrinkValidWithSeperateIds $ costsInputOccasionalTransportPayments ci
      costsInputDaycareSemestres <- shrinkValidWithSeperateIds $ costsInputDaycareSemestres ci
      costsInputDaycareTimeslots <- shrinkValidWithSeperateIds $ costsInputDaycareTimeslots ci
      costsInputDaycareSignups <- shrinkValidWithSeperateIds $ costsInputDaycareSignups ci
      costsInputDaycarePayments <- shrinkValidWithSeperateIds $ costsInputDaycarePayments ci
      costsInputOccasionalDaycareSignups <-
        shrinkValidWithSeperateIds $ costsInputOccasionalDaycareSignups ci
      costsInputOccasionalDaycarePayments <-
        shrinkValidWithSeperateIds $ costsInputOccasionalDaycarePayments ci
      costsInputDaycareActivities <- shrinkValidWithSeperateIds $ costsInputDaycareActivities ci
      costsInputDaycareActivitySignups <-
        shrinkValidWithSeperateIds $ costsInputDaycareActivitySignups ci
      costsInputDaycareActivityPayments <-
        shrinkValidWithSeperateIds $ costsInputDaycareActivityPayments ci
      costsInputCustomActivities <- shrinkValidWithSeperateIds $ costsInputCustomActivities ci
      costsInputCustomActivitySignups <-
        shrinkValidWithSeperateIds $ costsInputCustomActivitySignups ci
      costsInputCustomActivityPayments <-
        shrinkValidWithSeperateIds $ costsInputCustomActivityPayments ci
      costsInputExtraCharges <- shrinkValidWithSeperateIds $ costsInputExtraCharges ci
      costsInputDiscounts <- shrinkValidWithSeperateIds $ costsInputDiscounts ci
      pure CostsInput {..}

parentsWithAccount :: AccountId -> Gen [Entity Parent]
parentsWithAccount aid = modEntL (\p -> p {parentAccount = aid}) <$> validsWithSeperateIDs

stripeCustomersWithAccount :: AccountId -> Gen [Entity StripeCustomer]
stripeCustomersWithAccount aid =
  fmap (modEnt (\sc -> sc {stripeCustomerAccount = aid})) <$> genValid

stripePaymentsWithCustomers :: [StripeCustomerId] -> Gen [Entity StripePayment]
stripePaymentsWithCustomers [] = pure []
stripePaymentsWithCustomers ids = do
  charges <- genValid
  if null charges
    then pure []
    else fmap makeStripePaymentsDistinct
      $ genValidsWithSeperateIDs
      $ do
        stripePaymentCustomer <- elements ids
        -- Must have a positive amount
        stripePaymentAmount <- scale (* 10) strictlyPositiveValidAmount
        stripePaymentCharge <- elements charges
        stripePaymentTimestamp <- genValid
        stripePaymentEvent <- genValid
        stripePaymentSession <- genValid
        stripePaymentPaymentIntent <- genValid
        pure StripePayment {..}
  where
    makeStripePaymentsDistinct =
      nubBy ((==) `on` (\(Entity _ StripePayment {..}) -> stripePaymentCharge))
        . nubBy
          ( (==)
              `on` (\(Entity _ StripePayment {..}) -> (stripePaymentCustomer, stripePaymentTimestamp))
          )

enrollmentsAndPlansFor ::
  [TransportSignupId] -> Gen ([Entity TransportPaymentPlan], [Entity TransportEnrollment])
enrollmentsAndPlansFor signupIds = do
  tppids <- genSeperateIds
  tpeids <- genSeperateIds
  fmap (unzip . catMaybes)
    $ forM (zip (zip tppids tpeids) signupIds)
    $ \((tppid, tpeid), sid) -> do
      b <- genValid
      if b
        then pure Nothing
        else do
          a <- absAmount <$> genValid `suchThat` (> zeroAmount)
          let tpp = TransportPaymentPlan {transportPaymentPlanTotalAmount = a}
          let tpe =
                TransportEnrollment
                  { transportEnrollmentSignup = sid,
                    transportEnrollmentPaymentPlan = tppid
                  }
          pure $ Just (Entity tppid tpp, Entity tpeid tpe)

checkoutsFor :: AccountId -> [Entity StripePayment] -> Gen [Entity Checkout]
checkoutsFor aid spes =
  -- A list of checkouts that have a stripe payment attached to it
  do
    cs1 <-
      forM spes $ \(Entity spid StripePayment {..}) -> do
        let checkoutAccount = aid
        let checkoutAmount = stripePaymentAmount
        let checkoutPayment = Just spid
        checkoutTimestamp <- genValid
        pure Checkout {..}
    -- A list of checkouts that doesn't have a stripe payment attached to it
    cs2 <-
      genListOf $ do
        let checkoutAccount = aid
        -- Must have a negative amount
        checkoutAmount <- negateAmount . absAmount <$> strictlyPositiveValidAmount
        let checkoutPayment = Nothing
        checkoutTimestamp <- genValid
        pure Checkout {..}
    genSeperateIdsFor $ cs1 ++ cs2

transportPaymentsFor ::
  NonEmpty (CheckoutId, Amount) -> -- Checkouts, and their total amount
  [TransportPaymentPlanId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity TransportPayment])
-- Checkouts and the amount left for them. This can be negative. In that case we will need discounts.
transportPaymentsFor checkouts [] = pure (checkouts, [])
transportPaymentsFor checkouts paymentPlanIds = do
  tups <-
    forM checkouts $ \(cid, totalAmount) -> do
      (transportPaymentsAmount, leftover) <- choosePartialAmountFromCheckout totalAmount
      as <- splitAmounts transportPaymentsAmount
      tpps <- go cid as
      pure ((cid, leftover), tpps)
  let (cids', tpps) = NE.unzip tups
  tppes <- genSeperateIdsFor $ mergePayments $ concat tpps
  pure (cids', tppes)
  where
    go cid as =
      forM as $ \a -> do
        ppid <- elements paymentPlanIds
        pure
          TransportPayment
            { transportPaymentPaymentPlan = ppid,
              transportPaymentCheckout = cid,
              transportPaymentAmount = a
            }
    mergePayments :: [TransportPayment] -> [TransportPayment]
    mergePayments = map squash . groupBy ((==) `on` func) . sortOn func
      where
        func TransportPayment {..} = (transportPaymentCheckout, transportPaymentPaymentPlan)
        squash :: [TransportPayment] -> TransportPayment
        squash tps =
          TransportPayment
            { transportPaymentPaymentPlan = transportPaymentPaymentPlan $ head tps,
              transportPaymentCheckout = transportPaymentCheckout $ head tps,
              transportPaymentAmount = sumAmount $ map transportPaymentAmount tps
            }

occasionalTransportPayments ::
  NonEmpty (CheckoutId, Amount) -> -- Checkouts, and their leftover amount
  [OccasionalTransportSignupId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity OccasionalTransportPayment])
occasionalTransportPayments checkouts signupIds = do
  let go (cs, otps) signupId = do
        b <- genValid
        if b
          then pure (cs, otps)
          else do
            (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
            pure
              ( cs',
                OccasionalTransportPayment
                  { occasionalTransportPaymentSignup = signupId,
                    occasionalTransportPaymentCheckout = cid,
                    occasionalTransportPaymentAmount = a
                  }
                  : otps
              )
  (checkouts', otps) <- foldM go (checkouts, []) signupIds
  otpes <- genSeperateIdsFor otps
  pure (checkouts', otpes)

chooseCheckoutAndPartialAmount ::
  NonEmpty (CheckoutId, Amount) -> Gen (NonEmpty (CheckoutId, Amount), (CheckoutId, Amount))
chooseCheckoutAndPartialAmount checkouts = do
  checkouts' <- shuffleNE checkouts
  case checkouts' of
    ((cid, a) :| rest) -> do
      let chooseAmount = do
            (chosenAmount, theRest) <- choosePartialAmountFromCheckout a
            pure ((cid, theRest) :| rest, (cid, chosenAmount))
      if a == zeroAmount -- If the amount is zero, maybe just leave it and try again
        then oneof [chooseCheckoutAndPartialAmount checkouts', chooseAmount]
        else chooseAmount

choosePartialAmountFromCheckout :: Amount -> Gen (Amount, Amount) -- First: chosen, second: leftover
choosePartialAmountFromCheckout a =
  if a > zeroAmount -- If the amount is positive, then we can use either a part, or the whole thing
    then
      oneof -- TODO turn into frequency
        [ pure (a, zeroAmount), -- Use the whole thing
          do
            (a1, a2) <- splitAmount a -- Use only a part
            pure (a2, a1)
        ]
    else -- If the amount is negative, we can use anything and compensate with discounts
    do
      a' <- strictlyPositiveValidAmount
      pure (a', a `subAmount` a')

chooseTotalAmountFromCheckout :: Amount -> Gen (Amount, Amount) -- First: chosen, second: leftover
chooseTotalAmountFromCheckout a =
  if a > zeroAmount -- If the amount is positive then we must use the whole thing
    then pure (a, zeroAmount) -- Use the whole thing
    -- If the amount is negative, we can use anything and compensate with discounts
    else do
      a' <- strictlyPositiveValidAmount
      pure (a', a `subAmount` a')

shuffleNE :: NonEmpty a -> Gen (NonEmpty a)
shuffleNE = fmap (fromJust . NE.nonEmpty) . shuffle . NE.toList

daycareSemestres :: Gen [Entity DaycareSemestre]
daycareSemestres = do
  tss <- genValid
  tups <- genValidSeperateFor tss
  o <- genValid
  genSeperateIdsFor
    $ flip map tups
    $ \((sy, sem), ts) ->
      DaycareSemestre
        { daycareSemestreYear = sy,
          daycareSemestreSemestre = sem,
          daycareSemestreCreationTime = ts,
          daycareSemestreOpen = o
        }

daycareTimeslotsFor :: [DaycareSemestreId] -> Gen [Entity DaycareTimeslot]
daycareTimeslotsFor ids =
  (>>= genSeperateIdsFor)
    $ fmap concat
    $ forM ids
    $ \i -> map (\dcts -> dcts {daycareTimeslotSemestre = i}) <$> genValid

daycareActivitiesFor :: [DaycareSemestreId] -> [DaycareTimeslotId] -> Gen [Entity DaycareActivity]
daycareActivitiesFor ids dtsids =
  (>>= genSeperateIdsFor)
    $ fmap (catMaybes . concat)
    $ forM ids
    $ \i -> do
      tries <- map (\dcts -> dcts {daycareActivitySemestre = i}) <$> genValid
      forM tries $ \dcts -> do
        case dtsids of
          [] -> pure Nothing
          _ -> do
            tsid <- elements dtsids
            pure $ Just $ dcts {daycareActivityTimeslot = tsid}

daycareSignupsFor :: [ChildId] -> [DaycareTimeslotId] -> Gen [Entity DaycareSignup]
daycareSignupsFor cids dctsids =
  (>>= genSeperateIdsFor)
    $ fmap catMaybes
    $ forM ((,) <$> cids <*> dctsids)
    $ \(cid, dctsid) ->
      frequency
        [ (3, pure Nothing),
          ( 1,
            do
              ts <- genValid
              pure
                $ Just
                  DaycareSignup
                    { daycareSignupChild = cid,
                      daycareSignupTimeslot = dctsid,
                      daycareSignupCreationTime = ts,
                      daycareSignupDeletedByAdmin = False
                    }
          )
        ]

daycarePayments ::
  NonEmpty (CheckoutId, Amount) ->
  [DaycareSignupId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity DaycarePayment])
daycarePayments checkouts signupIds = do
  let go (cs, dcps) signupId =
        frequency
          [ (3, pure (cs, dcps)),
            ( 1,
              do
                (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
                now <- genValid
                pure
                  ( cs',
                    DaycarePayment
                      { daycarePaymentSignup = signupId,
                        daycarePaymentCheckout = cid,
                        daycarePaymentAmount = a,
                        daycarePaymentTime = now
                      }
                      : dcps
                  )
            )
          ]
  (checkouts', dcps) <- foldM go (checkouts, []) signupIds
  dcpes <- genSeperateIdsFor dcps
  pure (checkouts', dcpes)

occasionalDaycarePayments ::
  NonEmpty (CheckoutId, Amount) -> -- Checkouts, and their leftover amount
  [OccasionalDaycareSignupId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity OccasionalDaycarePayment])
occasionalDaycarePayments checkouts signupIds = do
  let go (cs, otps) signupId = do
        b <- genValid
        if b
          then pure (cs, otps)
          else do
            (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
            now <- genValid
            pure
              ( cs',
                OccasionalDaycarePayment
                  { occasionalDaycarePaymentSignup = signupId,
                    occasionalDaycarePaymentCheckout = cid,
                    occasionalDaycarePaymentAmount = a,
                    occasionalDaycarePaymentTime = now
                  }
                  : otps
              )
  (checkouts', otps) <- foldM go (checkouts, []) signupIds
  otpes <- genSeperateIdsFor otps
  pure (checkouts', otpes)

daycareActivitySignupsFor :: [ChildId] -> [DaycareActivityId] -> Gen [Entity DaycareActivitySignup]
daycareActivitySignupsFor cids dctsids =
  (>>= genSeperateIdsFor)
    $ fmap catMaybes
    $ forM ((,) <$> cids <*> dctsids)
    $ \(cid, dcaid) ->
      frequency
        [ (3, pure Nothing),
          ( 1,
            do
              ts <- genValid
              pure
                $ Just
                  DaycareActivitySignup
                    { daycareActivitySignupChild = cid,
                      daycareActivitySignupActivity = dcaid,
                      daycareActivitySignupCreationTime = ts,
                      daycareActivitySignupDeletedByAdmin = False
                    }
          )
        ]

daycareActivityPayments ::
  NonEmpty (CheckoutId, Amount) ->
  [DaycareActivitySignupId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity DaycareActivityPayment])
daycareActivityPayments checkouts signupIds = do
  let go (cs, dcps) signupId =
        frequency
          [ (3, pure (cs, dcps)),
            ( 1,
              do
                (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
                now <- genValid
                pure
                  ( cs',
                    DaycareActivityPayment
                      { daycareActivityPaymentSignup = signupId,
                        daycareActivityPaymentCheckout = cid,
                        daycareActivityPaymentAmount = a,
                        daycareActivityPaymentTime = now
                      }
                      : dcps
                  )
            )
          ]
  (checkouts', dcps) <- foldM go (checkouts, []) signupIds
  dcpes <- genSeperateIdsFor dcps
  pure (checkouts', dcpes)

customActivitySignupsFor :: [ChildId] -> [CustomActivityId] -> Gen [Entity CustomActivitySignup]
customActivitySignupsFor cids casids =
  (>>= genSeperateIdsFor)
    $ fmap catMaybes
    $ forM ((,) <$> cids <*> casids)
    $ \(cid, casid) ->
      frequency
        [ (3, pure Nothing),
          ( 1,
            do
              ts <- genValid
              pure
                $ Just
                  CustomActivitySignup
                    { customActivitySignupChild = cid,
                      customActivitySignupActivity = casid,
                      customActivitySignupCreationTime = ts,
                      customActivitySignupDeletedByAdmin = False
                    }
          )
        ]

customActivityPayments ::
  NonEmpty (CheckoutId, Amount) ->
  [CustomActivitySignupId] ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity CustomActivityPayment])
customActivityPayments checkouts signupIds = do
  let go (cs, caps) signupId =
        frequency
          [ (3, pure (cs, caps)),
            ( 1,
              do
                (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
                now <- genValid
                pure
                  ( cs',
                    CustomActivityPayment
                      { customActivityPaymentSignup = signupId,
                        customActivityPaymentCheckout = cid,
                        customActivityPaymentAmount = a,
                        customActivityPaymentTime = now
                      }
                      : caps
                  )
            )
          ]
  (checkouts', caps) <- foldM go (checkouts, []) signupIds
  capes <- genSeperateIdsFor caps
  pure (checkouts', capes)

yearlyFeePayments ::
  AccountId ->
  NonEmpty (CheckoutId, Amount) ->
  Gen (NonEmpty (CheckoutId, Amount), [Entity YearlyFeePayment])
yearlyFeePayments aid checkouts = do
  sytups <- genValidSeperateForNE checkouts
  tups <-
    forM sytups $ \(sy, (cid, a)) -> do
      (yearlyFeePaymentAmount, leftover) <- chooseTotalAmountFromCheckout a
      pure
        ( (cid, leftover),
          YearlyFeePayment
            { yearlyFeePaymentSchoolyear = sy,
              yearlyFeePaymentAccount = aid,
              yearlyFeePaymentCheckout = cid,
              yearlyFeePaymentAmount = yearlyFeePaymentAmount
            }
        )
  let (cids', yfps) = NE.unzip tups
  yfpes <- genSeperateIdsForNE yfps
  pure (cids', NE.toList yfpes)

extraChargesWithAccount ::
  AccountId -> [(CheckoutId, Amount)] -> Gen ([(CheckoutId, Amount)], [Entity ExtraCharge])
extraChargesWithAccount aid checkouts = do
  let go :: ([a], [b]) -> (a, [b]) -> ([a], [b])
      go (l1, l2) (co, l3) = (co : l1, l2 ++ l3)
  -- Every Checkout could have 0-N extra charges
  let goCheckout :: (CheckoutId, Amount) -> Gen ((CheckoutId, Amount), [ExtraCharge])
      goCheckout (cid, a) =
        frequency
          [ (3, pure ((cid, a), [])),
            ( 1,
              do
                (extraChargeAmount, leftover) <- choosePartialAmountFromCheckout a
                let extraChargeAccount = aid
                extraChargeReason <- genValid
                let extraChargeCheckout = Just cid
                extraChargeTimestamp <- genValid
                let ec = ExtraCharge {..}
                ((cid', a'), restCharges) <- goCheckout (cid, leftover)
                pure ((cid', a'), ec : restCharges)
            )
          ]
  (checkouts', paidCharges) <- foldl' go ([], []) <$> mapM goCheckout checkouts
  -- And there can also be extra charges without a checkout.
  unpaidCharges <-
    fmap (\ec -> ec {extraChargeAccount = aid, extraChargeCheckout = Nothing}) <$> genValid
  echarges <- genSeperateIdsFor $ paidCharges ++ unpaidCharges
  pure (checkouts', echarges)

-- Note, the checkouts with their amounts must be negative
discountsWithAccount :: AccountId -> [(CheckoutId, Amount)] -> Gen [Entity Discount]
discountsWithAccount aid checkouts =
  -- Generate discounts for the checkouts that aren't zero now
  do
    ds1 <-
      fmap catMaybes
        $ forM checkouts
        $ \(cid, amount) ->
          if amount >= zeroAmount
            then pure Nothing
            else
              Just <$> do
                let discountAmount = negateAmount amount
                let discountAccount = aid
                discountReason <- genValid
                let discountCheckout = Just cid
                discountTimestamp <- genValid
                pure Discount {..}
    ds2 <- map (\d -> d {discountAccount = aid, discountCheckout = Nothing}) <$> genValid
    -- Generate discounts that haven't been used
    genSeperateIdsFor $ ds1 ++ ds2

busSchoolYearsFor ::
  [Entity TransportSignup] -> [Entity DaycareSemestre] -> Gen [Entity BusSchoolYear]
busSchoolYearsFor costsInputTransportSignups costsInputDaycareSemestres = do
  let rawSchoolYears :: [SchoolYear]
      rawSchoolYears =
        nub
          $ concat
            [ map (transportSignupSchoolYear . entityVal) costsInputTransportSignups,
              map (daycareSemestreYear . entityVal) costsInputDaycareSemestres
            ]
  bsys <-
    forM rawSchoolYears $ \busSchoolYearYear -> do
      busSchoolYearOpen <- frequency [(4, pure True), (1, pure False)]
      pure BusSchoolYear {..}
  genSeperateIdsFor bsys
