{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Calculate where

import GenImport
import Regtool.Calculation.Costs.Calculate
import Regtool.Data
import Regtool.Data.Gen

instance GenValid YearlyFeePaymentStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid TransportPaymentStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportPaymentStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
    oneof
      [ OccasionalTransportPaymentUnpaid ec eots <$> strictlyPositiveValidAmount,
        do
          eotp <-
            modEnt (\otp -> otp {occasionalTransportPaymentSignup = entityKey eots}) <$> genValid
          pure $ OccasionalTransportPaymentPaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycarePaymentStatus where
  genValid =
    sized $ \n -> do
      (a, b, c, d) <- genSplit4 n
      ec <- resize a genValid
      edcts <- resize b genValid
      eds <-
        resize c
          $ modEnt
            (\ds -> ds {daycareSignupChild = entityKey ec, daycareSignupTimeslot = entityKey edcts})
          <$> genValid
      oneof
        [ DaycarePaymentUnpaid ec edcts eds <$> resize d strictlyPositiveValidAmount,
          do
            edp <-
              resize d $ modEnt (\dp -> dp {daycarePaymentSignup = entityKey eds}) <$> genValid
            pure $ DaycarePaymentPaid ec edcts eds edp
        ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityPaymentStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        ( \ds ->
            ds
              { daycareActivitySignupChild = entityKey ec,
                daycareActivitySignupActivity = entityKey edcts
              }
        )
        <$> genValid
    oneof
      [ DaycareActivityPaymentUnpaid ec edcts eds <$> strictlyPositiveValidAmount,
        do
          edp <- modEnt (\dp -> dp {daycareActivityPaymentSignup = entityKey eds}) <$> genValid
          pure $ DaycareActivityPaymentPaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycarePaymentStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalDaycareSignupChild = entityKey ec}) <$> genValid
    oneof
      [ OccasionalDaycarePaymentUnpaid ec eots <$> strictlyPositiveValidAmount,
        do
          eotp <-
            modEnt (\otp -> otp {occasionalDaycarePaymentSignup = entityKey eots}) <$> genValid
          pure $ OccasionalDaycarePaymentPaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid CustomActivityPaymentStatus where
  genValid = do
    ec <- genValid
    eca <- genValid
    ecas <-
      modEnt
        ( \cas ->
            cas
              { customActivitySignupChild = entityKey ec,
                customActivitySignupActivity = entityKey eca
              }
        )
        <$> genValid
    oneof
      [ pure $ CustomActivityPaymentUnpaid ec ecas eca,
        do
          ecap <- modEnt (\cap -> cap {customActivityPaymentSignup = entityKey ecas}) <$> genValid
          pure $ CustomActivityPaymentPaid ec ecas ecap
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid CalculatedSemestrelyCosts where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      CalculatedSemestrelyCosts <$> resize a genValid <*> resize b genValid
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid CalculatedYearlyCosts where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      CalculatedYearlyCosts <$> resize a genValid <*> resize b genValid <*> resize c genValid
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid CalculatedCosts where
  genValid =
    sized $ \n -> do
      (a, b, c, d, e) <- genSplit5 n
      calculatedCostsMap <- resize a genValid
      calculatedCostsOccasionalTransport <-
        resize b $ genValid `suchThat` (distinct . map occasionalTransportPaymentStatusSignup)
      calculatedCostsOccasionalDaycare <-
        resize c $ genValid `suchThat` (distinct . map occasionalDaycarePaymentStatusSignup)
      calculatedCostsCustomActivity <-
        resize d $ genValid `suchThat` (distinct . map customActivityPaymentStatusSignup)
      calculatedCostsExtraCharges <-
        resize e $ modEntL (\d_ -> d_ {extraChargeCheckout = Nothing}) <$> genValid
      calculatedCostsDiscounts <-
        resize e $ modEntL (\d_ -> d_ {discountCheckout = Nothing}) <$> genValid
      pure CalculatedCosts {..}
  shrinkValid = shrinkValidStructurally
