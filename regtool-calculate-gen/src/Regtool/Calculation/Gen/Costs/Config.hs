{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Config where

import GenImport
import Regtool.Calculation.Costs.Config
import Regtool.Data
import Regtool.Data.Gen

instance GenValid CostsConfig where
  genValid =
    sized $ \n -> do
      let pa = positiveValidAmount
      (a, b, c, d) <- genSplit4 n
      yearly <- resize b $ genDecreasingListOf pa
      yearlyDefault <-
        case yearly of
          [] -> pure zeroAmount
          _ -> resize c $ pa `suchThat` (<= last yearly)
      CostsConfig <$> resize a pa <*> pure yearly <*> pure yearlyDefault <*> resize d pa
  shrinkValid = shrinkValidStructurally

genDecreasingListOf :: (Ord a) => Gen a -> Gen [a]
genDecreasingListOf g = genListOf g `suchThat` decreasing
