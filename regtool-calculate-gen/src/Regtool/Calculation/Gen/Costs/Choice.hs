{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Choice where

import qualified Data.List.NonEmpty as NE
import Data.Map (Map)
import qualified Data.Map as M
import GenImport
import Regtool.Calculation.Costs.Choice
import Regtool.Data
import Regtool.Data.Gen

instance GenValid Choices where
  genValid =
    sized $ \n -> do
      (a, b, c, d, e, f) <- genSplit6 n
      Choices
        <$> resize a genNonNullMap
        <*> resize b genValid
        <*> resize c genValid
        <*> resize d genValid
        <*> (modEntL (\ec_ -> ec_ {extraChargeCheckout = Nothing}) <$> resize f genValid)
        <*> (modEntL (\d_ -> d_ {discountCheckout = Nothing}) <$> resize e genValid)
  shrinkValid = shrinkValidStructurally

instance GenValid YearlyChoices where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      YearlyChoices
        <$> resize a (mgen strictlyPositiveValidAmount)
        <*> resize b genValid
        <*> resize c genNonNullMap
  shrinkValid = shrinkValidStructurally

genNonNullMap :: (GenValid a, Ord a, GenValid m, Eq m, Monoid m) => Gen (Map a m)
genNonNullMap =
  fmap M.fromList
    $ genListOf
    $ do
      key <- genValid
      value <-
        let go = do
              v <- genValid
              if v == mempty
                then scale (+ 1) go
                else pure v
         in go
      pure (key, value)

instance GenValid SemestrelyChoices where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      SemestrelyChoices <$> resize a genValid <*> resize b genValid
  shrinkValid = shrinkValidStructurally

instance GenValid TransportInstallmentChoices where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      TransportInstallmentChoices
        <$> resize a (suchThatMap (genListOf strictlyPositiveValidAmount) NE.nonEmpty)
        <*> resize b genValid
        <*> resize c genValid
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportChoice where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      ec <- resize a genValid
      eots <-
        resize b $ modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
      am <- resize c strictlyPositiveValidAmount
      pure $ OccasionalTransportChoice ec eots am
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareChoice where
  genValid =
    sized $ \n -> do
      (a, b, c, d) <- genSplit4 n
      DaycareChoice
        <$> resize a genValid
        <*> resize b genValid
        <*> resize c genValid
        <*> resize d positiveValidAmount
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityChoice where
  genValid =
    sized $ \n -> do
      (a, b, c, d) <- genSplit4 n
      DaycareActivityChoice
        <$> resize a genValid
        <*> resize b genValid
        <*> resize c genValid
        <*> resize d positiveValidAmount
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycareChoice where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      ec <- resize a genValid
      eots <-
        resize b $ modEnt (\ots -> ots {occasionalDaycareSignupChild = entityKey ec}) <$> genValid
      am <- resize c positiveValidAmount
      pure $ OccasionalDaycareChoice ec eots am
  shrinkValid = shrinkValidStructurally

instance GenValid CustomActivityChoice where
  genValid =
    sized $ \n -> do
      (a, b, c) <- genSplit3 n
      ec <- resize a genValid
      eca <- resize b genValid
      ecas <-
        resize c
          $ modEnt
            ( \cas ->
                cas
                  { customActivitySignupChild = entityKey ec,
                    customActivitySignupActivity = entityKey eca
                  }
            )
          <$> genValid
      pure $ CustomActivityChoice ec ecas eca
  shrinkValid = shrinkValidStructurally
