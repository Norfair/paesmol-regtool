{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Checkout where

import GenImport
import Regtool.Calculation.Costs.Checkout
import Regtool.Data.Gen ()

instance GenValid CheckoutsOverview where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid CheckoutOverview where
  genValid =
    sized $ \z -> do
      (x, y) <- genSplit z
      (a, b, c, d, e) <- genSplit5 x
      (f, g, h, i, j, k) <- genSplit6 y
      checkoutOverviewCheckout <- resize a genValid
      checkoutOverviewPayment <- resize b genValid
      checkoutOverviewYearlyFeePayments <- resize c genValid
      checkoutOverviewTransportPayments <- resize d genValid
      checkoutOverviewOccasionalTransportPayments <- resize e genValid
      checkoutOverviewDaycarePayments <- resize f genValid
      checkoutOverviewDaycareActivityPayments <- resize g genValid
      checkoutOverviewOccasionalDaycarePayments <- resize h genValid
      checkoutOverviewCustomActivityPayments <- resize i genValid
      checkoutOverviewExtraCharges <- resize j genValid
      checkoutOverviewDiscounts <- resize k genValid
      pure CheckoutOverview {..}
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
