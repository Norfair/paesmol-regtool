{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Calculation.Gen.Costs.Invoice where

import GenImport
import Regtool.Calculation.Costs.Invoice
import Regtool.Data.Gen

instance GenValid Invoice where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      Invoice <$> resize a genValid <*> resize b genValid
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid CostLine where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      CostLine <$> resize a genValid <*> resize b positiveValidAmount
  shrinkValid = shrinkValidStructurally

instance GenValid DiscountLine where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      DiscountLine <$> resize a genValid <*> resize b strictlyPositiveValidAmount
  shrinkValid = shrinkValidStructurally
