{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Calculation.Gen.Checkout where

import GenImport
import Regtool.Calculation.Consequences
import Regtool.Data.Gen

instance GenValid PaymentConsequences where
  genValid =
    sized $ \n -> do
      (x, y) <- genSplit n
      (a, b, c, d, e) <- genSplit5 x
      (f, g, h, i, j) <- genSplit5 y
      paymentConsequencesYearlyFeePayments <- resize a genValid
      paymentConsequencesTransportPaymentConsequences <- resize b genValid
      paymentConsequencesOccasionalTransportPaymentConsequences <- resize c genValid
      paymentConsequencesDaycarePaymentConsequences <- resize d genValid
      paymentConsequencesDaycareActivityPaymentConsequences <- resize e genValid
      paymentConsequencesOccasionalDaycarePaymentConsequences <- resize f genValid
      paymentConsequencesCustomActivityPaymentConsequences <- resize g genValid
      paymentConsequencesCompleteExtraCharges <- resize h genValid
      paymentConsequencesCompleteDiscounts <- resize i genValid
      paymentConsequencesNewDiscount <- resize j genValid
      pure PaymentConsequences {..}
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid TransportPaymentConsequences where
  genValid =
    oneof
      [ NewEnrollmentWithPlanAndPayments
          <$> genValid
          <*> genValid
          <*> genValid
          <*> genNonEmptyOf strictlyPositiveValidAmount,
        NewPaymentsWith <$> genValid
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycarePaymentConsequences where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid DaycareActivityPaymentConsequences where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
