{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Calculation.ConsequencesSpec
  ( spec,
  )
where

import qualified Data.List.NonEmpty as NE
import Data.Time
import Database.Persist
import Regtool.Calculation.Checkout.Utils
import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Config
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Calculation.Gen (genCostsInputWithOnlyUnpaidTransport)
import Regtool.Data
import Regtool.Data.Gen
import TestImport

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: Spec
spec = do
  genValidSpec @PaymentConsequences
  genValidSpec @TransportPaymentConsequences
  describe "yearlyFeePayment"
    $ it "produces valid yearly fee payments"
    $ forAllValid
    $ \(aid, sy, cid, yc) ->
      shouldBeValid $ yearlyFeePayments aid sy cid (yearlyChoicesYearlyFee yc)
  describe "transportPaymentConsequences"
    $ it "produces valid transport payment consequences"
    $ producesValid2 transportPaymentConsequences
  describe "occasionalTransportPaymentConsequences" $ do
    it "produces valid occasional transport payment consequences"
      $ producesValid2 occasionalTransportPaymentConsequences
    it "Has one payment per choice"
      $ forAllValid
      $ \(cid, otcs) ->
        length (occasionalTransportPaymentConsequences cid otcs) `shouldBe` length otcs
  describe "semestrelyPaymentConsequences"
    $ it "produces valid payment consequences"
    $ producesValid2 semestrelyPaymentConsequences
  describe "newDiscount" $ it "produces valid discounts" $ producesValid3 newDiscount
  describe "secondStageTransportPaymentConsequence"
    $ it "produces valid consequences"
    $ forAllValid
    $ \(tppid, spid, tsid) ->
      forAll (suchThatMap (genListOf strictlyPositiveValidAmount) NE.nonEmpty) $ \as ->
        shouldBeValid $ secondStageTransportPaymentConsequence tppid spid tsid as
  describe "paymentConsequences" $ do
    it "produces valid consequences" $ forAllValid $ producesValid3 . paymentConsequences
    monoidSpec @PaymentConsequences
    it "has one occasional transport payment per occasional transport choice"
      $ forAllValid
      $ \aid ->
        forAllValid $ \eid ->
          forAllValid $ \now ->
            forAllValid $ \cs ->
              length
                ( paymentConsequencesOccasionalTransportPaymentConsequences
                    $ paymentConsequences aid eid now cs
                )
                `shouldBe` length (choicesOccasionalTransport cs)
    it "produces consequences that leave the costs input valid with only transport signups"
      $ forAllShrink genCostsInputWithOnlyUnpaidTransport shrinkValid
      $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              ( genValid
                  `suchThat` ( (`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentCharge
                             )
                  `suchThat` ( (`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentTimestamp
                             )
              )
              $ \sp -> do
                let ac = makeAbstractCosts ci
                    cc = calculateCosts conf ac
                    cs = deriveChoices cc
                    spe = Entity spid sp
                ci' <- resolveConsequences spe ci cs
                shouldBeValid ci'
    it "produces consequences that leave the costs input valid"
      $ forAllValid
      $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              ( genValid
                  `suchThat` ( (`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentCharge
                             )
                  `suchThat` ( (`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentTimestamp
                             )
              )
              $ \sp -> do
                let ac = makeAbstractCosts ci
                    cc = calculateCosts conf ac
                    cs = deriveChoices cc
                    spe = Entity spid sp
                ci' <- resolveConsequences spe ci cs
                shouldBeValid ci'
    it
      "produces consequences that leave nothing unpaid if everything is to be paid at once for a positive amount with only transport signups"
      $ forAllShrink genCostsInputWithOnlyUnpaidTransport shrinkValid
      $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              ( genValid
                  `suchThat` ( (`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentCharge
                             )
                  `suchThat` ( (`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentTimestamp
                             )
              )
              $ \sp ->
                forAllValid $ \now ->
                  forAllValid $ \ce' -> do leavesNothingUnpaid ci conf spid sp now ce'
    it
      "produces consequences that leave nothing unpaid if everything is to be paid at once for a positive amount"
      $ forAllValid
      $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              ( genValid
                  `suchThat` ( (`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentCharge
                             )
                  `suchThat` ( (`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci))
                                 . stripePaymentTimestamp
                             )
              )
              $ \sp ->
                forAllValid $ \now ->
                  forAllValid $ \ce' -> leavesNothingUnpaid ci conf spid sp now ce'

leavesNothingUnpaid ::
  CostsInput ->
  CostsConfig ->
  Key StripePayment ->
  StripePayment ->
  UTCTime ->
  CheckoutId ->
  IO ()
leavesNothingUnpaid ci conf spid sp now ce' = do
  let ac = makeAbstractCosts ci
      cc = calculateCosts conf ac
      cs = deriveChoices cc
      i = deriveInvoice cs
      spe = Entity spid sp
  when (invoiceTotal i > zeroAmount)
    $ do
      -- TODO make the other test too
      let aid = costsInputAccount ci
      ci' <- resolveConsequences spe ci cs
      shouldBeValid ci'
      let ac' = makeAbstractCosts ci'
          cc' = calculateCosts conf ac'
          cs' = deriveChoices cc'
          i' = deriveInvoice cs'
          pc' = paymentConsequences aid ce' now cs'
      shouldBeValid ac'
      shouldBeValid cc'
      shouldBeValid cs'
      shouldBeValid i'
      shouldBeValid pc'
      when (invoiceTotal i' /= zeroAmount)
        $ expectationFailure
        $ unlines
          [ "Everything should have been paid",
            "This was the input:",
            ppShow ci,
            "These were the abstract costs:",
            ppShow ac,
            "These were the calculated costs:",
            ppShow cc,
            "These were the choices costs:",
            ppShow cs,
            "This was the invoice",
            ppShow i,
            "This was the second input:",
            ppShow ci',
            "This was the second choices:",
            ppShow cs',
            "but got this invoice instead:",
            ppShow i',
            "for this config",
            ppShow conf
          ]
      invoiceTotal i' `shouldBe` zeroAmount
      pc' `shouldBe` mempty
