{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Regtool.Calculation.TestUtils
  (
  )
where

-- , doesNotShrinkToItselfSpec
-- import TestImport
-- TODO consider putting this in validity
-- doesNotShrinkToItselfSpec ::
--      forall a. (Show a, Eq a, GenValid a)
--   => Spec
-- doesNotShrinkToItselfSpec =
--   describe "shrinkValid" $
--   it "does not shrink a value to itself for the first 100 elements" $
--   forAll genValid $ \v -> take 100 (shrinkValid (v :: a)) `shouldNotContain` [v]
