{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}

module Regtool.Calculation.Checkout.IntegrationSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Data.Time
import Database.Persist.Sqlite
import Regtool.Calculation.Checkout.Utils
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Config
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Calculation.Gen ()
import Regtool.Data
import Regtool.Data.Gen
import TestImport

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: Spec
spec =
  describe "checkout integration tests"
    $ it "works for this example in one school year"
    $ forAllValid
    $ \busSchoolYear ->
      forAll (genValid `suchThat` uncurry (/=)) $ \(chargeId, chargeId2) ->
        forAll strictlyPositiveValidAmount $ \yearlyFee ->
          forAllValid $ \(c1, c2, c3, c4, c5) ->
            forAllValid $ \(ts1, ts2, ts3, ts4, ts5) ->
              forAllValid $ \(ots1, ots2, ec, d) ->
                forAllValid $ \((dcs, dcts1, dcts2), (dcs1, dcs2, dcs3)) -> do
                  let schoolYear = busSchoolYearYear busSchoolYear
                      aid = toSqlKey 0
                      ec1 = Entity (toSqlKey 1) c1
                      ec2 = Entity (toSqlKey 2) c2
                      ec3 = Entity (toSqlKey 3) c3
                      ec4 = Entity (toSqlKey 4) c4
                      ec5 = Entity (toSqlKey 5) c5
                      ets1 =
                        Entity (toSqlKey 1)
                          $ ts1
                            { transportSignupChild = toSqlKey 1,
                              transportSignupInstalments = 1,
                              transportSignupSchoolYear = schoolYear
                            }
                      ets2 =
                        Entity (toSqlKey 2)
                          $ ts2
                            { transportSignupChild = toSqlKey 2,
                              transportSignupInstalments = 2,
                              transportSignupSchoolYear = schoolYear
                            }
                      ets3 =
                        Entity (toSqlKey 3)
                          $ ts3
                            { transportSignupChild = toSqlKey 3,
                              transportSignupInstalments = 3,
                              transportSignupSchoolYear = schoolYear
                            }
                      ets4 =
                        Entity (toSqlKey 4)
                          $ ts4
                            { transportSignupChild = toSqlKey 4,
                              transportSignupInstalments = 6,
                              transportSignupSchoolYear = schoolYear
                            }
                      ets5 =
                        Entity (toSqlKey 5)
                          $ ts5
                            { transportSignupChild = toSqlKey 5,
                              transportSignupInstalments = 12,
                              transportSignupSchoolYear = schoolYear
                            }
                      eots1 =
                        Entity (toSqlKey 0)
                          $ ots1
                            { occasionalTransportSignupChild = toSqlKey 2,
                              occasionalTransportSignupBusStop = toSqlKey 0
                            }
                      eots2 =
                        Entity (toSqlKey 1)
                          $ ots2
                            { occasionalTransportSignupChild = toSqlKey 4,
                              occasionalTransportSignupBusStop = toSqlKey 1
                            }
                      edcsem1 = Entity (toSqlKey 0) $ dcs {daycareSemestreYear = schoolYear}
                      edcts1 =
                        Entity (toSqlKey 0)
                          $ dcts1
                            { daycareTimeslotSemestre = toSqlKey 0,
                              daycareTimeslotFee = amountInCents 19
                            }
                      edcts2 =
                        Entity (toSqlKey 1)
                          $ dcts2
                            { daycareTimeslotSemestre = toSqlKey 0,
                              daycareTimeslotFee = amountInCents 17
                            }
                      edcs1 =
                        Entity (toSqlKey 0)
                          $ dcs1 {daycareSignupChild = toSqlKey 1, daycareSignupTimeslot = toSqlKey 0}
                      edcs2 =
                        Entity (toSqlKey 1)
                          $ dcs2 {daycareSignupChild = toSqlKey 1, daycareSignupTimeslot = toSqlKey 1}
                      edcs3 =
                        Entity (toSqlKey 2)
                          $ dcs3 {daycareSignupChild = toSqlKey 2, daycareSignupTimeslot = toSqlKey 1}
                      eec =
                        Entity (toSqlKey 0)
                          $ ec
                            { extraChargeAccount = aid,
                              extraChargeAmount = amountInCents 6,
                              extraChargeReason = Nothing,
                              extraChargeCheckout = Nothing
                            }
                      ed =
                        Entity (toSqlKey 0)
                          $ d
                            { discountAccount = aid,
                              discountAmount = amountInCents 2,
                              discountReason = Nothing,
                              discountCheckout = Nothing
                            }
                      ci =
                        CostsInput
                          { costsInputAccount = aid,
                            costsInputParents = [],
                            costsInputChildren = [ec1, ec2, ec3, ec4, ec5],
                            costsInputBusSchoolYears = [Entity (toSqlKey 0) busSchoolYear],
                            costsInputTransportSignups = [ets1, ets2, ets3, ets4, ets5],
                            costsInputStripePayments = [],
                            costsInputCheckouts = [],
                            costsInputYearlyFeePayments = [],
                            costsInputTransportPaymentPlans = [],
                            costsInputTransportPayments = [],
                            costsInputTransportEnrollments = [],
                            costsInputOccasionalTransportSignups = [eots1, eots2],
                            costsInputOccasionalTransportPayments = [],
                            costsInputDaycareSemestres = [edcsem1],
                            costsInputDaycareTimeslots = [edcts1, edcts2],
                            costsInputDaycareSignups = [edcs1, edcs2, edcs3],
                            costsInputDaycarePayments = [],
                            costsInputOccasionalDaycareSignups = [],
                            costsInputOccasionalDaycarePayments = [],
                            costsInputDaycareActivities = [],
                            costsInputDaycareActivitySignups = [],
                            costsInputDaycareActivityPayments = [],
                            costsInputCustomActivities = [],
                            costsInputCustomActivitySignups = [],
                            costsInputCustomActivityPayments = [],
                            costsInputExtraCharges = [eec],
                            costsInputDiscounts = [ed]
                          }
                  shouldBeValid ci -- Does not make sense otherwise
                  now <- getCurrentTime
                  let ac = makeAbstractCosts ci
                      conf =
                        CostsConfig
                          { costsConfigAnnualFee = yearlyFee,
                            costsConfigBusYearly =
                              [amountInCents 432, amountInCents 216, amountInCents 108],
                            costsConfigBusYearlyDefault = amountInCents 54,
                            costsConfigOccasionalBus = amountInCents 11
                          }
                      cc = calculateCosts conf ac
                      cs = trimChoices $ deriveChoices cc
                      totalAmount = invoiceTotal i
                      spe =
                        Entity
                          (toSqlKey 1)
                          StripePayment
                            { stripePaymentCustomer = toSqlKey 1,
                              stripePaymentAmount = totalAmount,
                              stripePaymentCharge = chargeId,
                              stripePaymentTimestamp = now,
                              stripePaymentEvent = Nothing,
                              stripePaymentSession = Nothing,
                              stripePaymentPaymentIntent = Nothing
                            }
                      i = deriveInvoice cs
                  -- Make sure the abstract costs are right.
                  do
                    abstractCostsOccasionalTransport ac
                      `shouldBe` [ OccasionalTransportFeeUnpaid ec2 eots1,
                                   OccasionalTransportFeeUnpaid ec4 eots2
                                 ]
                    (bsy, AbstractYearlyCosts {..}) <- expectSingleAbstractCost ac
                    bsy `shouldBe` busSchoolYear
                    aycYearlyFee `shouldBe` YearlyFeeStatusNotPaid
                    let expected =
                          M.fromList
                            [ (ec1, TransportFeePaymentNotStartedButNecessary ets1),
                              (ec2, TransportFeePaymentNotStartedButNecessary ets2),
                              (ec3, TransportFeePaymentNotStartedButNecessary ets3),
                              (ec4, TransportFeePaymentNotStartedButNecessary ets4),
                              (ec5, TransportFeePaymentNotStartedButNecessary ets5)
                            ]
                    yearlyAbstractBusFeesShouldBe aycYearlyBus expected
                  -- Make sure the calculated costs are right
                  do
                    calculatedCostsOccasionalTransport cc
                      `shouldBe` [ OccasionalTransportPaymentUnpaid ec2 eots1 $ amountInCents 11,
                                   OccasionalTransportPaymentUnpaid ec4 eots2 $ amountInCents 11
                                 ]
                    (bsy, CalculatedYearlyCosts {..}) <-
                      case M.toList (calculatedCostsMap cc) of
                        [] -> expectationFailure "Should not be an empty calculated cost map."
                        [cyc] -> pure cyc
                        cycs ->
                          expectationFailure
                            $ unlines
                              [ "Should have been a calculated cost map with one year, but got this:",
                                ppShow cycs
                              ]
                    bsy `shouldBe` busSchoolYear
                    cycYearly `shouldBe` YearlyFeeNotPaid yearlyFee
                    let pp1 = TransportPaymentPlan $ amountInCents 432
                        pp2 = TransportPaymentPlan $ amountInCents 216
                        pp3 = TransportPaymentPlan $ amountInCents 108
                        pp4 = TransportPaymentPlan $ amountInCents 54
                        pp5 = TransportPaymentPlan $ amountInCents 54
                        a1 = amountInCents 432
                        a2 = amountInCents 108
                        a3 = amountInCents 36
                        a4 = amountInCents 9
                        a5 = amountInCents 5
                        a5' = amountInCents 4
                        expected =
                          M.fromList
                            [ (ec1, TransportPaymentNotStartedButNecessary ets1 pp1 (a1 :| [])),
                              (ec2, TransportPaymentNotStartedButNecessary ets2 pp2 (a2 :| [a2])),
                              (ec3, TransportPaymentNotStartedButNecessary ets3 pp3 (a3 :| [a3, a3])),
                              ( ec4,
                                TransportPaymentNotStartedButNecessary
                                  ets4
                                  pp4
                                  (a4 :| [a4, a4, a4, a4, a4])
                              ),
                              ( ec5,
                                TransportPaymentNotStartedButNecessary
                                  ets5
                                  pp5
                                  (a5 :| [a5, a5, a5, a5, a5, a5', a5', a5', a5', a5', a5'])
                              )
                            ]
                    yearlyCalculatedBusFeesShouldBe cycBus expected
                  -- Make sure the cost lines are correct
                  -- The yearly fee
                  -- All of the first child (1 installment)
                  -- Half of the second child (2 instalments)
                  -- A third of the third child (3 instalments)
                  -- A sixth of the default value (6 instalments)
                  -- A twelfth of the default value (12 instalments)
                  -- The two occasional transport services
                  -- The three daycare signups
                  -- The discount
                  amountsShouldBe
                    ac
                    cc
                    i
                    [amountCents yearlyFee, 432, 108, 36, 9, 5, 11, 11, 19, 17, 17, 6]
                    [2]
                  ci2 <- resolveConsequences spe ci cs
                  shouldBeValid ci2 -- Does not make sense otherwise
                  now2 <- getCurrentTime
                  let ac2 = makeAbstractCosts ci2
                      cc2 = calculateCosts conf ac2
                      cs2 = trimChoices $ deriveChoices cc2
                      i2 = deriveInvoice cs2
                      totalAmount2 = invoiceTotal i2
                      spe2 =
                        Entity
                          (toSqlKey 2)
                          StripePayment
                            { stripePaymentCustomer = toSqlKey 1,
                              stripePaymentAmount = totalAmount2,
                              stripePaymentCharge = chargeId2,
                              stripePaymentTimestamp = now2,
                              stripePaymentEvent = Nothing,
                              stripePaymentSession = Nothing,
                              stripePaymentPaymentIntent = Nothing
                            }
                  yfp <-
                    case costsInputYearlyFeePayments ci2 of
                      [] ->
                        expectationFailure
                          "Should have gotten a yearly fee payment in the first round."
                      [yfp] -> pure yfp
                      yfps ->
                        expectationFailure
                          $ unwords
                            [ "Should have gotten a single yearly fee payment in the first round, got this instead:",
                              ppShow yfps
                            ]
                  let [ete1, ete2, ete3, ete4, ete5] = costsInputTransportEnrollments ci2
                  let [etpp1, etpp2, etpp3, etpp4, etpp5] = costsInputTransportPaymentPlans ci2
                  let [etp1, etp2, etp3, etp4, etp5] = costsInputTransportPayments ci2
                  let [eotp1, eotp2] = costsInputOccasionalTransportPayments ci2
                  let [eco] = costsInputCheckouts ci2
                  -- Make sure the abstract costs are right.
                  do
                    abstractCostsOccasionalTransport ac2
                      `shouldBe` [ OccasionalTransportFeePaid ec2 eots1 eotp1,
                                   OccasionalTransportFeePaid ec4 eots2 eotp2
                                 ]
                    (bsy, AbstractYearlyCosts {..}) <-
                      case M.toList (abstractCostsMap ac2) of
                        [] -> expectationFailure "Should not be an empty abstract cost map."
                        [ayc] -> pure ayc
                        aycs ->
                          expectationFailure
                            $ unlines
                              [ "Should have been an abstract cost map with one year, but got this:",
                                ppShow aycs
                              ]
                    bsy `shouldBe` busSchoolYear
                    aycYearlyFee `shouldBe` YearlyFeeStatusPaid yfp
                    let expected =
                          M.fromList
                            [ (ec1, TransportFeePaymentEntirelyDone ets1 ete1 etpp1 [etp1] [eco]),
                              ( ec2,
                                TransportFeePaymentStartedButNotDone ets2 ete2 etpp2 [etp2] [eco]
                              ),
                              ( ec3,
                                TransportFeePaymentStartedButNotDone ets3 ete3 etpp3 [etp3] [eco]
                              ),
                              ( ec4,
                                TransportFeePaymentStartedButNotDone ets4 ete4 etpp4 [etp4] [eco]
                              ),
                              ( ec5,
                                TransportFeePaymentStartedButNotDone ets5 ete5 etpp5 [etp5] [eco]
                              )
                            ]
                    yearlyAbstractBusFeesShouldBe aycYearlyBus expected
                  -- Make sure the calculated costs are right
                  do
                    calculatedCostsOccasionalTransport cc2
                      `shouldBe` [ OccasionalTransportPaymentPaid ec2 eots1 eotp1,
                                   OccasionalTransportPaymentPaid ec4 eots2 eotp2
                                 ]
                    (bsy, CalculatedYearlyCosts {..}) <-
                      case M.toList (calculatedCostsMap cc2) of
                        [] -> expectationFailure "Should not be an empty calculated cost map."
                        [cyc] -> pure cyc
                        cycs ->
                          expectationFailure
                            $ unlines
                              [ "Should have been a calculated cost map with one year, but got this:",
                                ppShow cycs
                              ]
                    bsy `shouldBe` busSchoolYear
                    cycYearly `shouldBe` YearlyFeePaid yfp
                    let a2 = amountInCents 108
                        a3 = amountInCents 36
                        a4 = amountInCents 9
                        a5 = amountInCents 5
                        a5' = amountInCents 4
                        expected =
                          M.fromList
                            [ (ec1, TransportPaymentEntirelyDone ets1 ete1 etpp1 [etp1] [eco]),
                              ( ec2,
                                TransportPaymentStartedButNotDone
                                  ets2
                                  ete2
                                  etpp2
                                  [etp2]
                                  [eco]
                                  (a2 :| [])
                              ),
                              ( ec3,
                                TransportPaymentStartedButNotDone
                                  ets3
                                  ete3
                                  etpp3
                                  [etp3]
                                  [eco]
                                  (a3 :| [a3])
                              ),
                              ( ec4,
                                TransportPaymentStartedButNotDone
                                  ets4
                                  ete4
                                  etpp4
                                  [etp4]
                                  [eco]
                                  (a4 :| [a4, a4, a4, a4])
                              ),
                              ( ec5,
                                TransportPaymentStartedButNotDone
                                  ets5
                                  ete5
                                  etpp5
                                  [etp5]
                                  [eco]
                                  (a5 :| [a5, a5, a5, a5, a5', a5', a5', a5', a5', a5'])
                              )
                            ]
                    yearlyCalculatedBusFeesShouldBe cycBus expected
                  -- Make sure the cost lines are correct
                  -- No yearly fee
                  -- Nothing for first child (1 installment)
                  -- The second half of the second child (2 instalments)
                  -- The second third of the third child (3 instalments)
                  -- The second sixth of the default value (6 instalments)
                  -- The second twelfth of the default value (12 instalments)
                  amountsShouldBe ac2 cc2 i2 [108, 36, 9, 5] []
                  ci3 <- resolveConsequences spe2 ci2 cs2
                  shouldBeValid ci3 -- Does not make sense otherwise
                  pure () :: IO ()
