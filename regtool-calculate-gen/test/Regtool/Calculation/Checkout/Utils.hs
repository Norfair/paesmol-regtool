{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Calculation.Checkout.Utils
  ( resolveConsequences,
    amountsShouldBe,
    expectSingleAbstractCost,
    yearlyAbstractBusFeesShouldBe,
    yearlyCalculatedBusFeesShouldBe,
  )
where

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Data.Time
import Database.Persist.Sqlite
import Regtool.Calculation.Consequences
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Costs.Input
import Regtool.Calculation.Costs.Invoice
import Regtool.Calculation.Gen ()
import Regtool.Data
import TestImport

{-# ANN module ("HLint: ignore Use lambda-case" :: String) #-}

genEnt ::
  (ToBackendKey SqlBackend a) =>
  [Entity a] ->
  a ->
  IO (Entity a)
genEnt es v = Entity <$> generate (genValid `suchThat` notDuplicateKey) <*> pure v
  where
    notDuplicateKey key = key `notElem` (entityKey <$> es)

genEnts ::
  ( ToBackendKey SqlBackend a,
    Traversable t
  ) =>
  [Entity a] ->
  t a ->
  IO [Entity a]
genEnts es vs = genEntsWith es vs id

genEntsWith ::
  ( ToBackendKey SqlBackend a,
    Traversable t
  ) =>
  [Entity a] ->
  t b ->
  (b -> a) ->
  IO [Entity a]
genEntsWith es values convert = newEntities <$> foldM makeEnt (es, []) values
  where
    newEntities (_, newEs) = reverse newEs
    makeEnt entsTup v = appendEnt entsTup <$> genEnt (fst entsTup ++ snd entsTup) (convert v)
    appendEnt (oldEnts, newEnts) e = (oldEnts, e : newEnts)

-- The stripe payment entity is just a prototype
resolveConsequences :: Entity StripePayment -> CostsInput -> Choices -> IO CostsInput
resolveConsequences (Entity spid sp) ci cs = do
  now <- getCurrentTime
  let totalAmount = invoiceTotal (deriveInvoice cs)
  let mspe =
        if totalAmount > zeroAmount
          then
            Just
              ( Entity spid
                  $ sp
                    { stripePaymentCustomer = stripePaymentCustomer sp,
                      stripePaymentAmount = totalAmount
                    }
              )
          else Nothing
  eco@(Entity cid _) <-
    genEnt (costsInputCheckouts ci)
      $ choicesCheckout (costsInputAccount ci) cs (entityKey <$> mspe) now
  let pc = paymentConsequences (costsInputAccount ci) cid now cs
  ps <- genEnts (costsInputYearlyFeePayments ci) (paymentConsequencesYearlyFeePayments pc)
  let go ::
        ([Entity TransportPaymentPlan], [Entity TransportPayment], [Entity TransportEnrollment]) ->
        TransportPaymentConsequences ->
        IO
          ( [Entity TransportPaymentPlan],
            [Entity TransportPayment],
            [Entity TransportEnrollment]
          )
      go (tppes_, tpes_, tees_) tpc =
        case tpc of
          NewPaymentsWith tps -> do
            tpes <- genEnts (costsInputTransportPayments ci ++ tpes_) (NE.toList tps)
            pure (tppes_, tpes_ ++ tpes, tees_)
          NewEnrollmentWithPlanAndPayments cid'' tsid tpp as -> do
            cid'' `shouldBe` cid
            tppe@(Entity tppid _) <- genEnt (costsInputTransportPaymentPlans ci ++ tppes_) tpp
            let SecondStageTransportPaymentConsequence tps te =
                  secondStageTransportPaymentConsequence tppid cid tsid as
            tpes <- genEnts (costsInputTransportPayments ci ++ tpes_) (NE.toList tps)
            tee <- genEnt (costsInputTransportEnrollments ci ++ tees_) te
            pure (tppes_ ++ [tppe], tpes_ ++ tpes, tees_ ++ [tee])
  (tppes, tpes, tees) <- foldM go ([], [], []) (paymentConsequencesTransportPaymentConsequences pc)
  otpes <-
    genEnts
      (costsInputOccasionalTransportPayments ci)
      (paymentConsequencesOccasionalTransportPaymentConsequences pc)
  dpes <-
    genEntsWith (costsInputDaycarePayments ci) (paymentConsequencesDaycarePaymentConsequences pc) $ \(DaycarePaymentConsequences dsid cid_ a) ->
      DaycarePayment
        { daycarePaymentSignup = dsid,
          daycarePaymentCheckout = cid_,
          daycarePaymentAmount = a,
          daycarePaymentTime = now
        }
  dapes <-
    genEntsWith
      (costsInputDaycareActivityPayments ci)
      (paymentConsequencesDaycareActivityPaymentConsequences pc)
      $ \(DaycareActivityPaymentConsequences dsid cid_ a) ->
        DaycareActivityPayment
          { daycareActivityPaymentSignup = dsid,
            daycareActivityPaymentCheckout = cid_,
            daycareActivityPaymentAmount = a,
            daycareActivityPaymentTime = now
          }
  odpes <-
    genEnts
      (costsInputOccasionalDaycarePayments ci)
      (paymentConsequencesOccasionalDaycarePaymentConsequences pc)
  capes <-
    genEnts
      (costsInputCustomActivityPayments ci)
      (paymentConsequencesCustomActivityPaymentConsequences pc)
  let eces =
        flip map (costsInputExtraCharges ci) $ \de@(Entity di d) ->
          case find ((== di) . snd) $ paymentConsequencesCompleteExtraCharges pc of
            Nothing -> de
            Just (checkoutId, _) -> Entity di d {extraChargeCheckout = Just checkoutId}
  let des =
        flip map (costsInputDiscounts ci) $ \de@(Entity di d) ->
          case find ((== di) . snd) $ paymentConsequencesCompleteDiscounts pc of
            Nothing -> de
            Just (checkoutId, _) -> Entity di d {discountCheckout = Just checkoutId}
  mde <- genEnts des (paymentConsequencesNewDiscount pc)
  pure
    ci
      { costsInputStripePayments = costsInputStripePayments ci ++ maybeToList mspe,
        costsInputCheckouts = costsInputCheckouts ci ++ [eco],
        costsInputYearlyFeePayments = costsInputYearlyFeePayments ci ++ ps,
        costsInputTransportPaymentPlans = costsInputTransportPaymentPlans ci ++ tppes,
        costsInputTransportPayments = costsInputTransportPayments ci ++ tpes,
        costsInputTransportEnrollments = costsInputTransportEnrollments ci ++ tees,
        costsInputOccasionalTransportPayments = costsInputOccasionalTransportPayments ci ++ otpes,
        costsInputDaycarePayments = costsInputDaycarePayments ci ++ dpes,
        costsInputDaycareActivityPayments = costsInputDaycareActivityPayments ci ++ dapes,
        costsInputOccasionalDaycarePayments = costsInputOccasionalDaycarePayments ci ++ odpes,
        costsInputCustomActivityPayments = costsInputCustomActivityPayments ci ++ capes,
        costsInputExtraCharges = eces,
        costsInputDiscounts = des ++ mde
      }

amountsShouldBe :: AbstractCosts -> CalculatedCosts -> Invoice -> [Int] -> [Int] -> Expectation
amountsShouldBe ac cc i cis dis = do
  let cls = invoiceCostLines i
      dls = invoiceDiscountLines i
      cas1 = sort $ map costLineAmount cls
      das1 = sort $ map discountLineAmount dls
      cas2 = sort $ map amountInCents cis
      das2 = sort $ map amountInCents dis
  when (cas1 /= cas2 || das1 /= das2)
    $ expectationFailure
    $ unlines
      [ "These two should have been equal: (cost lines)",
        unwords ["actual:  ", show $ map amountCents cas1],
        unwords ["expected:", show $ map amountCents cas2],
        "These two should have been equal: (discount lines)",
        unwords ["actual:  ", show $ map amountCents das1],
        unwords ["expected:", show $ map amountCents das2],
        "These were the abstract costs:",
        ppShow ac,
        "These were the calculated costs:",
        ppShow cc,
        "This was the full invoice:",
        ppShow i
      ]

expectSingleAbstractCost :: AbstractCosts -> IO (BusSchoolYear, AbstractYearlyCosts)
expectSingleAbstractCost ac =
  case M.toList (abstractCostsMap ac) of
    [] -> expectationFailure "Should not be an empty abstract cost map."
    [ayc] -> pure ayc
    aycs ->
      expectationFailure
        $ unlines ["Should have been an abstract cost map with one year, but got this:", ppShow aycs]

yearlyAbstractBusFeesShouldBe ::
  M.Map (Entity Child) TransportFeeStatus -> M.Map (Entity Child) TransportFeeStatus -> IO ()
yearlyAbstractBusFeesShouldBe actualMap expectedMap =
  forM_ (zip (sort $ M.toList actualMap) (sort $ M.toList expectedMap)) $ \((ae, afs), (ee, efs)) -> do
    ae `shouldBe` ee
    when (afs /= efs)
      $ expectationFailure
      $ unlines
        [ "Abstract Transport Fee Status differs from expected value",
          "For this entity keys:",
          show (entityKey ae),
          "actual:",
          ppShow afs,
          "expected:",
          ppShow efs
        ]

yearlyCalculatedBusFeesShouldBe ::
  M.Map (Entity Child) TransportPaymentStatus ->
  M.Map (Entity Child) TransportPaymentStatus ->
  IO ()
yearlyCalculatedBusFeesShouldBe actualMap expectedMap =
  forM_ (zip (sort $ M.toList actualMap) (sort $ M.toList expectedMap)) $ \((ae, afs), (ee, efs)) -> do
    ae `shouldBe` ee
    when (afs /= efs)
      $ expectationFailure
      $ unlines
        [ "Calculated Transport Fee Status differs from expected value",
          "For this entity key:",
          show (entityKey ae),
          "actual:",
          ppShow afs,
          "expected:",
          ppShow efs
        ]
