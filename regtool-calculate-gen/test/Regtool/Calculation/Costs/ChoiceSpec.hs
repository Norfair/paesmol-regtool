{-# LANGUAGE TypeApplications #-}

module Regtool.Calculation.Costs.ChoiceSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Regtool.Calculation.Costs.Choice
import Regtool.Calculation.Gen
import TestImport

spec :: Spec
spec = do
  genValidSpec @DaycareChoice
  -- shrinkValidSpecWithLimit @DaycareChoice 100
  -- doesNotShrinkToItselfSpec @DaycareChoice
  jsonSpec @DaycareChoice
  genValidSpec @OccasionalTransportChoice
  -- shrinkValidSpecWithLimit @OccasionalTransportChoice 100
  -- doesNotShrinkToItselfSpec @OccasionalTransportChoice
  jsonSpec @OccasionalTransportChoice
  genValidSpec @OccasionalDaycareChoice
  -- shrinkValidSpecWithLimit @OccasionalDaycareChoice 100
  -- doesNotShrinkToItselfSpec @OccasionalDaycareChoice
  jsonSpec @OccasionalDaycareChoice
  genValidSpec @TransportInstallmentChoices
  -- shrinkValidSpecWithLimit @TransportInstallmentChoices 100
  -- doesNotShrinkToItselfSpec @TransportInstallmentChoices
  jsonSpec @TransportInstallmentChoices
  genValidSpec @SemestrelyChoices
  -- shrinkValidSpecWithLimit @SemestrelyChoices 100
  -- doesNotShrinkToItselfSpec @SemestrelyChoices
  jsonSpec @SemestrelyChoices
  genValidSpec @YearlyChoices
  -- shrinkValidSpecWithLimit @YearlyChoices 100
  -- doesNotShrinkToItselfSpec @YearlyChoices
  describe "genNonNullMap"
    $ it "generates maps where the values arae not empty"
    $ forAll genNonNullMap
    $ \m ->
      forM_ (M.toList m) $ \(k, l) -> do
        shouldBeValid (k :: Int)
        (l :: [Int]) `shouldNotSatisfy` (== mempty)
  jsonSpec @YearlyChoices
  genValidSpec @Choices
  -- shrinkValidSpecWithLimit @Choices 100
  -- doesNotShrinkToItselfSpec @Choices
  jsonSpec @Choices
  describe "deriveChoices" $ it "produces valid cost choices" $ producesValid deriveChoices
  describe "deriveYearlyChoices"
    $ it "produces valid cost choices"
    $ producesValid deriveYearlyChoices
  describe "deriveSemestrelyChoices"
    $ it "produces valid cost choices"
    $ producesValid deriveSemestrelyChoices
  describe "deriveOccasionalTransportChoices"
    $ it "produces valid occasional transport choices"
    $ producesValid deriveOccasionalTransportChoices
  describe "deriveOccasionalDaycareChoices"
    $ it "produces valid occasional transport choices"
    $ producesValid deriveOccasionalDaycareChoices
  describe "deriveDaycareChoices"
    $ it "produces valid daycare choices"
    $ producesValid deriveDaycareChoice
