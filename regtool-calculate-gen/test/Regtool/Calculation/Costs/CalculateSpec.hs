{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}

module Regtool.Calculation.Costs.CalculateSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Regtool.Calculation.Costs.Abstract
import Regtool.Calculation.Costs.Calculate
import Regtool.Calculation.Costs.Config
import Regtool.Calculation.Gen ()
import Regtool.Data
import Regtool.Data.Gen
import TestImport

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

{-# ANN module ("HLint: ignore Use lambda-case" :: String) #-}

spec :: Spec
spec = do
  genValidSpec @YearlyFeePaymentStatus
  genValidSpec @OccasionalTransportPaymentStatus
  genValidSpec @TransportPaymentStatus
  genValidSpec @DaycarePaymentStatus
  genValidSpec @DaycareActivityPaymentStatus
  genValidSpec @OccasionalDaycarePaymentStatus
  genValidSpec @CalculatedSemestrelyCosts
  genValidSpec @CalculatedYearlyCosts
  genValidSpec @CalculatedCosts
  describe "calculateTransportPaymentStatusForOneChild" $ do
    it "produces valid transport payment statusses if the amount is zero"
      $ producesValid (`calculateTransportPaymentStatusForOneChild` zeroAmount)
    it "produces valid transport payment statusses"
      $ producesValid2 calculateTransportPaymentStatusForOneChild
    it "uses the amount for a payment that is done already"
      $ forAllValid
      $ \a ->
        forAllValid $ \(ts, te, tpp, tps, sps) ->
          fst
            ( calculateTransportPaymentStatusForOneChild
                (TransportFeePaymentEntirelyDone ts te tpp tps sps)
                a
            )
            `shouldBe` True
    it "does not use the amount if the child is not signed up"
      $ forAllValid
      $ \a ->
        fst (calculateTransportPaymentStatusForOneChild TransportFeeNotSignedUp a) `shouldBe` False
    describe "Not started but necessarry" $ do
      it "uses the amount for a payment that is not not started but necessary"
        $ forAllValid
        $ \a ->
          forAllValid $ \ts ->
            fst
              ( calculateTransportPaymentStatusForOneChild
                  (TransportFeePaymentNotStartedButNecessary ts)
                  a
              )
              `shouldBe` True
      it "uses the amount in the new transport payment plan"
        $ forAll strictlyPositiveValidAmount
        $ \a ->
          forAllValid $ \ts -> do
            let (_, r) =
                  calculateTransportPaymentStatusForOneChild
                    (TransportFeePaymentNotStartedButNecessary ts)
                    a
            case r of
              TransportPaymentNotStartedButNecessary _ TransportPaymentPlan {..} _ ->
                transportPaymentPlanTotalAmount `shouldBe` a
              _ -> expectationFailure "should be necessary"
      it "uses the amount as-is if everything is to be paid at once"
        $ forAll strictlyPositiveValidAmount
        $ \a ->
          forAll (modEnt (\ts -> ts {transportSignupInstalments = 1}) <$> genValid) $ \ts ->
            calculateTransportPaymentStatusForOneChild
              (TransportFeePaymentNotStartedButNecessary ts)
              a
              `shouldBe` (True, TransportPaymentNotStartedButNecessary ts (TransportPaymentPlan a) (a :| []))
      it "uses half of the amount as-is if two payments are to be made"
        $ forAll strictlyPositiveValidAmount
        $ \a ->
          forAll (modEnt (\ts -> ts {transportSignupInstalments = 2}) <$> genValid) $ \ts -> do
            let r =
                  calculateTransportPaymentStatusForOneChild
                    (TransportFeePaymentNotStartedButNecessary ts)
                    a
            as_ <-
              case divAmount a 2 of
                DivSuccess as -> pure as
                _ -> expectationFailure "Should be able to divide"
            r
              `shouldBe` (True, TransportPaymentNotStartedButNecessary ts (TransportPaymentPlan a) as_)
    describe "started but not done" $ do
      it "uses the amount for a payment that is not entirely done"
        $ forAll strictlyPositiveValidAmount
        $ \a ->
          forAllValid $ \(ts, te, tpp, tps, sps) ->
            fst
              ( calculateTransportPaymentStatusForOneChild
                  (TransportFeePaymentStartedButNotDone ts te tpp tps sps)
                  a
              )
              `shouldBe` True
      it "ignores the input amount"
        $ forAllValid
        $ \(a1, a2) ->
          forAllValid $ \(ts, te, tpp, tps, sps) ->
            let c =
                  calculateTransportPaymentStatusForOneChild
                    $ TransportFeePaymentStartedButNotDone ts te tpp tps sps
             in c a1 `shouldBe` c a2
  describe "calculateTransportPayments" $ do
    it "produces valid transport payment statusses for singleton maps"
      $ forAllValid
      $ \cc ->
        forAllValid $ \(ec, tfs) ->
          shouldBeValid $ calculateTransportPayments cc $ M.singleton ec tfs
    it "produces valid transport payment statusses for maps with two elements"
      $ forAllValid
      $ \cc ->
        forAllValid $ \(ec1, tfs1) ->
          forAllValid $ \(ec2, tfs2) ->
            shouldBeValid $ calculateTransportPayments cc $ M.fromList [(ec1, tfs1), (ec2, tfs2)]
    it "produces valid yearly fee payment statusses if any of the bus fees are zero"
      $ forAll (genListOf positiveValidAmount)
      $ \as1 ->
        forAll (genListOf positiveValidAmount) $ \as2 ->
          forAll ((\cc -> cc {costsConfigBusYearly = as1 ++ [zeroAmount] ++ as2}) <$> genValid) $ \cc ->
            forAllValid $ \afsm -> shouldBeValid $ calculateTransportPayments cc afsm
    it "produces valid yearly fee payment statusses if the default bus fee is zero"
      $ forAll ((\cc -> cc {costsConfigBusYearlyDefault = zeroAmount}) <$> genValid)
      $ \cc ->
        forAllValid $ \afsm -> shouldBeValid $ calculateTransportPayments cc afsm
    it "produces valid transport payment statusses"
      $ producesValid2 calculateTransportPayments
    it "ignores the yearly fee"
      $ forAllValid
      $ \(a1, a2) ->
        forAllValid $ \cc ->
          forAllValid $ \afsm ->
            calculateTransportPayments (cc {costsConfigAnnualFee = a1}) afsm
              `shouldBe` calculateTransportPayments (cc {costsConfigAnnualFee = a2}) afsm
    it "retains all the children"
      $ forAllValid
      $ \conf ->
        forAllValid $ \m -> M.keysSet m == M.keysSet (calculateTransportPayments conf m)
    it
      "correctly calculates the second child as having the second price, if the first's payments haven't started yet and everything is paid in single instalments"
      $ forAllValid
      $ \conf ->
        forAll genValid $ \(c1, ts1) ->
          forAll genValid $ \(c2, ts2) -> do
            let m =
                  M.fromList
                    [ ( c1,
                        TransportFeePaymentNotStartedButNecessary
                          $ modEnt (\ts -> ts {transportSignupInstalments = 1}) ts1
                      ),
                      ( c2,
                        TransportFeePaymentNotStartedButNecessary
                          $ modEnt (\ts -> ts {transportSignupInstalments = 1}) ts2
                      )
                    ]
                m' = calculateTransportPayments conf m
            M.keysSet m `shouldBe` M.keysSet m'
            sort
              ( M.elems
                  ( M.map
                      ( \tps ->
                          case tps of
                            (TransportPaymentNotRequiredBecauseOfAmount _) -> zeroAmount
                            (TransportPaymentNotStartedButNecessary _ _ (a :| [])) -> a
                            _ -> zeroAmount -- Should not happen.
                      )
                      m'
                  )
              )
              `shouldBe` sort
                ( take (length [c1, c2])
                    $ costsConfigBusYearly conf
                    ++ repeat (costsConfigBusYearlyDefault conf)
                )
  describe "calculateYearlyPayment" $ do
    it "produces valid yearly fee payment statusses if the yearly fee is zero"
      $ forAll ((\cc -> cc {costsConfigAnnualFee = zeroAmount}) <$> genValid)
      $ \cc ->
        forAllValid $ \yfs -> shouldBeValid $ calculateYearlyPayment cc yfs
    it "produces valid yearly fee payment statusses"
      $ producesValid2 calculateYearlyPayment
  describe "calculateCosts" $ do
    it "produces valid costs if the yearly fee is zero"
      $ forAll ((\cc -> cc {costsConfigAnnualFee = zeroAmount}) <$> genValid)
      $ \cc ->
        forAllValid $ \ac -> shouldBeValid $ calculateCosts cc ac
    it "produces valid costs if any of the bus fees are zero"
      $ forAll (genListOf positiveValidAmount)
      $ \as1 ->
        forAll (genListOf positiveValidAmount) $ \as2 ->
          forAll ((\cc -> cc {costsConfigBusYearly = as1 ++ [zeroAmount] ++ as2}) <$> genValid) $ \cc ->
            forAllValid $ \ac -> shouldBeValid $ calculateCosts cc ac
    it "produces valid costs if the default bus fee is zero"
      $ forAll ((\cc -> cc {costsConfigBusYearlyDefault = zeroAmount}) <$> genValid)
      $ \cc ->
        forAllValid $ \ac -> shouldBeValid $ calculateCosts cc ac
    it "produces valid calculated costs" $ producesValid2 calculateCosts
    it "produces costs with as many amounts as there were children in the abstract costs"
      $ forAllValid
      $ \conf ->
        forAllValid $ \ac ->
          let cc = calculateYearlyCosts conf ac
           in length (cycBus cc) `shouldBe` length (aycYearlyBus ac)
