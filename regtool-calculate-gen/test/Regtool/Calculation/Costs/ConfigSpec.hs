{-# LANGUAGE TypeApplications #-}

module Regtool.Calculation.Costs.ConfigSpec
  ( spec,
  )
where

import Regtool.Calculation.Costs.Config
import Regtool.Calculation.Gen ()
import TestImport

spec :: Spec
spec = do
  genValidSpec @CostsConfig

-- shrinkValidSpec @CostsConfig
-- doesNotShrinkToItselfSpec @CostsConfig
