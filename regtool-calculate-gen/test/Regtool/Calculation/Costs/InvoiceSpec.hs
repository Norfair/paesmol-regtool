{-# LANGUAGE TypeApplications #-}

module Regtool.Calculation.Costs.InvoiceSpec
  ( spec,
  )
where

import Regtool.Calculation.Costs.Invoice
import Regtool.Calculation.Gen ()
import TestImport

spec :: Spec
spec = do
  genValidSpec @Invoice
  -- shrinkValidSpec @Invoice
  -- doesNotShrinkToItselfSpec @Invoice
  genValidSpec @CostLine
  -- shrinkValidSpec @CostLine
  -- doesNotShrinkToItselfSpec @CostLine
  genValidSpec @DiscountLine
  describe "deriveInvoice" $ it "produces valid invoices" $ producesValid deriveInvoice
