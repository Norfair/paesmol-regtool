{-# LANGUAGE TypeApplications #-}

module Regtool.Calculation.Costs.CheckoutSpec
  ( spec,
  )
where

import Regtool.Calculation.Costs.Checkout
import Regtool.Calculation.Gen ()
import TestImport

spec :: Spec
spec = do
  genValidSpec @CheckoutsOverview
  -- shrinkValidSpec @CheckoutsOverview
  -- doesNotShrinkToItselfSpec @CheckoutsOverview
  genValidSpec @CheckoutOverview
  -- shrinkValidSpec @CheckoutOverview
  -- doesNotShrinkToItselfSpec @CheckoutOverview
  describe "deriveCheckoutsOverview"
    $ it "produces valid cost checkouts overviews"
    $ producesValid deriveCheckoutsOverview
